function retval = g1(w, z, Ts)
  retval = 1 - exp(-2 * z * w * Ts);
endfunction

