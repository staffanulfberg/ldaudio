library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

library STD;
use STD.textio.all;

library UNIMACRO;
use unimacro.Vcomponents.all;

use work.reedSolomon_types.all;

entity efmtest is
end efmtest;

architecture a1 of efmtest is
  constant Tclk: time := 10 ns;
  signal clk: std_logic;
  
  signal efmBitClk: std_logic;
  signal efmBitInEn: std_logic;
  
  signal efmBit: std_logic;  
  
  signal efmToCircBufferStart: std_logic;
  signal efmToCircBufferData: std_logic_vector(7 downto 0);
  signal efmToCircBufferDataEn: std_logic;
  
  signal circBufferOutEn: std_logic;
  signal leftOut: signed(15 downto 0);
  signal leftErased: std_logic;
  signal rightOut: signed(15 downto 0);
  signal rightErased: std_logic;
    
  type InputFile is file of boolean;
  type SampleFile is file of integer;
  
  signal frameCount: integer := 0;
  
begin
  process
    variable i: unsigned(3 downto 0) := "0000";
  begin
    efmBitClk <= i(3);
    i := i + 1;
    clk <= '0';
    wait for Tclk / 2;
    clk <= '1';
    wait for Tclk / 2;
  end process;
    
  efm: entity work.efm port map (
    clk => clk,
    inEn => efmBitInEn,
    dataIn => efmBit,
    enableErasures => '0',
    frameStart => efmToCircBufferStart,
    dataOutEn => efmToCircBufferDataEn,
    dataOut => efmToCircBufferData,
    erasedOut => open,
    locked => open
  );
  
  circBuffer: entity work.circBuffer port map (
    clock => clk,
    frameStart => efmToCircBufferStart,
    inEn => efmToCircBufferDataEn,
    dataIn => efmToCircBufferData,
    erasedIn => '0',
    outEn => circBufferOutEn,
    leftOut => leftOut,
    leftErased => leftErased,
    rightOut => rightOut,
    rightErased => rightErased,
    c1Enable => '1',
    c2Enable => '1',
    c1status => open,
    c2status => open
  );
    
  process(clk)
  begin
    if rising_edge(clk) then
      if efmToCircBufferDataEn = '1' then
        frameCount <= frameCount + 1;
      end if;
    end if;
  end process;
  
  process
    file f: InputFile open read_mode is "jp-62.5MHz.efmbits";
    variable c: boolean;
  begin
    if not endfile(f) then
      wait until efmBitClk ='1' and efmBitClk'event;
      read(f, c);
      if c then
        efmBit <= '1';
      else 
        efmBit <= '0';
      end if;
      efmBitInEn <= '1';
      wait until clk = '1' and clk'event; -- wait to consume
      efmBitInEn <= '0';
    else
      wait for 5000ms;  
    end if;
  end process;
  
  process(clk)
    file f: SampleFile open write_mode is "jp-62.5MHz.pcm";
--    file t: text open write_mode is "ve-snw-cut.txt";
--    variable row: line;
    variable x: signed(31 downto 0);
    variable counter: integer := 0;
    
    variable lastLeft: signed(15 downto 0) := to_signed(0, 16);
    variable lastRight: signed(15 downto 0) := to_signed(0, 16);
    variable left: signed(15 downto 0);
    variable right: signed(15 downto 0);
  begin
    if clk = '1' and clk'event then
      if circBufferOutEn = '1' then
        if leftErased = '0' then
          left := leftOut;
          lastLeft := left;
        else
          left := lastLeft;
        end if;
        if rightErased = '0' then
          right := rightOut;
          lastRight := right;
        else
          right := lastRight;
        end if;
    
        x := left & right;
        write(f, to_integer(signed(std_logic_vector(x))));

--          write(row, to_hstring(to_bitvector(std_logic_vector(to_unsigned(counter + i * 4, 32)))), right, 9); 
--          write(row, to_hstring(to_bitvector(std_logic_vector(left))), right, 5); 
--          write(row, to_hstring(to_bitvector(std_logic_vector(righ))), right, 5); 
--          writeLine(t, row);
      counter := counter + 4;
      end if;
    end if;
  end process;
end a1;

