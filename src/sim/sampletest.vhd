library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use ieee.std_logic_textio.all;

library STD;
use STD.textio.all;

use work.efmTypes.all;
use work.circBufferTypes.all;

entity sampletest is
end sampletest;

architecture a1 of sampletest is
  constant Tclk: time := 10 ns;
  signal clk: std_logic;
  
  signal efmSample: std_logic;
  signal efmBitEn: std_logic;
  signal efmBit: Sample;  
  signal efmLocked: std_logic;
  signal efmToCircBufferDataEn: std_logic;
  signal efmToCircBufferData: ByteArray(0 to 31);
  signal circBufferOutEn: std_logic;
  signal leftOut: SampleArray;
  signal rightOut: SampleArray;
  
  type SampleFile is file of integer;
  
begin  
  efmIn: entity work.efmIn generic map (
    clkFrequency => 100000000
  )
  port map (
    clk => clk,
    dataIn => efmSample,
    outEn => efmBitEn,
    dataOut => efmBit,
    locked => efmLocked
  );

  efm: entity work.efm port map (
    clk => clk,
    inEn => efmBitEn,
    dataIn => efmBit,
    outEn => efmToCircBufferDataEn,
    dataOut => efmToCircBufferData,
    locked => efmLocked
  );
    
  circ: entity work.circBuffer port map (
    enable => '0',
    reset => '0',
    clock => clk,
    inEn => efmToCircBufferDataEn,
    dataIn => efmToCircBufferData,
    outEn => circBufferOutEn,
    leftOut => leftOut,
    rightOut => rightOut
  );
  
  process
  begin
    clk <= '0';
    wait for Tclk / 2;
    clk <= '1';
    wait for Tclk / 2;
  end process;
  
  process
    file f: text open read_mode is "terminator2.samples"; -- "ve-snw-cut.samples"; -- 
    variable l: LINE;
    variable b: bit;
  begin
    while not endfile(f) loop
      wait until clk ='1' and clk'event;
      readline(f, l);
      read(l, b);
      if b = '0' then
        efmSample <= '0';
      else 
        efmSample <= '1';
      end if;
    end loop;
    wait for 100 ns;
  end process;
  
--  process(efmSampleClk)
--    file f: SampleFile open write_mode is "terminator2.pcm";
--    file t: text open write_mode is "terminator2.txt";
--    variable row: line;
--    variable x: signed(31 downto 0);
--    variable counter: integer := 0;
--  begin
--    if efmSampleClk = '1' and efmSampleClk'event then
--      if circBufferOutEn = '1' then
--        for i in 0 to 5 loop
--          x := leftOut(i) & rightOut(i);
--          write(f, to_integer(signed(std_logic_vector(x))));
  
--          write(row, to_hstring(TO_UNSIGNED(counter + i * 4, 32)), right, 9); 
--          write(row, to_hstring(leftOut(i)), right, 5); 
--          write(row, to_hstring(rightOut(i)), right, 5); 
--          writeLine(t, row);
--        end loop;
--        counter := counter + 24;
--      end if;
--    end if;
--  end process;
end a1;

