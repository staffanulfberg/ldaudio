library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity test is
end test;

architecture Behavioral of test is
    constant T: time := 10 ns;
    signal clock: std_logic;
    signal sw: std_logic_vector(3 downto 0) := "0000";
    signal btn: std_logic_vector(3 downto 0) := "0000";
    signal led: std_logic_vector(3 downto 0);
begin
    target: entity work.top port map (
        CLK100MHZ => clock,
        sw => sw,
        btn => btn,
        led => led
    );
    
    process 
    begin
        clock <= '0';
        wait for T/2;
        clock <= '1';
        wait for T/2;
    end process;
    
    process
    begin
--        wait for 100 * T;
        sw(0) <= '1';
        wait for 1000 * T;
        btn(0) <= '1';
        wait for 4 * T;
        btn(0) <= '0';
        wait for 1000 * T;
        sw(0) <= '0';
    end process;

end Behavioral;
