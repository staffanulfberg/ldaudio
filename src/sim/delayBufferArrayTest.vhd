library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity delayLineArrayTest is
end delayLineArrayTest;

architecture a1 of delayLineArrayTest is
  constant Tclk: time := 10 ns;
  signal clk: std_logic;

  signal delayWrLineNo: natural range 0 to 31;
  signal delayRdLineNo: natural range 0 to 31;
  signal delayIn: std_logic_vector(7 downto 0);
  signal delayErasedIn: std_logic;
  signal delayWrEn: std_logic;
  signal delayOut: std_logic_vector(7 downto 0);
  signal delayErasedOut: std_logic;

  type OutArray is array(0 to 3) of std_logic_vector(7 downto 0);
  signal outputs: OutArray;

  signal i: integer := 0;
  
begin
  process
  begin
    clk <= '0';
    wait for Tclk / 2;
    clk <= '1';
    wait for Tclk / 2;
  end process;

  dDelayArray: entity work.delayLineArray 
  generic map (
    delays => (0, 1, 2, 3)
  ) 
  port map (
    clock => clk,
    wrEn => delayWrEn,
    wrLineNo => delayWrLineNo,
    dataIn => delayIn,
    dataInErased => delayErasedIn,
    rdLineNo => delayRdLineNo,
    dataOut => delayOut,
    dataOutErased => delayErasedOut   
  );

  process(clk)
    variable readIx: integer := 0;
  begin
    if rising_edge(clk) then
      delayWrEn <= '1';
      delayWrLineNo <= i mod 4;
      delayIn <= std_logic_vector(to_unsigned(i, 8));
      delayErasedIn <= '1' when i mod 2 = 1 else '0';

      if i > 1 then
        readIx := (i - 1) mod 4;
        delayRdLineNo <= readIx;
      end if;
      if i >= 3 then
        outputs((i - 3) mod 4) <= delayOut;
      end if;     
      i <= i + 1;
    end if;
  end process;
end a1;
