library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity multtest is
end multtest;

architecture a1 of multtest is
  signal result: std_logic_vector(7 downto 0);
begin
  m: entity work.mult port map (
    a => "00000010",
    b => "00000100",
    c => result);
end a1;
