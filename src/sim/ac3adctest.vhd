library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

library STD;
use STD.textio.all;

entity ac3adctest is
end ac3adctest;

architecture a1 of ac3adctest is
  signal qpskClk: std_logic;  -- 2.88 * 16 MHz

  signal dataIn: signed(7 downto 0) := to_signed(0, 8); -- from A/D converter
  signal dataOut: std_logic; -- after filtering and clipping
  signal ac3SymbolSignal: std_logic_vector(1 downto 0); -- the output from ac3In
    
  type InputFile is file of character;
begin
  process
  begin
    qpskClk <= '0';
    wait for 10.85 ns;
    qpskClk <= '1';
    wait for 10.85 ns;
  end process;

  adc: entity work.ac3adc port map (
    clk => qpskClk,
    dataIn => dataIn,
    dataOut => dataOut);
    
  qpskIn: entity work.qpskIn port map (
    clk => qpskClk,
    dataIn => dataOut,
    dataOut => ac3SymbolSignal
  );
  
  process
    file f: InputFile open read_mode is "alien-46.08MHz.bin";
    variable c: character;
  begin
    if not endfile(f) then
      wait until rising_edge(qpskClk);
      read(f, c);
      dataIn <= to_signed((character'POS(c) - 128), 8);
    end if;
  end process;
end a1;
