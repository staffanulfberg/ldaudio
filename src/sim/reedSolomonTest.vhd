library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.reedSolomon_types.all;

entity reedSolomonTest is
end reedSolomonTest;

architecture a1 of reedSolomonTest is
  constant Tclk: time := 10 ns;
  signal clk: std_logic;
  
  constant frameSize: natural := 36;
  signal start: std_logic;
  signal done: std_logic;
  signal outputFrame: ByteArray(0 to frameSize - 1);
  signal dataIn: std_logic_vector(7 downto 0);
  signal erasedIn: std_logic;
  signal dataOut: std_logic_vector(7 downto 0);
  signal inEn: std_logic;
  signal OutEn: std_logic;
  signal inputIx: natural range 0 to frameSize;
  signal outputIx: natural range 0 to frameSize;
   
  type ram_type is array (0 to frameSize - 1) of std_logic_vector(8 downto 0);
  
  constant data: ram_type := (
    2 => '1' & x"07",
    others => '0' & x"00");
    
  constant data2: ram_type := (
    2 => '1' & x"07",
    4 => '1' & x"aa",
    7 => '1' & x"bb",
    31 => '1' & x"cc",
    others => '0' & x"00");

  constant data3: ram_type := (
      '0' & x"10", '0' & x"00", '0' & x"a3", '0' & x"e5",
      '0' & x"14", '0' & x"2b", '0' & x"88", '0' & x"fb",
      '0' & x"a0", '0' & x"8a", '0' & x"95", '0' & x"94",
      '0' & x"8d", '0' & x"30", '0' & x"19", '0' & x"e8",
      '0' & x"5f", '0' & x"7a", '0' & x"44", '0' & x"74",
      '0' & x"58", '0' & x"d6", '0' & x"1d", '0' & x"2d",
      '0' & x"d5", '0' & x"bb", '0' & x"d6", '0' & x"36",
      '0' & x"3d", '0' & x"17", '0' & x"24", '0' & x"e8",
      '0' & x"39", '0' & x"b9", '0' & x"a9", '0' & x"e0");  
  
begin
  process
  begin
    clk <= '0';
    wait for Tclk / 2;
    clk <= '1';
    wait for Tclk / 2;
  end process;

  C1: entity work.reedSolomon
  generic map (
    poly => 9x"187",
    fcr => 120,
    frameSize => frameSize)
  port map (
    clk => clk,
    enabled => '1',
    start => start,
    inEn => inEn,
    erasedIn => erasedIn,
    dataIn => dataIn,
    done => done,
    status => open,
    outEn => outEn,
    dataOut => dataOut
  );
  
  process(clk)
    variable i: integer := 0;
    constant t1: integer := 10;
    constant t2: integer := 1000;
    constant t3: integer := 2000;
  begin
    if rising_edge(clk) then
      start <= '1' when /* i = t1 or */ i = t2 or i = t3 else '0';
      inEn <= '0';
--      if i > t1 and i <= t1 + frameSize then
--        inEn <= '1';
--        inputIx <= i - t1 - 1;
--        dataIn <= data(i - t1 - 1)(7 downto 0);
--        erasedIn <= data(i - t1 - 1)(8);
--      end if;
      if i > t2 and i <= t2 + frameSize then
        inEn <= '1';
        inputIx <= i - t2 - 1;
        dataIn <= data2(i - t2 - 1)(7 downto 0);
        erasedIn <= data2(i - t2 - 1)(8);
      elsif i > t3 and i <= t3 + frameSize then
        inEn <= '1';
        inputIx <= i - t3 - 1;
        dataIn <= data3(i - t3 - 1)(7 downto 0);
        erasedIn <= data3(i - t3 - 1)(8);
      end if;
      i := i + 1;
    end if;
  end process;

  process(clk)
   begin
    if rising_edge(clk) then
      if done = '1' then
        outputIx <= 0;
      end if;
      if outEn = '1' then
        outputFrame(outputIx) <= dataOut;
        outputIx <= outputIx + 1;
      end if;
    end if;
  end process;
end a1;
