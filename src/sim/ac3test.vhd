library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

library STD;
use STD.textio.all;

Library UNISIM;
use UNISIM.vcomponents.all;

use work.reedSolomon_types.all;
use work.serialDebug_types.all;

entity ac3test is
end ac3test;

architecture a1 of ac3test is
  signal qpskClk: std_logic;  -- 2.88 * 16 MHz

  signal ac3SymbolEn: std_logic;
  signal ac3Symbol: std_logic_vector(1 downto 0); -- the symbol reclocked and read at the middle of symbols
  signal ac3FrameNoEn: std_logic;
  signal ac3FrameNo: unsigned(6 downto 0);
  signal ac3FrameDataEn: std_logic;
  signal ac3FrameData: std_logic_vector(7 downto 0);

  signal ac3WrEn: std_logic;
  signal ac3WrBlockSel: natural range 0 to 1;
  signal ac3WrAddr: natural range 0 to 1535;
  signal ac3WrData: std_logic_vector(7 downto 0);
  signal ac3RdBlockSel: natural range 0 to 1;
  signal ac3RdAddr: natural range 0 to 1535;
  signal ac3RdData: std_logic_vector(7 downto 0);
  signal ac3BlockReady: std_logic := '0';
  signal ac3ReadyBlockIndex: natural range 0 to 1;

  signal c1Done: std_logic;
  signal c1Status: CompletionStatus;
  signal c2Done: std_logic;
  signal c2Status: CompletionStatus;
  signal serialDebugData: DebugData;
  signal serialDebugTxEn: std_logic;
  signal serialDebugBusy: std_logic;

  signal ac3Led0R: std_logic;
  signal ac3Led0G: std_logic;
  signal ac3Led0B: std_logic;

  type InputFile is file of character;
begin
  process
  begin
    qpskClk <= '0';
    wait for 10.85 ns;
    qpskClk <= '1';
    wait for 10.85 ns;
  end process;
    
   ac3Framing: entity work.ac3InputFraming port map (
     clk => qpskClk,
     inEn => ac3SymbolEn,
     dataIn => ac3Symbol,
    frameNoOutEn => ac3FrameNoEn,
    frameNoOut => ac3FrameNo,
    outEn => ac3FrameDataEn,
    dataOut => ac3FrameData
   );
  
   ac3BlocksOut: entity work.ac3BlockBuffer port map (
    clk => qpskClk,
    wrEn => ac3WrEn,
    rdBlockSel => ac3RdBlockSel,
    rdAddr => ac3RdAddr,
    rdData => ac3RdData,
    wrBlockSel => ac3WrBlockSel,
    wrAddr => ac3WrAddr,
    wrData => ac3WrData
  );

  ac3Deinterleaver: entity work.ac3Deinterleave port map (
    clk => qpskClk,
    frameNoInEn => ac3FrameNoEn,
    frameNoIn => ac3FrameNo,
    inEn => ac3FrameDataEn,
    dataIn => ac3FrameData,
    c1Enable => '1',
    c1Done => c1Done,
    c1Status => c1Status,
    c2Enable => '1',
    c2Done => c2Done,
    c2Status => c2Status,
    ac3WrEn => ac3WrEn,
    ac3WrBlockSel => ac3WrBlockSel,
    ac3WrAddr => ac3WrAddr,
    ac3WrData => ac3WrData,
    ac3BlockReady => ac3BlockReady,
    ac3ReadyBlockIndex => ac3ReadyBlockIndex
  );
 
  process
    file f: InputFile open read_mode is "alien-symbols.txt";
    variable c: character;
    variable i: natural range 0 to 159;
  begin
    if not endfile(f) then
      wait until rising_edge(qpskClk);
      read(f, c);
      case character'POS(c) is
        when 48 => ac3Symbol <= "00";
        when 49 => ac3Symbol <= "01";
        when 50 => ac3Symbol <= "10";
        when 51 => ac3Symbol <= "11";
        when others => assert false;
      end case;
      ac3SymbolEn <= '1';

      for i in 1 to 159 loop
        wait until rising_edge(qpskClk);
        ac3SymbolEn <= '0';
      end loop;
    end if;
  end process;
end a1;
