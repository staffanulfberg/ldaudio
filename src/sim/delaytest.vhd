library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_textio.all;
use ieee.numeric_std.all;
library STD;
use STD.textio.all;

use work.common.all;

entity delaytest is
end delaytest;

architecture Behavioral of delaytest is
  constant T: time := 10 ns;
  signal clock: std_logic := '0';
  
  signal input: ByteArray(0 to 3) := (x"01", x"02", x"03", x"04");
  signal output: ByteArray(0 to 3);
begin
  delays: for i in 0 to 3 generate
    d1: entity work.delayLine 
    generic map (delay => i) 
    port map(
      clock => clock, 
      dataIn => input(i),
      dataOut => output(i));
  end generate;

  process(clock)
  begin
    if clock = '1' and clock'event then
      for i in 0 to 3 loop
        input(i) <= std_logic_vector(unsigned(input(i)) + to_unsigned(4, 8));
      end loop;
    end if;
  end process;
  
  process 
  begin
    clock <= not clock;
    wait for T/2;
  end process;
  
  
end Behavioral;
