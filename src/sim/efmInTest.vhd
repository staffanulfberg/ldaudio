library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

library STD;
use STD.textio.all;

use work.circBufferTypes.all;

entity efmInTest is
end efmInTest;

architecture a1 of efmInTest is
  constant Tclk: time := 10 ns;
  signal clk: std_logic;
  
  signal bitIn: std_logic;
  signal efmBit: std_logic;  
  signal efmBitEn: std_logic;  
  signal efmLocked: std_logic;  
    
  type InputFile is file of character;
begin
  process
  begin
    clk <= '0';
    wait for Tclk / 2;
    clk <= '1';
    wait for Tclk / 2;
  end process;
    
  efmIn: entity work.efmIn generic map (
    clkFrequency => 100000000
  )
  port map (
    clk => clk,
    dataIn => bitIn,
    outEn => efmBitEn,
    dataOut => efmBit
  );
    
    
  efm: entity work.efm port map (
    clk => clk,
    inEn => efmBitEn,
    dataIn => efmBit,
    enableErasures => '1',
    frameStart => open,
    dataOutEn => open,
    dataOut => open,
    erasedOut => open,
    locked => open
  );
  
  process
    file f: InputFile open read_mode is "jp-62.5MHz.b.bin"; -- FIXME: sample
                                                            -- rate changed!
    variable c: character;
  begin
    if not endfile(f) then
      wait until rising_edge(clk);
      read(f, c);
      if character'POS(c) < 128 then
        bitIn <= '0';
      else 
        bitIn <= '1';
      end if;
    end if;
  end process;
end a1;
