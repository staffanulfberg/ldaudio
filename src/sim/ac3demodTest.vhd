library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

library STD;
use STD.textio.all;

Library UNISIM;
use UNISIM.vcomponents.all;

entity ac3modTest is
end ac3modTest;

architecture a1 of ac3modTest is
  signal CLK100MHZ: std_logic;
  
  signal qpskClk: std_logic;  -- 2.88 * 16 MHz
  signal phaseErrorEn: std_logic;
  signal phaseError: signed(7 downto 0);

  signal ac3InSync: std_logic; -- synchronized
  signal ac3SymbolSignal: std_logic_vector(1 downto 0); -- the output from ac3In
  signal ac3SymbolEn: std_logic;
  signal ac3Symbol: std_logic_vector(1 downto 0); -- the symbol reclocked and read at the middle of symbols
  signal ac3FrameNoEn: std_logic;
  signal ac3FrameNo: unsigned(6 downto 0);
  signal ac3FrameDataEn: std_logic;
  signal ac3FrameData: std_logic_vector(7 downto 0);

  type InputFile is file of character;
begin
  process
  begin
    CLK100MHZ <= '0';
    wait for 5 ns;
    CLK100MHZ <= '1';
    wait for 5 ns;
  end process;
    
  qpskDpll: entity work.dpll generic map (
    clkInFrequency => 100000000,
    targetBaseFrequency => 11520000,
    outputMultF => 60.0,
    outputDivF => 15.0,
    counterBits => 20,
    phaseErrorBits => 8,
    errorSumBits => 17,
    invPcoeff => 4,
    invIcoeff => 128,
    maxAdjust => 511
  ) port map (
    clkIn => CLK100MHZ,
    clkOut => qpskClk,
    phaseErrorSrcEn => phaseErrorEn,
    phaseErrorSrc => phaseError,
    restartSrc => '0'
  );

  qpskIn: entity work.qpskIn port map (
    clk => qpskClk,
    dataIn => ac3InSync,
    dataOut => ac3SymbolSignal
  );
  
  qpskReclocker: entity work.qpskReclock port map (
    qpskClk => qpskClk,
    dataIn => ac3SymbolSignal,
    outEn => ac3SymbolEn,
    dataOut => ac3Symbol,
    phaseErrorEn => phaseErrorEn,
    phaseError => phaseError
  );

  ac3Framing: entity work.ac3InputFraming port map (
    clk => qpskClk,
    inEn => ac3SymbolEn,
    dataIn => ac3Symbol,
    frameNoOutEn => ac3FrameNoEn,
    frameNoOut => ac3FrameNo,
    outEn => ac3FrameDataEn,
    dataOut => ac3FrameData
  );

  process
    file f: InputFile open read_mode is "alien-46.08MHz-filtered.bin";
    variable c: character;
  begin
    if not endfile(f) then
      wait until rising_edge(qpskClk);
      read(f, c);
      if character'POS(c) < 117 then
        ac3InSync <= '0';
      else 
        ac3InSync  <= '1';
      end if;
    end if;
  end process;
end a1;
