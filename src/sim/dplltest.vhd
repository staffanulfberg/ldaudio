library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity dplltest is
end dplltest;

architecture Behavioral of dplltest is
  constant Tclk: time := 0.5 ns;
  signal clk: std_logic;
  signal qpskClk: std_logic;
  signal phaseErrorEn: std_logic;
  signal phaseError: std_logic_vector(8 downto 0);

begin
  process
  begin
    clk <= '0';
    wait for Tclk / 2;
    clk <= '1';
    wait for Tclk / 2;
  end process;

  qpskDpll: entity work.dpll generic map (
    clkInFrequency => 2000000000,
    targetBaseFrequency => 11520000,
    outputMultF => 60.0,
    outputDivF => 15.0,
    counterBits => 16,
    phaseErrorBits => 9,
    errorSumBits => 14,
    invPcoeff => 1,
    invIcoeff => 16,
    maxAdjust => 63
  ) port map (
    clkIn => CLK,
    clkOut => qpskClk,
    phaseErrorSrcEn => phaseErrorEn,
    phaseErrorSrc => phaseError,
    restartSrc => '0'
  );
  
  process(qpskClk)
  begin
    if rising_edge(qpskClk) then
      phaseErrorEn <= '0';
    end if;
  end process;
end Behavioral;
