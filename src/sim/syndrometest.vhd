library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.circBufferTypes.all;

entity syndrometest is
end syndrometest;

architecture a1 of syndrometest is
  constant Tclk: time := 10 ns;
  signal clk: std_logic;
  signal start: std_logic;
  signal frameIn: ByteArray(0 to 31);
  signal done: std_logic;
  signal syndromes: ByteArray(0 to 3);
begin
  process
  begin
    clk <= '0';
    wait for Tclk / 2;
    clk <= '1';
    wait for Tclk / 2;
  end process;
  
  s: entity work.syndrome 
  generic map (n => 32) 
  port map (
    clk => clk,
    start => start,
    frameIn => frameIn,
    done => done,
    s => syndromes);
    
  process(clk)
    variable i: integer := 0;
  begin
    if rising_edge(clk) then
      if i = 0 then
        for i in 0 to 31 loop
          frameIn(i) <= std_logic_vector(to_unsigned(i, 8));
        end loop;
      end if;
      if i = 10 then
        start <= '1';
      else
        start <= '0';
      end if;
      i := i + 1;
    end if;
  end process;
end a1;
