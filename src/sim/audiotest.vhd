library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
Library UNISIM;
use UNISIM.vcomponents.all;
library UNIMACRO;
use unimacro.Vcomponents.all;

entity audiotest is
end audiotest;

architecture behavioral of audiotest is
  constant TefmSampleClk: time := 10 ns;
  signal efmSampleClk: std_logic;

  constant Tmclk: time := 88.6 ns;
  signal mclk: std_logic;
  signal mclkGen: std_logic := '0';
  signal clkLocked: std_logic;
  signal clkFeedback: std_logic;  
  signal CLK100MHZ: std_logic := '0';
  signal counter: unsigned(14 downto 0) := to_unsigned(0, 15);
  
  signal tx_mclk: std_logic;
  signal tx_lrck: std_logic;
  signal tx_sclk: std_logic;
  signal tx_data: std_logic;
  signal tx_spdif: std_logic;
    
  signal dualChannelSampleWr: std_logic_vector(31 downto 0);
  signal audioWriteEn: std_logic;
  signal audioBufferFull: std_logic;
  
  signal dualChannelSampleRd: std_logic_vector(31 downto 0);
  signal sampleRdAvail: std_logic;
  signal sampleRdUnavail: std_logic;
  signal sampleRdEn: std_logic;

  constant TableSize: integer := 16;
  type SampleTable is array(0 to TableSize - 1) of signed(15 downto 0);
  constant LeftTable: SampleTable := (
    to_signed(0, 16),
    to_signed(12539, 16),
    to_signed(23169, 16),
    to_signed(30272, 16),
    to_signed(32767, 16),
    to_signed(30272, 16),
    to_signed(23169, 16),
    to_signed(12539, 16),
    to_signed(0, 16),
    to_signed(-12539, 16),
    to_signed(-23169, 16),
    to_signed(-30272, 16),
    to_signed(-32767, 16),
    to_signed(-30272, 16),
    to_signed(-23169, 16),
    to_signed(-12539, 16)
  );
  
  signal rst: std_logic;
  signal resetDone: std_logic;
  
begin
  process (mclk)
    variable i: unsigned(3 downto 0) := to_unsigned(0, 4);
  begin
    if mclk = '1' and mclk'event then
      rst <= '0';
      resetDone <= '0';
      if i > to_unsigned(4, 4) and i < to_unsigned(10, 4) then
        rst <= '1';
      end if;
      if i = "1111" then
        resetDone <= '1';
      else
        i := i + 1;
      end if;
    end if;
  end process;

  fifo: FIFO_DUALCLOCK_MACRO generic map (
   DEVICE => "7SERIES",            -- Target Device: "VIRTEX5", "VIRTEX6", "7SERIES"
   ALMOST_FULL_OFFSET => X"0080",  -- Sets almost full threshold
   ALMOST_EMPTY_OFFSET => X"0080", -- Sets the almost empty threshold
   DATA_WIDTH => 32,               -- Valid values are 1-72 (37-72 only valid when FIFO_SIZE="36Kb")
   FIFO_SIZE => "18Kb",            -- Target BRAM, "18Kb" or "36Kb"
   FIRST_WORD_FALL_THROUGH => FALSE) -- Sets the FIFO FWFT to TRUE or FALSE
  port map (
    ALMOSTEMPTY => open,
    ALMOSTFULL => open,
    DO => dualChannelSampleRd, -- Output data, width defined by DATA_WIDTH parameter
    EMPTY => sampleRdUnavail,
    FULL => audioBufferFull,
    RDCOUNT => open,
    RDERR => open,
    WRCOUNT => open,
    WRERR => open,
    DI => dualChannelSampleWr, -- Input data, width defined by DATA_WIDTH parameter
    RDCLK => mclk,
    RDEN => sampleRdEn,
    RST => rst,
    WRCLK => efmSampleClk,
    WREN => audioWriteEn
  );
  
  sampleRdAvail <= not sampleRdUnavail;
  
  audio: entity work.audio port map (
    enable => resetDone,
    mclk => mclk,
    tx_mclk => tx_mclk,
    tx_lrck => tx_lrck,
    tx_sclk => tx_sclk,
    tx_data => tx_data,
    tx_spdif => tx_spdif,
    sampleAvailable => sampleRdAvail,
    sampleData => dualChannelSampleRd,
    sampleDataRdEn => sampleRdEn,
    sampleBufferAlmostFull => '1'
  );

--  process 
--  begin
--      mclk <= '0';
--      wait for Tmclk/2;
--      mclk <= '1';
--      wait for Tmclk/2;
--  end process;
  
  process 
  begin
      CLK100MHZ <= '0';
      wait for 5ns;
      CLK100MHZ <= '1';
      wait for 5ns;
  end process;

  process(CLK100MHZ)
    variable newCounter: unsigned(14 downto 0);
  begin
    newCounter := counter + to_unsigned(1764, 15);
    if newCounter >= to_unsigned(15625, 15) then
      newCounter := newCounter - to_unsigned(15625, 15);
      mclkGen <= not mclkGen;
    end if;
    counter <= newCounter;
  end process;
   -- mclk generated at 11.2896 MHz = 44100 * 32 * 8 * (1 + 253e-9)
  MMCME2_BASE_inst: MMCME2_BASE generic map (
    CLKFBOUT_MULT_F => 64.0, -- Multiply value for all CLKOUT (2.000-64.000).
    CLKIN1_PERIOD => 88.577, -- Input clock period in ns to ps resolution (i.e. 33.333 is 30 MHz).
    CLKOUT0_DIVIDE_F => 64.0, 
    DIVCLK_DIVIDE => 1, -- Master division value (1-106)
    CLKOUT1_DIVIDE => 1 -- for the efmSampleClock; 1 doesn't seem to work. Too high frequency?
  )
  port map (
      CLKIN1 => mclkGen,
      CLKFBOUT => clkFeedback,
      CLKFBIN => clkFeedback,
      CLKOUT0 => mclk,
      CLKOUT1 => open,
      LOCKED => clkLocked, 
      RST => '0',
      PWRDWN => '0',
      CLKOUT0B => open,
      CLKOUT1B => open,
      CLKOUT2 => open,
      CLKOUT2B => open,
      CLKOUT3 => open,
      CLKOUT3B => open,
      CLKOUT4 => open,
      CLKOUT5 => open,
      CLKOUT6 => open,
      CLKFBOUTB => open
  );
  
  process 
  begin
      efmSampleClk <= '0';
      wait for TefmSampleClk/2;
      efmSampleClk <= '1';
      wait for TefmSampleClk/2;
  end process;
  
  process(efmSampleClk)
    variable writing: boolean := false;
    variable i: integer := 0;
  begin
    if efmSampleClk = '1' and efmSampleClk'event then
      audioWriteEn <= '0';
      if resetDone = '1' then
        if writing then -- wait for the next edge to make the write happen
          writing := false;
        elsif audioBufferFull = '0' then
          dualChannelSampleWr <= std_logic_vector(LeftTable(i)) & std_logic_vector(LeftTable(i) - 1);
          audioWriteEn <= '1';
          writing := true;
          i := (i + 1) mod TableSize;
        end if;
      end if;
    end if;
  end process;
end behavioral;
