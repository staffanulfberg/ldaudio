library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.reedSolomon_types.all;

entity reedSolomon is
  generic(
    poly: std_logic_vector(8 downto 0); -- only 11d and 187 implemented
    fcr: natural range 0 to 255;
    maxFrameSize: natural -- 32 (EFM C1) / 28 (EFM C2) / 37 (AC3 C1) / 36 (AC3 C2) -- always 4 parity bytes at the end
  );
  port (
    frameSize: in natural range 4 to maxFrameSize;
    clk: in std_logic;    
    enabled: in std_logic; -- set to correct data, unset to pass through
    start: in std_logic; -- assert before the first inEn to start reading data, works a bit like a reset to move to the initial state
    inEn: std_logic;
    erasedIn: in std_logic;
    dataIn: in std_logic_vector(7 downto 0);

    done: out std_logic; -- asserted for only one cycle before the data is clocked out
    status: out CompletionStatus; -- set when done is asserted, stays set until next time started

    outEn: out std_logic; -- clocked after done to output all data
    dataOut: out std_logic_vector(7 downto 0)
  );
end reedSolomon;

architecture rtl of reedSolomon is
  type State is (
    ReadingInput, TempWait,
    SyndromeComputation, SyndromeEval, SyndromeEval2, 
    TryCorrectOneError, OneStep2, OneStep21, OneStep22, OneStep23, OneStep24, OneStep25, OneStep3, OneStep4, OneStep5, OneStep6,
    TryCorrectTwoErrors, TwoStep2, TwoStep3, TwoStep4, TwoStep5, TwoStep6, TwoStep7, TwoStep7b, TwoStep7c, TwoStep8, TwoStep9, TwoStep10, TwoStep11, TwoStep12,
    ErasureDecoding, ErasureBeforeStep2, ErasureStep2, ErasureStep3, ErasureStep4, ErasureStep5, ErasureStep6, ErasureStep7,
    StartWriteOutput, WritingOutput, AllDone);

  signal frame: ByteArray(maxFrameSize - 1 downto 0); 
  constant maxErasures: natural := 4;
  signal noOfErasures: natural range 0 to maxFrameSize;
  type PosArray is array(natural range <>) of natural range 0 to maxFrameSize - 1;
  signal erasurePositions: PosArray(0 to maxErasures - 1);
    
  constant alpha: std_logic_vector(7 downto 0) := "00000010"; 
  signal alphaSquared: std_logic_vector(7 downto 0);
    
  signal currentState: State := AllDone;
  signal nextState: State; -- we use TempWait to jump to nextState one cycle later 
  signal rwIndex: natural range 0 to maxFrameSize - 1 := 0;
  signal syndromeStart: std_logic := '0';
  signal syndromeInEn: std_logic;
  signal syndromeIn: std_logic_vector(7 downto 0);
  signal syndromes: ByteArray(0 to 3);
  
  signal errorPos: unsigned(7 downto 0);

  -- signals used in several places
  signal i: natural range 0 to maxFrameSize;
  signal alphaI: std_logic_vector(7 downto 0);
  
  -- signals used for two error correction
  signal lambda0: std_logic_vector(7 downto 0);
  signal lambda1: std_logic_vector(7 downto 0);
  signal lambda2: std_logic_vector(7 downto 0);
  signal rootsFound: natural range 0 to 2;
  signal errorPositions: PosArray(0 to 1);
  type RootArray is array(0 to 1) of std_logic_vector(7 downto 0);
  signal roots: RootArray;
  signal x0PlusX1Inv: std_logic_vector(7 downto 0);
  signal tmp: std_logic_vector(7 downto 0);

  -- signals for inputs and outputs of the arithmetic operations
  signal invFcrA: std_logic_vector(7 downto 0);
  signal invFcrB: std_logic_vector(7 downto 0);
  signal logA: std_logic_vector(7 downto 0);
  signal logB: std_logic_vector(7 downto 0);
  signal invA: std_logic_vector(7 downto 0);
  signal invB: std_logic_vector(7 downto 0);
  signal mul1A: std_logic_vector(7 downto 0);
  signal mul1B: std_logic_vector(7 downto 0);
  signal mul1C: std_logic_vector(7 downto 0);
  signal mul2A: std_logic_vector(7 downto 0);
  signal mul2B: std_logic_vector(7 downto 0);
  signal mul2C: std_logic_vector(7 downto 0);
  
  -- signals used during erasure decoding
  signal errorIx: natural range 0 to maxErasures - 1;
  signal erasurePosAlphaPowers: ByteArray(0 to maxErasures - 1);
  
  signal eqnStart: std_logic;
  signal eqnInEn: std_logic;
  signal eqnXin: std_logic_vector(7 downto 0);
  signal eqnSin: std_logic_vector(7 downto 0);
  signal eqnOutEn: std_logic;
  signal eqnYout: std_logic_vector(7 downto 0);
  signal errorValues: ByteArray(0 to maxErasures);
  
begin
  syndromeComputer: entity work.syndrome generic map (
    poly => poly,
    fcr => fcr,
    maxN => maxFrameSize
  ) port map (
    n => frameSize,
    clk => clk,
    start => syndromeStart,
    inEn => syndromeInEn,
    dataIn => syndromeIn,
    s => syndromes
  );
  
  log: entity work.log 
  generic map ( poly => poly )
  port map (
    clk => clk,
    a => logA,
    b => logB
  );
  
  invFcr: entity work.invFcr
  generic map ( poly => poly, fcr => fcr )
  port map (
    clk => clk,
    a => invFcrA,
    b => invFcrB
  );

  inv: entity work.inv
  generic map ( poly => poly )
  port map (
    clk => clk,
    a => invA,
    b => invB
  );

  mul1: entity work.mult
  generic map ( poly => poly )
  port map (
    a => mul1A,
    b => mul1B,
    c => mul1C
  );

  mul2: entity work.mult
  generic map ( poly => poly )
  port map (
    a => mul2A,
    b => mul2B,
    c => mul2C
  );
  
  eqnSolver: entity work.eqnSolver
  generic map ( poly => poly, maxDimension => maxErasures )
  port map (
    clk => clk,
    start => eqnStart,
    n => noOfErasures,
    inEn => eqnInEn,
    xIn => eqnXin,
    sIn => eqnSin,
    outEn => eqnOutEn,
    yOut => eqnYout
  );
  
  process(clk)
  begin
    if rising_edge(clk) then
      done <= '0';
      outEn <= '0';
      syndromeStart <= '0';
      syndromeInEn <= '0';
      eqnStart <= '0';
      eqnInEn <= '0';

      if start = '1' then
        rwIndex <= 0;
        noOfErasures <= 0;
        currentState <= ReadingInput;
      end if;
      case currentState is 
        when ReadingInput =>
          if inEn = '1' then
            -- the first (leftmost) data will be multiplied with the highest order components of the parity check matrix
            frame(frameSize - 1 - rwIndex) <= dataIn;
            if erasedIn = '1' then
              if noOfErasures < maxErasures then
                erasurePositions(noOfErasures) <= frameSize - 1 - rwIndex;
                noOfErasures <= noOfErasures + 1;
              end if;
            end if;
            if rwIndex < frameSize - 1 then
              rwIndex <= rwIndex + 1;
            else
              if enabled = '1' then
                status <= Running;
                currentState <= SyndromeComputation;
                syndromeStart <= '1';
                rwIndex <= 0;
              else
                status <= NoErrors;
                currentState <= StartWriteOutput;
              end if;
            end if;
          end if;

        when TempWait =>
          currentState <= nextState;
          
        when SyndromeComputation =>
          syndromeInEn <= '1';
          syndromeIn <= frame(rwIndex); -- the first byte of input is multiplied by the lowest order alpha power
          if rwIndex < frameSize - 1 then
            rwIndex <= rwIndex + 1;
          else
            currentState <= TempWait;
            nextState <= SyndromeEval;
          end if;
          
          mul1A <= alpha; -- compute this now since we are waiting anyway
          mul1B <= alpha;
          alphaSquared <= mul1C;
          
        when SyndromeEval =>
          if syndromes(0) = "00000000" and syndromes(1) = "00000000" and syndromes(2) = "00000000" and syndromes(3) = "00000000" then -- all good
            status <= NoErrors;
            currentState <= StartWriteOutput;
          else
            mul1A <= syndromes(1);
            mul1B <= syndromes(1);
            mul2A <= syndromes(0);
            mul2B <= syndromes(2);  
            currentState <= SyndromeEval2;
          end if;
        
        when SyndromeEval2 =>
          if (mul1C xor mul2C) = "00000000" then -- check determinant          
            currentState <= TryCorrectOneError;
          else
            currentState <= TryCorrectTwoErrors;
          end if;
          
        when TryCorrectOneError =>       
          if syndromes(0) = "00000000" or syndromes(1) = "00000000" or syndromes(2) = "00000000" or syndromes(3) = "00000000" then -- cannot correct
            currentState <= ErasureDecoding; -- failed; try erasure decoding
          else
            invA <= syndromes(0);
            nextState <= OneStep2;
            currentState <= TempWait;
          end if;
          
        when OneStep2 =>
          mul1A <= syndromes(1);
          mul1B <= invB;  -- mul1C will be syndromes(1) / syndromes(0)
          currentState <= OneStep21;
          
        when OneStep21 =>  
          tmp <= mul1C; -- save s1 / s0 to compare with s2/s1 and s3/s2
          invA <= syndromes(1);
          nextState <= OneStep22;
          currentState <= TempWait;
        
        when OneStep22 =>  
          mul1A <= syndromes(2);
          mul1B <= invB;
          currentState <= OneStep23;
                  
        when OneStep23 =>
          if mul1C /= tmp then
            nextState <= ErasureDecoding; -- inconsistent syndromes
          else
            invA <= syndromes(2);
            nextState <= OneStep24;
            currentState <= TempWait;            
          end if;
          
        when OneStep24 =>  
          mul1A <= syndromes(3);
          mul1B <= invB;
          currentState <= OneStep25;                  
        
        when OneStep25 =>  
          if mul1C /= tmp then
            currentState <= ErasureDecoding; -- inconsistent syndromes
          else
            currentState <= OneStep3;
          end if;            
          
        when OneStep3 =>
          logA <= tmp; -- logB will be log(syndromes(1) / syndromes(0)), which is the error position
          nextState <= OneStep4;
          currentState <= TempWait;

        when OneStep4 =>  -- logB is not error position and will not be used for anything else 
          errorPos <= unsigned(logB);
          if unsigned(logB) >= frameSize then -- error positions out of range
            currentState <= ErasureDecoding; -- failed; try erasure decoding
          else
            invFcrA <= logB;
            nextState <= OneStep5;
            currentState <= TempWait;          
          end if;
          
        when OneStep5 =>
          mul1A <= invFcrB;
          mul1B <= syndromes(0);
          currentState <= OneStep6;
          
        when OneStep6 =>
          frame(to_integer(errorPos)) <= frame(to_integer(errorPos)) xor mul1C;
          status <= CorrectedOneError;
          currentState <= StartWriteOutput;
      
        when TryCorrectTwoErrors =>
          invA <= mul1C xor mul2C; -- compute determinant inverse
          currentState <= TwoStep2;
          
        when TwoStep2 =>
          mul1A <= syndromes(1);
          mul1B <= syndromes(2);
          mul2A <= syndromes(0);
          mul2B <= syndromes(3);  
          currentState <= TwoStep3;
          
        when TwoStep3 =>
          mul1A <= mul1C xor mul2C;
          mul1B <= invB;
          currentState <= TwoStep4;
          
        when TwoStep4 =>
          lambda1 <= mul1C;
          mul1A <= syndromes(2);
          mul1B <= syndromes(2);
          mul2A <= syndromes(1);
          mul2B <= syndromes(3);  
          currentState <= TwoStep5;
          
        when TwoStep5 =>
          mul1A <= mul1C xor mul2C;
          mul1B <= invB;
          currentState <= TwoStep6;
          
        when TwoStep6 =>
          lambda2 <= mul1C; -- actually lambda[j] will contain lambda[j] * alpha ^ (j*i)
          i <= 0;
          rootsFound <= 0;
          alphaI <= "00000001";
          lambda0 <= "00000001";
          currentState <= TwoStep7;
          
        when TwoStep7 =>
          if i < frameSize then
            if (lambda1 xor lambda0) = lambda2 then
              if rootsFound = 2 then
                currentState <= ErasureDecoding; -- failed (more than two roots; can this happen?); try erasure decoding
              else
                roots(rootsFound) <= alphaI;
                errorPositions(rootsFound) <= i;
                rootsFound <= rootsFound + 1;
              end if;
            end if;
            
            mul1A <= alphaI;
            mul1B <= alpha;
            currentState <= TwoStep7b;
            i <= i + 1;
          else
            if rootsFound = 2 then
              currentState <= TwoStep8;
            else
              currentState <= ErasureDecoding; -- failed; try erasure decoding
            end if;
          end if;
          
        when TwoStep7b =>
          alphaI <= mul1C;
          mul1A <= lambda1;
          mul1B <= alpha;
          mul2A <= lambda0;
          mul2B <= alphaSquared;
          currentState <= TwoStep7c;
          
        when TwoStep7c =>          
          lambda1 <= mul1C;
          lambda0 <= mul2C;
          currentState <= TwoStep7;

        when TwoStep8 =>
          invA <= roots(0) xor roots(1);
          nextState <= TwoStep9;    
          currentState <= TempWait;

        when TwoStep9 =>
          x0PlusX1Inv <= invB;
          invFcrA <= std_logic_vector(to_unsigned(errorPositions(0), 8));
          mul1A <= syndromes(0);
          mul1B <= roots(1);
          mul2A <= syndromes(0);
          mul2B <= roots(0);
          nextState <= TwoStep10;
          currentState <= TempWait;
        
        when TwoStep10 =>
          tmp <= invFcrB;
          invFcrA <= std_logic_vector(to_unsigned(errorPositions(1), 8));
          mul1A <= mul1C xor syndromes(1);
          mul1B <= x0PlusX1Inv;
          mul2A <= mul2C xor syndromes(1);
          mul2B <= x0PlusX1Inv;
          nextState <= TwoStep11;
          currentState <= TempWait;
         
        when TwoStep11 =>
          mul1A <= mul1C;
          mul1B <= tmp;
          mul2A <= mul2C;
          mul2B <= invFcrB;
          currentState <= TwoStep12;

        when TwoStep12 =>
          frame(errorPositions(0)) <= frame(errorPositions(0)) xor mul1C;
          frame(errorPositions(1)) <= frame(errorPositions(1)) xor mul2C;
          status <= CorrectedTwoErrors;
          currentState <= StartWriteOutput;
           
        when ErasureDecoding =>
          -- We jump here when 1 or 2 error decoding failed.
          -- Since we cannot correct unerased errors and erased
          -- errors simultaneously there is no point trying if less than 3 erasues.
          -- (This could be improved to correct 1 error, 2 erasures!)
          if noOfErasures <= 2 or noOfErasures > maxErasures then
            status <= Failed;
            currentState <= StartWriteOutput;
          else
            i <= 0;
            errorIx <= noOfErasures - 1;
            alphaI <= "00000001";
            currentState <= ErasureBeforeStep2;
          end if;
            
        when ErasureBeforeStep2 =>
          mul1A <= alpha;
          mul1B <= alphaI;
          currentState <= ErasureStep2;

        when ErasureStep2 => -- we search backwards since the lowest error index will be last in erasurePositions
          if i = erasurePositions(errorIx) then
            erasurePosAlphaPowers(errorIx) <= alphaI;
            errorIx <= errorIx - 1 when errorIx > 0 else 0;
          end if;
          alphaI <= mul1C;
          mul1B <= mul1C;
          if i < frameSize - 1 then 
            i <= i + 1;
          else
            eqnStart <= '1';
            assert(errorIx = 0);
            i <= 0;
            currentState <= ErasureStep3;
          end if;
                
        when ErasureStep3 => -- write data to eqn solver
          if i < noOfErasures then
            eqnInEn <= '1';
            eqnXin <= erasurePosAlphaPowers(i);
            eqnSin <= syndromes(i);
            i <= i + 1;
          else
            i <= 0;
            currentState <= ErasureStep4;
          end if;
                 
        when ErasureStep4 => -- wait for eqn solver to complete
          if eqnOutEn = '1' then
            errorValues(i) <= eqnYout;
            if i < noOfErasures - 1 then
              i <= i + 1;
            else
              i <= 0;
              currentState <= ErasureStep5;
            end if;
          end if;
                 
        when ErasureStep5 =>
          invFcrA <= std_logic_vector(to_unsigned(erasurePositions(i), 8));
          nextState <= ErasureStep6;
          currentState <= TempWait;
          
        when ErasureStep6 =>
          mul1A <= invFcrB;
          mul1B <= errorValues(i);
          currentState <= ErasureStep7;
          
        when ErasureStep7 =>
          frame(erasurePositions(i)) <= frame(erasurePositions(i)) xor mul1C;
          if i < noOfErasures - 1 then
            i <= i + 1;
            currentState <= ErasureStep5;
          else
            status <= CorrectedErasures;
            currentState <= StartWriteOutput;
          end if;        
            
        when StartWriteOutput =>
          done <= '1';
          rwIndex <= 0;
          currentState <= WritingOutput;
          
        when WritingOutput =>
          outEn <= '1';
          dataOut <= frame(frameSize - 1 - rwIndex);
          if rwIndex < frameSize - 1 then
            rwIndex <= rwIndex + 1;
          else
            currentState <= AllDone;            
          end if;
              
        when AllDone =>
      end case;
    end if;
  end process;
end rtl;
