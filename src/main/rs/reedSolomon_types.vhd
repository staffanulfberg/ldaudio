library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package reedSolomon_types is
  type CompletionStatus is (Running, NoErrors, CorrectedOneError, CorrectedTwoErrors, CorrectedErasures, Failed);
  type ByteArray is array(natural range <>) of std_logic_vector(7 downto 0);
end package;

