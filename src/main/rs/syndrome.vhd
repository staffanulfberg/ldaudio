library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.reedSolomon_types.all;

entity syndrome is
  generic (
    poly: std_logic_vector(8 downto 0); -- only 11d and 187 implemented
    fcr: natural range 0 to 255;
    maxN: positive
  );
  port (
    n: positive range 4 to maxN; -- 28, 32 or 36 in this application
    clk: in std_logic;
    start: in std_logic;
    inEn: in std_logic;
    dataIn: in std_logic_vector(7 downto 0);
    s: out ByteArray(0 to 3)
  );
end syndrome;

architecture a1 of syndrome is
  signal powers: ByteArray(0 to 3); -- alpha^(currentByte * i)
  signal m1Out: ByteArray(0 to 3);
  signal m2Out: ByteArray(0 to 3) ;
  
  function firstAlphaPowers(fcr: natural range 0 to 255) return ByteArray is
  begin
    if fcr = 0 then return ( "00000001", "00000010", "00000100", "00001000" );
    elsif fcr = 120 then return ( "11100001", "01000101", "10001010", "10010011" ); 
    else 
      assert false report "poly is set to an unsupported value." severity failure; 
      return (x"00", x"00", x"00", x"00"); -- get rid of warning
    end if;
  end function;
  constant alphaPowI: ByteArray(0 to 3) := firstAlphaPowers(fcr);
begin
  syndrome_compute: for i in 0 to 3 generate
    m1: entity work.mult generic map (poly => poly) port map(a => dataIn, b => powers(i), c => m1Out(i));
    m2: entity work.mult generic map (poly => poly) port map(a => powers(i), b => alphaPowI(i), c => m2Out(i));
    process(clk)
    begin
      if rising_edge(clk) then
        if start = '1' then
          s(i) <= "00000000";
          powers(i) <= "00000001";
        elsif inEn = '1' then
          s(i) <= s(i) xor m1Out(i);
          powers(i) <= m2Out(i);
        end if;
      end if;
    end process;
  end generate;
end a1;
