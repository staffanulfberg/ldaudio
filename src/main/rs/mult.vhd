library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity mult is
  generic (
    poly: std_logic_vector(8 downto 0) -- only 11d and 187 implemented
  );
  port (
    a: in std_logic_vector(7 downto 0);
    b: in std_logic_vector(7 downto 0);
    c: out std_logic_vector(7 downto 0)
  );
end mult;

architecture a1 of mult is
  signal x: std_logic_vector(14 downto 0);
begin
  x(0) <= a(0) and b(0);
  x(1) <= (a(0) and b(1)) xor (a(1) and b(0));
  x(2) <= (a(0) and b(2)) xor (a(1) and b(1)) xor (a(2) and b(0));
  x(3) <= (a(0) and b(3)) xor (a(1) and b(2)) xor (a(2) and b(1)) xor (a(3) and b(0));
  x(4) <= (a(0) and b(4)) xor (a(1) and b(3)) xor (a(2) and b(2)) xor (a(3) and b(1)) xor (a(4) and b(0));
  x(5) <= (a(0) and b(5)) xor (a(1) and b(4)) xor (a(2) and b(3)) xor (a(3) and b(2)) xor (a(4) and b(1)) xor (a(5) and b(0));
  x(6) <= (a(0) and b(6)) xor (a(1) and b(5)) xor (a(2) and b(4)) xor (a(3) and b(3)) xor (a(4) and b(2)) xor (a(5) and b(1)) xor (a(6) and b(0));
  x(7) <= (a(0) and b(7)) xor (a(1) and b(6)) xor (a(2) and b(5)) xor (a(3) and b(4)) xor (a(4) and b(3)) xor (a(5) and b(2)) xor (a(6) and b(1)) xor (a(7) and b(0));
  x(8) <= (a(1) and b(7)) xor (a(2) and b(6)) xor (a(3) and b(5)) xor (a(4) and b(4)) xor (a(5) and b(3)) xor (a(6) and b(2)) xor (a(7) and b(1));
  x(9) <= (a(2) and b(7)) xor (a(3) and b(6)) xor (a(4) and b(5)) xor (a(5) and b(4)) xor (a(6) and b(3)) xor (a(7) and b(2));
  x(10) <= (a(3) and b(7)) xor (a(4) and b(6)) xor (a(5) and b(5)) xor (a(6) and b(4)) xor (a(7) and b(3));
  x(11) <= (a(4) and b(7)) xor (a(5) and b(6)) xor (a(6) and b(5)) xor (a(7) and b(4));
  x(12) <= (a(5) and b(7)) xor (a(6) and b(6)) xor (a(7) and b(5));
  x(13) <= (a(6) and b(7)) xor (a(7) and b(6));
  x(14) <= a(7) and b(7);

  gen: if poly = 9x"11d" generate  
    c(0) <= x(0) xor x(8) xor x(12) xor x(13) xor x(14);
    c(1) <= x(1) xor x(9) xor x(13) xor x(14);
    c(2) <= x(2) xor x(8) xor x(10) xor x(12) xor x(13);
    c(3) <= x(3) xor x(8) xor x(9) xor x(11) xor x(12);
    c(4) <= x(4) xor x(8) xor x(9) xor x(10) xor x(14);
    c(5) <= x(5) xor x(9) xor x(10) xor x(11);
    c(6) <= x(6) xor x(10) xor x(11) xor x(12);
    c(7) <= x(7) xor x(11) xor x(12) xor x(13);
  elsif poly = 9x"187" generate
    c(0) <= x(0) xor x(8) xor x(9) xor x(10) xor x(11) xor x(12) xor x(13);
    c(1) <= x(1) xor x(8) xor x(14);
    c(2) <= x(2) xor x(8) xor x(10) xor x(11) xor x(12) xor x(13);
    c(3) <= x(3) xor x(9) xor x(11) xor x(12) xor x(13) xor x(14);
    c(4) <= x(4) xor x(10) xor x(12) xor x(13) xor x(14);
    c(5) <= x(5) xor x(11) xor x(13) xor x(14);
    c(6) <= x(6) xor x(12) xor x(14);
    c(7) <= x(7) xor x(8) xor x(9) xor x(10) xor x(11) xor x(12);  
  else generate
    assert false report "poly is set to an unsupported value." severity failure;
  end generate;
end a1;
