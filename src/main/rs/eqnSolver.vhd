library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
use work.reedSolomon_types.all;


--Solves the equation system
--x(0)^k y(0) + ... + x(n-1)^k y(n-1) = s(k), for all k = 0...n-1

entity eqnSolver is
  generic (
      poly: std_logic_vector(8 downto 0); -- only 11d and 187 implemented
      maxDimension: natural
  );
  port (
    clk: in std_logic;
    start: in std_logic; -- resets and starts to listen to input
    n: in natural range 1 to maxDimension; -- the dimension of the equation system; needs to be kept constant during operation
    inEn: in std_logic; -- consume one pair of (x, s).
    xIn: in std_logic_vector(7 downto 0);
    sIn: in std_logic_vector(7 downto 0);
    outEn: out std_logic; -- asserted once per output byte
    yOut: out std_logic_vector(7 downto 0)
  );
end eqnSolver;

architecture a1 of eqnSolver is
  type State is (ReadingInput, BuildMatrix, StoreMatrixEntry,
    Elimination1, StoreLeadingInverse, FirstElementUnitLoop, StoreInFirstelementUnitLoop, StoreSFirstElementUnit,
    RowsUnderLoop, RowsUnderInnerLoop, RowsUnderInnerLoop2, StoreSRowsUnderLoop,
    MakeBottomRightUnit, StoreBottomRightInverse, StoreBottomRightInverse2,
    Elimination2, RowsOverLoop, StoreRowOverLoop,
    WriteOutput, AllDone);
  signal currentState: State := AllDone;
  signal waitCounter: natural range 0 to 1 := 0;
  constant alpha: std_logic_vector(7 downto 0) := "00000010"; 

  signal x: ByteArray(0 to maxDimension - 1);
  signal s: ByteArray(0 to maxDimension - 1);
  type Matrix is array(0 to maxDimension - 1) of ByteArray(0 to maxDimension - 1);
  signal mat: Matrix;

  signal i: natural range 0 to maxDimension;
  signal j: natural range 0 to maxDimension;
  signal k: natural range 0 to maxDimension;
  signal leadingInverse: std_logic_vector(7 downto 0);

  signal invA: std_logic_vector(7 downto 0);
  signal invB: std_logic_vector(7 downto 0);
  signal mul1A: std_logic_vector(7 downto 0);
  signal mul1B: std_logic_vector(7 downto 0);
  signal mul1C: std_logic_vector(7 downto 0);

begin
  inv: entity work.inv
  generic map ( poly => poly )
  port map (
    clk => clk,
    a => invA,
    b => invB
  );

  mul1: entity work.mult
  generic map ( poly => poly )
  port map (
    a => mul1A,
    b => mul1B,
    c => mul1C
  );
  
  process(clk)
  begin
    if rising_edge(clk) then
      outEn <= '0';
      if start = '1' then
        i <= 0;
        waitCounter <= 0;
        currentState <= ReadingInput;
      end if;
      if waitCounter /= 0 then
        waitcounter <= waitCounter - 1;
      else
        case currentState is 
          when ReadingInput =>
            if inEn = '1' then
              x(i) <= xIn;
              s(i) <= sIn;
              i <= i + 1;
              if i = n - 1 then
                i <= 0;
                j <= 0;
                currentState <= BuildMatrix;
              end if;
            end if;
            
          when BuildMatrix =>
            if i = 0 then
              mat(i)(j) <= "00000001";
              if j < n - 1 then
                j <= j + 1;
              else
                j <= 0;
                i <= i + 1;
              end if;
            elsif i < n then
              mul1A <= mat(i - 1)(j);
              mul1B <= x(j);
              currentState <= StoreMatrixEntry;
            else
              i <= 0;
              currentState <= Elimination1;
            end if;
          
          when StoreMatrixEntry =>
            mat(i)(j) <= mul1C;
            if j < n - 1 then
              j <= j + 1;
            else
              j <= 0;
              i <= i + 1;
            end if;
            currentState <= BuildMatrix;
          
          when Elimination1 =>
            invA <= mat(i)(i);
            waitCounter <= 1;
            currentState <= StoreLeadingInverse;
            
          when StoreLeadingInverse =>
            leadingInverse <= invB;
            j <= i;
            currentState <= FirstElementUnitLoop;
          
          when FirstElementUnitLoop =>
            mul1A <= mat(i)(j);
            mul1B <= leadingInverse;
            currentState <= StoreInFirstelementUnitLoop;
            
          when StoreInFirstelementUnitLoop =>
            mat(i)(j) <= mul1C;
            if j < n - 1 then
              j <= j + 1;
              currentState <= FirstElementUnitLoop;
            else
              mul1A <= s(i);
              currentState <= StoreSFirstElementUnit;
            end if;
            
          when StoreSFirstElementUnit =>
            s(i) <= mul1C;
            k <= i + 1;
            currentState <= RowsUnderLoop;
            
          when RowsUnderLoop =>
            mul1A <= mat(k)(i);
            j <= i;
            currentState <= RowsUnderInnerLoop;      
            
          when RowsUnderInnerLoop =>
            mul1B <= mat(i)(j);
            currentState <= RowsUnderInnerLoop2;
            
          when RowsUnderInnerLoop2 =>
            mat(k)(j) <= mat(k)(j) xor mul1C;
            if j < n - 1 then
              j <= j + 1;
              currentState <= RowsUnderInnerLoop;
            else
              mul1B <= s(i);
              currentState <= StoreSRowsUnderLoop;
            end if;
            
          when StoreSRowsUnderLoop =>
            s(k) <= s(k) xor mul1C;
            if k < n - 1 then
              k <= k + 1;
              currentState <= RowsUnderLoop;
            else
              if i < n - 2 then
                i <= i + 1;
                currentState <= Elimination1;
              else
                currentState <= MakeBottomRightUnit;
              end if;
            end if;
            
          when MakeBottomRightUnit =>
            invA <= mat(n - 1)(n - 1);
            waitCounter <= 1;
            currentState <= StoreBottomRightInverse;
            
          when StoreBottomRightInverse =>
            mat(n - 1)(n - 1) <= "00000001";
            mul1A <= invB;
            mul1B <= s(n - 1);
            currentState <= StoreBottomRightInverse2;

          when StoreBottomRightInverse2 =>            
            s(n - 1) <= mul1C;
            if n > 1 then
              i <= n - 1;
              currentState <= Elimination2;
            else
              i <= 0;
              currentState <= WriteOutput;
            end if;
            
          when Elimination2 => -- do not execute when n = 1
            k <= i - 1;
            currentState <= RowsOverLoop;
            
          when RowsOverLoop =>
            mul1A <= mat(k)(i);
            mul1B <= s(i);
            currentState <= StoreRowOverLoop;
          
          when StoreRowOverLoop =>
            s(k) <= s(k) xor mul1C;
            if k > 0 then
              k <= k - 1;
              currentState <= RowsOverLoop;
            else
              if i > 1 then
                i <= i - 1;
                currentState <= Elimination2;
              else
                i <= 0;
                currentState <= WriteOutput;                
              end if;
            end if;  
               
          when WriteOutput =>
            outEn <= '1';
            yOut <= s(i);
            if i < n - 1 then
              i <= i + 1;
            else
              currentState <= AllDone;
            end if;
          
          when AllDone =>
        end case;
      end if;
    end if;
  end process;
end a1;
