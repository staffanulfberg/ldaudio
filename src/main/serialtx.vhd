library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity serialtx is
  generic (
    clkFreq: natural
  );
  port (
    clk: in std_logic;
    txEn: in std_logic;
    dataIn: in std_logic_vector(7 downto 0);
    busy: out std_logic;
    tx_uart: out std_logic
  );
end serialtx;

architecture rtl of serialtx is
  constant clocksPerBit: natural := clkFreq / 9600;
  signal clkCounter: natural range 0 to clocksPerBit - 1 := 0;
  signal bitIndex: natural range 0 to 10 := 10; -- The current bit being sent; incremented when moving to the next; 10 means we are done
  signal txData: std_logic_vector(9 downto 0);
  signal clockedBusy: std_logic := '1';
  
  signal gapCounter: natural range 0 to 255; -- introduces a gap for long continuous transfers to ensure synchronization
  signal waitCounter: natural range 0 to clocksPerBit * 10 := clocksPerBit * 10; -- start waiting
begin
  busy <= clockedBusy or txEn; -- set the busy flag asynchronously so that the writer sees it right away

  process (clk)
  begin
    if rising_edge(clk) then
			tx_uart <= '1';
			
      if bitIndex = 10 then -- we are idle
        if waitCounter /= 0 then
          waitCounter <= waitCounter - 1;
        else 
          clockedBusy <= '0';
          if txEn = '1' then
            txData <= '1' & dataIn & '0';
            bitIndex <= 0;
            clkCounter <= 0;
            clockedBusy <= '1';
          end if;
        end if;
      else
        tx_uart <= txData(bitIndex);

        if clkCounter < clocksPerBit - 1 then
          clkCounter <= clkCounter + 1; 
        else 
          clkCounter <= 0;
          bitIndex <= bitIndex + 1;
        end if;
      end if;
    end if;
  end process;
end rtl;
