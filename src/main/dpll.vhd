library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.vcomponents.all;
library UNIMACRO;
use unimacro.Vcomponents.all;

entity dpll is
  -- Adding 1 extra (dpllAdjust = 1) increases the counter by R = real(clkInFrequency) / real(targetBaseFrequency) per target clock cycle,
  -- FIXME irrelevant: so 480 * R per line, which is 480 R / 2^couterBits cycles =  2 Pi 480 R / 2^counterBits radians = 0.027 radians
  --
  -- The DCO counts its nominal rate using "nominalAdd" computed as 2^counterBits * (targetFrequency / clkInFrequency).
  -- Also, an adjustment is added once per output cycle. The adjustment is computed as phaseError / invPcoeff + \int(phaseError) / invIcoeff.
  -- The phase error is updated when phaseErrorSrcEn is set, and the integrator is updated at the same time.
  --
  -- A positive phase error makes the oscillator speed up.
  --
  -- clkOut can be different from targetFrequency if outputMultF and outputDivF are applied.
  generic (
    clkInFrequency: natural; -- higher yields lower jitter
    targetBaseFrequency: natural; -- keep as low as possible (down to 10 MHz allowed) for smallest jitter
    outputMultF: real := 1.0; -- used to scale the output clock frequency (2.000-64.000)
    outputDivF: real := 1.0;
    counterBits: natural := 16;
    phaseErrorBits: natural;
    errorSumBits: natural := 14; -- it is usually good to have this so we can represent +/- maxAdjust * invIcoeff
    invPcoeff: natural := 4;
    invIcoeff: natural := 128
  );
  port ( 
    clkIn: in std_logic;
    clkOut: out std_logic;
    phaseErrorSrcEn: in std_logic; -- these inputs should be driven using the clkOut clock
    phaseErrorSrc: in signed(phaseErrorBits - 1 downto 0);
    restartSrc: in std_logic; -- FIXME: if the source has a faster clock than clkIn we could miss this
    errorSumDebug: out signed(errorSumBits - 1 downto 0) -- synchronized with clkIn
  );
end dpll;

architecture rtl of dpll is
  constant targetPeriodNs: real := 1.0e9 / real(targetBaseFrequency);
  signal clkGen: std_logic := '0';
  signal clkGenBuffered: std_logic;
  signal clkFeedback: std_logic;
  signal mmcmLocked: std_logic;

  constant nominalAdd: natural := integer(real(2 ** counterBits) * real(targetBaseFrequency) / real(clkInFrequency));
  signal dpllAdjust: signed(errorSumBits - 1 downto 0) := to_signed(0, errorSumBits);
  signal dpllCounter: unsigned(counterBits - 1 downto 0) := to_unsigned(0, counterBits);
 
  signal phaseErrorDstEn: std_logic;
  signal phaseErrorDst: std_logic_vector(phaseErrorBits - 1 downto 0);
  signal error: signed(phaseErrorBits - 1 downto 0) := to_signed(0, phaseErrorBits);
  signal errorSum: signed(errorSumBits - 1 downto 0) := to_signed(0, errorSumBits);  
 
  signal restartDst: std_logic;
begin
  errorSumDebug <= errorSum;

  process (clkIn)
  begin
    if rising_edge(clkIn) then
      clkGen <= dpllCounter(counterBits - 1);
      dpllCounter <= dpllCounter + nominalAdd + unsigned(resize(dpllAdjust, counterBits));
      
      if restartDst = '1' then
        error <= to_signed(0, phaseErrorBits);
        errorSum <= to_signed(0, errorSumBits);
      elsif phaseErrorDstEn = '1' then
        error <= signed(phaseErrorDst);
        errorSum <= errorSum + error;
      end if;
      dpllAdjust <= to_signed(to_integer(error) / invPcoeff + to_integer(errorSum) / invIcoeff, errorSumBits);
    end if;
  end process;

  clkBuffer: BUFR port map (I => clkGen, O => clkGenBuffered, CE => '1', CLR => '0');

  MMCME2_BASE_inst: MMCME2_BASE generic map (
    BANDWIDTH => "LOW",
    CLKFBOUT_MULT_F => outputMultF,
    CLKIN1_PERIOD => targetPeriodNs,
    CLKOUT0_DIVIDE_F => outputDivF,
    DIVCLK_DIVIDE => 1, -- Master division value (1-106)
    REF_JITTER1 => 0.99
  )
  port map (
    CLKIN1 => clkGenBuffered,
    CLKFBOUT => clkFeedback,
    CLKFBIN => clkFeedback,
    CLKOUT0 => clkOut,
    LOCKED => mmcmLocked,
    RST => '0',
    PWRDWN => '0'
  );

  pllSync: entity work.multiBitSynchronizer generic map (width => 8)
  port map (
    srcClk => clkOut,
    dataInEn => phaseErrorSrcEn,
    dataIn => std_logic_vector(phaseErrorSrc),
    busy => open, -- we know we write rarely enough
    dstClk => clkIn,
    dataOutEn => phaseErrorDstEn,
    dataOut => phaseErrorDst
  );
  
  restartSync: entity work.bitSynchronizer
  port map (
    dataIn => restartSrc,
    dstClk => clkIn,
    dataOut => restartDst
  );    
end rtl;
