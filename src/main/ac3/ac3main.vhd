library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.reedSolomon_types.all;

entity ac3main is
  port (
    qpskClk: in std_logic;
    ac3In: in std_logic; -- 1 bit ac3rf input (after band pass filtering)
    tx_spdif: out std_logic;
    frameMissing: out std_logic;
    frameNoError: out std_logic;
    c1Enable: in std_logic;
    c2Enable: in std_logic;
    c1Done: out std_logic;
    c2Done: out std_logic;
    c1Status: out CompletionStatus;
    c2Status: out CompletionStatus;
    debugPins: out std_logic_vector(3 downto 0);
    dpllErrorSumDebug: out signed(19 downto 0)
  );
end ac3main;

architecture a1 of ac3main is
  signal ac3InSync: std_logic; -- synchronized
  signal ac3SymbolSignal: std_logic_vector(1 downto 0); -- the output from ac3In
  signal ac3SymbolEn: std_logic;
  signal ac3Symbol: std_logic_vector(1 downto 0); -- the symbol reclocked and read at the middle of symbols
  signal ac3FrameNoEn: std_logic;
  signal ac3FrameNo: unsigned(6 downto 0);
  signal ac3FrameDataEn: std_logic;
  signal ac3FrameData: std_logic_vector(7 downto 0);

  signal ac3WrEn: std_logic;
  signal ac3WrBlockSel: natural range 0 to 1;
  signal ac3WrAddr: natural range 0 to 1535;
  signal ac3WrData: std_logic_vector(7 downto 0);
  signal ac3RdBlockSel: natural range 0 to 1;
  signal ac3RdAddr: natural range 0 to 1535;
  signal ac3RdData: std_logic_vector(7 downto 0);
  signal ac3BlockReady: std_logic;
  signal ac3ReadyBlockIndex: natural range 0 to 1;
  
  signal spdifSending: std_logic;
begin
  ac3InputSync: entity work.bitSynchronizer port map (
    dataIn => ac3In,
    dstClk => qpskClk,
    dataOut => ac3InSync
  );
  
  qpskIn: entity work.qpskIn port map (
    clk => qpskClk,
    dataIn => ac3InSync,
    dataOut => ac3SymbolSignal
  );
  
  qpskReclocker: entity work.qpskReclock port map (
    clk => qpskClk,
    dataIn => ac3SymbolSignal,
    outEn => ac3SymbolEn,
    dataOut => ac3Symbol,
    errorSumDebug => dpllErrorSumDebug
  );

  ac3Framing: entity work.ac3InputFraming port map (
    clk => qpskClk,
    inEn => ac3SymbolEn,
    dataIn => ac3Symbol,
    frameNoOutEn => ac3FrameNoEn,
    frameNoOut => ac3FrameNo,
    outEn => ac3FrameDataEn,
    dataOut => ac3FrameData
  );

  ac3BlocksOut: entity work.ac3BlockBuffer port map (
    clk => qpskClk,
    wrEn => ac3WrEn,
    rdBlockSel => ac3RdBlockSel,
    rdAddr => ac3RdAddr,
    rdData => ac3RdData,
    wrBlockSel => ac3WrBlockSel,
    wrAddr => ac3WrAddr,
    wrData => ac3WrData
  );

--  debugPins(0) <= ac3WrEn;
--  debugPins(1) <= '1' when ac3ReadyBlockIndex = 1 else '0';
--  debugPins(2) <= spdifSending;
--  debugPins(3) <= '0';
  debugPins(0) <= ac3SymbolEn;
  debugPins(1) <= ac3SymbolSignal(0);
  debugPins(2) <= ac3SymbolSignal(1);
  debugPins(3) <= qpskClk;
  
  ac3Deinterleaver: entity work.ac3Deinterleave port map (
    clk => qpskClk,
    frameNoInEn => ac3FrameNoEn,
    frameNoIn => ac3FrameNo,
    frameNoError => frameNoError,
    inEn => ac3FrameDataEn,
    dataIn => ac3FrameData,
    c1Enable => c1Enable,
    c2Enable => c2Enable,
    c1Done => c1Done,
    c2Done => c2Done,
    c1status => c1Status,
    c2status => c2Status,
    ac3WrEn => ac3WrEn,
    ac3WrBlockSel => ac3WrBlockSel,
    ac3WrAddr => ac3WrAddr,
    ac3WrData => ac3WrData,
    ac3BlockReady => ac3BlockReady,
    ac3ReadyBlockIndex => ac3ReadyBlockIndex
  );
  
  spdif: entity work.ac3spdif port map (
    clk => qpskClk,
    tx_spdif => tx_spdif,
    ac3BlockReady => ac3BlockReady,
    ac3ReadyBlockIndex => ac3ReadyBlockIndex,
    ac3RdBlockSel => ac3RdBlockSel,
    ac3RdAddr => ac3RdAddr,
    ac3RdData => ac3RdData,
    outputActive => spdifSending
  );
end architecture a1;
