library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- samples the digital input and outputs two data bits. clk is assumed to be at 2.88 * 16 MHz.
-- The output should stay stable in the middle of symbols.

entity qpskIn is
  port (
    clk: in std_logic;
    dataIn: in std_logic;
    dataOut: out std_logic_vector(1 downto 0)
  );
end qpskIn;

architecture a1 of qpskIn is
  constant compareIntervalSize: positive := 32;
  constant shiftRegSize: positive := 16 * 10 + 4 + compareIntervalSize;
  signal shiftReg: std_logic_vector(shiftRegSize - 1 downto 0); -- FIXME: adjust upper limit to what we use
  
  function multiplySumAtShift(shiftReg: std_logic_vector(shiftRegSize - 1 downto 0); shift: positive) return integer is
    variable sum: integer range -compareIntervalSize to compareIntervalSize := 0;
    variable v: integer range -1 to 1;
  begin
    for i in 0 to compareIntervalSize - 1 loop
      v := -1 when (shiftReg(i) xor shiftReg(shift + i)) = '1' else 1;
      sum := sum + v;
    end loop;
    return sum;
  end multiplySumAtShift;

begin
  process (clk)
  begin
    if rising_edge(clk) then
      shiftReg(shiftRegSize - 1 downto 1) <= shiftReg(shiftRegSize - 2 downto 0);
      shiftReg(0) <= dataIn;
    end if;   
  end process;
    
  process (clk)
    variable sum0: integer range -compareIntervalSize to compareIntervalSize;
    variable sum1: integer range -compareIntervalSize to compareIntervalSize;
    variable sum2: integer range -compareIntervalSize to compareIntervalSize;
    variable sum3: integer range -compareIntervalSize to compareIntervalSize;
    variable diff02: integer range -2 * compareIntervalSize to 2 * compareIntervalSize;
    variable diff13: integer range -2 * compareIntervalSize to 2 * compareIntervalSize;
  begin
    if rising_edge(clk) then
      sum0 := multiplySumAtShift(shiftReg, 16 * 10);
      sum1 := multiplySumAtShift(shiftReg, 16 * 10 + 4);
      sum2 := multiplySumAtShift(shiftReg, 16 * 10 - 8);
      sum3 := multiplySumAtShift(shiftReg, 16 * 10 - 4);
      diff02 := sum0 - sum2;
      diff13 := sum1 - sum3;
      
      if abs(diff02) > abs(diff13) then
        dataOut <= "00" when diff02 > 0 else "11";
      else
        dataOut <= "01" when diff13 > 0 else "10";
      end if;
    end if;
  end process;
end a1;
