library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- two 36x66 byte buffers for deinterleaving incoming ac3 data with separate
-- read and write addresses.  The data is 9 bits wide and represents an erasure flag
-- and the 8 bit data.

entity ac3DeinterleaveBuffer is
  port (
    clk: in std_logic;
    wrEn: in std_logic;

    rdBlockSel: in natural range 0 to 1;
    rdRowSel: in natural range 0 to 35;
    rdColSel: in natural range 0 to 65;
    rdData: out std_logic_vector(8 downto 0);

    wrBlockSel: in natural range 0 to 1;
    wrRowSel: in natural range 0 to 35;
    wrColSel: in natural range 0 to 65;
    wrData: in std_logic_vector(8 downto 0)
  );
end ac3DeinterleaveBuffer;

architecture a1 of ac3DeinterleaveBuffer is
  type RamType is array (0 to 2 * 36 * 66 - 1) of std_logic_vector(7 downto 0);
  signal ram: RamType;
  attribute ram_style : string;
  attribute ram_style of ram: signal is "block";

  signal erasureFlags: std_logic_vector(0 to 2 * 36 - 1);
  
  signal rdAddress: natural range 0 to 2 * 36 * 66 - 1;
begin
  rdData <= erasureFlags(rdAddress / 74 + rdAddress mod 2) & ram(rdAddress);

  process(clk)
    variable wrAddress: natural range 0 to 2 * 36 * 66 - 1;
  begin
    if rising_edge(clk) then
      rdAddress <= rdBlockSel * 36 * 66 + rdRowSel * 66 + rdColSel;
      wrAddress := wrBlockSel * 36 * 66 + wrRowSel * 66 + wrColSel;
      if wrEn = '1' then
        ram(wrAddress) <= wrData(7 downto 0);
        erasureFlags(wrAddress / 74 + wrAddress mod 2) <= wrData(8);
      end if;
    end if;
  end process;
end a1;
