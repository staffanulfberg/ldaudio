library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity ac3spdif is
  port (
    -- 46.08 MHz  We divide this by 15 to get the 3.072 MHz (= 48k * 64) bit clock
    clk: in std_logic;
    tx_spdif: out std_logic;
    
    ac3BlockReady: in std_logic;
    ac3ReadyBlockIndex: in natural range 0 to 1;
    ac3RdBlockSel: out natural range 0 to 1;
    ac3RdAddr: out natural range 0 to 1535;
    ac3RdData: in std_logic_vector(7 downto 0);
    outputActive: out std_logic
  );
end ac3spdif;

architecture rtl of ac3spdif is
  signal divideCounter: natural range 0 to 14 := 0;
  signal counter: unsigned(5 downto 0) := to_unsigned(0, 6); -- 64 counts per frame, i.e., 1 count per bit)
  signal waitUntilNextBlock: natural range 0 to 1150 := 0; -- nominally, we need to wait (32 * 5 - 8) / 4 = 32 frames between blocks 
  
  type Ac3PreambleArray is array(0 to 7) of std_logic_vector(7 downto 0);
  constant Ac3BlockPreamble: Ac3PreambleArray := (x"f8", x"72", x"4e", x"1f", x"00", x"01", x"30", x"00");

  -- InvertAtStart and InvertTwice are used for '0' and '1'.  The others for sync bits,
  type OutputSignal is (InvertAtStart, InvertTwice, NoInvert, InvertInMiddle);
  type OutputArray is array(0 to 3) of OutputSignal; -- used for preambles
  signal output: OutputSignal := InvertTwice;
  
  -- signals to generate the output clock, which is twice the frequency of the bit clock
  -- this is a bit tricky since it means dividing the input clock by 7.5
  signal ffa: std_logic := '0';
  signal ffb: std_logic := '0';
  signal outputClk: std_logic;
  type ClkMuxSelections is (Low, High, OutClk, InvOutClk);
  signal clkMuxSeleciton: ClkMuxSelections;

  signal rightBuf: std_logic_vector(15 downto 0);
  signal outReg: std_logic_vector(15 downto 0);
begin
  tx_spdif <= '0' when clkMuxSeleciton = Low 
    else '1' when clkMuxSeleciton = High
    else outputClk when clkMuxSeleciton = OutClk
    else not outputClk when clkMuxSeleciton = InvOutClk;

  process (clk)
    variable nextCtr: natural range 0 to 14;
  begin
    if rising_edge(clk) then
      if divideCounter < 14 then
        divideCounter <= divideCounter + 1;
      else
        divideCounter <= 0;
        counter <= counter + 1;
        ffa <= not ffa;
      end if;
    end if;
    if falling_edge(clk) then
      if divideCounter = 7 then
        ffb <= not ffb;
      end if;
    end if;
  end process;

  outputClk <= ffa xor ffb;

  -- Sets outReg used by the next process.  It is set when counter=63,
  -- and is first used by the next process when divideCounter=11 (also when counter=63)
  process (clk)
    variable i: natural range 0 to 384 + 2 := 386; -- counts sent frames: preamble + 384 => 8 + 1536 bytes
    variable readState: natural range 0 to 6 := 6;
    variable hasWaitingBlock: boolean := false;
    variable waitingBlockIndex: natural range 0 to 1;
  begin
    if rising_edge(clk) then
      outputActive <= '1' when i < 386 else '0';
    
      if i = 386 and counter = 63 and divideCounter = 0 and waitUntilNextBlock > 0 then
        waitUntilNextBlock <= waitUntilNextBlock - 1;
      end if;
      if i = 386 and hasWaitingBlock and waitUntilNextBlock = 0 then
        i := 0;
        ac3RdBlockSel <= waitingBlockIndex;
        hasWaitingBlock := false;
      end if;
      if ac3BlockReady then
        if i = 386 and waitUntilNextBlock = 0 then     
          i := 0;
          ac3RdBlockSel <= ac3ReadyBlockIndex;
        elsif hasWaitingBlock then
          waitUntilNextBlock <= 0;
          i := 0;
          ac3RdBlockSel <= waitingBlockIndex;
          waitingBlockIndex := ac3ReadyBlockIndex;
        else
          hasWaitingBlock := true;
          waitingBlockIndex := ac3ReadyBlockIndex;
        end if;
      end if;

      -- TODO: remove rightBuf and read at 31 and 63
      if counter = 63 and divideCounter = 0 then -- time for the next frame`
        if i = 386 then
          outReg <= x"0000";
          rightBuf <= x"0000";
        elsif i < 2 then
          outReg <= Ac3BlockPreamble(i * 4) & Ac3BlockPreamble(i * 4 + 1);
          rightBuf <= Ac3BlockPreamble(i * 4 + 2) & Ac3BlockPreamble(i * 4 + 3);
          i := i + 1;
        else
          readState := 0;
        end if;
      end if;
      
      case readState is
        when 0 => 
          ac3RdAddr <= (i - 2) * 4;
          readState := readState + 1;
        when 1 =>
          ac3RdAddr <= (i - 2) * 4 + 1;
          readState := readState + 1;
        when 2 =>
          outReg(15 downto 8) <= ac3RdData;
          ac3RdAddr <= (i - 2) * 4 + 2;
          readState := readState + 1;
        when 3 =>
          outReg(7 downto 0) <= ac3RdData;
          ac3RdAddr <= (i - 2) * 4 + 3;
          readState := readState + 1;
        when 4 =>
          rightBuf(15 downto 8) <= ac3RdData;
          readState := readState + 1;
        when 5 =>
          rightBuf(7 downto 0) <= ac3RdData;
          readState := readState + 1;
          i := i + 1;
          if i = 386 then
            waitUntilNextBlock <= 1150;
          end if;
        when 6 => -- done
      end case;
      
      if counter = 31 and divideCounter = 0 then
        outReg <= rightBuf;
      end if;
    end if;
  end process;
  
  -- Uses outReg from the previous process.  It is read for the next bit when divideCounter=11.
  process(clk)
    constant preambleB: OutputArray := (InvertAtStart, InvertInMiddle, InvertTwice, NoInvert); -- 11 10 10 00
    constant preambleM: OutputArray := (InvertAtStart, InvertInMiddle, NoInvert, InvertTwice); -- 11 10 00 10
    constant preambleW: OutputArray := (InvertAtStart, InvertInMiddle, InvertInMiddle, InvertAtStart); -- 11 10 01 00
    variable preamble: OutputArray;
    variable nextCounter: unsigned(5 downto 0);
    variable frameCounter: natural range 0 to 191 := 191;
    variable subframe: std_logic;
    variable bitCounter: integer range 0 to 31;
    variable par: std_logic;
    variable b: std_logic; -- the bit value to output; `output` is then set depending on the last output 
  begin
    -- we pre compute the next bit to output
    if rising_edge(clk) then
      if divideCounter = 11 then -- output will be valid at 13 and 5 (when it is read to update the output mux)
        nextCounter := counter + 1;
        if nextCounter = 0 then
          frameCounter := frameCounter + 1 when frameCounter /= 191 else 0;
          par := '0';
        end if;
        subframe := nextCounter(5); -- next subframe
        bitCounter := to_integer(nextCounter(4 downto 0)); -- 0 .. 31
        
        case bitCounter is
          when 0 to 3 => 
            preamble := preambleW when subframe /= '0' else preambleB when frameCounter = 0 else preambleM;
            output <= preamble(bitCounter);
          when others => 
            case bitCounter is
              when 0 to 3 =>
              when 4 to 11 => b := '0';
              when 12 to 27 => b := outreg(bitCounter - 12);
              when 28 => b := '1'; -- dataValid; 1 means invalid and is used to prevent accidental playback as pcm
              when 29 => b := '0'; -- subcode data
              when 30 => b := '1' when frameCounter = 1 else '0';
              when 31 => b := par;
            end case;
            par := par xor b;
            output <= InvertAtStart when b = '0' else InvertTwice;
        end case;
      end if;
    
      if divideCounter = 13 then
        if output = InvertInMiddle or output = NoInvert then
          clkMuxSeleciton <= Low when tx_spdif = '0' else High;
        else -- invert at 0
          clkMuxSeleciton <= OutClk when tx_spdif xor not outputClk else InvOutClk;
        end if;
      elsif divideCounter = 5 then
        if output = InvertAtStart or output = NoInvert then
          clkMuxSeleciton <= Low when tx_spdif = '0' else High;
        else -- invert at 7.5
          clkMuxSeleciton <= OutClk when tx_spdif xor not outputClk else InvOutClk;
        end if;
      end if;      
    end if;
  end process;
end rtl;
