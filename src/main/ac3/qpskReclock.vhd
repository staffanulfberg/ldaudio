library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Processes the output from qpskIn (at approx 16*2.88 MHz) and generates one output symbol 
-- approximately every 160 clock cycles using a DPLL to lock on the center of symbols.

entity qpskReclock is
  port (
    clk: in std_logic;
    dataIn: in std_logic_vector(1 downto 0);
    outEn: out std_logic;
    dataOut: out std_logic_vector(1 downto 0);
    errorSumDebug: out signed(19 downto 0)
  );
end qpskReclock;

architecture a1 of qpskReclock is
  constant counterBits: positive := 16;
  signal clkCounter: signed(counterBits - 1 downto 0) := to_signed(0, counterBits);
  
  -- makes our counter wrap approx every 160 additions
  constant nominalAdd: signed(counterBits - 1 downto 0) := to_signed(410, counterBits);

  signal lastIn: std_logic_vector(1 downto 0) := "00";
  
  type FilterArithmeticStage is (Inactive, KnownError, KnownErrorSum);
  signal arithmeticStage: FilterArithmeticStage := Inactive;
  signal error: signed(counterBits - 1 downto 0);
  signal errorSum: signed(19 downto 0) := (others => '0'); 
  signal filterOut: signed(counterBits - 1 downto 0) := to_signed(0, counterBits);
begin
  process(clk)  
    variable toggleCount: integer range 0 to 127;
    variable firstTogglePos: signed(counterBits - 1 downto 0);
    variable lastTogglePos: signed(counterBits - 1 downto 0);
    variable newCounter: signed(counterBits - 1 downto 0) := to_signed(0, counterBits);
    variable filterNow: signed(counterBits - 1 downto 0);
  begin
    if rising_edge(clk) then
      if filterOut < -nominalAdd then
        filterOut  <= filterout + nominalAdd;
        filterNow := -nominalAdd;
      else
        filterNow := filterOut;
        filterOut <= to_signed(0, counterBits);
      end if;
    
      newCounter := clkCounter + nominalAdd + filterNow;
      clkCounter <= newCounter;
      
      if dataIn /= lastIn then
        toggleCount := toggleCount + 1;
        if toggleCount = 1 then
          firstTogglePos := clkCounter;
        end if;       
        lastTogglePos := clkCounter;
        lastIn <= dataIn;
      end if;
      
      outEn <= '0';
      if newCounter < clkCounter then 
        outEn <= '1';
        dataOut <= lastIn;
        
        if toggleCount /= 0 then
          error <= to_signed(-(to_integer(firstTogglePos) + to_integer(lastTogglePos)) / 2, counterBits);
        else
          error <= to_signed(0, counterBits); -- leave errorSum unchanged
        end if;
        arithmeticStage <= KnownError;
        
        toggleCount := 0;
      end if;
      
      case arithmeticStage is
        when Inactive =>
        when KnownError =>
          errorSum <= errorSum + error;
          arithmeticStage <= KnownErrorSum;
        when KnownErrorSum =>
          errorSumDebug <= errorSum;
          filterOut <= error / 16 + to_signed(to_integer(errorSum(19 downto 9)), counterBits);
          arithmeticStage <= Inactive;
      end case;
    end if;
  end process;
end a1;
