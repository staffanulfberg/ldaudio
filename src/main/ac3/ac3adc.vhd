library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity ac3adc is
  port ( 
    clk: in std_logic;
    dataIn: in signed(7 downto 0); -- from A/D converter
    dataOut: out std_logic -- after filtering and clipping
  );
end ac3adc;

architecture behavioral of ac3adc is
  signal filteredData: signed(7 downto 0) := to_signed(0, 8);
  signal avg: signed(17 downto 0) := to_signed(0, 18); -- the average value scaled by 1024
begin

  fir: entity work.fir generic map (
    coefficients => (
        to_signed(-6, 8), to_signed(70, 8), to_signed(11, 8), to_signed(4, 8), to_signed(-1, 8), to_signed(-6, 8), to_signed(-12, 8), to_signed(-15, 8), 
        to_signed(-17, 8), to_signed(-15, 8), to_signed(-9, 8), to_signed(-1, 8), to_signed(9, 8), to_signed(20, 8), to_signed(29, 8), to_signed(35, 8), 
        to_signed(35, 8), to_signed(30, 8), to_signed(20, 8), to_signed(4, 8), to_signed(-13, 8), to_signed(-31, 8), to_signed(-45, 8), to_signed(-54, 8), 
        to_signed(-54, 8), to_signed(-47, 8), to_signed(-31, 8), to_signed(-9, 8), to_signed(15, 8), to_signed(40, 8), to_signed(59, 8), to_signed(71, 8), 
        to_signed(72, 8), to_signed(62, 8), to_signed(41, 8), to_signed(14, 8), to_signed(-18, 8), to_signed(-48, 8), to_signed(-72, 8), to_signed(-86, 8), 
        to_signed(-87, 8), to_signed(-74, 8), to_signed(-50, 8), to_signed(-16, 8), to_signed(21, 8), to_signed(56, 8), to_signed(84, 8), to_signed(99, 8), 
        to_signed(100, 8), to_signed(85, 8), to_signed(57, 8), to_signed(19, 8), to_signed(-23, 8), to_signed(-62, 8), to_signed(-92, 8), to_signed(-108, 8), 
        to_signed(-109, 8), to_signed(-92, 8), to_signed(-62, 8), to_signed(-21, 8), to_signed(23, 8), to_signed(64, 8), to_signed(95, 8), to_signed(112, 8), 
        to_signed(112, 8), to_signed(95, 8), to_signed(64, 8), to_signed(23, 8), to_signed(-21, 8), to_signed(-62, 8), to_signed(-92, 8), to_signed(-109, 8), 
        to_signed(-108, 8), to_signed(-92, 8), to_signed(-62, 8), to_signed(-23, 8), to_signed(19, 8), to_signed(57, 8), to_signed(85, 8), to_signed(100, 8), 
        to_signed(99, 8), to_signed(84, 8), to_signed(56, 8), to_signed(21, 8), to_signed(-16, 8), to_signed(-50, 8), to_signed(-74, 8), to_signed(-87, 8), 
        to_signed(-86, 8), to_signed(-72, 8), to_signed(-48, 8), to_signed(-18, 8), to_signed(14, 8), to_signed(41, 8), to_signed(62, 8), to_signed(72, 8), 
        to_signed(71, 8), to_signed(59, 8), to_signed(40, 8), to_signed(15, 8), to_signed(-9, 8), to_signed(-31, 8), to_signed(-47, 8), to_signed(-54, 8), 
        to_signed(-54, 8), to_signed(-45, 8), to_signed(-31, 8), to_signed(-13, 8), to_signed(4, 8), to_signed(20, 8), to_signed(30, 8), to_signed(35, 8), 
        to_signed(35, 8), to_signed(29, 8), to_signed(20, 8), to_signed(9, 8), to_signed(-1, 8), to_signed(-9, 8), to_signed(-15, 8), to_signed(-17, 8), 
        to_signed(-15, 8), to_signed(-12, 8), to_signed(-6, 8), to_signed(-1, 8), to_signed(4, 8), to_signed(11, 8), to_signed(70, 8), to_signed(-6, 8))  
  ) port map (
    clk => clk,
    dataIn => dataIn,
    dataOut => filteredData
  );

  process(clk)
  begin
    if rising_edge(clk) then
      -- avg <= avg * 1023/1024 + filteredData/1024
      avg <= avg - resize(avg(17 downto 10), 18) + filteredData;
      if filteredData > avg(17 downto 10) then
        dataOut <= '1';
      else
        dataOut <= '0';
      end if;
    end if;
  end process;
end behavioral;
