library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;

package fir_data_types is
  type ArrayOfSigned is array(natural range <>) of signed(7 downto 0);
end package;

library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;
use work.fir_data_types.all;
use ieee.math_real.log2;

entity fir is
  generic (
    -- length needs to be power of two!
    coefficients: ArrayOfSigned -- first coefficient (index 0) will be multiplied by the
                                -- oldest data
  );
  port (
    clk: in std_logic;
    dataIn: in signed(7 downto 0);
    dataOut: out signed(7 downto 0)
  );
end fir;

architecture a1 of fir is
  -- index 0 holds the oldest data
  constant size: positive := coefficients'length;
  constant noAddStages: integer := integer(log2(real(size)));
  
  signal shiftReg: ArrayOfSigned(0 to size - 1) := (others => to_signed(0, 8));

  type ProdsType is array(0 to size - 1) of signed(15 downto 0);
  signal prods: ProdsType := (others => to_signed(0, 16));

  -- We need size - 1 adder destinations. The first size/2 are used for the
  -- first stage, etc.  We make them all the same size for simplicity.
  type SumsType is array(0 to size - 2) of signed(15 + noAddStages downto 0);
  signal sums: SumsType := (others => to_signed(0, 16 + noAddStages));

  signal output: signed(7 downto 0) := to_signed(0, 8);

  function sumStartOffset(stage: integer; totalSize: integer) return integer is
    variable ret: integer := 0;
    variable stageSize: integer := totalSize / 2;
  begin
    for i in 2 to stage loop
      ret := ret + stageSize;
      stageSize := stageSize / 2;       
    end loop;   
    return ret; 
  end function; 

begin
  process(clk)
  begin
    if rising_edge(clk) then
      shiftReg <= shiftReg(1 to size - 1) & dataIn;      
    end if;
  end process;

  multiplication: for i in 0 to coefficients'length - 1 generate
    process(clk)
    begin
      if rising_edge(clk) then
        prods(i) <= shiftReg(i) * coefficients(i);
      end if;
    end process;
  end generate;

  firstStageAdder: for i in 0 to size / 2 - 1 generate
    process(clk)
    begin
      if rising_edge(clk) then
        sums(i) <= resize(prods(2 * i), 16 + noAddStages) + resize(prods(2 * i + 1), noAddStages);
      end if;
    end process;
  end generate;

  addition: for stage in 2 to noAddStages generate
    adder: for i in 0 to size / (2 ** stage) - 1 generate
      process(clk)
      begin
        if rising_edge(clk) then
          sums(sumStartOffset(stage, size) + i) <= sums(sumStartOffset(stage - 1, size) + 2 * i) + sums(sumStartOffset(stage - 1, size) + 2 * i + 1);
        end if;
      end process;
    end generate;
  end generate;

  process(clk)
  begin
    if rising_edge(clk) then
      output <= sums(size - 2)(15 downto 8);
    end if;
  end process;
  
  dataOut <= output;
end a1;
