library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.reedSolomon_types.all;

-- processes the output from ac3Framing and deinterleaves the data

entity ac3Deinterleave is
  port (
    clk: in std_logic;
    frameNoInEn: in std_logic;
    frameNoIn: in unsigned(6 downto 0); -- 0..71
    inEn: in std_logic;
    dataIn: in std_logic_vector(7 downto 0);
    frameMissing: out std_logic;
    frameNoError: out std_logic;
    c1Enable: in std_logic;
    c2Enable: in std_logic;
    c1Done: out std_logic; -- set for one cycle after RS computation; rsStatus is valid
    c2Done: out std_logic;
    c1Status: out CompletionStatus;
    c2Status: out CompletionStatus;
    ac3WrEn: out std_logic;
    ac3WrBlockSel: out natural range 0 to 1;
    ac3WrAddr: out natural range 0 to 1535;
    ac3WrData: out std_logic_vector(7 downto 0);
    ac3BlockReady: out std_logic := '0';
    ac3ReadyBlockIndex: out natural range 0 to 1
  );
end ac3Deinterleave;

architecture a1 of ac3Deinterleave is
  type RsStateType is (Idle, CopyingToRs, WaitingForRs, CopyingFromRs);

  signal expectedFrameNo: unsigned(6 downto 0) := to_unsigned(0, 7);
  signal consecutiveInSequence: natural range 0 to 7 := 0;
  signal nextFrameCountdown: natural range 0 to 160 * 160 := 0; -- 25600

  signal memWrEn: std_logic;
  signal memWrBlockSel: natural range 0 to 1 := 0;
  signal memWrRowSel: natural range 0 to 35;
  signal memWrColSel: natural range 0 to 73;
  signal memWrData: std_logic_vector(8 downto 0);
  signal memRdBlockSel: natural range 0 to 1;
  signal memRdRowSel: natural range 0 to 35;
  signal memRdColSel: natural range 0 to 73;
  signal memRdData: std_logic_vector(8 downto 0);

  signal inputIx: natural range 0 to 37; -- 37 means we are idle; otherwise we are copying from dataIn to the buffer

  signal c1InputBuffer: ByteArray(0 to 147); -- keeps two consecutive lines of input for c1 processing, i.e., 4 frames
  signal c1InputBufferLineFilled: std_logic := '0'; -- set when a complete line is in the input buffer for one cycle
  signal c1InputBufferLastSOL: natural range 0 to 71; -- set when c1InputBufferLineFilled is set to the frame no of first of the two new frames available

  signal cx_inEn: std_logic;
  signal cx_in: std_logic_vector(7 downto 0);
  signal cx_outEn: std_logic;
  signal cx_out: std_logic_vector(7 downto 0);
  signal cxStart: std_logic := '0';
  signal cx_erasedIn: std_logic;
  signal cxFrameSize: natural range 36 to 37;
  signal cxEnable: std_logic;
  signal cxDone: std_logic;
  signal cxStatus: CompletionStatus;

  signal c1InputReadIx: natural range 0 to 74 := 74; -- 0..36 reading first c1 word, 37..73 reading second c1 word, 74 idle
  signal c1State: RsStateType := Idle;
  signal c1CopyOutIx: natural range 0 to 37; -- 37 means we are idle
  signal blockComplete: std_logic := '0'; -- a complete block has been written to the deinterleave matrix so we can start c2 processing
  
  signal c2CodewordIndex: natural range 0 to 66 := 66; -- 66 means we are done
  signal c2State: RsStateType := Idle;
  signal i: natural range 0 to 37 := 37; -- state for copying: the read range is 0 to 35, but we have two more states due to memory latency

  signal c2CodewordDone: std_logic := '0';
  signal c2Codeword: ByteArray(0 to 31); -- output from RS is copied here

  signal savedWhileScanning: ByteArray(0 to 4);
  signal savedCopyIx: natural range 0 to 5 := 5;
  signal scanIx: natural range 0 to 32 := 32;
  signal ac3BlockIx: natural range 0 to 1 := 0;
  signal ac3Ix: natural range 0 to 1536;
  type ScanStateType is (Searching, Found1, Found2, Found2plus1, Found2plus2, Found3, InBlock);
  signal scanState: ScanStateType;
    
begin
  deinterleaveBuffer: entity work.ac3DeinterleaveBuffer port map (
    clk => clk,
    wrEn => memWrEn,
    wrBlockSel => memWrBlockSel,
    wrRowSel => memWrRowSel,
    wrColSel => memWrColSel,
    wrData => memWrData,
    rdBlockSel => memRdBlockSel,
    rdRowSel => memRdRowSel,
    rdColSel => memRdColSel,
    rdData => memRdData
  );
  
  process(clk)
    variable useFrame: boolean;
    variable usedFrameNo: natural range 0 to 71;
  begin
    if rising_edge(clk) then
      c1InputBufferLineFilled <= '0';
      nextFrameCountdown <= nextFrameCountdown - 1 when nextFrameCountdown /= 0 else 0;
      frameMissing <= '1' when nextFrameCountdown = 0 else '0';
      if frameNoInEn = '1' then
        nextFrameCountdown <= 160 * 160;
        if frameNoIn /= expectedFrameNo then
          frameNoError <= '1';
          if consecutiveInSequence = 0 then
            useFrame := false;
          else
            useFrame := true;
            usedFrameNo := to_integer(expectedFrameNo);
          end if;
          consecutiveInSequence <= 0 when consecutiveInSequence = 0 else consecutiveInSequence - 1;
        else
          frameNoError <= '0';
          useFrame := true;
          usedFrameNo := to_integer(frameNoIn);
          consecutiveInSequence <= 7 when consecutiveInSequence = 7 else consecutiveInSequence + 1;
        end if;
      
        if useFrame then
          inputIx <= 0; -- start copying into buffer
          expectedFrameNo <= to_unsigned(0, 7) when usedFrameNo = 71 else to_unsigned(usedFrameNo + 1, 7);
        else
          expectedFrameNo <= to_unsigned(0, 7);
        end if;
      end if;
      if inEn = '1' and inputIx < 37 then
        c1InputBuffer((usedFrameNo mod 4) * 37 + inputIx) <= dataIn;
        if usedFrameNo mod 2 = 1 and inputIx = 36 then
          c1InputBufferLastSOL <= usedFrameNo / 2 * 2;
          c1InputBufferLineFilled <= '1';
        end if;
        inputIx <= inputIx + 1;
      end if;
    end if;
  end process;

  Cx: entity work.reedSolomon
    generic map (
      poly => 9x"187",
      fcr => 120,
      maxFrameSize => 37)
    port map (
      frameSize => cxFrameSize,
      clk => clk,
      enabled => cxEnable,
      start => cxStart,
      inEn => cx_inEn,
      erasedIn => cx_erasedIn,
      dataIn => cx_in,
      done => cxDone,
      status => cxStatus,
      outEn => cx_outEn,
      dataOut => cx_out
    );

  process(clk)
    variable readIx: natural range 0 to 73;
    variable erasure: std_logic;
  begin
    if rising_edge(clk) then
      cxStart <= '0';
      cx_inEn <= '0';

      -- C1 processing

      memWrEn <= '0';
      blockComplete <= '0';
      c1Done <= '0';

      if c1InputBufferLineFilled = '1' then
        c1InputReadIx <= 0;
        memWrRowSel <= c1InputBufferLastSOL / 2;
      end if;
      case c1State is
        when Idle =>
          if c1InputReadIx < 74 then
            c1State <= CopyingToRs;
            cxFrameSize <= 37;
            cxEnable <= c1Enable;
            cxStart <= '1';
          end if;
        when CopyingToRs =>
          if c1InputReadIx < 74 then
            cx_inEn <= '1';
            readIx := c1InputReadIx * 2 when c1InputReadIx < 37 else (c1InputReadIx - 37) * 2 + 1;
            cx_in <= c1InputBuffer((c1InputBufferLastSOL mod 4) * 37 + readIx);
            cx_erasedIn <= '0';
            c1InputReadIx <= c1InputReadIx + 1;
          end if;
          if c1InputReadIx = 36 or c1InputReadIx = 73 then
            c1State <= WaitingForRs;  
          end if;
        when WaitingForRs =>
          if cxDone = '1' then
            c1Done <= '1';
            c1Status <= cxStatus;
            c1State <= CopyingFromRs;
            c1CopyOutIx <= 0;
          end if;
        when CopyingFromRs =>
          if cx_outEn = '1' then
            if c1CopyOutIx < 36 then -- this looks weird, but makes sure to read all output before restarting C1
              if c1CopyOutIx < 33 then -- (if c1 is not enabled, we otherwise restart c1 too early)
                memWrColSel <= c1CopyOutIx * 2 when c1InputReadIx = 37 else c1CopyOutIx * 2 + 1; -- else means c1InputReadIx = 74
                erasure := '1' when cxStatus = Failed else '0';
                memWrData <= erasure & cx_out;
                memWrEn <= '1';
              end if;             
              c1CopyOutIx <= c1CopyOutIx + 1;
            else
              if c1InputReadIx = 74 then
                c1State <= Idle;              
                if c1InputBufferLastSOL = 70 then
                  blockComplete <= '1';
                  memRdBlockSel <= memWrBlockSel;
                  memWrBlockSel <= 1 - memWrBlockSel;
                end if;
              else
                c1State <= CopyingToRs;
                cxStart <= '1';
              end if;
            end if;
          end if;
      end case;        

      -- C2 processing

      if blockComplete = '1' then 
        c2CodewordIndex <= 0;
      end if;
      
      c2CodewordDone <= '0';
      c2Done <= '0';
      if c2CodewordIndex < 66 then
        case c2State is
          when Idle =>
            c2State <= CopyingToRs;
            i <= 0;
          when CopyingToRs =>
            if i = 0 then
              cxFrameSize <= 36;
              cxEnable <= c2Enable;
              cxStart <= '1';
            end if;
            if i < 36 then
              memRdRowSel <= i;
              memRdColSel <= c2CodewordIndex;
            end if;
            if i < 37 then
              i <= i + 1;
            else
              c2State <= WaitingForRs;  
            end if;
            if i >= 2 then
              cx_inEn <= '1';
              cx_in <= memRdData(7 downto 0); -- the last byte will be copied as we
                                              -- transistion state to WaitingForRs
              cx_erasedIn <= memRdData(8);
            end if;
          when WaitingForRs =>
            if cxDone = '1' then
              c2Done <= cxDone;
              c2Status <= cxStatus;
              c2State <= CopyingFromRs;
              i <= 0;
            end if;
          when CopyingFromRs =>
            if cx_outEn = '1' then
              if i < 32 then
                c2Codeword(i) <= cx_out;
                i <= i + 1;
              else
                c2State <= Idle;
                c2CodewordIndex <= c2CodewordIndex + 1;
                c2CodewordDone <= '1';
              end if;
            end if;
        end case;
      end if;
    end if;
  end process;

  -- scan codeword when codewordCorrected is set
  process(clk)
  begin
    if rising_edge(clk) then
      if c2CodewordDone = '1' and c2CodewordIndex - 1 < 66 then -- codewordIndex is the write index and was incremented before we see it here
        if c2CodewordIndex - 1 = 0 then -- first codeword in a block completed; should start with 0x10 0x00, but we currently do not check
          scanIx <= 2;
        else
          scanIx <= 0;
        end if;
      end if;
      ac3WrEn <= '0';
      ac3BlockReady <= '0';
      if scanIx < 32 then
        case scanState is
          when Searching =>
            if c2Codeword(scanIx) = x"0b" then
              scanState <= Found1;
              savedWhileScanning(0) <= c2Codeword(scanIx);
            end if;
          when Found1 =>
            if c2Codeword(scanIx) = x"77" then
              scanState <= Found2;
              savedWhileScanning(1) <= c2Codeword(scanIx);
            else
              scanState <= Searching;
            end if;
          when Found2 =>
            scanState <= Found2plus1;
            savedWhileScanning(2) <= c2Codeword(scanIx);
          when Found2plus1 =>
            scanState <= Found2plus2;
            savedWhileScanning(3) <= c2Codeword(scanIx);
          when Found2plus2 =>
            if c2Codeword(scanIx) = x"1c" then
              scanState <= Found3;
              savedWhileScanning(4) <= c2Codeword(scanIx);
              savedCopyIx <= 0;
              ac3Ix <= 0;
            else
              scanState <= Searching;
            end if;
          when Found3 =>
            if savedCopyIx < 5 then
              ac3WrEn <= '1';
              ac3WrBlockSel <= ac3BlockIx;
              ac3WrAddr <= ac3Ix;
              ac3Ix <= ac3Ix + 1;
              ac3WrData <= savedWhileScanning(savedCopyIx);
              savedCopyIx <= savedCopyIx + 1;
            else
              scanState <= InBlock;
            end if;
          when InBlock =>
            ac3WrEn <= '1';
            ac3WrBlockSel <= ac3BlockIx;
            ac3WrAddr <= ac3Ix;
            ac3Ix <= ac3Ix + 1;
            ac3WrData <= c2Codeword(scanIx);
            if ac3Ix = 1535 then
              scanState <= Searching;
              ac3BlockReady <= '1';
              ac3ReadyBlockIndex <= ac3BlockIx;
              ac3BlockIx <= 1 - ac3BlockIx;
            end if;
        end case;
        if scanState /= Found3 then
          scanIx <= scanIx + 1;
        end if;
      end if;
    end if;
  end process;
end a1;
