library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- processes the output from qpskReclock and groups the input sybols into frames of 37 bytes

entity ac3InputFraming is
  port (
    clk: in std_logic;
    inEn: in std_logic;
    dataIn: in std_logic_vector(1 downto 0);
    frameNoOutEn: out std_logic; -- set before outEn, which is then set 37 times
    frameNoOut: out unsigned(6 downto 0); -- 0..71;
    outEn: out std_logic;
    dataOut: out std_logic_vector(7 downto 0)
  );
end ac3InputFraming;

architecture a1 of ac3InputFraming is
  type NumericSymbolArray is array(natural range <>) of unsigned(1 downto 0);
  type SymbolArray is array(natural range <>) of std_logic_vector(1 downto 0);
  
  signal syncFrameSymbolsSeen: integer range 0 to 12;
  signal syncFrameNo: SymbolArray(0 to 3);
  signal symbolInFrameCounter: integer range 0 to 37 * 4 := 0;
  signal symbolsInFrame: SymbolArray(0 to 37 * 4);
  signal consecutiveSynched: Integer range 0 to 7 := 0;
  signal autoSync: boolean := false;
  signal startOutput: boolean := false;
  signal copyToOutputCounter: integer range 0 to 37 := 37; -- 37 means we are done
  
  signal lastFrameNo: SymbolArray(0 to 3);
  
  function nextFrameNo(fn: SymbolArray(0 to 3)) return SymbolArray is
    variable x: std_logic_vector(6 downto 0);
    variable unsignedFn: unsigned(6 downto 0);
    variable nextFn: unsigned(6 downto 0);
    variable result: SymbolArray(0 to 3);
  begin
    x := fn(0)(1) & fn(1) & fn(2) & fn(3);
    unsignedFn := unsigned(x);
    nextFn := to_unsigned(0, 7) when unsignedFn = 71 else unsignedFn + 1;
    result(0) := '0' & nextFn(6);
    result(1) := std_logic_vector(nextFn(5 downto 4));
    result(2) := std_logic_vector(nextFn(3 downto 2));
    result(3) := std_logic_vector(nextFn(1 downto 0));
    return result;
  end function;
begin
  process(clk)
    variable isNextSyncSymbol: boolean;
  begin
    if rising_edge(clk) then
      startOutput <= false;
      if inEn = '1' then
        if syncFrameSymbolsSeen < 12 then
          if not autoSync then
            case syncFrameSymbolsSeen is
              when 0 => isNextSyncSymbol := dataIn = "00";
              when 1 => isNextSyncSymbol := dataIn = "01";
              when 2 => isNextSyncSymbol := dataIn = "01";
              when 3 => isNextSyncSymbol := dataIn = "11";
              when 4 | 5 | 6 | 7 => 
                syncFrameNo(syncFrameSymbolsSeen - 4) <= dataIn;
                isNextSyncSymbol := true;
              when 8 | 9 | 10 | 11 => isNextSyncSymbol := dataIn = "00";
              when 12 => isNextSyncSymbol := false; -- to satisfy compiler
            end case;
            if isNextSyncSymbol then
              syncFrameSymbolsSeen <= syncFrameSymbolsSeen + 1;
            else 
              if consecutiveSynched > 0 then
                consecutiveSynched <= consecutiveSynched - 1;
                syncFrameNo <= nextFrameNo(lastFrameNo);
                autoSync <= true;
                syncFrameSymbolsSeen <= syncFrameSymbolsSeen + 1;
              else
                syncFrameSymbolsSeen <= 0;
              end if;
            end if;
          else
            syncFrameSymbolsSeen <= syncFrameSymbolsSeen + 1;
          end if;
        else -- all sync symbols seen
          if symbolInFrameCounter = 0 and not autoSync and consecutiveSynched < 7 then
            consecutiveSynched <= consecutiveSynched + 1;
          end if;
          autoSync <= false;
        
          symbolsInFrame(symbolInFrameCounter) <= dataIn;

          if symbolInFrameCounter = 37 * 4 - 1 then
            startOutput <= true;
            lastFrameNo <= syncFrameNo;
            symbolInFrameCounter <= 0;
            syncFrameSymbolsSeen <= 0;
          else
            symbolInFrameCounter <= symbolInFrameCounter + 1;
          end if;          
        end if;  
      end if;
    end if;
  
  -- copy the frame to the output when complete; there is plenty of time before the data is overwritten
    if rising_edge(clk) then
      frameNoOutEn <= '0';
      outEn <= '0';
      if startOutput then
        frameNoOut(6) <= syncFrameNo(0)(0);
        frameNoOut(5 downto 4) <= unsigned(syncFrameNo(1));
        frameNoOut(3 downto 2) <= unsigned(syncFrameNo(2));
        frameNoOut(1 downto 0) <= unsigned(syncFrameNo(3));
        frameNoOutEn <= '1';
        copyToOutputCounter <= 0;
      end if;
      if copyToOutputCounter < 37 then
        dataOut(7 downto 6) <= symbolsInFrame(copyToOutputCounter * 4);
        dataOut(5 downto 4) <= symbolsInFrame(copyToOutputCounter * 4 + 1);
        dataOut(3 downto 2) <= symbolsInFrame(copyToOutputCounter * 4 + 2);
        dataOut(1 downto 0) <= symbolsInFrame(copyToOutputCounter * 4 + 3);
        outEn <= '1';
        copyToOutputCounter <= copyToOutputCounter + 1;
      end if;
    end if;
  end process;
end a1;
