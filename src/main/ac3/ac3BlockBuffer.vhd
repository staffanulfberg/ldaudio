library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- two 1536 byte buffers for deinterleaving incoming ac3 data with separate
-- read and write addresses

entity ac3BlockBuffer is
  port (
    clk: in std_logic;
    wrEn: in std_logic;

    rdBlockSel: in natural range 0 to 1;
    rdAddr: in natural range 0 to 1535;
    rdData: out std_logic_vector(7 downto 0);

    wrBlockSel: in natural range 0 to 1;
    wrAddr: in natural range 0 to 1535;
    wrData: in std_logic_vector(7 downto 0)
  );
end ac3BlockBuffer;

architecture a1 of ac3BlockBuffer is
  type RamType is array (0 to 2 * 1536 - 1) of std_logic_vector(7 downto 0);
  signal ram: RamType;
  signal rdAddress: natural range 0 to 2 * 1536 - 1;
begin
  rdData <= ram(rdAddress);
      
  process(clk)
    variable wrAddress: natural range 0 to 2 * 1536 - 1;
  begin
    if rising_edge(clk) then
      rdAddress <= rdBlockSel * 1536 + rdAddr;
      wrAddress := wrBlockSel * 1536 + wrAddr;
      if wrEn = '1' then
        ram(wrAddress) <= wrData;
      end if;
    end if;
  end process;
end a1;
