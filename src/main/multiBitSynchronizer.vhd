library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity multiBitSynchronizer is
  generic (
    width: positive
  );
  port (
    srcClk: in std_logic;
    dataInEn: in std_logic; -- assert for 1 src clock cycle
    dataIn: in std_logic_vector(width - 1 downto 0);
    busy: out std_logic; -- for the source to read

    dstClk: in std_logic;
    dataOutEn: out std_logic; -- asserted for 1 dst clock cycle
    dataOut: out std_logic_vector(width - 1 downto 0)
  );
end multiBitSynchronizer;

architecture rtl of multiBitSynchronizer is
  signal dataInRegistered: std_logic_vector(width - 1 downto 0);
  signal reqIn: std_logic;
  signal reqOut: std_logic;
  signal ackIn: std_logic;
  signal ackOut: std_logic;
  
  type StateType is (Idle, SettingUpData1, SettingUpData2, WaitForAck, WaitForAckDeasserted);
  signal state: StateType;
  signal dstWaitingForReq: boolean := true;
begin  
  reqSync: entity work.bitSynchronizer port map (
      dataIn => reqIn,
      dstClk => dstClk,
      dataOut => reqOut
  );
  ackSync: entity work.bitSynchronizer port map (
      dataIn => ackIn,
      dstClk => srcClk,
      dataOut => ackOut
  );

  singleBits: for i in 0 to width - 1 generate
    bitSync: entity work.bitSynchronizer port map (
      dataIn => dataInRegistered(i),
      dstClk => dstClk,
      dataOut => dataOut(i)
    );
  end generate;

  busy <= '1' when (state /= Idle) or dataInEn = '1' else '0'; -- prevent writer from seeing non-busy status

  process(srcClk)
  begin
    if rising_edge(srcClk) then
      case state is
        when Idle =>
          if dataInEn = '1' then
            dataInRegistered <= dataIn;
            state <= SettingUpData1;
          end if;
        when SettingUpData1 =>
          state <= SettingUpData2;
        when SettingUpData2 =>
          reqIn <= '1';
          state <= WaitForAck;
        when WaitForAck => 
          if ackOut = '1' then
            reqIn <= '0';
            state <= WaitForAckDeasserted;
          end if;
        when WaitForAckDeasserted =>
          if ackOut = '0' then
            state <= Idle;
          end if;
      end case;
    end if;
  end process;

  process(dstClk)
  begin
    if rising_edge(dstClk) then
      dataOutEn <= '0';
      if dstWaitingForReq then
        if reqOut = '1' then
          dataOutEn <= '1';
          dstWaitingForReq <= false;
          ackIn <= '1';
        end if;
      else
        if reqOut = '0' then
          ackIn <= '0';
          dstWaitingForReq <= true;
        end if;
      end if;
    end if;
  end process;
end rtl;
