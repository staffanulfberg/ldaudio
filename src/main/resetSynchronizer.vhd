library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity resetSynchronizer is
  port (
    rstIn: in std_logic;
    dstClk: in std_logic;
    rstOut: out std_logic
  );
end resetSynchronizer;

architecture behavioral of resetSynchronizer is
  signal r1: std_logic;
  signal r2: std_logic;
  
  attribute ASYNC_REG: string;
  attribute ASYNC_REG of r1: signal is "TRUE";
  attribute ASYNC_REG of r2: signal is "TRUE";
begin
  process(dstClk)
  begin
    if rising_edge(dstClk) then
      r1 <= rstIn;
      r2 <= r1;
    end if;
  end process;

  rstOut <= r2 or rstIn;
end behavioral;
