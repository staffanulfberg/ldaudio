library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.reedSolomon_types.all;

entity circBuffer is
  port (
    clock: in std_logic;
    
    frameStart: in std_logic;
    inEn: in std_logic;
    dataIn: in std_logic_vector(7 downto 0);
    erasedIn: in std_logic;
    
    outEn: out std_logic;
    leftOut: out signed(15 downto 0);
    leftErased: out std_logic;
    rightOut: out signed(15 downto 0);
    rightErased: out std_logic;
    
    c1Enable: in std_logic;
    c2Enable: in std_logic;
    c1Done: out std_logic;
    c2Done: out std_logic;
    c1Status: out CompletionStatus;
    c2Status: out CompletionStatus
  );
end circBuffer;

architecture behavioral of circBuffer is
  signal C1_start: std_logic;
  signal C1_inEn: std_logic;
  signal C1_in: std_logic_vector(7 downto 0);
  signal C1_inErased: std_logic;
  signal C1_outEn: std_logic;
  signal C1_out: std_logic_vector(7 downto 0);
  signal C1_outErased: std_logic;
  signal C2_start: std_logic;
  signal C2_inEn: std_logic;
  signal C2_in: std_logic_vector(7 downto 0);
  signal C2_inErased: std_logic;
  signal C2_outEn: std_logic;
  signal C2_out: std_logic_vector(7 downto 0);
  signal C2_outErased: std_logic;

  signal firstDelayWrLineNo: natural range 0 to 31;
  signal firstDelayRdLineNo: natural range 0 to 31;
  signal firstDelayIn: std_logic_vector(7 downto 0);
  signal firstDelayErasedIn: std_logic;
  signal firstDelayWrEn: std_logic;
  signal firstDelayOut: std_logic_vector(7 downto 0);
  signal firstDelayErasedOut: std_logic;
  
  signal secondDelayWrLineNo: natural range 0 to 27;
  signal secondDelayRdLineNo: natural range 0 to 27;
  signal secondDelayIn: std_logic_vector(7 downto 0);
  signal secondDelayErasedIn: std_logic;
  signal secondDelayWrEn: std_logic;
  signal secondDelayOut: std_logic_vector(7 downto 0);
  signal secondDelayErasedOut: std_logic;

  signal thirdDelayWrLineNo: natural range 0 to 27;
  signal thirdDelayRdLineNo: natural range 0 to 27;
  signal thirdDelayIn: std_logic_vector(7 downto 0);
  signal thirdDelayErasedIn: std_logic;
  signal thirdDelayWrEn: std_logic;
  signal thirdDelayOut: std_logic_vector(7 downto 0);
  signal thirdDelayErasedOut: std_logic;

  signal delayed_C2_out: ByteArray(0 to 23);
  signal delayed_C2_outErased: std_logic_vector(0 to 23);
  
  signal inputIndex: natural range 0 to 33 := 33;
  signal c1InputIndex: natural range 0 to 34 := 34;
  signal c1OutDelayIndex: natural range 0 to 33 := 33;
  signal c2InputIndex: natural range 0 to 30 := 30;
  signal c2OutDelayIndex: natural range 0 to 29 := 29;
  signal sampleDecodeIndex: natural range 0 to 27 := 27;
  signal sampleOutputIndex: natural range 0 to 6 := 6;
  
  procedure makeSample(
    hiByte: in std_logic_vector(7 downto 0);
    hiErased: in std_logic;
    loByte: in std_logic_vector(7 downto 0);
    loErased: in std_logic;
    signal sample: out signed(15 downto 0);
    signal erased: out std_logic)
  is
  begin
    sample <= signed(std_logic_vector'(hiByte & loByte));
    erased <= hiErased or loErased;
  end procedure;
begin
  firstDelayArray: entity work.delayLineArray 
  generic map (
    delays => (0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1) -- 32 delay lines
  ) 
  port map (
    clock => clock,
    wrEn => firstDelayWrEn,
    wrLineNo => firstDelayWrLineNo,
    dataIn => firstDelayIn,
    dataInErased => firstDelayErasedIn,
    rdLineNo => firstDelayRdLineNo,
    dataOut => firstDelayOut,
    dataOutErased => firstDelayErasedOut   
  );

  C1: entity work.reedSolomon
  generic map (
    poly => 9x"11d",
    fcr => 0,
    maxFrameSize => 32)
  port map (
    frameSize => 32,
    clk => clock,
    enabled => c1Enable,
    start => C1_start,
    inEn => C1_inEn,
    erasedIn => C1_inErased,
    dataIn => C1_in,
    done => c1Done,
    status => c1Status,
    outEn => C1_outEn,
    dataOut => C1_out
  );

  secondDelayArray: entity work.delayLineArray 
  generic map (
    delays => (108, 104, 100, 96, 92, 88, 84, 80, 76, 72, 68, 64, 60, 56, 52, 48, 44, 40, 36, 32, 28, 24, 20, 16, 12, 8, 4, 0) -- 28 delay lines
  )
  port map (
    clock => clock,
    wrEn => secondDelayWrEn,
    wrLineNo => secondDelayWrLineNo,
    dataIn => secondDelayIn,
    dataInErased => secondDelayErasedIn,
    rdLineNo => secondDelayRdLineNo,
    dataOut => secondDelayOut,
    dataOutErased => secondDelayErasedOut   
  );
  
  C2: entity work.reedSolomon
  generic map (
    poly => 9x"11d",
    fcr => 0,
    maxFrameSize => 28)
  port map (
    frameSize => 28,
    clk => clock,
    enabled => c2Enable,
    start => C2_start,
    inEn => C2_inEn,
    erasedIn => C2_inErased,
    dataIn => C2_in,
    done => C2Done,
    status => c2Status,
    outEn => C2_outEn,
    dataOut => C2_out
  );

  thirdDelayArray: entity work.delayLineArray 
  generic map (
    delays => (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2) -- 24 delay lines
  )
  port map (
    clock => clock,
    wrEn => thirdDelayWrEn,
    wrLineNo => thirdDelayWrLineNo,
    dataIn => thirdDelayIn,
    dataInErased => thirdDelayErasedIn,
    rdLineNo => thirdDelayRdLineNo,
    dataOut => thirdDelayOut,
    dataOutErased => thirdDelayErasedOut   
  );
  
  process(clock)
  begin
    if rising_edge(clock) then
      if frameStart = '1' then
        inputIndex <= 0;
      end if;
      firstDelayWrEn <= '0';
      if inEn = '1' then
        inputIndex <= inputIndex + 1;
        
        firstDelayWrEn <= '1';
        firstDelayWrLineNo <= inputIndex;
        firstDelayIn <= not dataIn 
          when inputIndex = 12 or inputIndex = 13 or inputIndex = 14 or inputIndex = 15 or
            inputIndex = 28 or inputIndex = 29 or inputIndex = 30 or inputIndex = 31 
          else dataIn;
        firstDelayErasedIn <= erasedIn;       
      end if;

      C1_start <= '0';
      if inputIndex = 32 then
        inputIndex <= 33;
        c1InputIndex <= 0;
        C1_start <= '1';
      end if;    
  
      C1_inEn <= '0';
      if c1InputIndex < 34 then
        c1InputIndex <= c1InputIndex + 1;
        if c1InputIndex < 32 then
          firstDelayRdLineNo <= c1InputIndex;
        end if;
        if c1InputIndex >= 2 then -- reading takes 2 cycles
          C1_inEn <= '1';
          C1_in <= firstDelayOut;
          C1_inErased <= firstDelayErasedOut;
        end if;
      end if;

      if c1Done then
        c1OutDelayIndex <= 0;
        case c1Status is
          when Failed => 
            C1_outErased <= '1';
          when CorrectedOneError | CorrectedTwoErrors | NoErrors | CorrectedErasures => 
            C1_outErased <= '0';
          when Running =>
            assert false report "C1 is done but claims to be running";
        end case;
      end if;
      
      secondDelayWrEn <= '0';
      if C1_outEn = '1' then
        c1OutDelayIndex <= c1OutDelayIndex + 1;
        if c1OutDelayIndex < 28 then
          secondDelayWrEn <= '1';
          secondDelayWrLineNo <= c1OutDelayIndex;
          secondDelayIn <= C1_out;
          secondDelayErasedIn <= C1_outErased;
        end if;
      end if;
      
      C2_start <= '0';
      if c1OutDelayIndex = 32 then
        c1OutDelayIndex <= 33;
        c2InputIndex <= 0;
        C2_start <= '1';
      end if;    
  
      C2_inEn <= '0';
      if c2InputIndex < 30 then
        c2InputIndex <= c2InputIndex + 1;
        if c2InputIndex < 28 then
          secondDelayRdLineNo <= c2InputIndex;
        end if;
        if c2InputIndex >= 2 then -- reading takes 2 cycles
          C2_inEn <= '1';
          C2_in <= secondDelayOut;
          C2_inErased <= secondDelayErasedOut;
        end if;
      end if;
    
      if c2Done then
        c2OutDelayIndex <= 0;
        case c2Status is
          when Failed => 
            C2_outErased <= '1';
          when CorrectedOneError | CorrectedTwoErrors | CorrectedErasures | NoErrors => 
            C2_outErased <= '0';
          when Running =>
            assert false report "C2 is done but claims to be running";
        end case;
      end if;

      thirdDelayWrEn <= '0';
      if C2_outEn = '1' then
        c2OutDelayIndex <= c2OutDelayIndex + 1;
        thirdDelayWrEn <= '1' when c2OutDelayIndex < 12 or c2OutDelayIndex >= 16;
        thirdDelayWrLineNo <= c2OutDelayIndex when c2OutDelayIndex < 12 else c2OutDelayIndex - 4;
        thirdDelayIn <= C2_out;
        thirdDelayErasedIn <= C2_outErased;
      end if;
      
      if c2OutDelayIndex = 28 then
        c2OutDelayIndex <= 29;
        sampleDecodeIndex <= 0;
      end if;    

      if sampleDecodeIndex < 26 then
        sampleDecodeIndex <= sampleDecodeIndex + 1;
        if sampleDecodeIndex < 24 then
          thirdDelayRdLineNo <= sampleDecodeIndex;
        end if;
        if sampleDecodeIndex >= 2 then -- reading takes 2 cycles
          delayed_C2_out(sampleDecodeIndex - 2) <= thirdDelayOut;
          delayed_C2_outErased(sampleDecodeIndex - 2) <= thirdDelayErasedOut;
        end if;
      elsif sampleDecodeIndex = 26 then
        sampleDecodeIndex <= 27;
        sampleOutputIndex <= 0;
      end if;
      
      outEn <= '0';
      if sampleOutputIndex < 6 then
        sampleOutputIndex <= sampleOutputIndex + 1;
        outEn <= '1';
        case sampleOutputIndex is
          when 0 =>
            makeSample(delayed_C2_out(0), delayed_C2_outErased(0), delayed_C2_out(1), delayed_C2_outErased(1), leftOut, leftErased);
            makeSample(delayed_C2_out(6), delayed_C2_outErased(6), delayed_C2_out(7), delayed_C2_outErased(7), rightOut, rightErased);
          when 1 =>
            makeSample(delayed_C2_out(12), delayed_C2_outErased(12), delayed_C2_out(13), delayed_C2_outErased(13), leftOut, leftErased);
            makeSample(delayed_C2_out(18), delayed_C2_outErased(18), delayed_C2_out(19), delayed_C2_outErased(19), rightOut, rightErased);
          when 2 =>
            makeSample(delayed_C2_out(2), delayed_C2_outErased(2), delayed_C2_out(3), delayed_C2_outErased(3), leftOut, leftErased);
            makeSample(delayed_C2_out(8), delayed_C2_outErased(8), delayed_C2_out(9), delayed_C2_outErased(9), rightOut, rightErased);
          when 3 =>
            makeSample(delayed_C2_out(14), delayed_C2_outErased(14), delayed_C2_out(15), delayed_C2_outErased(15), leftOut, leftErased);
            makeSample(delayed_C2_out(20), delayed_C2_outErased(20), delayed_C2_out(21), delayed_C2_outErased(21), rightOut, rightErased);
          when 4 =>
            makeSample(delayed_C2_out(4), delayed_C2_outErased(4), delayed_C2_out(5), delayed_C2_outErased(5), leftOut, leftErased);
            makeSample(delayed_C2_out(10), delayed_C2_outErased(10), delayed_C2_out(11), delayed_C2_outErased(11), rightOut, rightErased);
          when 5 =>
            makeSample(delayed_C2_out(16), delayed_C2_outErased(16), delayed_C2_out(17), delayed_C2_outErased(17), leftOut, leftErased);
            makeSample(delayed_C2_out(22), delayed_C2_outErased(22), delayed_C2_out(23), delayed_C2_outErased(23), rightOut, rightErased);
          when others =>
        end case;
      end if;
    end if;
  end process;
end Behavioral;
