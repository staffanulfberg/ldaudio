library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.reedSolomon_types.all;

entity efm is
  Port (
    clk: in std_logic;
    dataIn: in std_logic;
    inEn: in std_logic;
    enableErasures: in std_logic;
    
    locked: out std_logic; -- set true after two successful frame synchronizations is a row, false after 64 failed frames
    frameStart: out std_logic; -- true for one cycle at the start of a new frame; controlDataOut is valid at this time
    controlDataOut: out std_logic_vector(7 downto 0);
    dataOutEn: out std_logic; -- set true 32 times during a frame
    dataOut: out std_logic_vector(7 downto 0);
    erasedOut: out std_logic
  );
end efm;

architecture Behavioral of efm is
  signal shiftRegister: std_logic_vector(23 downto 0);
  signal byteIndex: integer := 34;
  signal bitIndex: integer := 0;
  
  signal decodeEnable: std_logic := '0';
  signal decodeAddr: std_logic_vector(13 downto 0);
  signal decodedData: std_logic_vector(8 downto 0); -- bit 8 is the erasure flag
  
  signal isLocked: std_logic := '0';
begin
  decodeRom: entity work.blk_mem_gen_efm port map (
    clka => clk,
    ena => decodeEnable,
    addra => decodeAddr,
    douta => decodedData
  );
  
  locked <= isLocked;

  process(clk)
    variable sync: boolean;
    variable bitsSinceSync: unsigned(9 downto 0) := to_unsigned(0, 10); -- enough to count to 588
    variable framesSinceSync: unsigned(2 downto 0) := "000";
    variable consecutiveSyncs: unsigned(1 downto 0) := "00";
  begin
    if clk = '1' and clk'event then
      frameStart <= '0';
      dataOutEn <= '0';
      if inEn = '1' then
        shiftRegister(23 downto 1) <= shiftRegister(22 downto 0);
        shiftRegister(0) <= dataIn;
        
        sync := shiftRegister = "100000000001000000000010";
        if sync then
          consecutiveSyncs := consecutiveSyncs + 1;
          if consecutiveSyncs = 3 then
            isLocked <= '1';
          end if;
          bitsSinceSync := to_unsigned(0, 10);
          framesSinceSync := "000";
        else
          bitsSinceSync := bitsSinceSync + 1;
          if bitsSinceSync = 588 then
            if isLocked = '1' then
              sync := true;
            end if;
            bitsSinceSync := to_unsigned(0, 10);
            consecutiveSyncs := "00";
            framesSinceSync := framesSinceSync + 1;
            if framesSinceSync = 7 then
              isLocked <= '0';
            end if;
          end if;
        end if;
                        
        if sync then
          byteIndex <= 0;
          bitIndex <= 0;
        elsif byteIndex < 34 then
          if bitIndex = 16 then
            bitIndex <= 0;
            byteIndex <= byteIndex + 1;
          else
            bitIndex <= bitIndex + 1;
          end if;
          
          if byteIndex <= 32 then
            if bitIndex = 16 then
              decodeAddr <= shiftRegister(13 downto 0);
              decodeEnable <= '1';
            else
              decodeEnable <= '0';
            end if;
          end if;
          
          if bitIndex = 1 then
            if byteIndex = 1 then
              controlDataOut <= decodedData(7 downto 0);
              frameStart <= '1';
            elsif byteIndex /= 0 then
              dataOut <= decodedData(7 downto 0);
              erasedOut <= decodedData(8) and enableErasures;
              dataOutEn <= '1';
            end if;
          end if;
        end if;
      end if;
    end if;
  end process;
end Behavioral;
