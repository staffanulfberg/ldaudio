library IEEE;

package delayLineArray_types is
  type ArrayOfNatural is array(natural range <>) of natural;
end package;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.delayLineArray_types.all;

entity delayLineArray is
  generic (
    delays: ArrayOfNatural
  );
  port (
    clock: in std_logic;
    wrEn: in std_logic;
    wrLineNo: in natural range 0 to delays'length - 1;
    dataIn: in std_logic_vector(7 downto 0);
    dataInErased: in std_logic;
    rdLineNo: in natural range 0 to delays'length - 1;
    dataOut: out std_logic_vector(7 downto 0);
    dataOutErased: out std_logic
  );
end delayLineArray;

architecture rtl of delayLineArray is
  constant numberOfDelayLines: positive := delays'length;

  function computeStartOffsets return ArrayOfNatural is
    variable startOffsets: ArrayOfNatural(0 to numberOfDelayLines - 1);
    variable sum: natural := 0;
  begin
    for i in 0 to numberOfDelayLines - 1 loop
      startOffsets(i) := sum;
      sum := sum + delays(i) + 1; -- for an n stage delay line, we need n + 1 registers
    end loop;
    return startOffsets;
  end function;
  
  constant startOffsets: ArrayOfNatural(0 to numberOfDelayLines - 1) := computeStartOffsets;

  type PositionArrayType is array(0 to numberOfDelayLines - 1) of natural range 0 to maximum(delays);
  signal positions: PositionArrayType := (others => 0);

  constant memorySize: natural := startOffsets(numberOfDelayLines - 1) + delays(numberOfDelayLines - 1) + 1;
  type MemoryType is array(0 to memorySize - 1) of std_logic_vector(8 downto 0);
  signal memory: MemoryType;
  signal rdAddress: natural range 0 to memorySize - 1;
  type StateType is (Idle, Writing);
begin
  dataOut <= memory(rdAddress)(7 downto 0);
  dataOutErased <= memory(rdAddress)(8);
    
  process(clock)
    variable writePos: natural range 0 to maximum(delays);
    variable wrAddress: natural range 0 to memorySize - 1;
  begin
    if rising_edge(clock) then
      if wrEn = '1' then
        writePos := positions(wrLineNo);
        wrAddress := startOffsets(wrLineNo) + writePos;
        memory(wrAddress) <= dataInErased & dataIn;
        positions(wrLineNo) <= writePos + 1 when writePos < delays(wrLineNo) else 0;
      end if;
      rdAddress <= startOffsets(rdLineNo) + positions(rdLineNo);    
    end if;
  end process;
end rtl;
