library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity audio is
  port (
    enable: in std_logic;
    mclk: in std_logic;
    tx_mclk: out std_logic;
    tx_lrck: out std_logic;
    tx_sclk: out std_logic;
    tx_data: out std_logic;
    tx_spdif: out std_logic;
    
    sampleData: in std_logic_vector(31 downto 0);
    sampleAvailable: in std_logic;
    sampleDataRdEn: out std_logic := '0'
  );
end audio;

architecture rtl of audio is
  signal mclkCounter: unsigned(7 downto 0) := to_unsigned(0, 8); -- 8 counts per sclk, 256 counter per lrck
  signal rightBuf: std_logic_vector(15 downto 0);
  signal outReg: std_logic_vector(15 downto 0);
  signal dataValid: std_logic := '0';

  function parity(a: std_logic_vector) return std_logic is
    variable y: std_logic := '0';
  begin
    for i in a'RANGE loop
      y := y xor a(i);
    end loop;
    return y;
  end parity;
begin
  tx_mclk <= mclk;  
      
  process(mclk)
    variable readIssued: std_logic := '0';
  begin
    if mclk = '1' and mclk'event then
      sampleDataRdEn <= '0';
      if enable = '1' then
        mclkCounter <= mclkCounter + 1;
        tx_sclk <= mclkCounter(2);
        if (mclkCounter >=120) and (mclkCounter < 248) then
          tx_lrck <= '1';
        else
          tx_lrck <= '0';
        end if;
      
        if mclkCounter(2 downto 0) = "000" then
          tx_data <= outReg(15 - to_integer(mclkCounter(6 downto 3))); -- not checking if dataValid is true; the last sample will be output again
        end if;
      
        if mclkCounter = 253 then
          if sampleAvailable = '1' then
            sampleDataRdEn <= '1';
          end if;
          readIssued := sampleAvailable;
        elsif mclkCounter = 255 then
          if readIssued = '1' then
            outReg <= sampleData(31 downto 16);
            rightBuf <= sampleData(15 downto 0);
          end if;
          dataValid <= readIssued;
        elsif mclkCounter = 127 then
          outReg <= rightBuf;
        end if;
      end if;
    end if;
  end process;
  
  process(mclk)
    constant preambleB: std_logic_vector(0 to 7) := "10011100"; -- 11101000 is the biphase code, but we represent when to toggle
    constant preambleM: std_logic_vector(0 to 7) := "10010011"; -- 11100010
    constant preambleW: std_logic_vector(0 to 7) := "10010110"; -- 11100100
    variable preamble: std_logic_vector(0 to 7);
    variable frameCounter: natural range 0 to 191 := 191;
    variable subframe: std_logic;
    variable bitCounter: integer range 0 to 31;
    variable biphase: natural range 0 to 1; -- 0 / 1 during first / second half of bit
    variable output: std_logic;
    variable b: std_logic;
    variable prevOut: std_logic := '0';
    variable status: std_logic;
    variable par: std_logic;
  begin
    if mclk = '1' and mclk'event then
      if enable = '1' then
        if mclkCounter(0) = '0' then
          subframe := mclkCounter(7);
          bitCounter := to_integer(mclkCounter(6 downto 2)); -- 0 .. 31
          biphase := 1 when mclkCounter(1) = '1' else 0;
          if bitCounter = 0 and subframe = '0' and biphase = 0 then
            frameCounter := frameCounter + 1 when frameCounter /= 191 else 0;
          end if;
          
          case frameCounter is
            when 0 to 1 => status := '0'; -- consumer
            when 2 => status := '1'; -- allow copy
            when 3 => status := '0'; -- pre emphasis
            when others => status := '0'; -- common, 2 ch, no subcode
          end case;      
          case bitCounter is
            when 0 to 3 => 
              preamble := preambleW when subframe /= '0' else preambleB when frameCounter = 0 else preambleM;
              output := preamble(bitCounter * 2 + biphase) xor prevOut;
            when others => 
              case bitCounter is
                when 0 to 3 => -- to silent compiler
                when 4 to 11 => b := '0';
                when 12 to 27 => b := outreg(bitCounter - 12) when dataValid = '1' else '0';
                when 28 => b := dataValid; -- valid
                when 29 => b := '0'; -- subcode data
                when 30 => b := status;
                when 31 => 
                  par := parity(outReg) when dataValid = '1' else '0';
                  b := par xor dataValid xor status; -- bits 0-3 (not real bits) not included; outreg still contains valid data!
              end case;
              if biphase = 0 then
                output := not prevOut;
              else
                output := prevOut xor b;
              end if;
          end case;
          tx_spdif <= output;
          prevOut := output;
        end if;
      else
        frameCounter := 191;
        tx_spdif <= '0';
        prevOut := '0';
      end if;
    end if;
  end process;
end rtl;
