library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity efmIn is
  generic (
    clkFrequency: in positive
  );
  port (
    clk: in std_logic;
    dataIn: in std_logic;
    outEn: out std_logic;
    dataOut: out std_logic
  );
end efmIn;

architecture behavioral of efmIn is
  constant counterBits: positive := 16;
  signal clkCounter: signed(counterBits - 1 downto 0) := to_signed(0, counterBits);
  
  constant nominalFrequency: unsigned(22 downto 0) := to_unsigned(4321800, 23);
  constant nominalAdd: signed(counterBits - 1 downto 0) := to_signed(to_integer(to_unsigned(2 ** counterBits, 25) * nominalFrequency / to_unsigned(clkFrequency, 48)), counterBits);
  
  signal lastIn: std_logic := 'U';
  
  type FilterArithmeticStage is (Inactive, KnownError, KnownErrorSum);
  signal arithmeticStage: FilterArithmeticStage := Inactive;
  signal error: signed(counterBits - 1 downto 0);
  signal errorSum: signed(23 downto 0) := to_signed(0, 24); 
  signal filterOut: signed(counterBits - 1 downto 0) := to_signed(0, counterBits);
begin
  process(clk)  
    variable toggleCount: integer range 0 to 127;
    variable togglePos: signed(counterBits - 1 downto 0);
    variable newCounter: signed(counterBits - 1 downto 0) := to_signed(0, counterBits);
  begin
    if clk = '1' and clk'event then
      newCounter := clkCounter + nominalAdd + filterOut;
      clkCounter <= newCounter;
      
      if (dataIn = '0' or dataIn = '1') and dataIn /= lastIn then
        toggleCount := toggleCount + 1;
        togglePos := clkCounter;
        lastIn <= dataIn;
      end if;
      
      outEn <= '0';
      if newCounter < clkCounter then 
        outEn <= '1';
        
        if toggleCount mod 2 = 1 then
          dataOut <= '1';
        else 
          dataOut <= '0';
        end if;
        
        if toggleCount = 1 then
          error <= -togglePos;
        else
          error <= to_signed(0, counterBits); -- leave errorSum unchanged
        end if;
        arithmeticStage <= KnownError;
        
        toggleCount := 0;
      end if;
      
      filterOut <= to_signed(0, counterBits);
      case arithmeticStage is
        when Inactive =>
        when KnownError =>
          if error > 0 and errorSum + error < errorSum then
            errorSum <= x"7FFFFF";
          elsif error < 0 and errorSum + error > errorSum then
            errorSum <= x"800000";
          else
            errorSum <= errorSum + error;
          end if;
          arithmeticStage <= KnownErrorSum;
        when KnownErrorSum =>
          filterOut <= error / 16 + to_signed(to_integer(errorSum(23 downto 14)), counterBits);
          arithmeticStage <= Inactive;
      end case;
    end if;
  end process;
end behavioral;
