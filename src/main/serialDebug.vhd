library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package serialDebug_types is
  -- This data is transmitted over serial as ABCD xx xx xx xx\n
  type DebugData is record
    text: string(1 to 4);
    value: std_logic_vector(31 downto 0);
  end record;
end package;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.serialDebug_types.all;

entity serialDebug is
  generic (
    clkFreq: natural
  );
  port (
    clk: in std_logic;
    tx_uart: out std_logic;
    inEn: in std_logic;
    dataIn: in DebugData;
    busy: out std_logic
  );
end serialDebug;

architecture rtl of serialDebug is
  constant numberOfOutputCharacters: natural := 17;
  signal data: DebugData;
  signal txByteIndex: natural range 0 to numberOfOutputCharacters := numberOfOutputCharacters; -- current byte being sent; max means we are done
  signal clockedBusy: std_logic;
  
  signal serialTxData: std_logic_vector(7 downto 0);
  signal serialTxEn: std_logic;
  signal serialBusy: std_logic;
begin
   serial: entity work.serialtx 
   generic map ( clkFreq => clkFreq )
   port map (
    clk => clk,
    txEn => serialTxEn,
    dataIn => serialTxData,
    busy => serialBusy,
    tx_uart => tx_uart
  );

  busy <= clockedBusy or inEn; -- set the busy flag asynchronously so that the writer sees it right away

  process (clk)
    variable c: std_logic_vector(7 downto 0);
    variable hexDigitIndex: natural range 0 to 7;
    variable tmp: natural range 0 to 15;
  begin
    if rising_edge(clk) then
      serialTxEn <= '0';
      if txByteIndex = numberOfOutputCharacters then
        clockedBusy <= '0';
        if inEn = '1' then
          clockedBusy <= '1';
          data <= dataIn;
          txByteIndex <= 0;
        end if;
      else
        if serialBusy = '0' then
          case txByteIndex is
            when 0 | 1 | 2 | 3 =>
              c := std_logic_vector(to_unsigned(character'pos(data.text(txByteIndex + 1)), 8));
            when 4 | 7 | 10 | 13 =>
              c := x"20";
            when 5 | 6 | 8 | 9 | 11 | 12 | 14 | 15 =>
              hexDigitIndex := (txByteIndex - 5) / 3 * 2 + ((txByteIndex - 5) mod 3);
              tmp := to_integer(unsigned(data.value(31 - hexDigitIndex * 4 downto 28 - hexDigitIndex * 4)));
              c := std_logic_vector(to_unsigned(character'pos('0') + tmp, 8)) when tmp < 10 else std_logic_vector(to_unsigned(character'pos('a') + tmp - 10, 8));
            when 16 =>
              c := x"0a";
            when others =>
          end case;
          serialTxData <= c;
          serialTxEn <= '1';
          txByteIndex <= txByteIndex + 1;
        end if;
      end if;
    end if;
  end process;
end rtl;
