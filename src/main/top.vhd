library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

Library UNISIM;
use UNISIM.vcomponents.all;

use work.reedSolomon_types.all;
use work.serialDebug_types.all;

entity top is
  port ( 
    CLK100MHZ: in std_logic;
    
    -- various stuff defined in the xdc file needs to be defined somewhere
    led: out std_logic_vector(3 downto 0);
    led0_g: out std_logic;
    led0_r: out std_logic;
    led0_b: out std_logic;
    led1_g: out std_logic;
    led1_r: out std_logic;
    led1_b: out std_logic;
    sw: in std_logic_vector(3 downto 0);
    btn: in std_logic_vector(3 downto 0);

    ac3In: in std_logic;
    ac3Mute: in std_logic;
    digitizedIn: in std_logic;

--    tx_mclk: out std_logic;
--    tx_lrck: out std_logic;
--    tx_sclk: out std_logic;
--    tx_data: out std_logic;
    tx_spdif: out std_logic;

    jd: out std_logic_vector(3 downto 0); -- debug output
    tx_uart: out std_logic
  );
end top;

architecture rtl of top is 
  -- switches for enabling / disabling reed solomon decoder
  signal c1Enabled: std_logic;
  signal c2Enabled: std_logic;
  signal ac3C1Done: std_logic;
  signal ac3C1Status: CompletionStatus;
  signal ac3C2Done: std_logic;
  signal ac3C2Status: CompletionStatus;
  signal efmC1Done: std_logic;
  signal efmC1Status: CompletionStatus;
  signal efmC2Done: std_logic;
  signal efmC2Status: CompletionStatus;
 
  -- reset logic  
  signal rst: std_logic;
  signal resetDone: std_logic := '0';
  
  -- switch for what to send to the spdif output
  signal ac3Enabled: std_logic; 
  signal ac3TxSpdif: std_logic;
  signal efmTxSpdif: std_logic;

  -- ac3 clock
  signal qpskClk: std_logic;  -- 2.88 * 16 MHz
  signal qpskClkLocked: std_logic;
  signal qpskClkFeedback: std_logic;

  -- various diagnostic outputs
  signal audioBufferFull: std_logic;
  signal sampleRdUnavail: std_logic;
  signal efmErasure: std_logic;
  signal ac3FrameMissing: std_logic;
  signal ac3FrameNoError: std_logic;
  signal ac3DpllErrorSum: signed(19 downto 0);
  
  -- debugging signals  
  signal ac3DebugSignal: std_logic_vector(3 downto 0);
  signal efmDebugSignal: std_logic_vector(3 downto 0);
  
  signal serialDebugData: DebugData;
  signal serialDebugTxEn: std_logic;
  signal serialDebugBusy: std_logic;
  
  signal ac3Led0R: std_logic;
  signal ac3Led0G: std_logic;
  signal ac3Led0B: std_logic;
  signal ac3Led1R: std_logic;
  signal ac3Led1G: std_logic;
  signal ac3Led1B: std_logic;
  signal efmLed0R: std_logic;
  signal efmLed0G: std_logic;
  signal efmLed0B: std_logic;
  signal efmLed1R: std_logic;
  signal efmLed1G: std_logic;
  signal efmLed1B: std_logic;
begin
  resetLogic: process (CLK100MHZ)
    variable i: unsigned(7 downto 0) := to_unsigned(0, 8);
  begin
    if rising_edge(CLK100MHZ) then
      rst <= '0';
      resetDone <= '0';
      if i > to_unsigned(10, 8) and i < to_unsigned(100, 8) then
        rst <= '1';
      end if;
      if i = "11111111" then
        resetDone <= '1';
      else
        i := i + 1;
      end if;
    end if;
  end process;
  
  led(0) <= '0' when ac3Enabled else ac3Mute;
  led(1) <= '0' when ac3Enabled else audioBufferFull;
  led(2) <= ac3FrameMissing when ac3Enabled else sampleRdUnavail;
  led(3) <= ac3FrameNoError when ac3Enabled else '0';
  
  c1Enabled <= sw(1);
  c2Enabled <= sw(2);
  ac3Enabled <= sw(3);
  jd(3 downto 0) <= ac3debugSignal when ac3Enabled else efmDebugSignal;
  tx_spdif <= ac3TxSpdif when ac3Enabled else efmTxSpdif;
  
  led0_r <= ac3Led0R when ac3Enabled else efmLed0R;
  led0_g <= ac3Led0G when ac3Enabled else efmLed0G;
  led0_b <= ac3Led0B when ac3Enabled else efmLed0B;
  led1_r <= ac3Led1R when ac3Enabled else efmLed1R;
  led1_g <= ac3Led1G when ac3Enabled else efmLed1G;
  led1_b <= ac3Led1B when ac3Enabled else efmLed1B;
    
  efmmain: entity work.efmmain port map (
    CLK100MHZ => CLK100MHZ,
    rst => rst,
    resetDone => resetDone,
    efmNrzIn => digitizedIn,
    audioBufferFull => audioBufferFull,
    efmErasuresEnabled => sw(0),
    efmErasure => efmErasure,
    c1Enable => c1Enabled,
    c1Done => efmC1Done,
    c1Status => efmC1Status,
    c2Enable => c2Enabled,
    c2Done => efmC2Done,
    c2Status => efmC2Status,
    tx_sclk => open, -- tx_sclk, -- the analog audio is always driven from EFM
    tx_mclk => open, -- tx_mclk, -- would be nice to convert ac3 data to analog audio but that is a complex project...
    tx_lrck => open, -- tx_lrck,
    tx_data => open, -- tx_data,
    tx_spdif => efmTxSpdif,
    debug => efmDebugSignal
  );
  
  -- qpskClk generated at 2.88 * 16 MHz (not exact -- difference of 1569 Hz)
  MMCME2_BASE_ac3_inst: MMCME2_BASE generic map (
    CLKFBOUT_MULT_F => 11.75, -- Multiply value for all CLKOUT (2.000-64.000).
    CLKIN1_PERIOD => 10.0, -- Input clock period in ns to ps resolution (i.e. 33.333 is 30 MHz).
    CLKOUT0_DIVIDE_F => 25.50, 
    DIVCLK_DIVIDE => 1, -- Master division value (1-106)
    CLKOUT1_DIVIDE => 1
  )
  port map (
      CLKIN1 => CLK100MHZ,
      CLKFBOUT => qpskClkFeedback,
      CLKFBIN => qpskClkFeedback,
      CLKOUT0 => qpskClk,
      LOCKED => qpskClkLocked, 
      RST => '0',
      PWRDWN => '0'
  );
  
  ac3main: entity work.ac3main port map (
    qpskClk => qpskClk,
    ac3In => ac3In,
    tx_spdif => ac3TxSpdif,
    frameNoError => ac3FrameNoError,
    c1Enable => c1Enabled,
    c1Done => ac3C1Done,
    c1Status => ac3C1Status,
    c2Enable => c2Enabled,
    c2Done => ac3C2Done,
    c2Status => ac3C2Status,
    debugPins => ac3DebugSignal,
    dpllErrorSumDebug => ac3DpllErrorSum
  );
  
  -- code below is for diagnostics and debugging
  
  updateAc3LEDs: process (qpskClk)
    variable c1timer: natural range 0 to 1023 := 0;
    variable c2timer: natural range 0 to 1023 := 0;
  begin
    if rising_edge(qpskClk) then
      if c1timer /= 0 then
        c1timer := c1timer - 1;
      else
        ac3Led0R <= '0';
        ac3Led0G <= '0';
        ac3Led0B <= '0';
      end if;
      if ac3C1Done = '1' then
        case ac3C1Status is
          when NoErrors =>
          when CorrectedOneError =>
            ac3Led0G <= '1';
          when CorrectedTwoErrors =>
            ac3Led0B <= '1';
          when CorrectedErasures => -- won't happen
            ac3Led0G <= '1';
          when Failed =>
            ac3Led0R <= '1';
          when others =>
        end case;
        c1timer := 1023; 
      end if;
      
      if c2timer /= 0 then
        c2timer := c2timer - 1;
      else
        ac3Led1R <= '0';
        ac3Led1G <= '0';
        ac3Led1B <= '0';
      end if;
      if ac3C2Done = '1' then
        case ac3C2Status is
          when NoErrors =>
          when CorrectedOneError =>
            ac3Led1G <= '1';           
          when CorrectedTwoErrors =>
            ac3Led1B <= '1';
          when CorrectedErasures =>
            ac3Led0G <= '1';
          when Failed =>
            ac3Led1R <= '1';
          when others =>
        end case;
        c2timer := 1023; 
      end if;
    end if;
  end process;
  
  updateEfmLEDs: process (CLK100MHZ)
    variable c1timer: natural range 0 to 2047 := 0;
    variable c2timer: natural range 0 to 2047 := 0;
  begin
    if rising_edge(CLK100MHZ) then
      if c1timer /= 0 then
        c1timer := c1timer - 1;
      else
        efmLed0R <= '0';
        efmLed0G <= '0';
        efmLed0B <= '0';
      end if;
      if efmC1Done = '1' then
        case efmC1Status is
          when NoErrors =>
          when CorrectedOneError =>
            efmLed0G <= '1';           
          when CorrectedTwoErrors =>
            efmLed0B <= '1';
          when CorrectedErasures => -- won't happen
            efmLed1G <= '1';
          when Failed =>
            efmLed0R <= '1';
          when others =>
        end case;
        c1timer := 2047; 
      end if;

      if c2timer /= 0 then
        c2timer := c2timer - 1;
      else
        efmLed1R <= '0';
        efmLed1G <= '0';
        efmLed1B <= '0';
      end if;
      if efmC2Done = '1' then
        case efmC2Status is
          when NoErrors =>
          when CorrectedOneError =>
            efmLed1G <= '1';
          when CorrectedTwoErrors =>
            efmLed1B <= '1';
          when CorrectedErasures => 
            efmLed1G <= '1';
          when Failed =>
            efmLed1R <= '1';
          when others =>
        end case;
        c2timer := 2047; 
      end if;
    end if;
  end process;
  
  debugAc3OverSerial: if true generate
  begin  
    serialDebug: entity work.serialDebug 
    generic map ( clkFreq => 46080000 )
    port map (
      clk => qpskClk,
      tx_uart => tx_uart,
      inEn => serialDebugTxEn,
      dataIn => serialDebugData,
      busy => serialDebugBusy
    );
    
    ac3statemachine: block 
      type States is (Idle, C1Single, C1Dual, C1Fail, C1Tot, C2Single, C2Dual, C2Erasures, C2Fail, C2Tot, Dpll);
      signal state: States := Idle;
    begin
      process(qpskClk)
        variable counter: natural range 0 to 46079999 := 46079999; -- one second at qpsk frequency
        variable c1SingleCorrections: natural range 0 to 2047; -- theoretical max is 66 * 25 = 1650
        variable c1DualCorrections: natural range 0 to 2047;
        variable c1Failures: natural range 0 to 2047;
        variable c1Total: natural range 0 to 2047;
        variable c2SingleCorrections: natural range 0 to 2047;
        variable c2DualCorrections: natural range 0 to 2047;
        variable c2ErasureCorrections: natural range 0 to 2047;
        variable c2Failures: natural range 0 to 2047;
        variable c2Total: natural range 0 to 2047;
      begin
        if rising_edge(qpskClk) then
          if counter = 0 then
            counter := 46079999;
            state <= C1Single;
          else
            counter := counter - 1;
          end if;
          
          serialDebugTxEn <= '0';
          if serialDebugBusy = '0' then
            case state is
              when Idle =>
              when C1Single =>
                serialDebugData.text <= "C11C";
                serialDebugData.value <= std_logic_vector(to_unsigned(c1SingleCorrections, 32));
                serialDebugTxEn <= '1';
                c1SingleCorrections := 0;
                state <= C1Dual;
              when C1Dual =>
                serialDebugData.text <= "C12C";
                serialDebugData.value <= std_logic_vector(to_unsigned(c1DualCorrections, 32));
                serialDebugTxEn <= '1';
                c1DualCorrections := 0;
                state <= C1Fail;
              when C1Fail =>
                serialDebugData.text <= "C1 F";
                serialDebugData.value <= std_logic_vector(to_unsigned(c1Failures, 32));
                serialDebugTxEn <= '1';
                c1Failures := 0;
                state <= C1Tot;
              when C1Tot =>
                serialDebugData.text <= "TOT1";
                serialDebugData.value <= std_logic_vector(to_unsigned(c1Total, 32));
                serialDebugTxEn <= '1';
                c1Total := 0;
                state <= C2Single;
              when C2Single =>
                serialDebugData.text <= "C21C";
                serialDebugData.value <= std_logic_vector(to_unsigned(c2SingleCorrections, 32));
                serialDebugTxEn <= '1';
                c2SingleCorrections := 0;
                state <= C2Dual;
              when C2Dual =>
                serialDebugData.text <= "C22C";
                serialDebugData.value <= std_logic_vector(to_unsigned(c2DualCorrections, 32));
                serialDebugTxEn <= '1';
                c2DualCorrections := 0;
                state <= C2Erasures;
              when C2Erasures =>
                serialDebugData.text <= "C2 E";
                serialDebugData.value <= std_logic_vector(to_unsigned(c2ErasureCorrections, 32));
                serialDebugTxEn <= '1';
                c2ErasureCorrections := 0;
                state <= C2Fail;
              when C2Fail =>
                serialDebugData.text <= "C2 F";
                serialDebugData.value <= std_logic_vector(to_unsigned(c2Failures, 32));
                serialDebugTxEn <= '1';
                c2Failures := 0;
                state <= C2Tot;
              when C2Tot =>
                serialDebugData.text <= "TOT2";
                serialDebugData.value <= std_logic_vector(to_unsigned(c2Total, 32));
                serialDebugTxEn <= '1';
                c2Total := 0;
                state <= Dpll;
              when Dpll =>
                serialDebugData.text <= "DPLL"; -- the error sum is from the wrong clock domain but it changes rarely so no big risk
                serialDebugData.value <= std_logic_vector(resize(ac3DpllErrorSum, 32));
                serialDebugTxEn <= '1';
                state <= Idle;
            end case;
          end if;
     
          if ac3C1Done = '1' then
            c1Total := c1Total + 1;
            case ac3C1Status is
              when CorrectedOneError => c1SingleCorrections := c1SingleCorrections + 1;
              when CorrectedTwoErrors => c1DualCorrections := c1DualCorrections + 1;
              when CorrectedErasures =>
              when Failed => c1Failures := c1Failures + 1;
              when others =>
            end case;
          end if;      
          if ac3C2Done = '1' then
            c2Total := c2Total + 1;
            case ac3C2Status is
              when CorrectedOneError => c2SingleCorrections := c2SingleCorrections + 1;
              when CorrectedTwoErrors => c2DualCorrections := c2DualCorrections + 1;
              when CorrectedErasures => c2ErasureCorrections := c2ErasureCorrections + 1;
              when Failed => c2Failures := c2Failures + 1;
              when others =>
            end case;
          end if;      
        end if;
      end process;
    end block;
  end generate;
  
  debugEfmOverSerial: if false generate
  begin
    serialDebug: entity work.serialDebug 
    generic map ( clkFreq => 100000000 )
    port map (
      clk => CLK100MHZ,
      tx_uart => tx_uart,
      inEn => serialDebugTxEn,
      dataIn => serialDebugData,
      busy => serialDebugBusy
    );
      
    process(CLK100MHZ)
      variable counter: natural range 0 to 99999999 := 99999999; -- one second at 100 MHz frequency
      variable c1SingleCorrections: natural range 0 to 8191; -- theoretical max is 44100 / 6 = 7350
      variable c1DualCorrections: natural range 0 to 8191;
      variable c1Failures: natural range 0 to 8191;
      variable c2SingleCorrections: natural range 0 to 8191;
      variable c2DualCorrections: natural range 0 to 8191;
      variable c2Failures: natural range 0 to 8191;
      variable rsTotal: natural range 0 to 8191;
      variable c1singlePending: boolean := false;
      variable c1dualPending: boolean := false;
      variable c1failuresPending: boolean := false;
      variable c2singlePending: boolean := false;
      variable c2dualPending: boolean := false;
      variable c2failuresPending: boolean := false;
      variable totalPending: boolean := false;
    begin
      if rising_edge(CLK100MHZ) then
        if counter = 0 then
          counter := 99999999;
          c1singlePending := true;
          c1dualPending := true;
          c1failuresPending := true;
          c2singlePending := true;
          c2dualPending := true;
          c2failuresPending := true;
          totalPending := true;
        else
          counter := counter - 1;
        end if;
        
        serialDebugTxEn <= '0';
        if serialDebugBusy = '0' then
          if c1singlePending then
            serialDebugData.text <= "C11C";
            serialDebugData.value <= std_logic_vector(to_unsigned(c1SingleCorrections, 32));
            serialDebugTxEn <= '1';
            c1singlePending := false;
            c1SingleCorrections := 0;
          elsif c1dualPending then
            serialDebugData.text <= "C12C";
            serialDebugData.value <= std_logic_vector(to_unsigned(c1DualCorrections, 32));
            serialDebugTxEn <= '1';
            c1dualPending := false;
            c1DualCorrections := 0;
          elsif c1failuresPending then
            serialDebugData.text <= "C1 F";
            serialDebugData.value <= std_logic_vector(to_unsigned(c1Failures, 32));
            serialDebugTxEn <= '1';
            c1failuresPending := false;
            c1Failures := 0;
          elsif c2singlePending then
            serialDebugData.text <= "C21C";
            serialDebugData.value <= std_logic_vector(to_unsigned(c2SingleCorrections, 32));
            serialDebugTxEn <= '1';
            c2singlePending := false;
            c2SingleCorrections := 0;
          elsif c2dualPending then
            serialDebugData.text <= "C22C";
            serialDebugData.value <= std_logic_vector(to_unsigned(c2DualCorrections, 32));
            serialDebugTxEn <= '1';
            c2dualPending := false;
            c2DualCorrections := 0;
          elsif c2failuresPending then
            serialDebugData.text <= "C2 F";
            serialDebugData.value <= std_logic_vector(to_unsigned(c2Failures, 32));
            serialDebugTxEn <= '1';
            c2failuresPending := false;
            c2Failures := 0;
          elsif totalPending then
            serialDebugData.text <= "TOT ";
            serialDebugData.value <= std_logic_vector(to_unsigned(rsTotal, 32));
            serialDebugTxEn <= '1';
            totalPending := false;
            rsTotal := 0;
          end if;
        end if;
  
        if efmC1Done = '1' then
          rsTotal := rsTotal + 1;
          case efmC1Status is
            when CorrectedOneError => c1SingleCorrections := c1SingleCorrections + 1;
            when CorrectedTwoErrors => c1DualCorrections := c1DualCorrections + 1;
            when Failed => c1Failures := c1Failures + 1;
            when others =>
          end case;
        end if;      
        if efmC2Done = '1' then
          case efmC2Status is
            when CorrectedOneError => c2SingleCorrections := c2SingleCorrections + 1;
            when CorrectedTwoErrors => c2DualCorrections := c2DualCorrections + 1;
            when Failed => c2Failures := c2Failures + 1;
            when others =>
          end case;
        end if;      
      end if;
    end process;
  end generate;
end rtl;
