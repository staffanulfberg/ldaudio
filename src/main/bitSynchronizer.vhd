library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity bitSynchronizer is
  port (
    dataIn: in std_logic;
    dstClk: in std_logic;
    dataOut: out std_logic
  );
end bitSynchronizer;

architecture behavioral of bitSynchronizer is
  signal r1: std_logic;
  signal r2: std_logic;
  
  attribute ASYNC_REG: string;
  attribute ASYNC_REG of r1: signal is "TRUE";
  attribute ASYNC_REG of r2: signal is "TRUE";
begin
  process(dstClk)
  begin
    if rising_edge(dstClk) then
      r1 <= dataIn;
      r2 <= r1;
    end if;
  end process;

  dataOut <= r2;
end behavioral;
