set origin_dir "."
set _xil_proj_name_ "project"

create_project ${_xil_proj_name_} ./${_xil_proj_name_} -part xc7a35ticsg324-1L
set proj_dir [get_property directory [current_project]]

# Set project properties
set obj [current_project]
set_property -name "board_part" -value "digilentinc.com:arty-a7-35:part0:1.0" -objects $obj
set_property -name "corecontainer.enable" -value "1" -objects $obj
set_property -name "default_lib" -value "xil_defaultlib" -objects $obj
set_property -name "enable_vhdl_2008" -value "1" -objects $obj
set_property -name "ip_cache_permissions" -value "read write" -objects $obj
set_property -name "ip_output_repo" -value "$proj_dir/${_xil_proj_name_}.cache/ip" -objects $obj
set_property -name "mem.enable_memory_map_generation" -value "1" -objects $obj
set_property -name "platform.board_id" -value "arty-a7-35" -objects $obj
set_property -name "sim.central_dir" -value "$proj_dir/${_xil_proj_name_}.ip_user_files" -objects $obj
set_property -name "sim.ip.auto_export_scripts" -value "1" -objects $obj
set_property -name "simulator_language" -value "Mixed" -objects $obj
set_property -name "source_mgmt_mode" -value "DisplayOnly" -objects $obj
set_property -name "target_language" -value "VHDL" -objects $obj
set_property -name "webtalk.activehdl_export_sim" -value "21" -objects $obj
set_property -name "webtalk.ies_export_sim" -value "21" -objects $obj
set_property -name "webtalk.modelsim_export_sim" -value "21" -objects $obj
set_property -name "webtalk.questa_export_sim" -value "21" -objects $obj
set_property -name "webtalk.riviera_export_sim" -value "21" -objects $obj
set_property -name "webtalk.vcs_export_sim" -value "21" -objects $obj
set_property -name "webtalk.xsim_export_sim" -value "21" -objects $obj
set_property -name "webtalk.xsim_launch_sim" -value "187" -objects $obj
set_property -name "xpm_libraries" -value "XPM_MEMORY" -objects $obj

# Populate the 'sources_1' fileset
set obj [get_filesets sources_1]
set files [list \
 [file normalize "${origin_dir}/src/main/serialtx.vhd"] \
 [file normalize "${origin_dir}/src/main/serialDebug.vhd"] \
 [file normalize "${origin_dir}/src/main/resetSynchronizer.vhd"] \
 [file normalize "${origin_dir}/src/main/bitSynchronizer.vhd"] \
 [file normalize "${origin_dir}/src/main/multiBitSynchronizer.vhd"] \
 [file normalize "${origin_dir}/src/ip/blk_mem_gen_efm.xcix"] \
 [file normalize "${origin_dir}/src/main/rs/inv.vhd"] \
 [file normalize "${origin_dir}/src/main/rs/log.vhd"] \
 [file normalize "${origin_dir}/src/main/rs/invFcr.vhd"] \
 [file normalize "${origin_dir}/src/main/rs/mult.vhd"] \
 [file normalize "${origin_dir}/src/main/rs/reedSolomon_types.vhd"] \
 [file normalize "${origin_dir}/src/main/rs/syndrome.vhd"] \
 [file normalize "${origin_dir}/src/main/rs/reedSolomon.vhd"] \
 [file normalize "${origin_dir}/src/main/efm/audio.vhd"] \
 [file normalize "${origin_dir}/src/main/efm/delayLineArray.vhd"] \
 [file normalize "${origin_dir}/src/main/efm/circBuffer.vhd"] \
 [file normalize "${origin_dir}/src/main/efm/efm.vhd"] \
 [file normalize "${origin_dir}/src/main/efm/efmIn.vhd"] \
 [file normalize "${origin_dir}/src/main/efm/efmmain.vhd"] \
 [file normalize "${origin_dir}/src/main/dpll.vhd"] \
 [file normalize "${origin_dir}/src/main/ac3/ac3BlockBuffer.vhd"] \
 [file normalize "${origin_dir}/src/main/ac3/ac3DeinterleaveBuffer.vhd"] \
 [file normalize "${origin_dir}/src/main/ac3/ac3Deinterleave.vhd"] \
 [file normalize "${origin_dir}/src/main/ac3/ac3InputFraming.vhd"] \
 [file normalize "${origin_dir}/src/main/ac3/qpskIn.vhd"] \
 [file normalize "${origin_dir}/src/main/ac3/qpskReclock.vhd"] \
 [file normalize "${origin_dir}/src/main/ac3/ac3spdif.vhd"] \
 [file normalize "${origin_dir}/src/main/ac3/ac3main.vhd"] \
 [file normalize "${origin_dir}/src/main/top.vhd"] \
]
add_files -norecurse -fileset $obj $files

set file "$origin_dir/src/ip/blk_mem_gen_efm/blk_mem_gen_efm.xci"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sources_1] [list "*$file"]]
set_property -name "generate_files_for_reference" -value "0" -objects $file_obj
if { ![get_property "is_locked" $file_obj] } {
  set_property -name "generate_synth_checkpoint" -value "0" -objects $file_obj
}
set_property -name "registered_with_manager" -value "1" -objects $file_obj

set file_obj [get_files -of_objects [get_filesets sources_1] [list "*/main/*.vhd"]]
set_property -name "file_type" -value "VHDL 2008" -objects $file_obj

set obj [get_filesets sources_1]
set_property -name "top" -value "top" -objects $obj
set_property -name "top_auto_set" -value "0" -objects $obj

# Populate the 'constrs_1' fileset
set obj [get_filesets constrs_1]

set file "[file normalize "$origin_dir/src/xdc/arty.xdc"]"
set file_added [add_files -norecurse -fileset $obj [list $file]]
set file "$origin_dir/src/xdc/arty.xdc"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets constrs_1] [list "*$file"]]
set_property -name "file_type" -value "XDC" -objects $file_obj

set obj [get_filesets constrs_1]
set_property -name "target_constrs_file" -value "[file normalize "$origin_dir/src/xdc/arty.xdc"]" -objects $obj
set_property -name "target_part" -value "xc7a35ticsg324-1L" -objects $obj
set_property -name "target_ucf" -value "[file normalize "$origin_dir/src/xdc/arty.xdc"]" -objects $obj

# Create 'sim_efm' fileset
create_fileset -simset sim_efm
set obj [get_filesets sim_efm]
set files [list \
 [file normalize "${origin_dir}/src/sim/efmtest.vhd"] \
 [file normalize "${origin_dir}/data/jp-62.5MHz.efmbits"] \
 [file normalize "${origin_dir}/wcfg/efmtest_behav.wcfg" ]\
]
add_files -norecurse -fileset $obj $files

set file_obj [get_files -of_objects [get_filesets sim_efm] [list "*/sim/*.vhd"]]
set_property -name "file_type" -value "VHDL 2008" -objects $file_obj

set obj [get_filesets sim_efm]
set_property -name "top" -value "efmtest" -objects $obj
set_property -name "top_auto_set" -value "0" -objects $obj
set_property -name "xsim.simulate.runtime" -value "900ms" -objects $obj

# Create 'sim_audio' fileset
create_fileset -simset sim_audio
set obj [get_filesets sim_audio]
set files [list \
 [file normalize "${origin_dir}/src/main/efm/audio.vhd"] \
 [file normalize "${origin_dir}/src/sim/audiotest.vhd"] \
 [file normalize "${origin_dir}/wcfg/audiotest_behav.wcfg"] \
]
add_files -norecurse -fileset $obj $files

set file "$origin_dir/src/main/efm/audio.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sim_audio] [list "*$file"]]
set_property -name "file_type" -value "VHDL 2008" -objects $file_obj

set file "$origin_dir/src/sim/audiotest.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sim_audio] [list "*$file"]]
set_property -name "file_type" -value "VHDL 2008" -objects $file_obj

set obj [get_filesets sim_audio]
set_property -name "source_set" -value "" -objects $obj
set_property -name "top" -value "audiotest" -objects $obj
set_property -name "top_auto_set" -value "0" -objects $obj
set_property -name "xsim.simulate.runtime" -value "100ms" -objects $obj

# Create 'sim_efmin' fileset
create_fileset -simset sim_efmin
set obj [get_filesets sim_efmin]
set files [list \
 [file normalize "${origin_dir}/src/sim/efmInTest.vhd"] \
 [file normalize "${origin_dir}/data/jp-62.5MHz.b.bin"] \
]
add_files -norecurse -fileset $obj $files

set files [list \
 [file normalize "${origin_dir}/wcfg/efmInTest_behav.wcfg" ]\
]
set imported_files [import_files -fileset sim_efmin $files]

set file "$origin_dir/src/sim/efmInTest.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sim_efmin] [list "*$file"]]
set_property -name "file_type" -value "VHDL 2008" -objects $file_obj

set file "$origin_dir/data/jp-62.5MHz.b.bin"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sim_efmin] [list "*$file"]]

set obj [get_filesets sim_efmin]
set_property -name "top" -value "efmInTest" -objects $obj
set_property -name "top_auto_set" -value "0" -objects $obj
set_property -name "top_lib" -value "xil_defaultlib" -objects $obj
set_property -name "xsim.simulate.runtime" -value "900ms" -objects $obj

# Create 'sim_rs' fileset
create_fileset -simset sim_rs

set obj [get_filesets sim_rs]
set files [list \
 [file normalize "${origin_dir}/src/sim/reedSolomonTest.vhd"] \
]
add_files -norecurse -fileset $obj $files

set files [list \
 [file normalize "${origin_dir}/wcfg/reedSolomonTest_behav.wcfg" ]\
]
set imported_files [import_files -fileset sim_rs $files]

set file "$origin_dir/src/sim/reedSolomonTest.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sim_rs] [list "*$file"]]
set_property -name "file_type" -value "VHDL 2008" -objects $file_obj

set obj [get_filesets sim_rs]
set_property -name "top" -value "reedSolomonTest" -objects $obj
set_property -name "top_auto_set" -value "0" -objects $obj
set_property -name "top_lib" -value "xil_defaultlib" -objects $obj
set_property -name "xsim.simulate.runtime" -value "900ms" -objects $obj

# Create sim_ac3 fileset
create_fileset -simset sim_ac3

set obj [get_filesets sim_ac3]
set files [list \
 [file normalize "${origin_dir}/src/sim/ac3test.vhd"] \
 [file normalize "${origin_dir}/data/alien-symbols.txt"] \
]
add_files -norecurse -fileset $obj $files

set files [list \
 [file normalize "${origin_dir}/wcfg/ac3test_behav.wcfg" ]\
]
set imported_files [import_files -fileset sim_ac3 $files]

set file "$origin_dir/src/sim/ac3test.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sim_ac3] [list "*$file"]]
set_property -name "file_type" -value "VHDL 2008" -objects $file_obj

set obj [get_filesets sim_ac3]
set_property -name "top" -value "ac3test" -objects $obj
set_property -name "top_auto_set" -value "0" -objects $obj
set_property -name "xsim.simulate.runtime" -value "900ms" -objects $obj

# Create 'sim_delayline' fileset
create_fileset -simset sim_delayline
set obj [get_filesets sim_delayline]
set files [list \
 [file normalize "${origin_dir}/src/sim/delayBufferArrayTest.vhd"] \
 [file normalize "${origin_dir}/wcfg/delayLineArrayTest_behav.wcfg"] \
]
add_files -norecurse -fileset $obj $files

set file "$origin_dir/src/sim/delayBufferArrayTest.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sim_delayline] [list "*$file"]]
set_property -name "file_type" -value "VHDL 2008" -objects $file_obj

set obj [get_filesets sim_delayline]
set_property -name "top" -value "delayLineArrayTest" -objects $obj
set_property -name "top_auto_set" -value "0" -objects $obj
set_property -name "top_lib" -value "xil_defaultlib" -objects $obj
set_property -name "xsim.simulate.runtime" -value "900ms" -objects $obj
