package rs

import org.scalacheck.Gen.const
import org.scalacheck.{Gen, Prop, Properties}
import gfnumeric.GF
import gfnumeric.GFNumeric
import gfnumeric.GFNumeric.*
import circ.{C1, C2}

object ReedSolomonTest extends Properties("ReedSolomon") {
  property("c1 corrects one error") = {
    import gfnumeric.GF256_11d.given
    val num = summon[GFNumeric[GF[256]]]
    import num.*
    val c1decoder = C1(true)

    val data = (0 until 32).map(_ => GF256WithValue(zero): GF256WithStatus).toArray
    val gen = for { pos <- Gen.chooseNum(1, 1)
                    v <- Gen.chooseNum(1, 255) }
      yield (pos, v)

    Prop.forAll(gen) { case (pos, value) =>
      data(pos) = GF256WithValue(GF[256](value))
      c1decoder.doDecode(data)
      data.forall(d => d.v == zero && d.ok)
    }
  }

  property("c1 corrects two errors") = {
    import gfnumeric.GF256_11d.given
    val num = summon[GFNumeric[GF[256]]]
    import num.*
    val c1decoder = C1(true)

    val data = (0 until 32).map(_ => GF256WithValue(zero): GF256WithStatus).toArray
    val gen = for { pos <- Gen.chooseNum(0, 31)
                    v <- Gen.chooseNum(1, 255) }
      yield (pos, v)

    Prop.forAll(Gen.listOfN(2, gen).retryUntil(l => l.size == 2 && l.head._1 != l.last._1)) {
      case (p1, v1) :: (p2, v2) :: Nil =>
        data(p1) = GF256WithValue(GF[256](v1))
        data(p2) = GF256WithValue(GF[256](v2))
        c1decoder.doDecode(data)
        data.forall(d => d.v == zero && d.ok): Prop
      case _ => Prop.falsified // should not get here
    }
  }

  property("c2 corrects two erasures") = {
    import gfnumeric.GF256_11d.given
    val num = summon[GFNumeric[GF[256]]]
    import num.*
    val c2decoder = C2(true)

    val data = (0 until 28).map(_ => GF256WithValue(zero): GF256WithStatus).toArray
    val gen = for {pos <- Gen.chooseNum(0, 27)
                   v <- Gen.chooseNum(1, 255)}
    yield (pos, v)

    Prop.forAll(Gen.listOfN(2, gen).retryUntil(l => l.size == 2 && l.head._1 != l.last._1)) {
      case (p1, v1) :: (p2, v2) :: Nil =>
        data(p1) = ErasedGF256(GF[256](v1))
        data(p2) = ErasedGF256(GF[256](v2))
        c2decoder.doDecode(data)
        data.forall(d => d.v == zero && d.ok): Prop
      case _ => Prop.falsified // should not get here
    }
  }

  property("c2 corrects two erasures where only one is error") = {
    import gfnumeric.GF256_11d.given
    val num = summon[GFNumeric[GF[256]]]
    import num.*
    val c2decoder = C2(true)

    val data = (0 until 28).map(_ => GF256WithValue(zero): GF256WithStatus).toArray
    val gen = for {pos <- Gen.chooseNum(0, 27)
                   v <- Gen.chooseNum(1, 255)}
    yield (pos, v)

    Prop.forAll(Gen.listOfN(2, gen).retryUntil(l => l.size == 2 && l.head._1 != l.last._1)) {
      case (p1, v1) :: (p2, _) :: Nil =>
        data(p1) = ErasedGF256(GF[256](v1))
        data(p2) = data(p2).erased
        c2decoder.doDecode(data)
        data.forall(d => d.v == zero && d.ok): Prop
      case _ => Prop.falsified // should not get here
    }
  }

  property("c2 corrects three erasures") = {
    import gfnumeric.GF256_11d.given
    val num = summon[GFNumeric[GF[256]]]
    import num.*
    val c2decoder = C2(true)

    val data = (0 until 28).map(_ => GF256WithValue(zero): GF256WithStatus).toArray
    val gen = for { pos <- Gen.chooseNum(0, 27)
                    v <- Gen.chooseNum(1, 255) }
      yield (pos, v)

    Prop.forAll(Gen.listOfN(3, gen).retryUntil(l => l.size == 3 && l.map(_._1).toSet.size == 3)) {
      case (p1, v1) :: (p2, v2) :: (p3, v3) :: Nil =>
        data(p1) = ErasedGF256(GF[256](v1))
        data(p2) = ErasedGF256(GF[256](v2))
        data(p3) = ErasedGF256(GF[256](v3))
        c2decoder.doDecode(data)
        data.forall(d => d.v == zero && d.ok): Prop
      case _ => Prop.falsified // should not get here
    }
  }

  property("c2 corrects one error and two erasures") = {
    import gfnumeric.GF256_11d.given
    val num = summon[GFNumeric[GF[256]]]
    import num.*
    val c2decoder = C2(true)

    val data = (0 until 28).map(_ => GF256WithValue(zero): GF256WithStatus).toArray
    val gen = for {pos <- Gen.chooseNum(0, 27)
                   v <- Gen.chooseNum(1, 255)}
    yield (pos, v)

    Prop.forAll(Gen.listOfN(3, gen).retryUntil(l => l.size == 3 && l.map(_._1).toSet.size == 3)) {
      case (p1, v1) :: (p2, v2) :: (p3, v3) :: Nil =>
        data(p1) = ErasedGF256(GF[256](v1))
        data(p2) = ErasedGF256(GF[256](v2))
        data(p3) = GF256WithValue(GF[256](v3))
        c2decoder.doDecode(data)
        data.forall(d => d.v == zero && d.ok): Prop
      case _ => Prop.falsified // should not get here
    }
  }

  property("ReedSolomon size (36,32) for AC3 computes zero syndrome for correct codeword") = {
    import gfnumeric.GF256_187.given
    val num = summon[GFNumeric[GF[256]]]
    import num.*
    val rs = ReedSolomon(36, 32, 120, true)
    val data = IndexedSeq(
      0x10, 0x00, 0xa3, 0xe5,
      0x14, 0x2b, 0x88, 0xfb,
      0xa0, 0x8a, 0x95, 0x94,
      0x8d, 0x30, 0x19, 0xe8,
      0x5f, 0x7a, 0x44, 0x74,
      0x58, 0xd6, 0x1d, 0x2d,
      0xd5, 0xbb, 0xd6, 0x36,
      0x3d, 0x17, 0x24, 0xe8,
      0x39, 0xb9, 0xa9, 0xe0).map(b => GF256WithValue(GF[256](b)))
    val decoded = rs.decode(data)
    Prop(rs.statistics("input ok") == 1)
  }

  property("ReedSolomon size (36,32) for AC3 corrects one error") = {
    import gfnumeric.GF256_187.given
    val num = summon[GFNumeric[GF[256]]]
    import num.*
    val rs = ReedSolomon(36, 32, 120, true)

    val data = (0 until 36).map(_ => GF256WithValue(zero): GF256WithStatus).toArray
    val gen = for { pos <- Gen.chooseNum(1, 1)
                    v <- Gen.chooseNum(1, 255) }
    yield (pos, v)

    Prop.forAll(gen) { case (pos, value) =>
      data(pos) = GF256WithValue(GF[256](value))
      rs.doDecode(data)
      data.forall(d => d.v == zero && d.ok)
    }
  }

  property("ReedSolomon size (36,32) for AC3 corrects two errors") = {
    import gfnumeric.GF256_187.given
    val num = summon[GFNumeric[GF[256]]]
    import num.*
    val rs = ReedSolomon(36, 32, 120, true)

    val data = (0 until 36).map(_ => GF256WithValue(zero): GF256WithStatus).toArray
    val gen = for { pos <- Gen.chooseNum(0, 35)
                    v <- Gen.chooseNum(1, 255) }
    yield (pos, v)

    Prop.forAll(Gen.listOfN(2, gen).retryUntil(l => l.size == 2 && l.head._1 != l.last._1)) {
      case (p1, v1) :: (p2, v2) :: Nil =>
        data(p1) = GF256WithValue(GF[256](v1))
        data(p2) = GF256WithValue(GF[256](v2))
        rs.doDecode(data)
        data.forall(d => d.v == zero && d.ok): Prop
      case _ => Prop.falsified // should not get here
    }
  }

  property("ReedSolomon size (36,32) for AC3 corrects one erasure") = {
    import gfnumeric.GF256_187.given
    val num = summon[GFNumeric[GF[256]]]
    import num.*
    val rs = ReedSolomon(36, 32, 120, true)

    val data = (0 until 36).map(_ => GF256WithValue(zero): GF256WithStatus).toArray
    val gen = for { pos <- Gen.chooseNum(0, 35)
                    v <- Gen.chooseNum(1, 255) }
    yield (pos, v)

    Prop.forAll(Gen.listOfN(1, gen).retryUntil(l => l.size == 1)) {
      case (p1, v1):: Nil =>
        data(p1) = ErasedGF256(GF[256](v1))
        rs.doDecode(data)
        data.forall(d => d.v == zero && d.ok): Prop
      case _ => Prop.falsified // should not get here
    }
  }

  property("ReedSolomon size (36,32) for AC3 corrects two erasures") = {
    import gfnumeric.GF256_187.given
    val num = summon[GFNumeric[GF[256]]]
    import num.*
    val rs = ReedSolomon(36, 32, 120, true)

    val data = (0 until 36).map(_ => GF256WithValue(zero): GF256WithStatus).toArray
    val gen = for {pos <- Gen.chooseNum(0, 35)
                   v <- Gen.chooseNum(1, 255)}
    yield (pos, v)

    Prop.forAll(Gen.listOfN(2, gen).retryUntil(l => l.size == 2 && l.head._1 != l.last._1)) {
      case (p1, v1) :: (p2, v2) :: Nil =>
        data(p1) = ErasedGF256(GF[256](v1))
        data(p2) = ErasedGF256(GF[256](v2))
        rs.doDecode(data)
        data.forall(d => d.v == zero && d.ok): Prop
      case _ => Prop.falsified // should not get here
    }
  }

  property("ReedSolomon size (36,32) for AC3 corrects three erasures") = {
    import gfnumeric.GF256_187.given
    val num = summon[GFNumeric[GF[256]]]
    import num.*
    val rs = ReedSolomon(36, 32, 120, true)

    val data = (0 until 36).map(_ => GF256WithValue(zero): GF256WithStatus).toArray
    val gen = for { pos <- Gen.chooseNum(0, 35)
                    v <- Gen.chooseNum(1, 255) }
    yield (pos, v)

    Prop.forAll(Gen.listOfN(3, gen).retryUntil(l => l.size == 3 && l.map(_._1).toSet.size == 3)) {
      case (p1, v1) :: (p2, v2) :: (p3, v3) :: Nil =>
        data(p1) = ErasedGF256(GF[256](v1))
        data(p2) = ErasedGF256(GF[256](v2))
        data(p3) = ErasedGF256(GF[256](v3))
        rs.doDecode(data)
        data.forall(d => d.v == zero && d.ok): Prop
      case _ => Prop.falsified // should not get here
    }
  }

  property("ReedSolomon size (36,32) for AC3 corrects four erasures") = {
    import gfnumeric.GF256_187.given
    val num = summon[GFNumeric[GF[256]]]
    import num.*
    val rs = ReedSolomon(36, 32, 120, true)

    val data = (0 until 36).map(_ => GF256WithValue(zero): GF256WithStatus).toArray
    val gen = for {pos <- Gen.chooseNum(0, 35)
                   v <- Gen.chooseNum(1, 255)}
    yield (pos, v)

    Prop.forAll(Gen.listOfN(4, gen).retryUntil(l => l.size == 4 && l.map(_._1).toSet.size == 4)) {
      case (p1, v1) :: (p2, v2) :: (p3, v3) :: (p4, v4) :: Nil =>
        data(p1) = ErasedGF256(GF[256](v1))
        data(p2) = ErasedGF256(GF[256](v2))
        data(p3) = ErasedGF256(GF[256](v3))
        data(p4) = ErasedGF256(GF[256](v4))
        rs.doDecode(data)
        data.forall(d => d.v == zero && d.ok): Prop
      case _ => Prop.falsified // should not get here
    }
  }

  property("ReedSolomon size (36,32) for AC3 corrects one error and two erasures") = {
    import gfnumeric.GF256_187.given
    val num = summon[GFNumeric[GF[256]]]
    import num.*
    val rs = ReedSolomon(36, 32, 120, true)

    val data = (0 until 36).map(_ => GF256WithValue(zero): GF256WithStatus).toArray
    val gen = for {pos <- Gen.chooseNum(0, 27)
                   v <- Gen.chooseNum(1, 255)}
    yield (pos, v)

    Prop.forAll(Gen.listOfN(3, gen).retryUntil(l => l.size == 3 && l.map(_._1).toSet.size == 3)) {
      case (p1, v1) :: (p2, v2) :: (p3, v3) :: Nil =>
        data(p1) = ErasedGF256(GF[256](v1))
        data(p2) = ErasedGF256(GF[256](v2))
        data(p3) = GF256WithValue(GF[256](v3))
        rs.doDecode(data)
        data.forall(d => d.v == zero && d.ok): Prop
      case _ => Prop.falsified // should not get here
    }
  }

  property("ReedSolomon size (32,28) doesn't crash") = {
    import gfnumeric.GF256_11d.given
    val num = summon[GFNumeric[GF[256]]]
    import num.*
    val c1decoder = C1(true)

    val rawData = Seq(
      (0x53, false), (0xb9, false), (0x41, false), (0x24, false),
      (0x61, false), (0x2,  false), (0xfb, false), (0x23, false),
      (0x66, false), (0xff, true), (0x3c, false), (0xfe, false),
      (0xb3, false), (0x2,  false), (0xcd, false), (0x0,  false),
      (0x67, false), (0x8f, false), (0x79, false), (0xa8, false),
      (0x11, false), (0xff, true), (0x98, false), (0x3,  false),
      (0xa5, false), (0xfc, false), (0xcf, false), (0x1,  false),
      (0xe2, false), (0xff, false), (0xf3, false), (0x1,  false),
    )
    val data = rawData.map((v, e) => if (e) ErasedGF256(GF[256](v)) else GF256WithValue(GF[256](v))).toArray

    c1decoder.doDecode(data)
    // data.forall(d => d.v == zero && d.ok): Prop // we don't actually know if this codeword is correctable
    Prop.proved
  }
}
