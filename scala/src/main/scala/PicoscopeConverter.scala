// Copyright 2021 Staffan Ulfberg staffan@ulfberg.se
import java.io.{File, FileWriter, InputStream, PrintStream, PrintWriter}

import scala.io.Source
import scala.util.{Failure, Using}

/**
 * Reads a csv file exported from Picoscope to recreate one byte per channel (Picoscope samples are 8 bit,
 * but expressed as floating point Volts in csv files), resampled to the given frequency.
 * Resampling could be improved: no interpolation or filtering is done.
 */
object PicoscopeConverter {
  def main(args: Array[String]): Unit = {
    val linesForInitialStatistics = 5000
    var inputFilename: Option[String] = None
    var outputFilename: Option[String] = None
    var outputSampleRate: Double = 100e6
    var outputFormat: String = "csv:tab" // CVS format: time, channel a, channel b
    var outputLimit: Option[Int] = None

    def processArgs: List[String] => Boolean = {
      case Nil => true
      case "-or" :: sampleRateString :: rest =>
        outputSampleRate = sampleRateString.toDouble
        processArgs(rest)
      case "-of" :: format :: rest =>
        outputFormat = format
        processArgs(rest)
      case "-o" :: fn :: rest =>
        outputFilename = Some(fn)
        processArgs(rest)
      case "-l" :: l :: rest =>
        outputLimit = Some(l.toInt)
        processArgs(rest)
      case fn :: rest if inputFilename.isEmpty =>
        inputFilename = Some(fn)
        processArgs(rest)
      case _ => false
    }
    assert(processArgs(args.toList), "Bad command line arguments.")
    if (inputFilename.isEmpty) {
      System.err.println("No input filename")
      System.exit(1)
    }

    def parseLine(s: String): IndexedSeq[Double] = {
      s.split(',').toIndexedSeq.map(_.toDouble)
    }

    Using(Source.fromFile(inputFilename.get)) { source =>
      val lines = source.getLines()
      val channelNames = {
        val line = lines.next()
        val parts = line.split(',').toSeq
        assert(parts.sizeIs >= 2, "Too header few columns")
        if (parts.head != "Time") throw Exception("First line does not have Time as first column")
        parts.tail.map(_.stripPrefix("Channel "))
      }
      val timeMultiplier = {
        val line = lines.next()
        val s = Seq.fill(channelNames.size)(""",\(\w+\)""").mkString("")
        if (line.matches("""\(s\)""" + s)) 1.0
        else if (line.matches("""\(ms\)""" + s)) 1e-3
        else throw Exception("Units (second) line incorrect")
      }
      assert(lines.next() == "", "Third line incorrect")

      val firstValues = lines.take(linesForInitialStatistics).map(parseLine).toIndexedSeq

      val inputSampleRate = {
        val times = firstValues.map(_(0) * timeMultiplier)
        val tDelta = (times.last - times.head) / (linesForInitialStatistics - 1)
        1 / tDelta
      }
      System.err.println(f"Input sample rate: $inputSampleRate")
      System.err.println(f"Output sample rate: $outputSampleRate")

      // assume all values were calculated from a byte value using doubleValue = a + b * byteValue
      // minByteValue and maxByteValue are only used for debugging etc
      case class Transform(channelName: String, a: Double, b: Double, minByteValue: Int, maxByteValue: Int) {
        def byteToDouble(i: Int): Double = {
          assert(i >= 0 & i <= 255, s"value $i not a byte vale")
          a + b * i
        }
        def doubleToByte(d: Double): Int = {
          if (d.isInfinite)
            255
          else {
            val byteValue = (math.round((d - a) / b)).toInt
            assert(math.abs(byteToDouble(byteValue) - d) < b / 10, f"Channel $channelName error converting $d: nearest byte is $byteValue, but that converts back to ${byteToDouble(byteValue)}")
            byteValue
          }
        }
        override def toString: String = f"double = $a%5f + $b%5f * byte, byteRange: [$minByteValue, $maxByteValue], doubleRange: [${byteToDouble(minByteValue)}%5f, ${byteToDouble(maxByteValue)}%5f]"
      }

      def calculateTransform(name: String, values: Seq[Double]) = {
        val valueSet = values.toSet
        val allIntervals = valueSet.flatMap(v1 => valueSet.map((v1, _))).toSeq.map { case (a, b) => math.abs(a - b) }.distinct.filter(_ != 0).sorted

        var stepSize = allIntervals.head
        for (interval <- allIntervals) {
          val nearestMult = math.round(interval / stepSize)
          if (math.abs(nearestMult * stepSize - interval) > interval * 0.001) {
            assert(math.abs(nearestMult * stepSize - interval) < interval * 0.01, "not close") // if not, stepsize is an integer factor too large and we need to try something more sophisticated
            stepSize = interval / nearestMult
          }
        }

        val maxVal = values.max
        val minVal = values.min

        val byteValuesUsed = math.ceil((maxVal - minVal) / stepSize).toInt + 1
        if (byteValuesUsed > 256) {
          System.err.println(f"Cannot create transform min=$minVal, max=$maxVal, step=$stepSize")
          System.exit(1)
        }
        val minByteValue = (256 - byteValuesUsed) / 2
        val a = minVal - minByteValue * stepSize
        val b = stepSize
        val transform = Transform(name, a, b, minByteValue, maxByteValue = minByteValue + byteValuesUsed - 1)

        valueSet.foreach(v => transform.doubleToByte(v))

        transform
      }

      val transforms = channelNames.zipWithIndex.map { (n, i) =>
        val t = calculateTransform(n, firstValues.map(_ (i + 1)))
        System.err.println(s"Channel $n transform $t")
        t
      }

      val output = outputFilename.fold(System.out)(fn => new PrintStream(fn))

      var lineNumber = 4
      var timeStep = 0
      val inputStartTime = firstValues.head(0) * timeMultiplier
      var tInput = 0.0
      var tOutput = 0.0
      (firstValues.iterator ++ lines.map(parseLine)).foreach { seq =>
        val t = seq(0) * timeMultiplier
        if (math.abs(t - inputStartTime - tInput) > 5e-9 + timeStep * (1 / inputSampleRate) * 1e-3) {
          System.err.println(s"Time drift too high line $lineNumber")
          System.exit(1)
        }
        while (tOutput <= tInput) {
          outputResampledStep(output, outputFormat, timeStep, seq.tail.zip(transforms).map { case (v, t) => t.doubleToByte(v) })
          tOutput += 1 / outputSampleRate
        }

        lineNumber += 1
        timeStep += 1
        tInput += 1 / inputSampleRate

        if (outputLimit.exists(_ < timeStep))
          System.exit(0)
      }

      output.close()
    } match {
      case Failure(exception) => System.err.println(exception.toString)
      case _ =>
    }
  }

  def outputResampledStep(ps: PrintStream, format: String, t: Int, values: IndexedSeq[Int]): Unit = {
    require(t >= 0)
    require(values.forall(v => v >= 0 && v < 256))
    val Array(ftype, args) = format.split(':')
    ftype match {
      case "csv" =>
        var first = true
        if (args.contains('t')) { ps.print(t); first = false; }
        if (!first) ps.print(",") // FIXME: do not hard code column names
        if (args.contains('a')) { ps.print(values(0)); first = false; }
        if (!first) ps.print(",")
        if (args.contains('b')) { ps.print(values(1)); first = false; }
        ps.println()
      case "bin" =>
        if (args == "a") ps.write(Array[Byte](values(0).toByte))
        else if (args == "b") ps.write(Array[Byte](values(1).toByte))
        else throw new Exception("Unsupported binary format " + args)
    }
  }
}
