// Copyright 2021 Staffan Ulfberg staffan@ulfberg.se
package efm

import scala.collection.immutable.ArraySeq
import scala.collection.mutable
import scala.util.Random
import rs.{ByteWithStatus, ByteWithValue, ErasedByte}

trait EfmInverter {
  type FourteenType // the way that the 14 captured bits are represented
  def toFourteen(edges: Seq[Double]): FourteenType // times when edges were detected. 0.5, 1.5, ... 13.5 are the nominal positions
  def toByte(fourteenValue: FourteenType): ByteWithStatus
}

case object NearestHamming extends EfmInverter {
  override type FourteenType = Int

  private def hammingDist(a: Int, b: Int): Int = {
    (for (bit <- 0 until 14) yield (a & (1 << bit)) != (b & (1 << bit))).count(identity)
  }

  private val table =
    (for (i <- 0 until 2 << 14 - 1) yield {
      val dists = for (j <- Byte.MinValue to Byte.MaxValue) yield j.toByte -> hammingDist(i, EfmTable.getEfm(j.toByte))
      val nearest = dists.minBy(_._2)
      // println(f"$i%04x ${nearest._1} ${nearest._2}")
      i -> Some(nearest._1)
    }).toMap

  override def toFourteen(edges: Seq[Double]): FourteenType = {
    edges.map(v => math.max(0, math.min(13, 13 - v.toInt))).distinct.sorted.map(1 << _).sum
  }

  override def toByte(fourteenValue: Int): ByteWithStatus = table(fourteenValue).fold(ErasedByte(0): ByteWithStatus)(ByteWithValue.apply)
}

case object SmallestEdgeDist extends EfmInverter {
  override type FourteenType = Int

  private def dist(a: Int, b: Int): Option[Int] = {
    val aBits = (0 until 14).flatMap { bit => Option.when((a & (1 << bit)) != 0)(bit) }
    val bBits = (0 until 14).flatMap { bit => Option.when((b & (1 << bit)) != 0)(bit) }
    Option.when(aBits.size == bBits.size)(aBits.lazyZip(bBits).map((a, b) => math.abs(a - b)).sum)
  }

  private val table: Map[Int, ByteWithStatus] =
    (for (i <- 0 until 2 << 14 - 1) yield {
      val dists = (for (j <- Byte.MinValue to Byte.MaxValue) yield dist(i, EfmTable.getEfm(j.toByte)).map(j.toByte -> _)).flatten
      if (dists.nonEmpty) {
        val nearest = dists.minBy(_._2) // (byteValue, dist)
        val erased = nearest._2 >= 2.0
        i -> (if (erased) ErasedByte(nearest._1) else ByteWithValue(nearest._1))
      } else { // the number of edges doesn't even match -- figure out of there is something better to do later...
        i -> ErasedByte(0)
      }
    }).toMap

  override def toFourteen(edges: Seq[Double]): FourteenType = {
    edges.map(v => math.max(0, math.min(13, 13 - v.toInt))).distinct.sorted.map(1 << _).sum
  }

  override def toByte(fourteenValue: Int): ByteWithStatus = table(fourteenValue)
}

case object CosineSimilarity extends EfmInverter {
  override type FourteenType = Int

  private val table: ArraySeq[Byte] = EfmTable.efmerr2positionLUT.zip(EfmTable.efmerr2valueLUT).sortBy(_._1).map(_._2)

  override def toFourteen(edges: Seq[Double]): FourteenType = {
    edges.map(v => math.max(0, math.min(13, 13 - v.toInt))).distinct.sorted.map(1 << _).sum
  }

  override def toByte(fourteenValue: FourteenType): ByteWithStatus = ByteWithValue(table(fourteenValue))
}

object EfmInversion {
  val SamplesPerTest = 100000
  val inverters = Seq(NearestHamming, SmallestEdgeDist, CosineSimilarity)
  val jitterAmounts = Seq(0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6) // standard deviations in bit lengths

  private def getEdges(efmValue: Int): Seq[Double] = (0 until 14).flatMap { bit =>
    Option.when((efmValue & (1 << bit)) != 0)(13.5 - bit)
  }

  def main(args: Array[String]): Unit = {
    case class Stats(totalTrials: Int, correctResult: Int, noResult: Int)

    printTable(SmallestEdgeDist)

    val stats = mutable.HashMap[EfmInverter, Stats]()

    for (jitterAmount <- jitterAmounts) {
      inverters.foreach(i => stats(i) = Stats(0, 0, 0))
      for (_ <- 0 until SamplesPerTest) {
        val byteValue = Random.nextBytes(1)(0)
        val efmValue = EfmTable.getEfm(byteValue)
        val edges = (0 until 14).flatMap { bit =>
          Option.when((efmValue & (1 << bit)) != 0)(13.5 - bit + Random.nextGaussian() * jitterAmount)
        }.sorted
        for (inverter <- inverters) {
          val v = inverter.toByte(inverter.toFourteen(edges))
          val prevStats = stats(inverter)
          stats(inverter) = prevStats.copy(
            totalTrials = prevStats.totalTrials + 1,
            correctResult = prevStats.correctResult + (if (v.ok && v.v == byteValue) 1 else 0),
            noResult = prevStats.noResult + (if (!v.ok) 1 else 0))
        }
      }

      for (inverter <- inverters) {
        val s = stats(inverter)
        println(f"$inverter%-20s jitter $jitterAmount, success ${s.correctResult * 100.0 / s.totalTrials}%.1f %%, " +
          f"no output ${s.noResult * 100.0 / s.totalTrials}%.1f %%")
      }
      println()
    }
  }

  def invert(efmEncoded: Int): ByteWithStatus = {
    val inverter = SmallestEdgeDist
    val edges = getEdges(efmEncoded)
    inverter.toByte(inverter.toFourteen(edges))
  }

  // Generates the contents of the .coe file used by Vivado to initialize block memory
  // The highest bit tells if the inverse is a perfect inverse of an EFM codeword.
  // (To be used for experiments with erasing non-perfect decodes, or just to output some statistics
  // during decoding.)
  def printTable(inverter: EfmInverter): Unit = {
    println("memory_initialization_radix=2;")
    println("memory_initialization_vector=")

    val lines = for (efmValue <- 0 until 1 << 14) yield {
      val edges = getEdges(efmValue)
      val v = inverter.toByte(inverter.toFourteen(edges))
      val value = v.v & 0xff
      val erased = !v.ok
      f"${if (erased) "1" else "0"}${value.toBinaryString.toLong}%08d"
    }
    println(lines.mkString("", ",\n", ";\n"))
  }
}
