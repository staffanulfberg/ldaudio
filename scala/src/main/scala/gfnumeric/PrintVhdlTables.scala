package gfnumeric

import GFNumeric.*

object PrintVhdlTables {
  def printProcess(table: Seq[(Int, Int)], others: Int): Unit = {
    println("    process(clk)")
    println("    begin")
    println("      if rising_edge(clk) then")
    println("        case a is")
    for ((from, to) <- table)
      println(f"""          when "${from.toLong.toBinaryString.toLong}%08d" => b <= "${to.toLong.toBinaryString.toLong}%08d";""")
    println(f"""          when others => b <= "${others.toLong.toBinaryString.toLong}%08d";""")
    println("        end case;")
    println("      end if;")
    println("    end process;")
  }

  // used for entities that depend on generics
  def printEntity(name: String, generics: Seq[(String, String)], tables: Seq[(Seq[String], Seq[(Int, Int)], Int)]): Unit = {
    println("library IEEE;")
    println("use IEEE.STD_LOGIC_1164.ALL;")
    println()
    println(s"entity $name is")
    println("  generic (")
    println((for ((gname, gtype) <- generics) yield s"    $gname: $gtype").mkString(";\n"))
    println("  );")
    println("  port (")
    println("    clk: in std_logic;")
    println("    a: in std_logic_vector(7 downto 0);")
    println("    b: out std_logic_vector(7 downto 0)")
    println("  );")
    println(s"end $name;")
    println()
    println(s"architecture rtl of $name is")
    println("begin")
    var first = true
    for ((tableGenerics, tableData, others) <- tables) {
      println(s"${if (first) "  gen: if" else "  elsif"} ${generics.zip(tableGenerics).map{ case ((name, _), value) => s"$name = $value"}.mkString(" and ")} generate")
      printProcess(tableData, others)
      first = false
    }
    println("  else generate")
    println("""    assert false report "generic parameters set to unsupported values." severity failure;""")
    println("  end generate;")
    println("end rtl;")
  }

  def printLog(): Unit = {
    def logTable(gf: GFNumeric[GF[256]]): Seq[(Int, Int)] = {
      import gf.*
      for (i <- 1 until 256) yield {
        val g = GF[256](i)
        val l = log(g)
        i -> l
      }
    }

    val tables = Seq(
      (Seq("""9x"11d""""), logTable(gfnumeric.GF256_11d.gfNumeric), 0),
      (Seq("""9x"187""""), logTable(gfnumeric.GF256_187.gfNumeric), 0)
    )
    printEntity("log", Seq(("poly", "std_logic_vector(8 downto 0)")), tables)
  }

  def printInv(): Unit = {
    def invTable(gf: GFNumeric[GF[256]]): Seq[(Int, Int)] = {
      import gf.*
      for (i <- 1 until 256) yield {
        val g = GF[256](i)
        val l = inverse(g).a
        i -> l
      }
    }

    val tables = Seq(
      (Seq("""9x"11d""""), invTable(gfnumeric.GF256_11d.gfNumeric), 0),
      (Seq("""9x"187""""), invTable(gfnumeric.GF256_187.gfNumeric), 0)
    )
    printEntity("inv", Seq(("poly", "std_logic_vector(8 downto 0)")), tables)
  }

  // This operation is y = inv(alpha ** (fcr * x))
  def printInvFcr(): Unit = {
    def invFcrTable(fcr: Int, gf: GFNumeric[GF[256]]): Seq[(Int, Int)] = {
      import gf.*
      if (fcr == 0)
        Seq()
      else
        for (i <- 0 until 256) yield {
          val e = alphaPow(i * fcr)
          val l = inverse(e)
          i -> l.a
        }
    }

    val tables = Seq(
      (Seq("""9x"11d"""", "0"), invFcrTable(0, gfnumeric.GF256_11d.gfNumeric), 1),
      (Seq("""9x"187"""", "120"), invFcrTable(120, gfnumeric.GF256_187.gfNumeric), 0)
    )
    printEntity("invFcr", Seq(("poly", "std_logic_vector(8 downto 0)"), ("fcr", "natural range 0 to 255")), tables)
  }

  def printMultiplier(gf: GFNumeric[GF[256]]): Unit = {
    import gf.*
    println(f"MULT(${gf.irreduciblePolynomial}%x)")
    val coeffs = (for (i <- 0 to 14) yield {
      val p = alphaPow(i).a
      for (j <- 0 to 7) yield
        if (((1 << j) & p) != 0)
          Some(i -> j)
        else None
    }).flatten.flatten
    coeffs.groupBy(_._2).toSeq.sortBy(_._1).foreach { case (out, pairs) =>
      println(s"    c($out) <= ${pairs.map(_._1).map(i => s"x($i)").mkString(" xor ")};")
    }
  }

//  def printAlphaPow()(using gf: GFNumeric[GF[256]]): Unit = {
//    import gf.*
//    println(f"ALPHAPOW(${gf.irreduciblePolynomial}%x)")
//    println("    with a select b <=")
//    for (i <- 0 until 256) {
//      val e = alphaPow(i)
//      println(f"""      "${e.a.toLong.toBinaryString.toLong}%08d" when "${i.toLong.toBinaryString.toLong}%08d", -- $i""")
//    }
//  }

  def main(args: Array[String]): Unit = {
//    printLog()
//    printInv()
//    printInvFcr()
//    printAlphaPow()
    printMultiplier(gfnumeric.GF256_11d.gfNumeric)
    printMultiplier(gfnumeric.GF256_187.gfNumeric)
  }
}
