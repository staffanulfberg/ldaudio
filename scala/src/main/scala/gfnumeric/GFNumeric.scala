// Copyright 2021 Staffan Ulfberg staffan@ulfberg.se
package gfnumeric

object GFNumeric {
  extension [T](using num: GFNumeric[T])(lhs: T) {
    def +(rhs: T) = num.plus(lhs, rhs)
    def *(rhs: T) = num.times(lhs, rhs)
    def inverse = num.inverse(lhs)
    def toInt = num.toInt(lhs)
  }
}
trait GFNumeric[T] {
  def bits: Int
  def alpha: T // primitive element
  def zero: T
  def unit: T
  def irreduciblePolynomial: Int
  def plus(x: T, y: T): T
  def times(x: T, y: T): T
  def inverse(x: T): T
  def log(x: T): Int
  def alphaPow(i: Int): T
  def pow(x: T, y: Int): T
  def toInt(x: T): Int
  def fromInt(a: Int): T
}

object GF {
  private[gfnumeric] def times(a: Int, b: Int, irred: Int, bits: Int): Int = {
    var aa = a
    var bb = b
    var res = 0
    while (bb != 0) {
      if ((bb & 1) != 0)
        res ^= aa
      if ((aa & (1 << (bits - 1))) != 0)
        aa = (aa << 1) ^ irred
      else aa <<= 1
      bb >>>= 1
    }
    res
  }
}

case class GF[A <: Int](a: Int) {
  require(a >= 0) // should check upper bound also
  override def toString: String = s"$a"
}

trait GfNumericCommon[T <: GF[? <: Int]] { self: GFNumeric[T] =>
  override lazy val zero: T = fromInt(0)
  override lazy val unit: T = fromInt(1)
  override def toInt(x: T): Int = x.a
  override def plus(x: T, y: T): T = fromInt(x.a ^ y.a)
  override def times(x: T, y: T): T = fromInt(GF.times(x.a, y.a, irreduciblePolynomial, bits))
  override def inverse(x: T): T = {
    assert(x.a != 0)
    inverseTable(x.a)
  }
  override def log(x: T): Int = {
    assert(x != zero)
    logTable(toInt(x))
  }
  override def alphaPow(i: Int): T = powTable(i % ((1 << bits) - 1))
  override def pow(x: T, y: Int): T = alphaPow(y * log(x))

  lazy private val inverseTable: IndexedSeq[T] = zero +: (for (i <- 1 until 1 << bits) yield (1 until 1 << bits).find { x => GF.times(x, i, irreduciblePolynomial, bits) == 1 }.get).map(self.fromInt)

  lazy private val logTable: IndexedSeq[Int] = {
    -1 +: (1 to ((1 << bits) - 2)).scanLeft[Int](1) { case (v, i) =>
      GF.times(v, alpha.a, irreduciblePolynomial, bits)
    }.zipWithIndex.sortBy(_._1).map(x => x._2)
  }

  lazy private val powTable: IndexedSeq[T] = (0 until 1 << bits).scanLeft[Int](1)((v, i) => GF.times(v, alpha.a, irreduciblePolynomial, bits)).map(self.fromInt)
}


object GF2 {
  given GFNumeric[GF[2]] with GfNumericCommon[GF[2]] with {
    val bits = 1
    val alpha: GF[2] = GF[2](1)
    val irreduciblePolynomial: Int = 0x3 // x + 1 -- doesn't matter if we take this or just x
    def fromInt(a: Int): GF[2] = {
      require(a >= 0 && a < (1 << bits))
      GF[2](a)
    }
  }
}

object GF4 {
  given GFNumeric[GF[4]] with GfNumericCommon[GF[4]] with {
    val bits = 2
    val alpha: GF[4] = GF[4](2)
    val irreduciblePolynomial: Int = 0x7 // x^2 + x + 1 -- only one in GF[4]
    def fromInt(a: Int): GF[4] = {
      require(a >= 0 && a < (1 << bits))
      GF[4](a)
    }
  }
}

// we could make more for other irreducible polynomials -- what naming convention to use?
object GF256_11d {
  given gfNumeric: GFNumeric[GF[256]] with GfNumericCommon[GF[256]] with {
    val bits = 8
    val alpha: GF[256] = GF[256](2)
    val irreduciblePolynomial: Int = 0x11d // x^8 + x^4 + x^3 + x^2 + 1
    def fromInt(a: Int): GF[256] = {
      require(a >= 0 && a < (1 << bits), s"GF[$bits] value out of range: $a")
      GF[256](a)
    }
  }
}

object GF256_187 {
  given gfNumeric: GFNumeric[GF[256]] with GfNumericCommon[GF[256]] with {
    val bits = 8
    val alpha: GF[256] = GF[256](2)
    val irreduciblePolynomial: Int = 0x187 // x^8 + x^7 + x^2 + x + 1
    def fromInt(a: Int): GF[256] = {
      require(a >= 0 && a < (1 << bits), s"GF[$bits] value out of range: $a")
      GF[256](a)
    }
  }
}
