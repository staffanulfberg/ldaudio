// Copyright 2021 Staffan Ulfberg staffan@ulfberg.se
package experimental

import ac3.SymbolConversion.symbolsToBits

import scala.collection.parallel.*
import scala.util.Random

// This code turned out to be useless for this project (AC3RF decoding) but I keep in in case it is useful in some other context
object ViterbiTest {
  def main(args: Array[String]): Unit = {

    val viterbi = ViterbiDecoder(7, Vector(0x4f, 0x6d), Vector(true, true, false, true, true, false))
    val originalString = "111010100101001000100100100010010001010011101001001010"
    val original = originalString.map { _ == '1' }
    val encoded = viterbi.encode(original)

    val sent = encoded.map(b => if (Random.nextDouble() < 0.03) !b else b)

    val (decoded, cost, costVector) = viterbi.decode(sent)
    val decodedString = decoded.map(if (_) "1" else "0").mkString

    println(s"Original data: $originalString")
    println(s"Encoded:       ${encoded.map(if (_) "1" else "0").mkString}")
    println(s"Sent:          ${sent.map(if (_) "1" else "0").mkString}")
    println(s"Decoded data:  $decodedString")
    if (decodedString != originalString)
      assert(cost != 0)

    println(s"Cost=$cost! Total ${original.zip(decoded).count(_ != _)} bit errors; sent data modified in ${encoded.zip(sent).count(_ != _)} plcaes.")

    println(s"Free distance = ${viterbi.computeFreeDistance}")
  }
}
//Cost = 3; suspected good encoding 4f 4b: Puncturing size=14, Offset=10, swap12=false, free distance=3
object ExhaustiveViterbiSearch {
  def main(args: Array[String]): Unit = {
    val dataSet = Seq(
      "0201020332011333312202211232012032331022011111213111230213302231303113132332110311010000000000000000000033321023103203210020022230021031031333031001",
      "1211110102222110331221232313302212203023121111102220310220332111012103003101001020132312202133030023000000000000000001012222323210311000022123130232",
      "2102332221301330313323303301211123030022302032322020221210321231010132303001231212210010133320102002122130131331310213001022231032303111223323121113",
      "3011121202012301021001032202202030213123012222323002132003331130012312031013221010223013000000000000000000003110322030031233301321110023131111000002",
      "2013311232002233121011201231320032123113102302111021121031023203203121120030101320100103222300001230232021010102101002133030001001210211102323301102",
      "0121112021123123031321021023233302120222022212321320313211031203313331102311220030101031000000000000000020031032023302312012012131320212001300023213",
      "3232120302201132123231310210323031030000000000000000000222312103113210033033201310310213112233110333101202200312102133332311203220321312231132233333",
    )

    for (K <- 6 until 9) {
      println(s"K=$K")
      for {conn1 <- 1 << (K - 1) until (1 << K)} {
        println(s"conn1=$conn1")
        // conn1 <- Seq(0x4f); conn2 <- Seq(0x6d)
        ((1 until (1 << K)).toSet - conn1).toParArray.foreach { case conn2 =>

          val puncturePatterns = Vector("1101", "110110", "1101100110", "11010101100110").map(_.map(_ == '1'))
          for (puncturingPattern <- puncturePatterns) {
            val puncturingRate = puncturingPattern.count(identity).toDouble / puncturingPattern.size

            val decoder = ViterbiDecoder(K, Vector(conn1, conn2), puncturingPattern)
            val freeDistance = decoder.computeFreeDistance
            if (freeDistance >= 2) {
              case class Result(data: String, out: IndexedSeq[Boolean], offset: Int, cost: Int, costVector: Seq[Int])
              val goodData = dataSet.iterator.map { data =>
                (0 until puncturingPattern.size).flatMap { offset =>
                  val bits = symbolsToBits(data, offset, false)
                  val (out, cost, costVector) = decoder.decode(bits)
                  Option.when(cost < 6)(Result(data, out, offset, cost, costVector))
                }
              }.takeWhile(_.nonEmpty).toSeq
              if (goodData.size == dataSet.size) {
                val allResults = goodData.flatten
                println(f"Encoding $conn1%02x $conn2%02x with puncturing ${puncturingPattern.map(if (_) "1" else "0").mkString}, free distance=$freeDistance decodes all data, costs=${allResults.map(_.cost).mkString(", ")}")
                if (allResults.map(_.cost).min < 5) {
                  allResults.foreach { result =>
                    import result.*
                    println(f"Cost=$cost, offset=$offset, output=${out.map(if (_) "1" else "0").mkString}")
                    println(f"                       cost vector=${costVector.map(_.toString).mkString}")
                  }
                  println
                }
              }
            }
          }
        }
      }
    }
  }
}

/**
 * Viterbi decoder for constraint length K code defined by connection vectors.  The code rate is
 * 1 / connections.size before puncturing.
 * The connection vectors are represented by integers with bits 0 tp K - 1 set, indicating the number of delay steps from the input.
 * Bits are output from the connections in the order present in the vectors,
 * and the puncturing pattern is repeated as long as necessary.
 * If a non-punctured code is required, simply use the puncturingPattern Vector(true).
 */
class ViterbiDecoder(K: Int, connections: IndexedSeq[Int], puncturingPattern: IndexedSeq[Boolean]) {
  require(connections.nonEmpty)
  require(connections.forall(vec => vec >= 0 && vec < (1 << K)))
  require(puncturingPattern.sizeIs > 0)

  private def parity(i: Int): Boolean = {
    if (i == 0) false
    else parity(i >> 1) ^ (if ((i & 1) == 1) true else false)
  }

  val allStates: IndexedSeq[Int] = 0 until 1 << (K - 1)

  def nextState(state: Int, input: Boolean): Int = ((state << 1) | (if (input) 1 else 0)) & ((1 << (K - 1)) - 1)

  val outputMap: Map[(Int, Boolean), Seq[Boolean]] = { // Bits output for a state, input combination
    (for {s <- allStates; input <- Seq(false, true)} yield {
      (s, input)-> (for (conn <- connections) yield
         parity(((s << 1) | (if (input) 1 else 0)) & conn))
    }).toMap
  }

  val transitions: Map[(Int, Int), Boolean] = { // (state, nextState) -> inputBit
    (for { startState <- allStates; input <- Seq(false, true) } yield
      (startState, nextState(startState, input)) -> input).toMap
  }

  case class NodeData(bestPrev: Int, bestPrevInput: Boolean, bestPrevCost: Int)
  case class StepData(stateData: IndexedSeq[NodeData])

  // Well, we are a decoder but this is simple and good for testing
  def encode(bits: IndexedSeq[Boolean]): IndexedSeq[Boolean] = {
    val puncturingIterator = LazyList.continually(puncturingPattern).flatten.iterator
    var shiftReg = 0
     bits.flatMap { bit =>
      val shiftRegWithInput = ((shiftReg << 1) | (if (bit) 1 else 0))
      val outputs = connections.map(conn => parity(conn & shiftRegWithInput))
      shiftReg = shiftRegWithInput & ((1 << (K - 1)) - 1)
      val puncturings = puncturingIterator.take(outputs.size).toVector
      outputs.zip(puncturings).flatMap((b, p) => Option.when(p)(b))
    }
  }

  // returnds the decoded sequence, its total cost, and the cost for each decoded bit
  def decode(bits: IndexedSeq[Boolean]): (IndexedSeq[Boolean], Int, IndexedSeq[Int]) = {
    val puncturingIterator = LazyList.continually(puncturingPattern).flatten.iterator

    val steps = viterbi(bits, puncturingIterator)

    val (lastStateData, minCostIndex) = steps.head.stateData.zipWithIndex.minBy(_._1.bestPrevCost)
    val output = steps.foldLeft[(Int, Int, List[(Boolean, Int)])]((minCostIndex, lastStateData.bestPrevCost, Nil)) { case ((prevIx, prevCost, accData), step) =>
      (step.stateData(prevIx).bestPrev, step.stateData(prevIx).bestPrevCost, (step.stateData(prevIx).bestPrevInput, prevCost - step.stateData(prevIx).bestPrevCost) :: accData)
    }
    (output._3.tail.toIndexedSeq.map(_._1), lastStateData.bestPrevCost, output._3.tail.toIndexedSeq.map(_._2))
  }

  def computeFreeDistance = {
    val puncturingIterator = LazyList.continually(puncturingPattern).flatten.iterator
    val dists = for (punctureSkip <- 0 until puncturingPattern.size) yield {
      val steps = viterbi(Seq.fill(K * 10)(false), puncturingIterator.drop(punctureSkip), true)
//      for (step <- steps) {
//        println(step.stateData.zipWithIndex.map((nd, i) => f"$i%2s:${nd.bestPrev}%2s C${nd.bestPrevCost}%4s").mkString(", "))
//      }
      steps.map(_.stateData(0).bestPrevCost).filter(_ != 0).min
    }
//    println("dists: " + dists.mkString(", "))
    dists.min
  }

  private def viterbi(bits: Iterable[Boolean], puncturingIt: Iterator[Boolean], stopWhenStateZero: Boolean = false): Seq[StepData] = {
    case class UnpuncturedBit(valid: Boolean, data: Boolean) {
      def distance(d: Boolean): Int = if (!valid || data == d) 0 else 1
    }

    val unpuncturedBits = bits.flatMap { b =>
      puncturingIt.takeWhile(!_).map(_ => UnpuncturedBit(false, false)).toSeq :+ UnpuncturedBit(true, b)
    }

    val initialStepData = StepData(allStates.map(s => NodeData(0, false, if (s == 0) 0 else 1000)))

    var first = true
    var steps = List[StepData](initialStepData)
    for (bs <- unpuncturedBits.grouped(connections.size)) {
      val newStep = StepData(allStates.map { state =>
        val prevStep = steps.head
        val incoming = allStates.flatMap(fromState => { // find the two predecessor states
          transitions.get((fromState, state)).map { input =>
            val wouldOutput = outputMap((fromState, input))
            val cost = wouldOutput.zip(bs).map { case (wouldBe, inputBits) => inputBits.distance(wouldBe) }.sum
            (fromState, state, input, prevStep.stateData(fromState).bestPrevCost + cost)
          }
        }).filter(t =>  t._1 != 0 || t._2 != 0 || !stopWhenStateZero || !first)
        val best = incoming.minBy(_._4)
        NodeData(best._1, best._3, best._4)
      })
      steps = newStep :: steps
      first = false
    }
    steps
  }
}
