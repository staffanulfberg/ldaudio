package experimental

import ac3.SymbolConversion

import scala.io.Source

// This code is a lame attempt at guessing some convolutional interleavings.  It didn't find anything of interest for the ac3 data.
object Interleaver {
  val pattern = IndexedSeq(
    0x0b, 0x77,
  ).map(i => (i & 0xff).toByte).takeRight(10)

  def main(args: Array[String]): Unit = {
    val rawSymbols = Source.fromFile("ac3data.bin").getLines().next()
    for (skipFrequency <- Seq(1000000000, 4, 5, 8, 9)) {
      val symbols = rawSymbols.indices.filter(x => x % skipFrequency != 0).map(rawSymbols).mkString

      for (offset <- 0 until 4) {
        for {swap12 <- Seq(false, true); littleEndian <- Seq(false, true)} {
          println(s"Skip=$skipFrequency, offset=$offset, swap12=$swap12, littleEndian=$littleEndian")
          val bytes = SymbolConversion.symbolsToBytes(symbols, offset, swap12, littleEndian).drop(500)
          for (chunkSize <- Seq(120))
            tryInterleave(bytes, chunkSize, pattern, 1536 * 2 / chunkSize)
        }
      }
    }
  }

  def tryInterleave(bytes: IndexedSeq[Byte], chunkSize: Int, pattern: IndexedSeq[Byte], maxRows: Int): Unit = {
    // the interleave values are all positive and indicate the delay for the column at the corresponding index
    def score(interleave: IndexedSeq[Int], offset: Int): Double = {
      val rows = math.min((bytes.size - offset) / chunkSize, maxRows + interleave.max)

      val scoresAndResults = (interleave.max until rows).map { row =>
        val interleavedRow = (0 until chunkSize).map(col => bytes(offset + (row - interleave(col)) * chunkSize + col))
        val r = longestCommonSubstringsOptimizedReferentiallyTransparentFP(pattern, interleavedRow)
        val l = r.map(_.map(_.length).max).getOrElse(0)
        (l, r)
      }
      if (scoresAndResults.map(_._1).max >= 5) {
        println(s"Found good one: ${scoresAndResults.filter(_._1 == scoresAndResults.map(_._1).max)}")
        //printInterleaved(bytes, chunkSize, interleave, rows)
      }

      scoresAndResults.map(_._1).max.toDouble
    }

    val interleavesToTest1 = (-4 to 4).map(stride => (0 until chunkSize).map(i => i * stride)) ++ Seq(
      (0 until chunkSize).map(i => i % 2),
    )
    val interleavesToTest = interleavesToTest1.toVector.map { interleave =>
      val min = interleave.min
      interleave.map(_ - min)
    }

    for (interleave <- interleavesToTest) {
      for (i <- 0 until chunkSize) {
        val s = score(interleave, i)
        if (s >= 5) {
          println(f"chunkSize=$chunkSize, byte offset=$i, score=$s%.1f, interleave=${interleave.take(10).mkString(", ")}, ...")
        }
      }
    }
  }

  def printArray(s: String, xs: IndexedSeq[Byte]): Unit = {
    println(f"$s%-16s ${xs.map(d => f"${d}%02x").mkString(" ")}")
  }

  def printInterleaved(bytes: IndexedSeq[Byte], chunkSize: Int, interleave: IndexedSeq[Int], maxRows: Int) = {
    val rows = bytes.size / chunkSize
    (interleave.max until math.min(rows, maxRows) + interleave.max).map { row =>
      val interleavedRow = (0 until chunkSize).map(col => bytes((row - interleave(col)) * chunkSize + col))
      printArray("", interleavedRow)
    }
  }

  // taken from rosettacode.org
  def longestCommonSubstringsOptimizedReferentiallyTransparentFP(left: IndexedSeq[Byte], right: IndexedSeq[Byte]): Option[Set[IndexedSeq[Byte]]] =
    if (left.nonEmpty && right.nonEmpty) {
      val (shorter, longer) =
        if (left.length < right.length) (left, right)
        else (right, left)
      val lengths: Array[Int] = new Array(shorter.length) //mutable

      @scala.annotation.tailrec
      def recursive(
        indexLonger: Int = 0,
        indexShorter: Int = 0,
        currentLongestLength: Int = 0,
        lastIterationLength: Int = 0,
        accumulator: List[Int] = Nil
      ): (Int, List[Int]) =
        if (indexLonger < longer.length) {
          val length =
            if (longer(indexLonger) != shorter(indexShorter)) 0
            else if (indexShorter == 0) 1
            else lastIterationLength + 1
          val newLastIterationLength = lengths(indexShorter)
          lengths(indexShorter) = length //mutation
          val newCurrentLongestLength =
            if (length > currentLongestLength) length
            else currentLongestLength
          val newAccumulator =
            if ((length < currentLongestLength) || (length == 0)) accumulator
            else {
              val entry = indexShorter - length + 1
              if (length > currentLongestLength) List(entry)
              else entry :: accumulator
            }
          if (indexShorter < shorter.length - 1)
            recursive(
              indexLonger,
              indexShorter + 1,
              newCurrentLongestLength,
              newLastIterationLength,
              newAccumulator
            )
          else
            recursive(
              indexLonger + 1,
              0,
              newCurrentLongestLength,
              newLastIterationLength,
              newAccumulator
            )
        }
        else (currentLongestLength, accumulator)

      val (length, indexShorters) = recursive()
      if (indexShorters.nonEmpty)
        Some(
          indexShorters
            .map {
              indexShorter =>
                shorter.slice(indexShorter, indexShorter + length)
            }
            .toSet
        )
      else None
    }
    else None
}
