// Copyright 2021 Staffan Ulfberg staffan@ulfberg.se
package experimental

import ac3.SymbolConversion
import gfnumeric.GFNumeric.*
import gfnumeric.{GF, GFNumeric}

import scala.io.Source
import scala.language.implicitConversions
import scala.util.Random

object Matrix {
  def identityMatrix[T](n: Int)(using num: GFNumeric[T]): Matrix[T] = Matrix(n, n, (0 until n).flatMap(i => (0 until n).map(j => if (i == j) num.unit else num.zero)))

  def permuteMatrixRowsRandomly[T : GFNumeric](A: Matrix[T]) = {
    val rows = (0 until A.rows).map(A.getRow(_))
    val shuffled = Random.shuffle(rows)
    Matrix(A.rows, A.cols, shuffled.map(_.values).reduce(_ ++ _))
  }

  // returns the eliminated matrix T (column elimination), and a matrix A such that T = this * A
  def gaussianColumnElimination[T](using num: GFNumeric[T])(R: Matrix[T]): (Matrix[T], Matrix[T]) = {
    val l = R.cols
    var T = R
    var A = identityMatrix(l)
    for (i <- 0 until math.min(l, R.rows)) {
      if (T(i, i) == num.zero) { // swap with first column with non-zero at row i
        (i + 1 until l).find(T(i, _) != num.zero).foreach { col =>
          T = T.swapColumns(i, col)
          A = A.swapColumns(i, col)
        }
      }
      if (T(i, i) != num.zero) {
        val inv = inverse(T(i, i))
        T = T.multiplyColumn(i, inv)
        A = A.multiplyColumn(i, inv)
        for (j <- i + 1 until l) {
          val b: T = T(i, j)
          val bTimesTcoli = T.getColumn(i).multiplyColumn(0, b)
          T = T.addToColumn(j, bTimesTcoli)
          val bTimesAcoli = A.getColumn(i).multiplyColumn(0, b)
          A = A.addToColumn(j, bTimesAcoli)
        }
      }
    }
    (T, A)
  }

  // returns the eliminated matrix T (row elimination), and a matrix A such that T = this * A
  def gaussianRowElimination[T](using num: GFNumeric[T])(R: Matrix[T]): (Matrix[T], Matrix[T]) = {
    val l = R.rows
    var T = R
    var A = identityMatrix(l)
    for (j <- 0 until math.min(l, R.cols)) {
      if (T(j, j) == num.zero) { // swap with first row with non-zero at col j
        (j + 1 until l).find(T(_, j) != num.zero).foreach { row =>
          T = T.swapRows(j, row)
          A = A.swapRows(j, row)
        }
      }
      if (T(j, j) != num.zero) {
        val inv = inverse(T(j, j))
        T = T.multiplyRow(j, inv)
        A = A.multiplyRow(j, inv)
        for (i <- j + 1 until l) {
          val b: T = T(i, j)
          val bTimesTrowj = T.getRow(j).multiplyRow(0, b)
          T = T.addToRow(i, bTimesTrowj)
          val bTimesArowj = A.getRow(j).multiplyRow(0, b)
          A = A.addToRow(i, bTimesArowj)
        }
      }
    }
    (T, A)
  }
}

case class Matrix[T : GFNumeric](rows: Int, cols: Int, values: IndexedSeq[T]) {
  require(values.sizeIs == rows * cols)

  def apply(i: Int, j: Int): T = values(i * cols + j)

  private def rowOfIndex(i: Int) = i / cols

  private def columnOfIndex(i: Int) = i % cols

  def swapColumns(c1: Int, c2: Int): Matrix[T] =
    Matrix(rows, cols, values.zipWithIndex.map { (v, i) =>
      val r = rowOfIndex(i)
      val c = columnOfIndex(i)
      if (c == c1) apply(r, c2)
      else if (c == c2) apply(r, c1)
      else v
    })

  def swapRows(r1: Int, r2: Int): Matrix[T] =
    Matrix(rows, cols, values.zipWithIndex.map { (v, i) =>
      val r = rowOfIndex(i)
      val c = columnOfIndex(i)
      if (r == r1) apply(r2, c)
      else if (r == r2) apply(r2, c)
      else v
    })

  def multiplyRow(r: Int, m: T) =
    Matrix(rows, cols, values.zipWithIndex.map((v, i) => if (rowOfIndex(i) == r) v * m else v))

  def multiplyColumn(c: Int, m: T) =
    Matrix(rows, cols, values.zipWithIndex.map((v, i) => if (columnOfIndex(i) == c) v * m else v))

  def addToRow(row: Int, m: Matrix[T]): Matrix[T] = {
    require(m.cols == cols && m.rows == 1)
    Matrix(rows, cols, values.zipWithIndex.map((v, i) => if (rowOfIndex(i) == row) v + m(0, columnOfIndex(i)) else v))
  }

  def addToColumn(col: Int, m: Matrix[T]): Matrix[T] = {
    require(m.rows == rows && m.cols == 1)
    Matrix(rows, cols, values.zipWithIndex.map((v, i) => if (columnOfIndex(i) == col) v + m(rowOfIndex(i), 0) else v))
  }

  def getRow(row: Int): Matrix[T] = Matrix(1, cols, (0 until cols).map(apply(row, _)))
  def getColumn(col: Int): Matrix[T] = Matrix(rows, 1, (0 until rows).map(apply(_, col)))

  def print(name: String = ""): Unit = {
    println(s"Matrix $name ($rows, $cols):")
    for (i <- 0 until rows)
      println(s"${(0 until cols).map(j => f"${values(i * cols + j).toInt}%02x ").mkString(" ")}")
  }
}

object CodeWordLengthIdentification {
  import Matrix.*

  // used to identify code word length -- since the ac3 data isn't coded like that it didn't yield a result but saving the code for future reference
  def identifyLength[T](using num: GFNumeric[T])(r: IndexedSeq[T], q: Int, lMax: Int): Option[Int] = {
    val cardForL = for (l <- 1 to lMax) yield {
      val R = Matrix[T](r.size / l, l, r.take(r.size / l * l))
      val (t, a) = gaussianRowElimination(R)
      val dependentColumns = for (i <- 0 until l) yield {
        val nonZeroCount = t.getColumn(i).values.count(_ != num.zero)
        if (nonZeroCount < (r.size / l - l) / 2.0 * 0.5) 1 else 0
      }
      l -> dependentColumns.sum
    }
    //println(cardForL)
    println(cardForL.filter(_._2 != 0))
    None
  }

  def main(args: Array[String]): Unit = {
    import gfnumeric.GF256_187
    import gfnumeric.GF256_187.given

    val lines = Source.fromFile("inframes.txt").getLines().toVector

    val parsed = lines.map { line =>
      val Array(i, symbols) = line.split(' ')
      val sync = SymbolConversion.symbolsToBytes(i, 0, false, false)
      val bytes = SymbolConversion.symbolsToBytes(symbols, 0, false, false)
      (sync.head, bytes)
    }
    val grouped = {
      val tmpGrouped = parsed.grouped(72).toIndexedSeq
      tmpGrouped.foreach { group =>
        assert(group.map(_._1).sameElements(0 until 72))
        assert(group.forall(_._2.size == 37))
      }
      tmpGrouped.map(group => group.map(_._2.map(_.toInt & 0xff)))
    }

    //findInterleaveWidth(grouped)
    // the findInterleaveWidth call shows that the probable interleave width is 74. Print out what the de-interleaved data looks like!
    val start = 37 * 72 * 5
    val flattenedData = grouped.flatten.flatten.slice(start, start + 74 * 36)
    print("    ")
    for (c <- 0 until 36)
      print(f"$c%02d ")
    println()
    for (r <- 0 until 74) {
      print(f"$r%02d: ")
      for (c <- 0 until 36) {
        print(f"${flattenedData(c * 74 + r)}%02x ")
      }
      println()
    }
  }

  private def findInterleaveWidth(grouped: IndexedSeq[Vector[IndexedSeq[Int]]])(using num: GFNumeric[GF[256]]) = {
    val flattenedData = grouped.flatten.flatten.drop(37 * 72 * 4)
    val zeroRowCounts = for (cols <- 20 until 200) yield {
      print(s"$cols\r")
      val rows = cols
      val R = Matrix[GF[256]](rows, cols, flattenedData.take(rows * cols).map(GF[256]))
      val (t, a) = gaussianRowElimination(R)
      val zeroRows = (0 until rows).count(r => t.getRow(r).values.forall(_ == num.zero))
      (cols, zeroRows)
    }
    println("       ")
    for ((cols, count) <- zeroRowCounts if count > 1)
      println(s"cols=$cols: zero count=$count")
  }
}
