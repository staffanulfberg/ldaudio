// Copyright 2021 Staffan Ulfberg staffan@ulfberg.se
import scala.io.Source
import scala.util.Random

/*
 * Reads a file with NRZM data, i.e., data that is already reclocked and sampled, and creates a 100 MHz file with
 * "unclocked" samples for testing the sample to efm logic.
 */
object MakeSamplesFromEfm {
  def main(args: Array[String]): Unit = {
    val lines = Source.fromResource("ve-snw-cut.nrzm").getLines()
    val bits = lines.map(_.toInt)

    var v = 0
    val Tref = 1.0 / 4321800 // per bit
    val Tdst = 1e-8 // per sample
    var tSrc = 0.0
    var tDst = 0.0

    bits.take(200000).foreach { bit =>
      if (bit == 1)
        v = 1 - v
      tSrc += Tref

      val j = (Random.nextDouble() * 2 - 1) * Tref / 3 // 1/3 time unit both ways
      while (tDst < tSrc + j) {
        println(v)
        tDst += Tdst
      }
      //      if (tDst > 1e-3)
      //        Tdst = Tref * 1.01;
    }
  }
}