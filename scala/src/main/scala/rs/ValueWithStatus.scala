// Copyright 2021 Staffan Ulfberg staffan@ulfberg.se
package rs

import gfnumeric.GFNumeric.*
import gfnumeric.{GF, GFNumeric}

sealed trait ValueWithStatus[A] {
  def v: A
  def ok: Boolean
  def invert: ValueWithStatus[A]
  def erased: ValueWithStatus[A]
  def unerased: ValueWithStatus[A]
}

sealed trait ByteWithStatus extends ValueWithStatus[Byte] {
  override def invert: ByteWithStatus
  override def erased: ByteWithStatus = ErasedByte(v)
  override def unerased: ByteWithStatus = ByteWithValue(v)
  def toGF256(using GFNumeric[GF[256]]): GF256WithStatus
}

case class ByteWithValue(v: Byte) extends ByteWithStatus {
  override def ok = true
  override def invert: ByteWithStatus = ByteWithValue((~v).toByte)
  override def toGF256(using GFNumeric[GF[256]]): GF256WithStatus = GF256WithValue(GF[256](v & 0xff))
}

case class ErasedByte(v: Byte) extends ByteWithStatus {
  override def ok = false
  override def invert: ErasedByte = ErasedByte((~v).toByte)
  override def toGF256(using GFNumeric[GF[256]]): GF256WithStatus = ErasedGF256(GF[256](v))
}

sealed trait GF256WithStatus extends ValueWithStatus[GF[256]] {
  override def invert: GF256WithStatus
  override def erased: GF256WithStatus = ErasedGF256(v)
  override def unerased: GF256WithStatus = GF256WithValue(v)
  def toByte(using GFNumeric[GF[256]]): ByteWithStatus
}

case class GF256WithValue(v: GF[256]) extends GF256WithStatus {
  override def ok = true
  override def invert: GF256WithStatus = GF256WithValue(GF[256]((~v.a) & 0xff))
  override def toByte(using GFNumeric[GF[256]]): ByteWithStatus = ByteWithValue(v.toInt.toByte)
}

case class ErasedGF256(v: GF[256]) extends GF256WithStatus {
  override def ok = false
  override def invert: ErasedGF256 = ErasedGF256(GF[256]((~v.a) & 0xffe))
  override def toByte(using GFNumeric[GF[256]]): ByteWithStatus = ErasedByte(v.toInt.toByte)
}

sealed trait ShortWithStatus extends ValueWithStatus[Short] {
  override def invert: ShortWithStatus
  override def erased: ShortWithStatus = ErasedShort(v)
  override def unerased: ShortWithStatus = ShortWithValue(v)
}

case class ShortWithValue(v: Short) extends ShortWithStatus {
  override def ok = true
  override def invert: ShortWithStatus = ShortWithValue((~v).toShort)
}

case class ErasedShort(v: Short) extends ShortWithStatus {
  override def ok = false
  override def invert: ShortWithStatus = ErasedShort((~v).toShort)
}
