package rs
import breeze.stats.distributions.{RandBasis, ThreadLocalRandomGenerator}
import gfnumeric.{GF, GFNumeric}
import gfnumeric.GF256_187.given
import org.apache.commons.math3.random.MersenneTwister

import scala.collection.mutable
import scala.util.Random

object ReedSolomonCapabilityLab {
  val random = Random()
  val codewordSize = 37
  val num = summon[GFNumeric[GF[256]]]
  import num.*

  def main(args: Array[String]): Unit = {
    //test()
    totalCapability()
  }

  def generateErroneousData(numErrors: Int) = {
    var data: Array[GF256WithStatus] = null
    while {
      data = Array.fill(37)(0).map(v => GF256WithValue(GF[256](v)))
      for (_ <- 0 until numErrors) {
        val ix = random.nextInt(codewordSize)
        data(ix) = GF256WithValue(GF[256]((data(ix).v.a + random.between(1, 255)) % 256))
      }
      data.count(_.v.a != 0) != numErrors
    } do ()
    data.toIndexedSeq
  }

  def test(): Unit = {
    val rs = ReedSolomon(codewordSize, codewordSize - 4, 120, true)
    val numErrors = 3
    var okCount = 0
    var nonErasedCount = 0
    var totalPostErrorCount = 0
    for (_ <- 0 until 1000000) {
      val tmpdata = generateErroneousData(numErrors)
      val errorPositions = tmpdata.zipWithIndex.filter(_._1.v != zero).map(_._2)
      val data = tmpdata.zipWithIndex.map { case (d, i) =>
        if (errorPositions.contains(i)) {
          if (i == errorPositions(0)) d
          else if (i == errorPositions(1)) d.erased
          else ErasedGF256(GF[256](0))
        } else d
      }
      val decoded = rs.decode(data)
      val decodedOk = decoded.forall(_.v == zero)
      val notErased = decoded.forall(_.ok)
      if (decodedOk) okCount += 1
      if (notErased) nonErasedCount += 1
      totalPostErrorCount += decoded.count(_.v != zero)
    }
    println(s"okCount=$okCount, nonErasedCount=$nonErasedCount, totalErrors=$totalPostErrorCount")
    rs.printStatistics()
  }

  class C2InputStats() {
    val numberOfC2InputErasures: mutable.SortedMap[Int, Int] = collection.mutable.SortedMap[Int, Int]().withDefaultValue(0)
    val numberOfC2InputErasedErrors: mutable.SortedMap[Int, Int] = collection.mutable.SortedMap[Int, Int]().withDefaultValue(0)
    val numberOfC2InputUnerasedErrors: mutable.SortedMap[Int, Int] = collection.mutable.SortedMap[Int, Int]().withDefaultValue(0)
    val numberOfC2InputErasuresAndErrors: mutable.SortedMap[(Int, Int, Int), Int] = collection.mutable.SortedMap[(Int, Int, Int), Int]().withDefaultValue(0)

    def print(): Unit = {
      println(s"C2 input erasures frequencies: ${numberOfC2InputErasures.map((n, c) => s"$n: $c").mkString(", ")}")
      println(s"C2 input erased error frequencies: ${numberOfC2InputErasedErrors.map((n, c) => s"$n: $c").mkString(", ")}")
      println(s"C2 input unerased error frequencies: ${numberOfC2InputUnerasedErrors.map((n, c) => s"$n: $c").mkString(", ")}")
      println(s"C2 input erasures / erased errors / unerased errors frequencies: ${numberOfC2InputErasuresAndErrors.map((n, c) => s"$n: $c").mkString(", ")}")

      val uncorrectableOnly12ErrorCorrection = numberOfC2InputErasuresAndErrors.filter { case ((es, ee, ue), _) =>
        !(ee + ue <= 2)
      }
      val uncorrectableTryCorrectFirst = numberOfC2InputErasuresAndErrors.filter { case ((es, ee, ue), _) =>
        !(ee + ue <= 2 || es <= 4 && ue == 0)
      }
      val uncorrectableTryCanDecode1Error2Erasures = numberOfC2InputErasuresAndErrors.filter { case ((es, ee, ue), _) =>
        !(ee + ue <= 2 || es <= 4 && ue == 0 || es == 2 && ue == 1)
      }

      val suspect = numberOfC2InputErasuresAndErrors.filter { case ((es, ee, ue), _) =>
        es >= 3 && ue == 0 && ee > 2
      }

      println(s"C2 uncorrectableOnly12ErrorCorrection (${uncorrectableOnly12ErrorCorrection.values.sum}): ${uncorrectableOnly12ErrorCorrection.map((n, c) => s"$n: $c").mkString(", ")}")
      println(s"C2 uncorrectableTryCorrectFirst (${uncorrectableTryCorrectFirst.values.sum}): ${uncorrectableTryCorrectFirst.map((n, c) => s"$n: $c").mkString(", ")}")
      println(s"C2 uncorrectableTryCanDecode1Error2Erasures (${uncorrectableTryCanDecode1Error2Erasures.values.sum}): ${uncorrectableTryCanDecode1Error2Erasures.map((n, c) => s"$n: $c").mkString(", ")}")
      println(s"C2 suspect (${suspect.values.sum}): ${suspect.map((n, c) => s"$n: $c").mkString(", ")}")
    }
  }

  def totalCapability(): Unit = {
    val byteErrorRate = 0.007
    val noOfBlocks = 31 * 60 * 30
    val hasTwoErrorCorrection = true
    val oneErrorCorrectionFalsePositiveRate = 1e-6
    val twoErrorCorrectionFalsePositiveRate = 0.01

    val rsC1 = ReedSolomon(37, 33, 120, true)
    val rsC2 = ReedSolomon(36, 32, 120, true)

    val allCorrectData = IndexedSeq.fill(37)(0).map(v => GF256WithValue(GF[256](v)))
    val allErasedData = IndexedSeq.fill(37)(0).map(v => ErasedGF256(GF[256](v)))

    given randBasis: RandBasis = RandBasis(ThreadLocalRandomGenerator(MersenneTwister(Random.nextInt())))
    val binom = breeze.stats.distributions.Binomial(codewordSize, byteErrorRate)
    val cumBinom = (0 until 10).map(binom(_)).tail.scanLeft(binom(0))(_ + _)

    var numberOfInputCodewords = 0
    val numberOfInputErrors = collection.mutable.SortedMap[Int, Int]().withDefaultValue(0)
    val simulatedStats = C2InputStats()
    val decoderStats = C2InputStats()
    var numberOfC2Failures = 0

    for (blockIx <- 0 until noOfBlocks) {
      if (blockIx % (31.25 * 60).toInt == 0)
        println(s"\rSimulated ${blockIx / (31.25 * 60).toInt} minutes")
      val rows = for (_ <- 0 until 36) yield {
        def simulateAndRunC1(): (Seq[GF256WithStatus], Seq[GF256WithStatus]) = { // "theoretical", using decoder
          val p = random.nextDouble()
          val numErrors = cumBinom.indexWhere(_ > p)
          numberOfInputCodewords += 1
          numberOfInputErrors(numErrors) += 1
          val data = generateErroneousData(numErrors)
          val simulatedC1Result = numErrors match {
            case 0 => data
            case 1 => generateErroneousData(0)
            case 2 => if (hasTwoErrorCorrection) allCorrectData else allErasedData
            case _ =>
              val falsePositiveProbability = if (hasTwoErrorCorrection) twoErrorCorrectionFalsePositiveRate else oneErrorCorrectionFalsePositiveRate
              if (random.nextDouble() < falsePositiveProbability) generateErroneousData(numErrors + (if (hasTwoErrorCorrection) 2 else 1)) else data.map(_.erased)
          }
          val dataAfterC1 = rsC1.decode(data)
          (simulatedC1Result, dataAfterC1)
        }
        val (word1Simulated, word1AfterC1) = simulateAndRunC1()
        val (word2Simulated, word2AfterC1) = simulateAndRunC1()
        (word1Simulated.lazyZip(word2Simulated).flatMap((a, b) => Seq(a, b)).toIndexedSeq,
          word1AfterC1.lazyZip(word2AfterC1).flatMap((a, b) => Seq(a, b)).toIndexedSeq)
      }
      val simulatedRows = rows.map(_._1)
      val decodedRows = rows.map(_._2)

      def makeC2InputStatistics(rows: IndexedSeq[IndexedSeq[GF256WithStatus]], stats: C2InputStats): Unit = {
        for (c <- 0 until 66) {
          val column = rows.map(_(c))
          val numberOfErasures = column.count(!_.ok)
          val numberOfErasedErrors = column.count(v => !v.ok && v.v != zero)
          val numberOfUnerasedErrors = column.count(v => v.ok & v.v != zero)
          stats.numberOfC2InputErasures(numberOfErasures) += 1
          stats.numberOfC2InputErasedErrors(numberOfErasedErrors) += 1
          stats.numberOfC2InputUnerasedErrors(numberOfUnerasedErrors) += 1
          stats.numberOfC2InputErasuresAndErrors(numberOfErasures, numberOfErasedErrors, numberOfUnerasedErrors) += 1
        }
      }
      makeC2InputStatistics(simulatedRows, simulatedStats)
      makeC2InputStatistics(decodedRows, decoderStats)

      // run c2
      for (c <- 0 until 66) {
        val column = decodedRows.map(_(c))
        val dataAfterC2 = rsC2.decode(column)
        if (dataAfterC2.exists(_.v != zero))
          numberOfC2Failures += 1
      }
    }
    println(s"numberOfInputCodewords=$numberOfInputCodewords (total ${numberOfInputCodewords * 36 * 2} bytes)")
    println(s"Error frequencies: ${numberOfInputErrors.map((n, c) => s"$n: $c").mkString(", ")}")
    println("\nSimulated C2 input stats\n")
    simulatedStats.print()
    println("\nDecoder C2 input stats\n")
    decoderStats.print()
    println("\nC2 stats\n")
    rsC2.printStatistics()
    println(s"Number of C2 failures (output not zero): $numberOfC2Failures")
  }
}
