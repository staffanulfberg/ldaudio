// Copyright 2021 Staffan Ulfberg staffan@ulfberg.se
package rs

import gfnumeric.GFNumeric.*
import gfnumeric.{GF, GFNumeric}

import scala.collection.immutable.SortedMap

object ReedSolomon {
  def printTable(table: IndexedSeq[Byte]): Unit = {
    for (i <- 0 until 256) {
      println(f"""      "${(table(i) & 0xff).toBinaryString.toLong}%08d" when "${i.toBinaryString.toLong}%08d",""")
    }
  }
}

class ReedSolomon(val n: Int, val k: Int, fcr: Int, makeCorrections: Boolean)(using num: GFNumeric[GF[256]]) {
  import num.*
  private val alpha = summon[GFNumeric[GF[256]]].alpha
  lazy val alphaInverse: GF[256] = alpha.inverse
  val alphaSquared: GF[256] = alpha * alpha

  val H: IndexedSeq[IndexedSeq[GF[256]]] = {
    (0 until n - k).map { row =>
      (0 until n).map { col =>
        pow(pow(alpha, row + fcr), col)
      }
    }
  }

  val statistics: collection.mutable.Map[String, Int] = collection.mutable.HashMap[String, Int]().withDefaultValue(0)

  def resetStatistics(): Unit = statistics.clear()

  def printStatistics(): Unit = {
    statistics.to(SortedMap).foreach { case (s, i) =>
      println(f"$s%-35s$i%5d")
    }
  }

  def printArray(s: String, xs: IndexedSeq[GF256WithStatus]): Unit = {
    println(f"$s%-16s ${xs.map(d => if (d.ok) f" ${d.v.toInt}%02x " else f"(${d.v.toInt}%02x)").mkString(" ")}")
  }

  def doDecode(data: Array[GF256WithStatus]): Unit = {
    val numberOfErasures = data.count(!_.ok)
    val syndromes = computeSyndromes(data.toIndexedSeq)

    if (syndromes.forall(_ == zero)) {
      if (numberOfErasures == 0) {
        statistics("input ok") += 1
      } else {
        uneraseAll(data)
        // statistics(s"syndromes zero with $numberOfErasures erasures, unerased") += 1
      }
    } else {
      val determinant = syndromes(1) * syndromes(1) + syndromes(0) * syndromes(2)

      val corrected = if (numberOfErasures >= 1) {
        if (tryDecodeErasures(syndromes, data))
          true
        else if (numberOfErasures == 2) // since erasure decoding failed we know we have at least one error
          tryDecodeOneErrorTwoErasures(syndromes, data)
        else if (numberOfErasures == 1 && determinant != zero)
          tryCorrectTwoErrors(syndromes, data)
        else if (numberOfErasures == 1 && determinant == zero) // in this case the erasure is wrong -- risk of incorrect decoding?
          tryCorrectOneError(syndromes, data)
        else
          false
      } else {
        if (determinant != zero)
          tryCorrectTwoErrors(syndromes, data)
        else
          tryCorrectOneError(syndromes, data)
      }

      if (corrected) {
        val s = computeSyndromes(data.toIndexedSeq)
        if (s.count(_ != zero) != 0)
          println(s"Still ${s.count(_ != zero)} non-zero syndromes after correction")
        uneraseAll(data)
      } else
        eraseAll(data)
    }
  }

  protected def computeSyndromes(data: IndexedSeq[GF256WithStatus]): IndexedSeq[GF[256]] = {
    for (i <- 0 until n - k)
      yield H(i).lazyZip(data).map { (h, a) => h * a.v }.reduce(_ + _)
  }

  protected def eraseAll(data: Array[GF256WithStatus]): Unit = {
    for(i <- data.indices)
      data(i) = data(i).erased
    statistics("erased all") += 1
  }

  protected def uneraseAll(data: Array[GF256WithStatus]): Unit = {
    for(i <- data.indices)
      data(i) = data(i).unerased
    statistics("unerased all") += 1
  }

  // The first symbol in the input is the highest order coefficient
  def decode(rawData: IndexedSeq[GF256WithStatus]): IndexedSeq[GF256WithStatus] = {
    require(rawData.sizeIs == n)
    val data = rawData.reverse.toArray // we want the right end of the codeword to have location 0 -- this makes the index for coefficients the same as their corresponding monomial power

    statistics("numberOfCalls") += 1

    if (makeCorrections)
      doDecode(data)

    val result = data.reverse.toIndexedSeq

    if (k == 24)
      result.take(12) ++ result.drop(16)
    else
      result.take(k)
  }

  protected def tryCorrectOneError(syndromes: IndexedSeq[GF[256]], data: Array[GF256WithStatus]): Boolean = {
    if (syndromes.contains(zero)) {
      statistics("single error some syndrome zero") += 1
      false
    } else {
      val x0 = syndromes(1) * syndromes(0).inverse
      val errorPos0 = log(x0)
      val y0 = syndromes(0) * pow(alpha, errorPos0 * fcr).inverse

      if (errorPos0 < 0 || errorPos0 >= n) {
        statistics("single error outside range") += 1
        false
      } else if (syndromes(2) * syndromes(1).inverse != x0 || syndromes(3) * syndromes(2).inverse != x0) { // not in vhdl code
        statistics("single error inconsistent syndromes") += 1
        false
      } else {
        data(errorPos0) = GF256WithValue(data(errorPos0).v + y0)
        statistics("single correction count") += 1
        true
      }
    }
  }

  protected def tryCorrectTwoErrors(syndromes: IndexedSeq[GF[256]], data: Array[GF256WithStatus]): Boolean = {
    val determinant = syndromes(1) * syndromes(1) + syndromes(0) * syndromes(2)
    if (determinant == zero)
      statistics("dual determinant zero")
      false
    else {
      val detInv = determinant.inverse
      val lambda1 = (syndromes(1) * syndromes(2) + syndromes(0) * syndromes(3)) * detInv
      val lambda2 = (syndromes(2) * syndromes(2) + syndromes(1) * syndromes(3)) * detInv

      var roots: List[(GF[256], Int)] = Nil // inverse of root, i for root (alpha^-i, logAlpha(alpha^-i))
      var alphaPowI = unit
      var lambda1TimesAlphaPowI = lambda1 // for i == 0
      var lambda2TimesAlphaPow2I = unit // we swap lambda0 (which is 1) and lambda2 in order to get the error locations directly without inversion
      for (i <- 0 until n) {
        if (lambda1TimesAlphaPowI + lambda2TimesAlphaPow2I == lambda2)
          roots ::= (alphaPowI, i)
        alphaPowI = alphaPowI * alpha
        lambda1TimesAlphaPowI = lambda1TimesAlphaPowI * alpha
        lambda2TimesAlphaPow2I = lambda2TimesAlphaPow2I * alphaSquared

//        println(f"$i: ${alphaPowI.a}%02x ${alphaPowMinusI.a}%02x ${lambda1TimesAlphaPowI.a}%02x ${lambda2TimesAlphaPow2I.a}%02x")
      }

      if (roots.sizeIs != 2) {
        statistics("dual too few roots") += 1
        false
      } else {
        val (x0, errorPos0) = roots.head
        val (x1, errorPos1) = roots.last

        assert (errorPos0 >= 0 && errorPos0 < n && errorPos1 >= 0 && errorPos1 < n)

        val x0PlusX1Inv = (x0 + x1).inverse
        val y0 = (syndromes(1) + syndromes(0) * x1) * x0PlusX1Inv * pow(alpha, errorPos0 * fcr).inverse
        val y1 = (syndromes(1) + syndromes(0) * x0) * x0PlusX1Inv * pow(alpha, errorPos1 * fcr).inverse

        data(errorPos0) = GF256WithValue(data(errorPos0).v + y0)
        data(errorPos1) = GF256WithValue(data(errorPos1).v + y1)
        statistics("dual correction count") += 1
        true
      }
    }
  }

  protected def tryDecodeErasures(syndromes: IndexedSeq[GF[256]], data: Array[GF256WithStatus]): Boolean = {
    val errorPositions = data.toIndexedSeq.zipWithIndex.filter(!_._1.ok).map(_._2)
    if (errorPositions.sizeIs > syndromes.size) {
      statistics(s"erasure decoding failed with ${errorPositions.size} erasures")
      false
    } else {
      val x = errorPositions.map(pow(alpha, _))
      val y = ReedSolomonUtil.solveSyndromeEquations(x, syndromes.take(errorPositions.size))

      for ((errorPos, error) <- errorPositions zip y)
        data(errorPos) = GF256WithValue(data(errorPos).v + error * pow(alpha, errorPos * fcr).inverse)

      val s = computeSyndromes(data.toIndexedSeq)
      if (s.count(_ != zero) != 0) {
        // undo incorrect decoding
        for ((errorPos, error) <- errorPositions zip y)
          data(errorPos) = ErasedGF256(data(errorPos).v + error * pow(alpha, errorPos * fcr).inverse)
        statistics(s"erasure decoding incorrect decoding ${errorPositions.size} erasures") += 1
        false
      } else {
        statistics(s"corrected ${errorPositions.size} erasures") += 1
        true
      }
    }
  }

  protected def tryDecodeOneErrorTwoErasures(syndromes: IndexedSeq[GF[256]], data: Array[GF256WithStatus]): Boolean = {
    val errorPositions = data.toIndexedSeq.zipWithIndex.filter(!_._1.ok).map(_._2)
    if (errorPositions.sizeIs != 2) {
      statistics(s"one error two erasures needs two erasures")
      false
    } else {
      // See "Method for correcting both errors and erasures of RS codes using error-only and erasure-only decoding algorithms"
      // by Erl-Huei Lu, Pen-Yao Lu, Tso-Cho Chen (https://doi.org/10.1049/el.2013.1521)

      // Think of y0 and y1 being the erased errors at locators x0 and x1
      val alphaInv = alpha.inverse
      val s0prime = syndromes(0) + syndromes(1) * pow(alphaInv, errorPositions(0))
      val s1prime = syndromes(1) * pow(alphaInv, errorPositions(0)) + syndromes(2) * pow(alphaInv, 2 * errorPositions(0))
      val s2prime = syndromes(2) * pow(alphaInv, 2 * errorPositions(0)) + syndromes(3) * pow(alphaInv, 3 * errorPositions(0))
      val posDiff = errorPositions(1) - errorPositions(0) // positive
      val s0bis = s0prime + s1prime * pow(alphaInv, posDiff)
      val s1bis = s1prime * pow(alphaInv, posDiff) + s2prime * pow(alphaInv, 2 * posDiff)

      if (s0bis == zero || s1bis == zero) {
        statistics("single correction with two erasures failed s0bis or s1bis zero") += 1
        false
      } else {
        val x2prime = s1bis * s0bis.inverse
        val errorPos2prime = log(x2prime) // this is errorPos2 - errorPos1
        val errorPos2 = (errorPos2prime + errorPositions(1)) % 255 // Can this ever be out of range? Tried to make it happen but not so far
        if (errorPos2 < 0 || errorPos2 >= n) {
          statistics("single correction with two erasures failed errorPos2 out of range") += 1
          false
        } else {
          val y2prime = s0bis
          val x0 = alphaPow(errorPositions(0))
          val x2 = alphaPow(errorPos2)

          val check1 = unit + x0.inverse * x2
          val check2 = unit + x2prime
          if (check1 == zero || check2 == zero) {
            statistics(s"single correction with two erasures failed zero inverse $check1 $check2") += 1
            false
          } else {
            val y2BeforeFcrAdjust = y2prime * (unit + x0.inverse * x2).inverse * (unit + x2prime).inverse
            val y2 = y2BeforeFcrAdjust * pow(alpha, errorPos2 * fcr).inverse

            data(errorPos2) = GF256WithValue(data(errorPos2).v + y2)

            val fixedSyndromes = IndexedSeq(
              syndromes(0) + y2BeforeFcrAdjust,
              syndromes(1) + y2BeforeFcrAdjust * alphaPow(errorPos2)
            )

            val c = tryDecodeErasures(fixedSyndromes, data)
            if (c)
              statistics("single correction with two erasures") += 1
            else
              statistics("single correction with two erasures failed on erasures") += 1
            c
          }
        }
      }
    }
  }
}

object ReedSolomonUtil {
  /**
   * Solves the equation system
   *
   * x(0)^k y(0) + ... + x(n-1)^k y(n-1) = s(k), for all k = 0...n-1
   */
  def solveSyndromeEquations(x: IndexedSeq[GF[256]], s: IndexedSeq[GF[256]])(using num: GFNumeric[GF[256]]): IndexedSeq[GF[256]] = {
    import num.{unit, zero}

    val m = x.size
    assert(s.sizeIs == m)
    assert(!x.contains(zero))

    // compute the matrix by multiplying x element wise to the previous rows
    var row = Array.fill(m)(unit)
    val mat: Array[Array[GF[256]]] = row +: (1 until m).map { _ =>
      row = row.zip(x).map(_ * _)
      row
    }.toArray

    val y = s.toArray
    for (i <- 0 until m - 1) { // for each row, clear column i under it
      val leadingInverse = mat(i)(i).inverse
      for (j <- i until m) // make first element unit
        mat(i)(j) = mat(i)(j) * leadingInverse
      y(i) = y(i) * leadingInverse

      for (k <- i + 1 until m) { // rows under
        val leading = mat(k)(i)
        for (j <- i until m)
          mat(k)(j) = mat(k)(j) + leading * mat(i)(j)
        y(k) = y(k) + leading * y(i)
      }
    }

    // make bottom right element unit
    val brInverse = mat(m - 1)(m - 1).inverse
    mat(m - 1)(m - 1) = mat(m - 1)(m - 1) * brInverse
    y(m - 1) = y(m - 1) * brInverse

    for (i <- m - 1 until 0 by -1) { // for each row, clear column i over it
      for (k <- i - 1 to 0 by -1) { // rows over
        y(k) = y(k) + mat(k)(i) * y(i)
      }
    }

    y.toIndexedSeq
  }
}

object Test {
  def main(args: Array[String]): Unit = {
    import gfnumeric.GF256_187.given
    val num = summon[GFNumeric[GF[256]]]
    import num.*
    val rs = ReedSolomon(32, 28, 120, true)

    val data = new Array[Int](32)
    data(2) = 7;
    data(4) = 0xaa;
    val rsdata: Array[GF256WithStatus] = data.map(b => GF256WithValue(GF[256](b)))
    val decoded = rs.decode(rsdata.toIndexedSeq)
    rs.printArray("", decoded)
  }
}
