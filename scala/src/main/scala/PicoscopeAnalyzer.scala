// Copyright 2021 Staffan Ulfberg staffan@ulfberg.se

import java.io.{BufferedInputStream, FileInputStream, FileOutputStream, PrintWriter}
import scala.collection.SortedMap
import scala.io.Source
import scala.util.{Failure, Using}
import fft.*
import ac3.*
import muse.MuseProcessor
import rs.GF256WithStatus
import circ.{CircDecoder, EfmStreamDecoder}

/**
 * Reads a binary file created by PicoscopeConverter (the raw efm signal, or the
 * digitized efm signal, but in analog form), and processes this file in several ways.
 */
object PicoscopeAnalyzer {
  enum Action {
    case Passthrough, Bias, Eye, EyeEma, SampleEfm, DecodeEfm, DecodeCirc, FFT, FilterAC3, FilterAC3Alt,
    DemodulateAC3Qpsk, DecodeAC3, DemodulateAndDecodeAC3, DecodeSpdif
  }

  def main(args: Array[String]): Unit = {
    import Action.*
    var action: Action = Bias
    var inputFilename: Option[String] = None
    var outputFilename: Option[String] = None
    var sampleRateOpt: Option[Double] = None
    var outputSampleRateOpt: Option[Double] = None
    var limit: Option[Int] = None
    var skip: Option[Int] = None
    var ldsInput: Boolean = false

    def processArgs: List[String] => Boolean = {
      case Nil => true
      case "-a" :: a :: rest =>
        action = Action.valueOf(a)
        processArgs(rest)
      case "-o" :: fn :: rest =>
        outputFilename = Some(fn)
        processArgs(rest)
      case "-sr" :: rate :: rest =>
        sampleRateOpt = Some(rate.toDouble)
        processArgs(rest)
      case "-osr" :: rate :: rest => // only for Passthrough
        outputSampleRateOpt = Some(rate.toDouble)
        processArgs(rest)
      case "-l" :: l :: rest =>
        limit = Some(l.toInt)
        processArgs(rest)
      case "-lds" :: rest =>
        ldsInput = true
        processArgs(rest)
      case "-skip" :: s :: rest =>
        skip = Some(s.toInt)
        processArgs(rest)
      case fn :: rest if inputFilename.isEmpty =>
        inputFilename = Some(fn)
        processArgs(rest)
      case _ => false
    }

    System.err.println(s"Memory: total ${Runtime.getRuntime.totalMemory() / 1024 / 1024} MiB, max ${Runtime.getRuntime.maxMemory() / 1024 / 1024} MiB")

    assert(processArgs(args.toList), "Bad command line arguments.")
    if (inputFilename.isEmpty) {
      System.err.println("No input filename")
      System.exit(1)
    }
    val sampleRate: Double = sampleRateOpt match {
      case Some(sr) => sr
      case None =>
        action match {
          case DecodeAC3 => 0
          case Passthrough =>
            if (outputSampleRateOpt.isDefined) {
              System.err.println("Missing input sample rate")
              System.exit(1)
              -1
            } else 0
          case _ =>
            System.err.println("Missing input sample rate")
            System.exit(1)
            -1
        }
    }

    val stream = BufferedInputStream(FileInputStream(inputFilename.get))
    skip.foreach(stream.skip(_))
    val bytes = try {
      val byteCount = (Seq(stream.available) ++ limit.map(l => (l * (if (ldsInput) 1.25 else 1.0)).toInt)).min
      val bytes = new Array[Byte](byteCount)
      stream.read(bytes)
      if (ldsInput) {
        val converted = new Array[Byte](byteCount / 5 * 4) // round down
        for (i <- converted.indices)
          val bitIndex = i % 4 * 2
          val byteIndex = i / 4 * 5 + i % 4
          converted(i) = (((((bytes(byteIndex).toInt & 0xff) << (bitIndex + 2)) | ((bytes(byteIndex + 1).toInt & 0xff) >> (6 - bitIndex))) >> 2)).toByte
        converted
      } else
        bytes
    } finally
      stream.close()
    val shortValues = bytes.map(b => (b & 0xff).toShort)

    val t0 = System.currentTimeMillis()
    action match {
      case Passthrough =>
        val bytes: Array[Byte] = outputSampleRateOpt.fold(shortValues.map(_.toByte)) { osr =>
          val buffer = collection.mutable.ArrayBuffer[Byte]()
          var t = 0.0
          var inputTime = 0.0
          var lastV: Short = 0
          shortValues.foreach { v =>
            while (t < inputTime) {
              val part = (inputTime - t) * sampleRate // 0 if close to  next sample, 1 if close to previous
              buffer += (lastV * part + v * (1 - part)).toByte
              t += 1 / osr
            }
            inputTime += 1 / sampleRate
            lastV = v
          }
          buffer.toArray
        }
        outputFilename.foreach { fn =>
          val f = FileOutputStream(fn)
          f.write(bytes)
          f.close()
        }
      case Bias => bias(digitize(shortValues.toIndexedSeq)) // Just checks how many values are ove/under 128
      case Eye => // Checks distances between edges, when signal is digitized trivially
        val digitized = digitize(shortValues.toIndexedSeq)
        eye(digitized)
        bias(digitized)
      case EyeEma => // Checks distances between edged, when signal is digitized using a simple filter
        val digitized = digitizeEma(shortValues.toIndexedSeq)
        eye(digitized)
        bias(digitized)
      case SampleEfm => // sample the efm stream one per clock cycle, using a recovered clock from the signal
        val digitized = digitize(shortValues.toIndexedSeq)
        val efmBits = reclock(digitized)
        outputFilename.foreach { fn =>
          val f = FileOutputStream(fn)
          f.write(efmBits.map(bit => if (bit) 1.toByte else 0.toByte).toArray)
          f.close()
        }
      case DecodeEfm => // samples the efm stream and then decodes it into frames
        val digitized = digitize(shortValues.toIndexedSeq)
        val efmBits = reclock(digitized)
        val frames = EfmStreamDecoder.decode(efmBits)
        outputFilename.foreach { fn =>
          val f = FileOutputStream(fn)
          // FIXME: implement!
          f.close()
        }
      case DecodeCirc => // samples the efm stream, decodes it into frames, and runs the CIRC decoder
        val digitized = digitize(shortValues.toIndexedSeq)
        val efmBits = reclock(digitized)
        val frames = EfmStreamDecoder.decode(efmBits)
        val circDecoder = CircDecoder()
        val circDecoded = circDecoder.decode(frames)
        val samples = circDecoder.convertToSamples(circDecoded)
        circDecoder.printStatistics()
        outputFilename.foreach { fn =>
          val f = FileOutputStream(fn)
          samples.foreach { (left, right) =>
            val a = Array[Byte](
              (left & 0xff).toByte, ((left & 0xff00) >> 8).toByte,
              (right & 0xff).toByte, ((right & 0xff00) >> 8).toByte) // little endian
            f.write(a)
          }
          f.close()
        }
      case FFT =>
        // The frequency resolution is sampleFrequency / n.
        val n = 1024 * 16
        val complexValues = shortValues.take(n).toIndexedSeq.map(sv => Complex(sv.toDouble, 0))
        val transform = fft.FFT.fft(complexValues)
        outputFilename.foreach { fn => // output to a file suitable for gnuplot
          val f = FileOutputStream(fn)
          val p = PrintWriter(f)
          transform.zipWithIndex.foreach((v, i) => p.println(f"${i * sampleRate / n} ${v.abs}"))
          p.close()
        }
      case FilterAC3 =>
        val n = 1024 * 16
        val filterResponse = (0 until n).map { i =>
          val f = (if (i < n / 2) i else n - i) * sampleRate / n
          if (math.abs(f - 2.88e6) < 150e3) Complex(1, 0) else Complex(0, 0)
        }
        val rawFilterCoefficients = fft.FFT.rfft(filterResponse)
        assert(rawFilterCoefficients.forall(_.im < 1e-10)) // make sure the filter only has real coefficients
        assert((1 until rawFilterCoefficients.size).forall { i => // make sure the filter is symmetric
          math.abs(rawFilterCoefficients(i).re - rawFilterCoefficients(n - i).re) < 1e-10 })
        val filterSize = 64
        def hann(a0: Double)(i: Int) = a0 - (1 - a0) * math.cos(2 * math.Pi * i / n) // Hann window function.
        val hamming = hann(0.53836)
        val filterCoefficientsTmp = (rawFilterCoefficients.takeRight(filterSize - 1) ++ rawFilterCoefficients.take(filterSize)).map(_.re)
        val filterCoefficients = filterCoefficientsTmp.zipWithIndex.map((v, i) => hamming(i - filterSize) * v).toArray
        val filtered = collection.mutable.ArrayBuffer[Byte]()
        var i = filterCoefficients.size
        while (i < shortValues.length) {
          var o = 0.0
          var j = 0
          while (j < filterCoefficients.size) {
            o = o + filterCoefficients(j) * shortValues(i - j).toDouble
            j += 1
          }
          if (i % 1000000 == 0) println(s"\r$i")
          i += 1
          filtered += ((o * 64 + 128).toInt & 0xff).toByte
        }
        println("                ")
//        {
//          val f = new PrintWriter(new FileOutputStream("filter.txt"))
//          filterCoefficients.foreach { v => f.println(f"$v") }
//          f.close()
//        }
//        {
//          val complexValues = filterCoefficients.padTo(1024*16, 0.0).toIndexedSeq.map(sv => Complex(sv, 0))
//          val filterfft = fft.FFT.fft(complexValues)
//
//          val f = new PrintWriter(new FileOutputStream("filterfft.txt"))
//          filterfft.zipWithIndex.foreach((v, i) => f.println(f"${i * sampleRate / n} ${v.abs}"))
//          f.close()
//        }
        outputFilename.foreach { fn =>
          val f = FileOutputStream(fn)
          f.write(filtered.toArray)
          f.close()
        }
      case FilterAC3Alt =>
        /* FIR filter designed with http://t-filter.appspot.com
           sampling frequency: 46080000 Hz
           fixed point precision: 13 bits
            * 0 Hz - 2300000 Hz: gain = 0, desired attenuation = -20 dB, actual attenuation = n/a
            * 2736000 Hz - 3024000 Hz: gain = 1, desired ripple = 5 dB, actual ripple = n/a
            * 3400000 Hz - 23000000 Hz: gain = 0, desired attenuation = -20 dB, actual attenuation = n/a
        */
        val filterCoefficients: Array[Byte] = Array(
          -6, 70, 11, 4, -1, -6, -12, -15, -17, -15, -9, -1, 9, 20, 29, 35,
          35, 30, 20, 4, -13, -31, -45, -54, -54, -47, -31, -9, 15, 40, 59, 71,
          72, 62, 41, 14, -18, -48, -72, -86, -87, -74, -50, -16, 21, 56, 84, 99,
          100, 85, 57, 19, -23, -62, -92, -108, -109, -92, -62, -21, 23, 64, 95, 112,
          112, 95, 64, 23, -21, -62, -92, -109, -108, -92, -62, -23, 19, 57, 85, 100,
          99, 84, 56, 21, -16, -50, -74, -87, -86, -72, -48, -18, 14, 41, 62, 72,
          71, 59, 40, 15, -9, -31, -47, -54, -54, -45, -31, -13, 4, 20, 30, 35,
          35, 29, 20, 9, -1, -9, -15, -17, -15, -12, -6, -1, 4, 11, 70, -6)
        assert(filterCoefficients.sizeIs == 128)

        def writeVhdl() = filterCoefficients.toSeq.grouped(8).map { g =>
          s"${g.map(v => s"to_signed($v, 8), ").mkString("")}"
        }.mkString("\n")

        if (sampleRate != 46080000) {
          System.err.println("Only 4608000 Hz sample rate supported")
          System.exit(1)
        }
        val filtered = collection.mutable.ArrayBuffer[Byte]()
        var i = filterCoefficients.length
        var absmax = 0
        while (i < shortValues.length) {
          var o = 0
          var j = 0
          while (j < filterCoefficients.length) {
            val signedValue = shortValues(i - j) - 128
            o = o + filterCoefficients(j) * signedValue
            j += 1
          }
          if (i % 1000000 == 0) println(s"\r$i")
          i += 1
          assert(math.abs(o) < 32768 * 128)
          if (math.abs(o) > absmax)
            absmax = o
          val byteVal = (o >> 9) + 128
          assert(byteVal >= 0 && byteVal <= 255)
          filtered += byteVal.toByte // the output is unsigned!
        }
        println(s"max=$absmax                ")
        outputFilename.foreach { fn =>
          val f = FileOutputStream(fn)
          f.write(filtered.toArray)
          f.close()
        }
      case DemodulateAC3Qpsk =>
        val symbols = QpskDecoder.decode(sampleRate, shortValues.toIndexedSeq)
        outputFilename.foreach { fn =>
          val p = PrintWriter(FileOutputStream(fn))
          p.write(symbols.map(_.toChar).mkString)
          p.close()
        }
      case DecodeAC3 =>
        val ac3blocks = SymbolDecoder.decode(shortValues.map(_.toByte).toIndexedSeq)
        outputFilename.foreach { fn =>
          val f = FileOutputStream(fn)
          ac3blocks.foreach { block => f.write(block.toArray) }
          f.close()
        }
      case DemodulateAndDecodeAC3 =>
        val symbols = QpskDecoder.decode(sampleRate, shortValues.toIndexedSeq)
        val ac3blocks = SymbolDecoder.decode(symbols)
        outputFilename.foreach { fn =>
          val f = FileOutputStream(fn)
          ac3blocks.foreach { block => f.write(block.toArray) }
          f.close()
        }

      case DecodeSpdif =>
        val digitized = digitize(shortValues.toIndexedSeq)
        val subframes = SpdifDecoder.readSubframes(sampleRate, digitized)
//        val frames = SpdifDecoder.convertSubframesToFrames(subframes)
        outputFilename.foreach { fn => // output to a file suitable for gnuplot
          val p = PrintWriter(FileOutputStream(fn))
          subframes.foreach { subFrame =>
            p.println(subFrame.toString)
          }
//          frames.foreach { frame =>
//            p.println(s"${frame.map(b => f"$b%02x").mkString(" ")}")
//          }
          p.close()
        }
    }
    println(f"Total time ${(System.currentTimeMillis() - t0) / 1000.0}%.2f s")
  }

  def digitize(bytes: IndexedSeq[Short], threshold: Short = 128): IndexedSeq[Boolean] = {
    bytes.map(byte => byte >= threshold)
  }

  def digitizeEma(bytes: IndexedSeq[Short]): IndexedSeq[Boolean] = {
    var avg = 128.0
    val out = new Array[Boolean](bytes.size)
    for (i <- bytes.indices) {
      val byte = bytes(i)
      val bit = byte >= avg
      avg = (if (bit) 256 else 0) * 0.00005 + avg * 0.99995
      out(i) = bit
      if (i % 100000 == 0)
        println(f"avg=$avg%.1f")
    }
    out.toIndexedSeq
  }

  // Checks if the binary input array (one bit per element) is biased
  def bias(bits: IndexedSeq[Boolean]): Unit = {
    val count = bits.size
    val hiCount = bits.count(_ == true)
    println(f"Total $count bytes, $hiCount hi (${100.0 * hiCount / count}%.1f %%)")
  }

  def eye(bits: IndexedSeq[Boolean]) = {
    val zeroCrossings = bits.sliding(2).zipWithIndex.flatMap { case (IndexedSeq(a, b), i) =>
      Option.when(a != b)(i)
    }.toIndexedSeq

    val bins = collection.mutable.ArrayBuffer.fill[Int](350)(0)

    for (i <- zeroCrossings.indices) {
      var j = 1
      while (i + j < zeroCrossings.size && zeroCrossings(i + j) - zeroCrossings(i) < bins.size) {
        bins(zeroCrossings(i + j) - zeroCrossings(i)) += 1
        j += 1
      }
    }

    for (i <- bins.indices)
      println(f"$i%3d ${bins(i)}%4d")
  }

  // Decodes the raw samples data into bits by locking on bit transitions and
  // sample at the EFM bitrate
  // FIXME: honor the -sr argument to set the input sample rate; currently 100 MHz is assumed
  def reclock(bits: IndexedSeq[Boolean]): IndexedSeq[Boolean] = {
    val output = collection.mutable.ArrayBuffer[Boolean]()

    var totalBitsIn = 0
    val counterBits: Int = 16
    var clkCounter: Int = 0

    val nominalFrequency = 4321800
    val nominalAdd: Int = ((1 << counterBits) * nominalFrequency.toLong / 100000000).toInt

    var lastIn = false
    var error: Int = 0
    var errorSum: Int = 0
    val maxErrorSum = 0x7fffff
    val minErrorSum = -0x800000
    var filterOut: Int = 0

    var toggleCount: Int = 0
    var togglePos: Int = 0

    bits.foreach { dataIn =>
      totalBitsIn += 1

      if (dataIn != lastIn) {
        toggleCount += 1
        togglePos = clkCounter
        lastIn = dataIn
      }

      val newCounter = (clkCounter + nominalAdd + filterOut) & ((1 << counterBits) - 1)
      filterOut = 0
      if (newCounter < clkCounter) {
        output += (toggleCount % 2 == 1)

        filterOut =
          if (toggleCount == 1) {
            error = -(togglePos - (1 << (counterBits - 1)))
            if (error > 0 && errorSum + error > maxErrorSum)
              errorSum = maxErrorSum
            else if (error < 0 && errorSum + error < minErrorSum)
              errorSum = minErrorSum
            else
              errorSum += error
            //println(s"T: $toggleCount, errorSum=$errorSum error=$error")
            error / 16 + errorSum / (1 << 14)
          } else {
            errorSum / (1 << 14)
          }

        toggleCount = 0
      }
      clkCounter = newCounter
    }
    output.toIndexedSeq
  }
}