package fft

import FFT.*
/*
 * This code is taken from http://rosettacode.org/wiki/Fast_Fourier_transform#Scala
 */
object FftExample {
  def main(args: Array[String]): Unit = {

    val data = Seq(Complex(1, 0), Complex(1, 0), Complex(1, 0), Complex(1, 0),
      Complex(0, 0), Complex(0, 2), Complex(0, 0), Complex(0, 0))
    println(data)
    println(fft(data))
    println(rfft(fft(data)))
  }
}