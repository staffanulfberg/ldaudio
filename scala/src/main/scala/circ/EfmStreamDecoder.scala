// Copyright 2021 Staffan Ulfberg staffan@ulfberg.se
package circ

import efm.EfmInversion
import rs.{ByteWithStatus, ErasedByte}

import scala.io.Source

object ProcessEfmStream {
  def main(args: Array[String]): Unit = {
    val efmLines = Source.fromResource("ve-snw-cut.nrzm").getLines()
    val bits = efmLines.map { line =>
      val bit = line.toInt
      assert(bit == 0 || bit == 1)
      bit == 1
    }

    val frames = EfmStreamDecoder.decode(bits.toSeq)
    val circDecoder = CircDecoder()
    circDecoder.decode(frames)
    circDecoder.printStatistics()
  }
}

object EfmStreamDecoder {
  def decode(bits: Seq[Boolean]): IndexedSeq[IndexedSeq[ByteWithStatus]] = {
    val output = collection.mutable.ArrayBuffer[IndexedSeq[ByteWithStatus]]()
    var totalBits = 0
    var shiftRegister: Int = 0
    var bitIndex: Int = 0
    var byteIndex: Int = 33
    var outputCtl: ByteWithStatus = ErasedByte(0)
    val outputArray = new Array[ByteWithStatus](32)
    var bitsSinceSync = 0
    var consecutiveSyncs = 0
    var locked = false
    var consecutiveSyncFailures = 0 // if not at the exact expected place

    bits.foreach(handle)

    def handle(bbit: Boolean): Unit = {
      totalBits += 1
      val bit = if (bbit) 1 else 0
      shiftRegister = shiftRegister << 1 | bit

      bitsSinceSync += 1
      val sync = (shiftRegister & 0xffffff) == 0x801002 // 1000 0000 0001 0000 0000 0010

      if (sync && (bitsSinceSync == 588 || !locked)) { // expected -- all fine!
        bitIndex = 0
        byteIndex = 0
        bitsSinceSync = 0
        consecutiveSyncFailures = 0
        consecutiveSyncs += 1
        if (consecutiveSyncs >= 3 && !locked) {
          locked = true
          println(s"locked index $totalBits")
        }
      } else if (bitsSinceSync == 588 && locked) {
        bitIndex = 0
        byteIndex = 0
        bitsSinceSync = 0
        consecutiveSyncFailures += 1
        consecutiveSyncs = 0
        if (consecutiveSyncFailures >= 7 && locked) {
          locked = false
          println(s"lock lost index $totalBits")
        }
      } else if (byteIndex < 33) {
        if (byteIndex > 0) {
          if (bitIndex == 16) {
            val decodeAddr = shiftRegister & 0x3fff // 14 bits
            val octet = EfmInversion.invert(decodeAddr)

            if (byteIndex == 0)
              outputCtl = octet
            else
              outputArray(byteIndex - 1) = octet

            if (byteIndex == 32) {
              val allOk = outputArray.forall(_.ok)
              output += outputCtl +: outputArray.toIndexedSeq
            }
          }
        }
        if (bitIndex == 16) {
          bitIndex = 0
          byteIndex = byteIndex + 1
        } else
          bitIndex = bitIndex + 1
      }
    }

    output.toIndexedSeq
  }

//  def handle(outputCtl: Byte, outputArray: Array[Byte]): Unit = {
//    require(outputArray.length == 32)
//    println(f"ctl=$outputCtl%02x, data=${outputArray.map(d => f"$d%02x").mkString(" ")}")
//  }
}
