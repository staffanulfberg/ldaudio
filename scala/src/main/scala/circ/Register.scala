// Copyright 2021 Staffan Ulfberg staffan@ulfberg.se
package circ

import scala.collection.mutable
import gfnumeric.{GF, GFNumeric}
import rs.{ErasedGF256, GF256WithStatus}

class Register(size: Int)(using num: GFNumeric[GF[256]]) {
  private val values: mutable.IndexedSeq[GF256WithStatus] = mutable.IndexedSeq.fill(size)(ErasedGF256(num.zero))
  def getValues: IndexedSeq[GF256WithStatus] = values.toIndexedSeq
  def set(i: Int)(data: GF256WithStatus): Unit = values(i) = data
}

class DelayLine(time: Int, invert: Boolean, output: GF256WithStatus => Unit) {
  val queue: mutable.Queue[GF256WithStatus] = mutable.Queue()

  def handle(byteWithStatus: GF256WithStatus): Unit = {
    queue.enqueue(byteWithStatus)
    if (queue.size == time + 1) {
      val byteWithStatus = queue.dequeue()
      val out = if (invert) byteWithStatus.invert else byteWithStatus
      output(out)
    }
  }
}
