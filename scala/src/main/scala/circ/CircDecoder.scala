// Copyright 2021 Staffan Ulfberg staffan@ulfberg.se
package circ

import gfnumeric.GFNumeric.*
import gfnumeric.GF256_11d.given
import rs.{ByteWithStatus, ErasedShort, GF256WithStatus, ReedSolomon, ShortWithValue, ValueWithStatus}

import java.io.FileOutputStream

class C1(makeCorrections: Boolean) extends ReedSolomon(32, 28, 0, makeCorrections)
class C2(makeCorrections: Boolean) extends ReedSolomon(28, 24, 0, makeCorrections)

final class CircDecoder {
  val firstValidFrame = 108
  val c1decoder = C1(true)
  val c2decoder = C2(true)

  /*
  val r = Random
  for (_ <- 0 until 100000) {
    val x = new Array[Byte](32)
    x(r.nextInt(32)) = r.nextInt(256).toByte
    x(r.nextInt(32)) = r.nextInt(256).toByte
    x(r.nextInt(32)) = r.nextInt(256).toByte
    val y = c1decoder.decode(x)
    printArray("in", x)
    printArray("out", y)
    assert(y.forall(_ == 0))
  }
  System.exit(0)*/

  val c1InputRegister = Register(32)
  val c2InputRegister = Register(28)
  val outputRegister = Register(24)

  val initialDelays: IndexedSeq[DelayLine] = (0 until 32).map(i =>
    new DelayLine(i % 2, Set(12, 13, 14, 15, 28, 29, 30, 31).contains(i), c1InputRegister.set(i)))

  val c1c2Delays: IndexedSeq[DelayLine] = (0 until 28).map(i =>
    new DelayLine((27 - i) * 4, false, c2InputRegister.set(i)))

  val outputDelays: IndexedSeq[DelayLine] = (0 until 24).map(i =>
    new DelayLine(if (i >= 12) 2 else 0, false, outputRegister.set(i)))

  val leftOutputMap = Seq((0, 1), (12, 13), (2, 3), (14, 15), (4, 5), (16, 17))
  val rightOutputMap = Seq((6, 7), (18, 19), (8, 9), (20, 21), (10, 11), (22, 23))

  val leftBuffer = SingleChannelOutputBuffer()
  val rightBuffer = SingleChannelOutputBuffer()

  assert(Seq(leftOutputMap, rightOutputMap).flatMap {
    _.flatMap { case (a, b) =>
      assert(b == a + 1)
      Seq(a, b)
    }
  }.sorted == (0 until 24))

  val output = collection.mutable.ArrayBuffer[IndexedSeq[ByteWithStatus]]()
  def decode(data: IndexedSeq[IndexedSeq[ByteWithStatus]]): IndexedSeq[IndexedSeq[ByteWithStatus]] = {
    output.clear()
    data.foreach(frame => handle(frame.head, frame.tail))
    output.toIndexedSeq
  }

  var frameCount = 0
  private def handle(ctl: ByteWithStatus, byteData: IndexedSeq[ByteWithStatus]): Unit = {
    require(byteData.length == 32)
    val data = byteData.map(_.toGF256)
    frameCount += 1

    if frameCount == firstValidFrame then
      println("first valid frame")
      c1decoder.resetStatistics()
      c2decoder.resetStatistics()

    for (i <- 0 until 32)
      initialDelays(i).handle(data(i))

    val c1decoded = c1decoder.decode(c1InputRegister.getValues)

    for (i <- c1decoded.indices)
      c1c2Delays(i).handle(c1decoded(i))

    val c2decoded = c2decoder.decode(c2InputRegister.getValues)

    for (i <- c2decoded.indices)
      outputDelays(i).handle(c2decoded(i))

    val out = outputRegister.getValues

    if (frameCount >= firstValidFrame)
      output += out.map(_.toByte)

    //    printArray("input   ", data)
    //    printArray("c1Input ", c1InputRegister.getValues)
    //    printArray("c1Output", c1decoded)
    //    printArray("c2Input ", c2InputRegister.getValues)
    //    printArray("c2Output", c2decoded)
    //    printArray("output  ", outputRegister.getValues)
    //    println()
  }

  def convertToSamples(data: IndexedSeq[IndexedSeq[ByteWithStatus]]): IndexedSeq[(Short, Short)] = {
    data.flatMap { out =>
      if (out.exists(!_.ok))
        println(s"Erased ${out.count(!_.ok)} values in frame $frameCount")

      val Seq(leftOut, rightOut) = Seq(leftOutputMap, rightOutputMap).map {
        _.map { case (aIx, bIx) =>
          if (out(aIx).ok && out(bIx).ok) {
            val a: Byte = out(aIx).v
            val b: Byte = out(bIx).v
            val c: Short = (a.toShort << 8 | (b.toShort & 0xff)).toShort
            ShortWithValue(c)
          } else {
            ErasedShort(0)
          }
        }
      }

      for (i <- 0 until 6) {
        // println(f"l/r: ${leftOut(i)}%04x ${rightOut(i)}%04x")
        leftBuffer.handle(leftOut(i))
        rightBuffer.handle(rightOut(i))
      }

      val o = collection.mutable.ArrayBuffer[(Short, Short)]()
      while (leftBuffer.hasData && rightBuffer.hasData) {
        o += ((leftBuffer.dequeue(), rightBuffer.dequeue()))
      }
      o.toSeq
    }
  }

  /*
  var outputPos = 0

  def output(s: Short) = {
    if (outputPos % 16 == 0) {
      println()
      print(f"$outputPos%06x ")
    }
    print(f"$s%04x ")
    outputPos += 2
  }*/

  def printStatistics(): Unit = {
    println("C1 statistics:")
    c1decoder.printStatistics()
    println("C2 statistics:")
    c2decoder.printStatistics()
    println("Left channel interpolation statistics:")
    leftBuffer.printStatistics()
    println("Right channel interpolation statistics:")
    rightBuffer.printStatistics()
  }

  def printArray(s: String, xs: IndexedSeq[GF256WithStatus]): Unit = {
    println(f"$s%-16s ${xs.map(d => if (d.ok) f"${d.v.toInt}%02x" else "XX").mkString(" ")}")
  }
}

class SingleChannelOutputBuffer {
  private val buffer = collection.mutable.Queue[Short]()
  private var lastRealValue: Short = 0
  private var erasedValues = 0
  private val statistics = collection.mutable.HashMap[Int, Int]().withDefaultValue(0)

  def handle(sample: ValueWithStatus[Short]): Unit = {
    sample match {
      case ShortWithValue(v) =>
        if (erasedValues != 0) {
          statistics(erasedValues) += 1
          for (_ <- 0 until erasedValues) {
            buffer.enqueue(((v + lastRealValue) / 2).toShort)
          }
          erasedValues = 0
        }
        buffer.enqueue(v)
        lastRealValue = v
      case ErasedShort(_) =>
        erasedValues += 1
    }
  }

  def hasData: Boolean = {
    buffer.nonEmpty
  }

  def dequeue(): Short = {
    buffer.dequeue()
  }

  def printStatistics(): Unit = {
    statistics.foreach { case (s, i) =>
      println(f"$s%3d $i")
    }
  }
}
