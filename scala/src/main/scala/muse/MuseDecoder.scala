package muse

import java.io.{BufferedInputStream, DataInputStream, FileInputStream}

/**
 * Produces a series of still images and also audio files for the given input file that consists
 * of MUSE samples (sampled at 16.2 MHz, at the correct sample phase).  The input file does not have
 * to start at a new frame: the code searches for the next complete field.
 *
 * To play back, this converts the output to an mkv file (for B mode audio):
 *
 * ffmpeg -y -framerate 60 -pattern_type glob -i 'resampled*.png' -f s16be -ar 48000 -ac 1 -i channelB1.raw -f s16be -ar 48000 -ac 1 -i channelB2.raw -filter_complex "[1:0][2:0]amerge" out.mkv
 */
object MuseDecoder {
  def main(args: Array[String]): Unit = {
    var outputFilename: Option[String] = None
    var samplesLimit: Option[Int] = None
    var framesLimit: Option[Int] = None
    var skip: Option[Int] = None
    var readFloats = false
    var bigEndian = false

    def processArgs: List[String] => Boolean = {
      case Nil => true
      case "-o" :: fn :: rest =>
        outputFilename = Some(fn)
        processArgs(rest)
      case "-ls" :: l :: rest =>
        samplesLimit = Some(l.toInt)
        processArgs(rest)
      case "-lf" :: l :: rest =>
        framesLimit = Some(l.toInt)
        processArgs(rest)
      case "-skip" :: s :: rest =>
        skip = Some(s.toInt)
        processArgs(rest)
      case "-float" :: rest =>
        readFloats = true // input file consists of floats, default is shorts
        processArgs(rest)
      case "-short" :: rest =>
        readFloats = false // input file consists of shorts
        processArgs(rest)
      case "-big-endian" :: rest =>
        readFloats = false // input file consists of shorts
        bigEndian = true
        processArgs(rest)
      case fn :: rest =>
        processFile(fn)
        processArgs(rest)
    }

    assert(processArgs(args.toList), "Bad command line arguments.")

    def processFile(filename: String): Unit = {
      val clipName = {
        val pos = filename.lastIndexOf('.')
        if (pos > 0 && pos < filename.length() - 1)
          filename.substring(0, pos)
        else
          filename
      }
      println(s"Processing clip $clipName")

      val stream = DataInputStream(BufferedInputStream(FileInputStream(filename)))
      skip.foreach(samples => stream.skip(samples * 2))
      val n = (Seq(if (readFloats) stream.available / 4 else stream.available / 2) :++ samplesLimit).min

      val values =
        if (readFloats) Iterator.continually(stream.readFloat()).map(_ / 4)
        else Iterator.continually {
          val s = stream.readShort()
          if (bigEndian) s else (((s.toInt >> 8) & 0xff) | ((s & 0xff) << 8)).toShort
        }.map(_.toFloat / 4)

      val (initialEq, vs) = {
        val (firstValuesIt, restValues) = values.splitAt(480 * 1125)
        val firstValues = firstValuesIt.toSeq
        val y1 = firstValues.sorted.take(500).last
        val y2 = firstValues.sorted.reverse.take(500).last
        val eq = ((y2 - y1) / (239.0 - 16.0), y1 - 16.0)
        println(s"Initial eq=$eq")
        (eq, firstValues.iterator ++ restValues)
      }

      val museProcessor = MuseProcessor(clipName)
      museProcessor.process(vs.take(n), framesLimit, initialEq)

      stream.close()
    }
  }
}
