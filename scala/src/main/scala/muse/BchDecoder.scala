package muse

import scala.collection.SortedMap
import scala.collection.mutable

/** BCH SEC-DED decoder.  We assume that the distance d of the code is 4.
 *
 * @param n the codeword length
 * @param k the number of information symbols; there are n-k bits for error correction
 * @param generator FIXME rename
 * @param primPoly the primitive polynomial
 *
 * The degree of the primitive polynomial is m.
 * We have n <= 2 ** m - 1
 * Assume n > 2 ** (m - 1) - 1 => 2 ** (m - 1) < n + 1 <= 2 ** m => m - 1 < log2(n+1) <= m => m = ceil(log2(n+1))
 *
 * The generator polynomial has degree k.
 */
class BchDecoder(n: Int, k: Int, generator: Int) {
  //require((generator & (1 << (n - k))) != 0)
  val statistics: mutable.Map[String, Int] = collection.mutable.HashMap[String, Int]().withDefaultValue(0)

  def resetStatistics(): Unit = statistics.clear()

  def printStatistics(): Unit = {
    statistics.to(SortedMap).foreach { case (s, i) =>
      println(f"$s%-35s$i%5d")
    }
  }

  // Notice we only compute the powers up to n - 1.  This means that in the case of
  // multiple errors the log could fail, but we cannot correct in that case anyway.
  // If we make the log table complete we need to check if the log is in range for shortened codes.
  val alphaPowers = (0 until n - 1).scanLeft(1) { case (alphaPow, _) =>
    if ((alphaPow & (1 << (n - k - 1 - 1))) != 0)
      (alphaPow << 1) ^ generator
    else alphaPow << 1
  }

  val logs = alphaPowers.zipWithIndex.map((p, i) => p -> i).toMap

  // bits are left to right with msb first -- we therefore index bit i (0 based) as bits(n - i - 1)
  def decode(bits: IndexedSeq[Int]): (IndexedSeq[Int], Boolean) = {
    val s0 = bits.sum % 2
    val s1 = (0 until n).map(i => bits(n - i - 1) * alphaPowers(i)).reduce(_ ^ _)
    if (s0 == 0 && s1 == 0) {
      statistics("input ok") += 1
      (bits.slice(0, k), true) // ok
    } else if (s0 != 0) {
      logs.get(s1).fold {
        statistics("cannot correct bad syndrome") += 1
        (bits.slice(0, k), false)
      } { logS1 => // found log -- correct 1 bit
        statistics("corrected 1 error") += 1
        (bits.zipWithIndex.map((b, i) => if (logS1 == n - i - 1) 1 - b else b).slice(0, k), true)
      }
    } else {
      statistics("cannot correct two errors") += 1
      (bits.slice(0, k), false)
    }
  }
}
