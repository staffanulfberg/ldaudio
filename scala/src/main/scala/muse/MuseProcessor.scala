package muse

import com.sun.org.apache.xalan.internal.xsltc.compiler.Template

import java.awt.image.BufferedImage
import javax.imageio.ImageIO
import java.io.{File, FileInputStream, FileOutputStream, PrintWriter}
import javax.sound.sampled.AudioPermission
import scala.collection.immutable.SortedMap

class MuseProcessor(clipName: String) {
  private val audioProcessor = AudioProcessor(clipName)
  // input stream consists of floats, in "byte" range, i.e., should be 16-239 if the signal is properly normalized
  def process(values: Iterator[Float], framesLimit: Option[Int], initialEq: (Double, Double)): Unit = {
    val syncSeekValueCount = 480 * 1125 * 2 + 9 // two frames
    val (firstValuesIt, restValues) = values.splitAt(syncSeekValueCount) // should contain 10 syncs
    val firstValues = firstValuesIt.toIndexedSeq

    def isSyncGood(off: Int, expectedPositive: Boolean) = { // checks if the line starting at off has a positive or a negative sync
      val isPos = firstValues(off + 3) < firstValues(off + 5) && firstValues(off + 5) < firstValues(off + 7)
      val isNeg = firstValues(off + 3) > firstValues(off + 5) && firstValues(off + 5) > firstValues(off + 7)
      expectedPositive && isPos || !expectedPositive && isNeg
    }

    val goodSeqsForOffsets = for (pixelOffset <- 0 until 480) yield {
      val goodForStarts = for (startLine <- 0 until 2000 by 50) yield {
        val goodSeqPerPhase = for (phase <- 0 to 1) yield {
          val goodSyncs = for (line <- startLine until startLine + 50) yield {
            val off = line * 480 + pixelOffset
            isSyncGood(off, (line + phase) % 2 == 0)
          }
          goodSyncs.count(identity) > 47
        }
        goodSeqPerPhase.contains(true)
      }
      pixelOffset -> goodForStarts.count(identity)
    }
    val bestPixelOffset = goodSeqsForOffsets.maxBy(_._2)._1

    val lineOffsetGoodnesses = (0 until 1125).map { lineOffset =>
      val goodSyncsForOffset = (0 until 1125).map { line =>
        val expectedPosSync = line == 0 || line > 2 && line % 2 == 1
        isSyncGood((lineOffset + line) * 480 + bestPixelOffset, expectedPosSync)
      }.count(identity)
      lineOffset -> goodSyncsForOffset
    }
    val bestLineOffset = lineOffsetGoodnesses.maxBy(_._2)._1

    var totalPixelCounter = 0
    var pixelCounter: Int = 1 // 1 to 480
    var lineCounter: Int = 1 // 1 to 1125
    var frameCounter: Int = 1 // counts forever

    val numberOfStoredFields = 4
    val fieldStores = Array.fill(numberOfStoredFields)(FieldStore())
    var currentFieldStoreIndex = 0
    val controlSignal = Array.ofDim[Int](5, 94) // 94 includes the guard area
    // keeps lines 3 -- 46 (and 565 -- 608 for the second field)
    val audioLines: Array[Array[Int]] = Array.ofDim(44, 480)
    val hd = new Array[Float](11)

    fieldStores(currentFieldStoreIndex).frameNumber = frameCounter
    fieldStores(currentFieldStoreIndex).fieldParity = 0

    def newField(advanceFrame: Boolean): Unit = {
      audioProcessor.handle(audioLines, advanceFrame)
      VideoInterpolator.createOutputFrame(clipName, (1 to numberOfStoredFields).map(i => fieldStores((currentFieldStoreIndex + i) % numberOfStoredFields)))

      println(s"Frame $frameCounter, field ${if (advanceFrame) 1 else 0} done")

      currentFieldStoreIndex = (currentFieldStoreIndex + 1) % numberOfStoredFields
      if (advanceFrame) frameCounter += 1
      fieldStores(currentFieldStoreIndex).frameNumber = frameCounter
      fieldStores(currentFieldStoreIndex).fieldParity = if (advanceFrame) 0 else 1
      fieldStores(currentFieldStoreIndex).controlSignal = ControlSignalDecoder.decode(controlSignal.map(_.toIndexedSeq).toIndexedSeq)
    }

    // Used for equalization
    var line1HighSum = 0 // reference value is 239
    var line2LowSum = 0 // reference value is 16
    var blankingSum = 0 // reference value is 128
    var eqA = initialEq._1 // We assume that the sampled value = a * (transmitted value) + b
    var eqB = initialEq._2

    val firstFrameDataFile = PrintWriter(FileOutputStream("first-frame-x.txt"))

    println(s"Skipping ${bestLineOffset * 480 + bestPixelOffset} initial samples")
    val valuesAtStartOfFrame = (firstValues.iterator ++ restValues).drop(bestLineOffset * 480 + bestPixelOffset)
    framesLimit.fold(valuesAtStartOfFrame)(limit => valuesAtStartOfFrame.take(limit * 480 * 1125)).foreach { sampledValue =>
      val sample = ((sampledValue - eqB) / eqA).toFloat

      if (frameCounter == 1)
        firstFrameDataFile.println(s"$lineCounter $pixelCounter ${sample.toInt}")
      else if (frameCounter == 2 && lineCounter == 1 && pixelCounter == 1) {
        firstFrameDataFile.close()
      }

      if (pixelCounter < 12)
        hd(pixelCounter - 1) = sample

      if (lineCounter >= 3 && lineCounter <= 46)
        audioLines(lineCounter - 3)(pixelCounter - 1) = sample.toInt
      if (lineCounter >= 565 && lineCounter <= 608)
        audioLines(lineCounter - 565)(pixelCounter - 1) = sample.toInt

      def invTransmissionGammaC(y: Float) = {
        val sign = if (y < 128) -1.0 else 1.0
        val yMagnitude = math.abs(y - 128)
        val yNorm = math.min(112.0, yMagnitude) / 112.0
        val x = if (yNorm < 5.0 / 72.0)
          0.6 * yNorm
        else if (yNorm < 47.0 / 33.0 / 8.0) {
          val a = -48.0 / 11.0
          val b = 67.0 / 33.0
          val c = -1.0 / 132.0
          -b / (2 * a) - math.sqrt(b * b / (4 * a * a) - (c - yNorm) / a)
        } else
          33.0 / 31.0 * (yNorm - 2.0 / 33.0)
        128 + x * sign * 112.0
      }

      if (lineCounter >= 43 && lineCounter < 559 && pixelCounter >= 12 && pixelCounter < 106)
        fieldStores(currentFieldStoreIndex).colorBuffer(lineCounter - 43)(pixelCounter - 12) = invTransmissionGammaC(sample)
      if (lineCounter >= 605 && lineCounter < 1121 && pixelCounter >= 12 && pixelCounter < 106)
        fieldStores(currentFieldStoreIndex).colorBuffer(lineCounter - 605)(pixelCounter - 12) = invTransmissionGammaC(sample)

      def invTransmissionGammaYM(y: Float) = {
        val normY = (math.max(16, math.min(239, y)) - 16) / 224
        val x = 0.6 * normY * normY + 0.4 * normY
        val invGamma = x * 224 + 16
        invGamma
      }

      if (lineCounter >= 47 && lineCounter < 563 && pixelCounter >= 107)
        fieldStores(currentFieldStoreIndex).frameBuffer(lineCounter - 47)(pixelCounter - 107) = invTransmissionGammaYM(sample)
      if (lineCounter >= 609 && lineCounter < 1125 && pixelCounter >= 107)
        fieldStores(currentFieldStoreIndex).frameBuffer(lineCounter - 609)(pixelCounter - 107) = invTransmissionGammaYM(sample)

      // Notice the control signal is used for the next field
      if (lineCounter >= 559 && lineCounter < 564 && pixelCounter >= 13 && pixelCounter < 107)
        controlSignal(lineCounter - 559)(pixelCounter - 13) = sample.toInt
      if (lineCounter >= 1121 && lineCounter < 1125 && pixelCounter >= 13 && pixelCounter < 107)
        controlSignal(lineCounter - 1121)(pixelCounter - 13) = sample.toInt

//      if ((lineCounter == 563 || lineCounter == 1125) && pixelCounter >= 107)
//        print(s"${sample.toInt} ")

      if (lineCounter == 1 && pixelCounter == 1) {
        line1HighSum = 0
        line2LowSum = 0
        blankingSum = 0
      }
      if (lineCounter == 1 && pixelCounter >= 20 && pixelCounter < 260)
        line1HighSum += sampledValue.toInt
      if (lineCounter == 2 && pixelCounter >= 20 && pixelCounter < 260)
        line2LowSum += sampledValue.toInt
      if ((lineCounter == 563 || lineCounter == 1125) && pixelCounter >= 128 && pixelCounter <= 384)
        blankingSum += sampledValue.toInt
      if (lineCounter == 1125 && pixelCounter == 480) {
        val avgHigh = line1HighSum / 240.0
        val avgLow = line2LowSum / 240.0
        val avgBlanking = blankingSum / 512.0
        val (a, b) = linearRegression(Seq(16.0 -> avgLow, 128.0 -> avgBlanking, 239.0 -> avgHigh))
        eqA = eqA * 0.9 + a * 0.1
        eqB = eqB * 0.9 + b * 0.1
        println(f"eqA=$eqA%.3f, eqB=$eqB%.3f")
      }

      if (lineCounter == 563 && pixelCounter == 480)
        newField(false)
      if (lineCounter == 1125 && pixelCounter == 480)
        newField(true)

      totalPixelCounter += 1
      pixelCounter += 1
      if (pixelCounter > 480) {
        pixelCounter = 1
        lineCounter += 1
        if (lineCounter > 1125)
          lineCounter = 1
      }
    }
    firstFrameDataFile.close()
    audioProcessor.printTotalStatistics()
  }

  private def linearRegression(values: Seq[(Double, Double)]): (Double, Double) = {
    val n = values.size
    val sumX = values.map(_._1).sum
    val sumY = values.map(_._2).sum
    val sumXX = values.map((x, _) => x * x).sum
    val sumXY = values.map((x, y) => x * y).sum
    val a = (n * sumXY - sumX * sumY) / (n * sumXX - sumX * sumX)
    val b = (sumY - a * sumX) / n
    (a, b)
  }
}
