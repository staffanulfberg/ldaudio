package muse

import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

object VideoInterpolator {
  // input is 4 fields.  Index 0 is the oldest
  def createOutputFrame(clipName: String, fields: IndexedSeq[FieldStore]): Unit = {
    val image = BufferedImage(374 * 3, 516 * 2, java.awt.image.BufferedImage.TYPE_INT_BGR)

    val field24_0: Array[Array[Double]] = Array.ofDim(516, 374 * 3 / 2)
    val field24_1: Array[Array[Double]] = Array.ofDim(516, 374 * 3 / 2)
    val interpolatedFrame48: Array[Array[Double]] = Array.ofDim(516 * 2, 374 * 3)
    val interpolatedFilteredFrame48: Array[Array[Double]] = Array.ofDim(516 * 2, 374 * 3)
    val interpolatedLastField48: Array[Array[Double]] = Array.ofDim(516 * 2, 374 * 3)

    if (interpolateFrames(IndexedSeq(fields(0), fields(2)), field24_0) && interpolateFrames(IndexedSeq(fields(1), fields(3)), field24_1)) {
      val field0Parity = fields(0).fieldParity
      val field02phases = fields(0).controlSignal.fieldSubsamplingPhaseY.toSet ++ fields(2).controlSignal.fieldSubsamplingPhaseY.toSeq
      val field13phases = fields(1).controlSignal.fieldSubsamplingPhaseY.toSet ++ fields(3).controlSignal.fieldSubsamplingPhaseY.toSeq
      assert(field02phases.sizeIs == 1)
      assert(field13phases.sizeIs == 1)
      val field0phase = field02phases.head
      val field1phase = field13phases.head
      assert(field0phase != field1phase)

      for (row <- 0 until 516 * 2) {
        val (sourceLine, phase) = if (field0Parity == 1 - row % 2) (field24_0(row / 2), field0phase) else (field24_1(row / 2), field1phase)
        for (col <- 0 until 374 * 3 / 2) {
          interpolatedFrame48(row)(col * 2 + phase) = sourceLine(col)
        }
      }
      FilterUtil.filterImage(interpolatedFrame48, FilterUtil.factor1diamondFilter, 2.0, interpolatedFilteredFrame48)
    } else {
      interpolateField(fields.last, interpolatedFilteredFrame48) // fallback for when frame interpolation not possible -- this should be eliminated and not used
    }
    interpolateField(fields.last, interpolatedLastField48)

    def clamp(x: Int): Int = math.max(0, math.min(x, 255))

    val rawR48: Array[Array[Double]] = Array.ofDim(516 * 2, 374 * 3)
    val rawB48: Array[Array[Double]] = Array.ofDim(516 * 2, 374 * 3)
    val interpolatedR48: Array[Array[Double]] = Array.ofDim(516 * 2, 374 * 3)
    val interpolatedB48: Array[Array[Double]] = Array.ofDim(516 * 2, 374 * 3)

    if (fields.forall(_.fieldParity != -1)) {
      for (field <- fields if field.fieldParity != -1)
        fillColorValues(field, rawR48, rawB48)
      // the raw color frames now have 1/12 of their pixels filled in, so the filter needs a total
      // amplification factor of 12.  The filters in FilterUtil all sum to 1.

      FilterUtil.filterImage(rawR48, FilterUtil.diamondForColor, 12, interpolatedR48)
      FilterUtil.filterImage(rawB48, FilterUtil.diamondForColor, 12, interpolatedB48)
    } else {
      for { row <- 0 until 516 * 2; col <- 0 until 374 * 3 } {
        interpolatedR48(row)(col) = 128
        interpolatedB48(row)(col) = 128
      }
    }

    for (row <- 0 until 516 * 2) {
      for (col <- 0 until 374 * 3) {
        val YM = interpolatedFilteredFrame48(row)(col).toInt
        val RmYM = interpolatedR48(row)(col).toInt - 128
        val BmYM = interpolatedB48(row)(col).toInt - 128

        val g = clamp((YM - 0.25 * BmYM - 0.5 * RmYM).toInt)
        val b = clamp((YM + 1.25 * BmYM).toInt)
        val r = clamp(YM + RmYM)
        image.setRGB(col, row, b | (g << 8) | (r << 16))
      }
    }

    ImageIO.write(image, "png", new File(f"${clipName}_${fields.last.frameNumber}%05d-${fields.last.fieldParity}.png"))
  }

  // uses two fields from two different frames with the same field parity to produce a 24.3 MHz field
  // (inputs have 374 horizontal pixels, so the output has 374 * 1.5 pixels
  private def interpolateFrames(fields: IndexedSeq[FieldStore], output: Array[Array[Double]]): Boolean = {
    require(fields.sizeIs == 2)
    if (fields.map(_.fieldParity).distinct.sizeIs != 1) {
      System.err.println("Field parities not the same")
      return false
    }
    if (fields.exists(_.controlSignal.fieldSubsamplingPhaseY.isEmpty)) {
      System.err.println("Field subsampling phase not known")
      return false
    }
    if (fields.map(_.controlSignal.fieldSubsamplingPhaseY).distinct.sizeIs != 1) {
      System.err.println("Field subsampling phase not the same")
      return false
    }
    val fieldPhase = fields.head.controlSignal.fieldSubsamplingPhaseY.get

    if (fields.flatMap(_.controlSignal.frameSubsamplingPhaseYM).distinct.sizeIs != 2) {
      System.err.println(s"Frame subsampling phases not known or equal: ${fields.map(_.controlSignal).mkString(", ")}")
      return false
    }

    val tmp32 = new Array[Double](374 * 2)
    for (row <- 0 until 516) {
      for (field <- fields) {
        val phase = (field.controlSignal.frameSubsamplingPhaseYM.get + row + 1) % 2
        for (col <- 0 until 374)
          tmp32(col * 2 + phase) = field.frameBuffer(row)(col)
      }

      // tmp is 32.4 MHz. Logically, we should first up-sample to 48MHz, and then down-sample to 24MHz,
      // with the down-sampling phase dependent on the field phase.
      // We can do this in one go since 32->24 MHz conversion uses a 96 MHz signal internally that is
      // down-sampled to 24 MHz.  We just need to control the phase of this down-sampling.
      FilterUtil.convertSampleRate4to3(tmp32, output(row), fieldPhase * 2)
    }
    true
  }

  // interpolates a single field to 516*2 lines at 48.6 MHz (374 x 3 pixels wide)
  private def interpolateField(field: FieldStore, output: Array[Array[Double]]): Unit = {
    // convert sample rate from 16.2 MHz to 24.3 MHz: we read into every other position of tmp32 first
    val tmp32 = new Array[Double](374 * 2)
    val tmp24 = new Array[Double](374 * 3 / 2)

    val fieldParity = field.fieldParity
    val interFieldPhase = field.controlSignal.fieldSubsamplingPhaseY.getOrElse(0)
    val interFramePhase = field.controlSignal.frameSubsamplingPhaseYM.getOrElse(0)

    val buffer: Array[Array[Double]] = Array.ofDim(516 * 2, 374 * 3)

    for (row <- 0 until 516) {
      val phase = (interFramePhase + row + 1) % 2 // notice row is even when line no is odd (first row is line 47/609)
      for (col <- 0 until 374) {
        tmp32(col * 2 + phase) = 2 * field.frameBuffer(row)(col)
        tmp32(col * 2 + 1 - phase) = 0
      }
      FilterUtil.convertSampleRate4to3(tmp32, tmp24, interFieldPhase * 2)
      for (col <- 0 until 374 * 3 / 2) {
        buffer(row * 2 + 1 - fieldParity)(col * 2 + interFieldPhase) = tmp24(col).toInt
      }
    }

    FilterUtil.filterImage(buffer, FilterUtil.factor2diamondFilter, 1.0, output)
  }

  // Output is "full" resolution, i.e., 374*3 x 516*2 pixels -- we call this for each field
  // before applying the interpolation filter (this can clearly be optimized since not all the pixels are actually used;
  // most obviously only half of the horizontal lines and a third of the vertical lines).
  // Each color has 1/48 of its pixels filled in during one call to this method.
  private def fillColorValues(field: FieldStore, redOutput: Array[Array[Double]], blueOutput: Array[Array[Double]]): Unit = {
    val framePhase = field.controlSignal.frameSubsamplingPhaseC.getOrElse(0)
    val fp = field.fieldParity
    for (row <- 0 until 516) {
      val line = row + 43 + fp * 562 // line no as defined in MUSE
      val lineDiv2Odd = line / 2 % 2;
      val offset = (if (lineDiv2Odd == framePhase) 0 else 6) + fp * 3;
      val outLine = row * 2 + 1 - fp // actual frame output line

      val output = if (line % 2 == 1) redOutput else blueOutput
      for (col <- 0 until 94) {
        if (col * 12 + offset < 374 * 3) {
          output(outLine)(col * 12 + offset) = field.colorBuffer(row)(col)
        }
      }
    }
  }
}
