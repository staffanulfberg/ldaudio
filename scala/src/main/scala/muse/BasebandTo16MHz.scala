package muse

import java.io.*

object BasebandTo16MHz {
  def main(args: Array[String]): Unit = {
    var inputFilename: Option[String] = None
    var outputFilename: Option[String] = None
    var outputLimit: Option[Int] = None

    def processArgs: List[String] => Boolean = {
      case Nil => true
      case "-o" :: fn :: rest =>
        outputFilename = Some(fn)
        processArgs(rest)
      case fn :: rest if inputFilename.isEmpty =>
        inputFilename = Some(fn)
        processArgs(rest)
      case _ => false
    }

    assert(processArgs(args.toList), "Bad command line arguments.")
    if (inputFilename.isEmpty) {
      System.err.println("No input filename")
      System.exit(1)
    }
    if (outputFilename.isEmpty) {
      System.err.println("No output filename")
      System.exit(1)
    }

    for (skip <- Seq(0)) {
    //for (skip <- 17 until 480 * 54 / 16 * 1000 by 123) {
      val input = DataInputStream(BufferedInputStream(FileInputStream(inputFilename.get)))
      input.skipBytes(skip)
      println(s"Available: ${input.available}")
      val output = DataOutputStream(BufferedOutputStream(FileOutputStream(outputFilename.get)))
      val reader = ResamplingInputReader(input)
      val pll = Pll(reader, output)
      println(s"Processing using skip of $skip")
      pll.process(outputLimit)
      input.close()
      output.close()
    }
  }

  class ResamplingInputReader(input: DataInputStream) {
    // We keep a total of bufferSize values in the input queue for interpolation
    private val bufferSize = 4
    private var t: Double = 0 // the time of the previous read.

    private val buffer = collection.mutable.Queue.fill[Byte](bufferSize)(0)

    private var totalBytes = 0
    private var totalSamples = 0
    def stats: String = s"totalBytes: $totalBytes, totalSamples: $totalSamples"

    // Resamples the value dt time steps after the previous read
    // The value returned is an interpolated value and a Double for now to not have to think much about precision here
    def read(dt: Double): Double = {
      val t1 = t + dt
      var bytesRead = t.toInt
      while (bytesRead < math.floor(t1)) {
        val b = input.readByte()
        bytesRead += 1
        buffer.enqueue(b)
        buffer.dequeue()
        totalBytes += 1
      }
      totalSamples += 1
      t = t1
      val p = t - math.floor(t)

      val b0 = (buffer(0) & 0xff).toDouble
      val b1 = (buffer(1) & 0xff).toDouble
      val b2 = (buffer(2) & 0xff).toDouble
      val b3 = (buffer(3) & 0xff).toDouble

      // cubic spline though 4 points, f(x) = a0 + a1 x + a2 x(x-1) + a3 x(x-1)(x-2)
      val a0 = b0
      val a1 = b1 - a0
      val a2 = (b2 - a0 - 2 * a1) / 2
      val a3 = (b3 - a0 - 3 * a1 - 6 * a2) / 6
      // now evaluate at point 1 + p
      val x = 1 + p
      val y = a0 + a1 * x + a2 * x * (x - 1) + a3 * x * (x - 1) * (x - 2)
      // println(f"b0/b1/b2/b3: $b0/$b1/$b2/$b3, p=$p%.2f, out=$y%.1f")
      y
    }
  }

  class Pll(reader: ResamplingInputReader, output: DataOutputStream) {
    val inputSamplePerSampleRef: Double = 54 / 16.2
    var inputSamplesPerSample: Double = inputSamplePerSampleRef

    private val omega = 2 * math.Pi * 5000 / 54e6 // undamped frequency
    private val zeta = 0.75 // damping factor
    private val Ts = inputSamplePerSampleRef * 480 // We think of one line as one sample here since we detect the phase once per line
    private val G1 = 1 - math.exp(-2 * zeta * omega * Ts)
    private val G2 = 1 + math.exp(-2 * omega * zeta * Ts) - 2 * math.exp(-omega * zeta * Ts) * math.cos(omega * Ts * math.sqrt(1 - zeta * zeta));
    private val GpdGvco = 64 * (1 / inputSamplePerSampleRef) * 480
    private val g1 = G1 / GpdGvco
    private val g2 = G2 / GpdGvco
    println(f"g1=$g1%g, g2=$g2%g")
    private val errorSumMultiplier = g2 // 5.0 * 2e-6
    private val errorMultiplier = g1 // 32 * errorSumMultiplier

    def process(limit: Option[Int]): Unit = {
      try {
        var pixel: Int = 1
        var line: Int = 1
        var line1FramePulseSum: Int = 0
        var line2FramePulseSum: Int = 0
        var consecutiveGoodSyncs: Int = 0
        var avgSampleValue: Double = 128
        var missedLinePulses: Int = 0
        val shiftReg = collection.mutable.Queue.fill[Short](5)(0)
        enum State {
          case Searching, Locked, LockedHoriz
        }
        var state = State.Searching
        var prevState = state
        var error: Int = 0
        var errorSum: Int = 0

        var shouldWrite = false

        Iterator.continually(reader.read(inputSamplesPerSample)).foreach { input =>
          var newError: Option[Int] = None
          avgSampleValue = avgSampleValue * (1 - 1e-7) + 1e-7 * input

          if state == State.LockedHoriz then
            shouldWrite = true
          if shouldWrite then
            output.writeShort((input * 4).toShort) // 10 bit output

          val sample = input.toShort
          shiftReg.enqueue(sample)
          shiftReg.dequeue()

          if pixel == 316 then
            line1FramePulseSum = 0
            line2FramePulseSum = 0
          else if pixel >= 317 && pixel < 480 then // notice we ignore pixel 480
            val framePulsePixel = pixel - 317
            val line1FramePulseValue = if ((framePulsePixel < 140 && (framePulsePixel / 4) % 2 == 1) || (framePulsePixel >= 140 && framePulsePixel < 156)) 1 else -1
            val line2FramePulseValue = -line1FramePulseValue
            line1FramePulseSum += line1FramePulseValue * (sample - avgSampleValue.toInt)
            line2FramePulseSum += line2FramePulseValue * (sample - avgSampleValue.toInt)
          else if pixel == 480 then
            if line1FramePulseSum > 3000 then // TODO: maybe adjust or make threshold dynamic
              if state == State.LockedHoriz then
                line = 1
                state = State.Locked

            if state == State.Locked && (line == 1 || line == 2) then
              if (line == 1 && line1FramePulseSum > 3000) || (line == 2 && line2FramePulseSum > 3000) then
                missedLinePulses = 0
              else
                if missedLinePulses < 3 then
                  missedLinePulses += 1
                else
                  state = State.Searching

          if pixel == 8 then
            // When locked: line 1: positive, line 2: negative, line 3: negative, line 4: positive, then alternating up to 1125
            val syncIsPositive = line == 1 || (line > 3 && line % 2 == 0) // when state = Locked or state = Searching else sample > shiftReg(3);

            val syncIsGood = (syncIsPositive && shiftReg(0) < shiftReg(2) && shiftReg(2) < shiftReg(4)) ||
              (!syncIsPositive && shiftReg (0) > shiftReg(2) && shiftReg(2) > shiftReg(4))

            newError = Option.when(syncIsGood) {
              val avgLevel = (shiftReg(0) + shiftReg(4)) / 2
              if (syncIsPositive) shiftReg(2) - avgLevel else avgLevel - shiftReg(2) // negative means we sampled too early
            }

            if state == State.Searching then
              consecutiveGoodSyncs = if (syncIsGood) consecutiveGoodSyncs + 1 else if (consecutiveGoodSyncs >= 2) consecutiveGoodSyncs - 2 else 0
              if consecutiveGoodSyncs >= 50 then
                state = State.LockedHoriz

//            if state == State.Locked && !syncIsGood then
//              println(s"Bad sync at ${reader.stats}, line $line")

          if pixel < 480 then
            pixel += 1
          else if (state == State.Searching && ((consecutiveGoodSyncs < 5 && line == 50) || line == 100)) || (state == State.LockedHoriz && line == 1125) then
            consecutiveGoodSyncs = 0
            newError = None
            error = 0
            errorSum = 0
            state = State.Searching // TODO: add to VHDL
            line = 3
            pixel = 263
          else
            pixel = 1
            line = if (line == 1125) 1 else line + 1

          newError.foreach { e =>
            error = e
            errorSum += error
          }

          inputSamplesPerSample = inputSamplePerSampleRef - error * errorMultiplier - errorSum * errorSumMultiplier

          if state != prevState then
            println(s"New state $state at line $line, ${reader.stats}")
            prevState = state

          //print(f"$pixel%03d ")
        }
      } catch {
        case e: IOException =>
          println(s"IOException: ${e.getMessage} (assuming EOF) ")
          println(s"Total ${reader.stats}")
      }
    }
  }
}
