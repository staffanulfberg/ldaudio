package muse

import java.awt.image.BufferedImage
import java.io.{DataOutputStream, File, FileOutputStream, FileWriter, OutputStream, PrintWriter}
import javax.imageio.ImageIO
import scala.collection.mutable

class AudioProcessor(clipName: String) {
  private val deinterleaveStages = 25
  private val deinterleaveData = new Array[Boolean]((deinterleaveStages - 1) * 1350 + 1)
  private var totalDeinterleavedBits = 0

  private val symbolsOut: Option[PrintWriter] = Some(PrintWriter(FileOutputStream(s"symbols.csv")))
  // private val audioDataFiles = (1 to 4).map(i => PrintWriter(FileOutputStream(s"audio$i.csv")))

  private def addBit(b: Boolean): Unit = {
    for (i <- deinterleaveData.length - 1 until 0 by -1)
      deinterleaveData(i) = deinterleaveData(i - 1)
    deinterleaveData(0) = b
  }

  var q = 0

  private var queue: String = ""
  private val syncPattern = "0001001101011110" // frame marker

  private var consecutiveFailedSyncs = 100 // start by searching for sync pattern
  private var frameCounter = 0

  private val controlSignals = mutable.Map[String, Int]().withDefaultValue(0)
  private var activeControlSignal = ""

  private val bchDecoder = BchDecoder(82, 74, 137)
  private val rangeBchDecoder = BchDecoder(7, 3, 11)
  private val bchTotalStatistics = mutable.Map[String, Int]().withDefaultValue(0)
  private val rangeBchTotalStatistics = mutable.Map[String, Int]().withDefaultValue(0)
  private val aModeChannel1Decoder = AModeChannelDecoder(clipName, 1)
  private val aModeChannel2Decoder = AModeChannelDecoder(clipName, 2)
  private val aModeChannel3Decoder = AModeChannelDecoder(clipName, 3)
  private val aModeChannel4Decoder = AModeChannelDecoder(clipName, 4)
  private val bModeChannel1Decoder = BModeChannelDecoder(clipName, 1)
  private val bModeChannel2Decoder = BModeChannelDecoder(clipName, 2)

  def handle(audioLines: Array[Array[Int]], newFrame: Boolean): Unit = {
    if (newFrame) {
      assert(q == 0)
      //      println(s"BCH (82, 74) statistics")
      //      bchDecoder.printStatistics()
      //      println(s"BCH (7, 3) statistics")
      //      rangeBchDecoder.printStatistics()
      if (frameCounter > 1) { // don't sum up the initial errors before the deinterleaver is filled
        bchDecoder.statistics.foreach((name, v) => bchTotalStatistics(name) += v)
        rangeBchDecoder.statistics.foreach((name, v) => rangeBchTotalStatistics(name) += v)
      }
      bchDecoder.resetStatistics()
      rangeBchDecoder.resetStatistics()
      frameCounter += 1
    }
    val allLines = audioLines.flatten
    val skipStart = 2
    val skipEnd = 2 // to make inputUsed a multiple of 4
    val inputUsed = allLines.length - skipStart - skipEnd
    val resampled = new Array[Double](inputUsed * 3 / 4)

    FilterUtil.convertSampleRate4to3(allLines.map(_.toDouble).slice(skipStart, allLines.length - skipEnd), resampled, 0)

    val resampledPerLine = 480 * 3 / 4 // 360
    val allBits = (0 until 40).flatMap { i =>
      val offset = 9 + i * resampledPerLine
      decodeSymbols(resampled.slice(offset, offset + 348), symbolsOut)
    } ++ (40 until 44).flatMap { i =>
      val offset = 9 + i * resampledPerLine + 78
      decodeSymbols(resampled.slice(offset, offset + 270), symbolsOut)
    }
    // symbolsOut.foreach(_.close())

    // println(s"total erased: ${allBits.count(_.isEmpty)} out of ${allBits.length}")
    // val bitsString = allBits.map(b3 => if (b3.isEmpty) "XXX" else b3.map(if (_) "1" else "0").mkString).mkString
    // val syncStringMatches = bitsString.split(syncPattern).length - 1
    // println(s"Bits: ${bitsString.substring(0, 100)}... $syncStringMatches matches; first match at ${bitsString.indexOf(syncPattern)}")

    val deinterleavedBits = allBits.flatMap { b3 =>
      val bits = if (b3.isEmpty) Seq(false, false, false) else b3
      bits.flatMap { b =>
        addBit(b)
        val out = deinterleaveData(q * 1350)
        totalDeinterleavedBits += 1
        q = (q + 1) % deinterleaveStages
        Option.when(totalDeinterleavedBits > deinterleaveStages * 1350)(out)
      }
    }

    queue += deinterleavedBits.map(if (_) "1" else "0").mkString

    def takeBitsFromQueue(n: Int) = {
      val bits = queue.substring(0, n)
      queue = queue.substring(n)
      bits
    }

    var printedSearching = false
    while (queue.length >= 1350) {
      if (consecutiveFailedSyncs > 2) {
        if (!printedSearching) {
          println("searching for sync pattern")
          printedSearching = true
        }
        val ix = queue.indexOf(syncPattern)
        if (ix >= 0) {
          consecutiveFailedSyncs = 0
          queue = queue.substring(ix + syncPattern.length)
        } else {
          queue = queue.substring(1350 - 16)
        }
      } else {
        val presumedSyncBits = takeBitsFromQueue(16)
        if (presumedSyncBits != syncPattern) {
          consecutiveFailedSyncs += 1
          println("Sync pattern not recognized!")
        } else {
          consecutiveFailedSyncs = 0
        }
      }
      if (consecutiveFailedSyncs <= 2 && queue.length >= 1350 - 16) {
        val controlSignal = takeBitsFromQueue(22)
        controlSignals(controlSignal) += 1
        if (controlSignals.values.sum == 16) {
          val majority = controlSignals.maxBy(_._2)._1
          if (majority != activeControlSignal) {
            activeControlSignal = majority
            println(s"audio control signal now: $activeControlSignal")
          }
        }

        val data = takeBitsFromQueue(1350 - 16 - 22)
        val deinterleaveBlocks = data.grouped(16).toIndexedSeq.map(_.map(_.toString.toInt))
        val bchBlocks = deinterleaveBlocks.transpose

        val correctedBchBlocks: IndexedSeq[(IndexedSeq[Int], Boolean)] = bchBlocks.map { block =>
          assert(block.sizeIs == 82)
          bchDecoder.decode(block)
        }

        if (Set("0110010000000010100001", "0110011001000010101001").contains(activeControlSignal)) {
          // Mode A
          val range12 = correctedBchBlocks.map(_._1(0))
          val range1 = rangeBchDecoder.decode(range12.slice(0, 7))
          // discard bit 7 (Control flag for scrambling identification of sound signal)
          val range2 = rangeBchDecoder.decode(range12.slice(8, 15))
          // discard bit 15
          val audio1: Seq[(IndexedSeq[Int], Boolean)] = correctedBchBlocks.map((b, ok) => (b.slice(1, 9), ok)) ++ correctedBchBlocks.map((b, ok) => (b.slice(9, 17), ok))
          val audio2: Seq[(IndexedSeq[Int], Boolean)] = correctedBchBlocks.map((b, ok) => (b.slice(17, 25), ok)) ++ correctedBchBlocks.map((b, ok) => (b.slice(25, 33), ok))
          val range34 = correctedBchBlocks.map(_._1(33))
          val range3 = rangeBchDecoder.decode(range34.slice(0, 7))
          // discard bit 7
          val range4 = rangeBchDecoder.decode(range34.slice(8, 15))
          // discard bit 15
          val audio3: Seq[(IndexedSeq[Int], Boolean)] = correctedBchBlocks.map((b, ok) => (b.slice(34, 42), ok)) ++ correctedBchBlocks.map((b, ok) => (b.slice(42, 50), ok))
          val audio4: Seq[(IndexedSeq[Int], Boolean)] = correctedBchBlocks.map((b, ok) => (b.slice(50, 58), ok)) ++ correctedBchBlocks.map((b, ok) => (b.slice(58, 66), ok))
          // Bits 66 ... 73 contain auxiliary data, and bits 74 ... 81 contain the BCH correction code (already used at this stage)

          audio1.foreach(aModeChannel1Decoder.addSample(range1, _))
          audio2.foreach(aModeChannel2Decoder.addSample(range2, _))
          audio3.foreach(aModeChannel3Decoder.addSample(range3, _))
          audio4.foreach(aModeChannel4Decoder.addSample(range4, _))
        } else if (Set("1001100000000010100011", "1001100000000010100001").contains(activeControlSignal)) {
          // Mode B
          val range12 = correctedBchBlocks.map(_._1(0))
          val range1 = rangeBchDecoder.decode(range12.slice(0, 7))
          // discard bit 7 (Control flag for scrambling identification of sound signal)
          val range2 = rangeBchDecoder.decode(range12.slice(8, 15))
          // discard bit 15
          val audio1: Seq[(IndexedSeq[Int], Boolean)] = correctedBchBlocks.map((b, ok) => (b.slice(1, 12), ok))
            ++ correctedBchBlocks.map((b, ok) => (b.slice(12, 23), ok)) ++ correctedBchBlocks.map((b, ok) => (b.slice(23, 34), ok))
          val audio2: Seq[(IndexedSeq[Int], Boolean)] = correctedBchBlocks.map((b, ok) => (b.slice(34, 45), ok))
            ++ correctedBchBlocks.map((b, ok) => (b.slice(45, 56), ok)) ++ correctedBchBlocks.map((b, ok) => (b.slice(56, 67), ok))

          def writeAudioToFile(f: PrintWriter, rangeAndOkFlag: (IndexedSeq[Int], Boolean), data: Seq[(IndexedSeq[Int], Boolean)]): Unit = {
            f.println(s"${rangeAndOkFlag._2}, ${binaryLittleEndianSeqToIntUnsigned(rangeAndOkFlag._1)}, ${data.map { case (d, ok) => s"$ok, ${binaryBigEndianSeqToIntSigned(d)}" }.mkString(", ")}")
            f.flush()
          }
          //        writeAudioToFile(audioDataFiles(0), range1, audio1)
          //        writeAudioToFile(audioDataFiles(1), range2, audio2)

          audio1.foreach(bModeChannel1Decoder.addSample(range1, _))
          audio2.foreach(bModeChannel2Decoder.addSample(range2, _))
        } else if (activeControlSignal == "") {
          println("Audio control signal unknown")
        } else
          println("Cannot determine audio mode A/B")
      }
    }
  }

  def printTotalStatistics(): Unit = {
    println(s"BCH (82, 74) total statistics")
    bchTotalStatistics.to(collection.SortedMap).foreach { case (s, i) =>
      println(f"$s%-35s$i%5d")
    }
    println(s"BCH (7, 3) total statistics")
    rangeBchTotalStatistics.to(collection.SortedMap).foreach { case (s, i) =>
      println(f"$s%-35s$i%5d")
    }
    Seq(aModeChannel1Decoder, aModeChannel2Decoder, aModeChannel3Decoder, aModeChannel4Decoder, bModeChannel1Decoder, bModeChannel2Decoder).
      foreach(_.printStatistics())
  }

  // an empty seq represents an erasure
  private def decodeSymbols(symbolValues: Array[Double], output: Option[PrintWriter]): Seq[Seq[Boolean]] = {
    require(symbolValues.length % 2 == 0)
    output.foreach { f =>
      symbolValues.grouped(2).foreach { g =>
        f.println(s"${g(0)},${g(1)}")
      }
    }

    val bits = symbolValues.grouped(2).map { g =>
      adaptiveSamplesToSymbols(g(0), g(1)) match {
        case (0, 0) => Seq(false, false, false) // Seq(false, false, false) -- the commented out conversion table is incorrect
        case (0, 1) => Seq(false, false, true) // Seq(false, false, true) -- but is the one specified in the ITU-R BO.786 recommendation
        case (1, 2) => Seq(false, true, false) // Seq(false, true, false)
        case (0, 2) => Seq(false, true, true) // Seq(false, true, true)
        case (1, 0) => Seq(true, false, false) // Seq(true, false, false)
        case (2, 0) => Seq(true, false, true) // Seq(true, false, true)
        case (2, 2) => Seq(true, true, false) // Seq(true, true, false)
        case (2, 1) => Seq(true, true, true) // Seq(true, true, true)
        case (1, 1) => Seq() // throw Exception("Impossible with adaptive symbol decoder") // Seq()
      }
    }.toSeq
    bits
  }

  private def samplesToSymbols(s1: Double, s2: Double): (Int, Int) = {
    def sampleToSymbol(s: Double): Int = if (s < 82) 0 else if (s < 170) 1 else 2
    (sampleToSymbol(s1), sampleToSymbol(s2))
  }

  // scaled factor 16 to avoid too much rounding
  var symbolLocations: Map[(Int, Int), (Int, Int)] = (for {i <- 0 to 2; j <- 0 to 2} yield (i, j) -> (28 + i * 100, 28 + j * 100)).toMap.removed(1, 1).view.mapValues((a, b) => (a * 16, b * 16)).toMap

  private def adaptiveSamplesToSymbols(s1: Double, s2: Double): (Int, Int) = {
    val (xin, yin) = (s1.toInt * 16, s2.toInt * 16)
    val sortedDists = symbolLocations.map { case (i, j) -> (x, y) => (i, j) -> (math.abs(xin - x) + math.abs(yin - y)) }.toSeq.sortBy(_._2)
    val closest = sortedDists.head
    val nextClosest = sortedDists(1)
    if (math.abs(closest._2 - nextClosest._2) > 300) {
      val (closestX, closestY) = symbolLocations(closest._1)
      symbolLocations = symbolLocations.updated(closest._1, ((closestX * 15 + xin) / 16, (closestY * 15 + yin) / 16))
    }
    closest._1
  }
}

def binaryBigEndianSeqToIntSigned(s: Seq[Int]): Int = s.reverse.zipWithIndex.map((b, i) => b * (if (i == s.size - 1) -(1 << i) else 1 << i)).sum
def binaryLittleEndianSeqToIntUnsigned(s: Seq[Int]): Int = s.zipWithIndex.map((b, i) => b * (1 << i)).sum

abstract class ChannelDecoder(clipName: String, mode: String, channel: Int) {
  // play -t raw -c 1 -r 32000 -b 16 -e signed-integer --endian big channel1.raw
  protected val f = DataOutputStream(FileOutputStream(s"$clipName-channel$mode$channel.raw"))

  var skippedValues = 0
  var clippedValues = 0

  def printStatistics(): Unit = {
    if (skippedValues != 0 || clippedValues != 0)
      println(s"Channel $mode$channel: skippedValues=$skippedValues, clippedValues=$clippedValues")
  }
}

class AModeChannelDecoder(clipName: String, channel: Int) extends ChannelDecoder(clipName, "A", channel) {
  val maxValue = 16383
  val minValue = -16384
  var value: Int = 0 // scaled a factor 256

  def addSample(rangeAndOkFlag: (IndexedSeq[Int], Boolean), dataAndOkFlag: (IndexedSeq[Int], Boolean)): Unit = {
    val data = dataAndOkFlag._1
    if (rangeAndOkFlag._2) { // otherwise just repeat the previous sample; probably not good
      val range = rangeAndOkFlag._1
      require(range.size == 3)
      require(data.size == 8)
      val multiplier = 128 >> binaryLittleEndianSeqToIntUnsigned(range) // 1 to 128
      val dataValue = binaryBigEndianSeqToIntSigned(data)
      value = value - value / 16 + dataValue * multiplier
    }
    else
    {
      skippedValues += 1
    }
    if (value > maxValue) {
      clippedValues += 1
      value = maxValue
    }
    else if (value < minValue) {
      clippedValues += 1
      value = minValue
    }
    f.writeShort(value.toShort * 2)
  }
}

class BModeChannelDecoder(clipName: String, channel: Int) extends ChannelDecoder(clipName,"B", channel) {
  val maxValue = 32767
  val minValue = -32678
  var value: Int = 0 // scaled a factor 256

  def addSample(rangeAndOkFlag: (IndexedSeq[Int], Boolean), dataAndOkFlag: (IndexedSeq[Int], Boolean)): Unit = {
    val data = dataAndOkFlag._1
    if (rangeAndOkFlag._2) { // otherwise just repeat the previous sample; probably not good
      val range = rangeAndOkFlag._1
      require(range.size == 3)
      require(data.size == 11)
      val multiplier = 32 >> binaryLittleEndianSeqToIntUnsigned(range) // 1 to 128
      val dataValue = binaryBigEndianSeqToIntSigned(data)
      value = value - value / 16 + dataValue * multiplier
    }
    else
    {
      skippedValues += 1
    }
    if (value > maxValue) {
      clippedValues += 1
      value = maxValue
    }
    else if (value < minValue) {
      clippedValues += 1
      value = minValue
    }
    f.writeShort(value.toShort)
  }
}
