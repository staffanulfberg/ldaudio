package muse

class FieldStore() {
  var frameNumber: Int = -1
  var fieldParity: Int = -1 // 0 for the first field (Y lines 47-562), 1 for the second field (lines 609-1124). NB. The first field is rendered *below* the second!
  var controlSignal: ControlSignal = ControlSignal(None, None, None, None, None, None) // the transmission control signal contained in the previous field
  val frameBuffer: Array[Array[Double]] = Array.ofDim(516, 374)
  val colorBuffer: Array[Array[Double]] = Array.ofDim(516, 94)
}
