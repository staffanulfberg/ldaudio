package muse

import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

sealed trait MotionInformation
case object Normal extends MotionInformation
case object CompleteStillPicture extends MotionInformation
case object SlightlyInMotion extends MotionInformation
case object SceneChange extends MotionInformation
case class Motion(extent: Int) extends MotionInformation {
  require(extent >= 0 && extent < 4)
}

case class ControlSignal(fieldSubsamplingPhaseY: Option[Int], horizontalMotionVector: Option[Int], verticalMotionVector: Option[Int],
                         frameSubsamplingPhaseYM: Option[Int], frameSubsamplingPhaseC: Option[Int], motionInformation: Option[MotionInformation]) {
  override def toString: String = s"phases (fieldY frameY frameC) = " +
    s"(${fieldSubsamplingPhaseY.getOrElse("X")} ${frameSubsamplingPhaseYM.getOrElse("X")} ${frameSubsamplingPhaseC.getOrElse("X")}), " +
    s"hVector=${horizontalMotionVector.getOrElse("X")}, vVector=${verticalMotionVector.getOrElse("X")}, motion=$motionInformation"
}

object ControlSignalDecoder {
  private val H = Seq(
    Seq(1, 1, 1, 0, 1, 0, 0, 0),
    Seq(0, 1, 1, 1, 0, 1, 0, 0),
    Seq(1, 1, 0, 1, 0, 0, 1, 0),
    Seq(1, 0, 1, 1, 0, 0, 0, 1))
  private val columnIndex = H.transpose.zipWithIndex.toMap

//  var nextImgIx = 1
  // data is 5x80 samples; 2 for each bit
  def decode(input: IndexedSeq[IndexedSeq[Int]]): ControlSignal = {
//    val image = BufferedImage(94, 5, java.awt.image.BufferedImage.TYPE_INT_BGR)
//    for { i <- 0 until 5; j <- 0 until 94 } {
//      val x = input(i)(j)
//      image.setRGB(j, i, x | (x << 8) | (x << 16))
//    }
//    ImageIO.write(image, "png", new File(f"control-$nextImgIx.png"))
//    nextImgIx += 1

    val groups = (0 until 5).flatMap { row =>
      val inputRow = input(row).slice(7, 87) // discard guard area
      val bits = (0 until 80 by 2).map { col =>
        //print(s"${inputRow(col)} ${inputRow(col + 1)}  ")
        val d1 = inputRow(col)
        val d2 = inputRow(col + 1)
        if (d1 < 128 && d2 < 128) 0
        else if (d1 > 128 && d2 > 128) 1
        else {
          // println(s"d1=$d1, d2=$d2");
          if (d1 + d2 > 256) 1 else 0
        }
      }
      bits.grouped(8)
    }
    assert(groups.forall(_.sizeIs == 8))
    assert(groups.size == 25)

    val checked = groups.take(24).map { g =>
      val syndrome = H.map(v => g.zip(v).map((a, b) => a * b).reduce(_ ^ _))
      if (syndrome.forall(_ == 0)) { // no errors
        g.slice(0, 4)
      } else {
        val errorPos = columnIndex.get(syndrome)
        errorPos.fold { // not found, so uncorrectable
          IndexedSeq.empty
        } { pos =>
          val corrected = g.toArray
          corrected(pos) ^= 1
          corrected.toIndexedSeq.slice(0, 4)
        }
      }
    }.grouped(8).toIndexedSeq
    val data: Seq[(Seq[Int], Seq[Int])] = (0 until 8).map { i =>
      val results = (0 until 3).map(r => checked(r)(i)).groupBy(identity).view.mapValues(_.size).filter(_._1.size == 4).toMap
      if (results.isEmpty) {
        print("ERR   ")
        (Seq(0, 0, 0, 0), Seq(0, 0, 0, 0))
      } else if (results.size == 1) {
        print(s"${results.head._1.mkString("")}  ")
        (results.head._1, Seq(1, 1, 1, 1))
      } else {
        results.find(_._2 == 2).fold {
          print(s"DIFF  ")
          (Seq(0, 0, 0, 0), Seq(0, 0, 0, 0))
        } { majority =>
          print(s"${majority._1.mkString("")}? ")
          (majority._1, Seq(1, 1, 1, 1))
        }
      }
    }
    println()

    val d = 0 +: data.flatMap(_._1) // index from 1 below
    val valid = false +: data.flatMap(_._2).map(_ != 0)

    val fieldSubsamplingPhaseY = Option.when(valid(1))(d(1))
    val horizontalMotionVector: Option[Int] = Option.when((2 to 5).forall(valid(_)))(d(2) + 2 * d(3) + 4 * d(4) - 8 * d(5))
    val verticalMotionVector: Option[Int] = Option.when((6 to 8).forall(valid(_)))(d(6) + 2 * d(7) - 4 * d(8))
    val frameSubsamplingPhaseY: Option[Int] = Option.when(valid(9))(d(9))
    val frameSubsamplingPhaseC: Option[Int] = Option.when(valid(10))(d(10))
    val motionInformation = Option.when((16 to 18).forall(valid(_))) {
      d(16) + 2 * d(17) + 4 * d(18) match {
        case 0 => Normal
        case 1 => CompleteStillPicture
        case 2 => SlightlyInMotion
        case 3 => SceneChange
        case x => Motion(x - 4)
      }
    }

    val cs = ControlSignal(fieldSubsamplingPhaseY, horizontalMotionVector, verticalMotionVector, frameSubsamplingPhaseY, frameSubsamplingPhaseC, motionInformation)
    println(cs)
    cs
  }
}
