package muse

object FilterUtil {
  // from https://fiiir.com/
  // For MUSE audio we want a 12.15 MHz cutoff at the sample rate 97.2 MHz.
  // This is the same as 0.125 Hz cutoff at 1 Hz: the important thing is that the filter cutoff is at the sample rate / 8 -- 0.25 * the Nyquist frequency
  // we can thus use the same filter for the 16.2 -> 12.15 MHz sample rate conversion and 32 -> 24 MHz conversion.
  private val filter = IndexedSeq(
    // cutoff 0.125, transition 0.03, sampling freq 1, Rectangular: 31 coeffs (25 non-zero).  Looks +/- 15 samples in each direction
    -0.015752415092402161, -0.023868513282649006, -0.018175863568156325, 0.000000000000000010, 0.021480566035093858, 0.033415918595708603, 0.026254025154003564, -0.000000000000000010,
    -0.033755175198004597, -0.055693197659514346, -0.047257245277206421, 0.000000000000000010, 0.078762075462010708, 0.167079592978543023, 0.236286226386032083, 0.262448010933081788,
    0.236286226386032083, 0.167079592978543023, 0.078762075462010708, 0.000000000000000010, -0.047257245277206421, -0.055693197659514346, -0.033755175198004597, -0.000000000000000010,
    0.026254025154003564, 0.033415918595708603, 0.021480566035093858, 0.000000000000000010, -0.018175863568156325, -0.023868513282649006, -0.015752415092402161,
  )

  private val N: Int = filter.length / 2
  assert(filter.sizeIs == N * 2 + 1)
  private val indices: IndexedSeq[IndexedSeq[Int]] = (0 to 2).map(mod3 => (-N to N).filter(i => (i + 3000) % 3 == mod3))

  def convertSampleRate4to3(input: Array[Double], output: Array[Double], phase: Int): Unit = {
    require(input.length % 4 == 0)
    require(output.length % 3 == 0)
    require(input.length / 4 == output.length / 3)
    // Convert to 3/4 of the input sample rate by inserting two zeros between samples to up-sample 3x,
    // low-pass filter at 0.25 times the new Nyquist rate and then select every 4th sample

    // We do not represent the up-sampled signal explicitly, and also do not represent the entire filter output
    // since we only need every 4th sample

    for (i <- output.indices) {
      val hiFreqIx = i * 4 + phase // index of the output in the hi frequency lattice
      // the hi frequency lattice looks like input(0) 0 0 input(1) 0 0 ...
      // this means that if hiFreqIx = 0, the filter indices -6, -3, 0, 3, 6 etc correspond to non-zero inputs
      //                             = 1, the filter indices -7, -4, -1, 2, 6 etc correspond to non-zero inputs
      val filterBankSelection = hiFreqIx % 3 match { case 0 => 0; case 1 => 2; case 2 => 1 }

      output(i) = indices(filterBankSelection).map { j =>
        val ix = (hiFreqIx + j) / 3
        val v = if (ix >= 0 && ix < input.length) input(ix) else 128
        filter(j + N) * v
      }.sum * 3
    }
  }

  //  val factor1diamondFilter: IndexedSeq[IndexedSeq[Double]] = IndexedSeq(
  //    IndexedSeq(0.0, 0.125, 0.0),
  //    IndexedSeq(0.125, 0.5, 0.125),
  //    IndexedSeq(0.0, 0.125, 0.0)
  //  )

  val factor1diamondFilter: IndexedSeq[IndexedSeq[Double]] = IndexedSeq(
    IndexedSeq(0.00044666779723518996, -0.003726754385013813, 0.0026360990876899346, -0.0037267543850138148, 0.00044666779723518996),
    IndexedSeq(-0.003726754385013813, -0.0077735054621067105, 0.12195962059611092, -0.0077735054621067123, -0.003726754385013813),
    IndexedSeq(0.0026360990876899346, 0.12195962059611092, 0.56073850700439309, 0.12195962059611094, 0.0026360990876899346),
    IndexedSeq(-0.0037267543850138148, -0.0077735054621067123, 0.12195962059611094, -0.007773505462106714, -0.0037267543850138148),
    IndexedSeq(0.00044666779723518996, -0.003726754385013813, 0.0026360990876899346, -0.0037267543850138148, 0.00044666779723518996)
  )

  val factor2diamondFilter: IndexedSeq[IndexedSeq[Double]] = IndexedSeq(
      IndexedSeq(-0.0020013581782554481, -0.003713946697531697, 0.0032923516678682401, 0.011510495301374627, 0.0032923516678682405, -0.003713946697531697, -0.0020013581782554481),
      IndexedSeq(-0.003713946697531697, 0.0012685569894963034, 0.051655531553867977, 0.095119622332928672, 0.051655531553867991, 0.0012685569894963034, -0.003713946697531697),
      IndexedSeq(0.0032923516678682388, 0.051655531553867977, 0.24878484208890961, 0.39273131894544228, 0.24878484208890969, 0.051655531553867977, 0.0032923516678682388),
      IndexedSeq(0.011510495301374627, 0.095119622332928672, 0.39273131894544228, 0.60047459788677926, 0.39273131894544233, 0.095119622332928672, 0.011510495301374627),
      IndexedSeq(0.0032923516678682392, 0.051655531553867991, 0.24878484208890969, 0.39273131894544233, 0.24878484208890972, 0.051655531553867991, 0.0032923516678682392),
      IndexedSeq(-0.003713946697531697, 0.0012685569894963034, 0.051655531553867977, 0.095119622332928672, 0.051655531553867991, 0.0012685569894963034, -0.003713946697531697),
      IndexedSeq(-0.0020013581782554481, -0.003713946697531697, 0.0032923516678682401, 0.011510495301374627, 0.0032923516678682405, -0.003713946697531697, -0.0020013581782554481)
  )

  val diamondForColor: IndexedSeq[IndexedSeq[Double]] = IndexedSeq(
    IndexedSeq(0.000331, 0.000887, 0.002231, 0.003575, 0.004132, 0.003575, 0.002231, 0.000887, 0.000331),
    IndexedSeq(0.000887, 0.002382, 0.005989, 0.009597, 0.011091, 0.009597, 0.005989, 0.002382, 0.000887),
    IndexedSeq(0.002231, 0.005989, 0.015062, 0.024135, 0.027893, 0.024135, 0.015062, 0.005989, 0.002231),
    IndexedSeq(0.003575, 0.009597, 0.024135, 0.038672, 0.044694, 0.038672, 0.024135, 0.009597, 0.003575),
    IndexedSeq(0.004132, 0.011091, 0.027893, 0.044694, 0.051653, 0.044694, 0.027893, 0.011091, 0.004132),
    IndexedSeq(0.003575, 0.009597, 0.024135, 0.038672, 0.044694, 0.038672, 0.024135, 0.009597, 0.003575),
    IndexedSeq(0.002231, 0.005989, 0.015062, 0.024135, 0.027893, 0.024135, 0.015062, 0.005989, 0.002231),
    IndexedSeq(0.000887, 0.002382, 0.005989, 0.009597, 0.011091, 0.009597, 0.005989, 0.002382, 0.000887),
    IndexedSeq(0.000331, 0.000887, 0.002231, 0.003575, 0.004132, 0.003575, 0.002231, 0.000887, 0.000331))

  def filterImage(input: Array[Array[Double]], filter: IndexedSeq[IndexedSeq[Double]], multiplier: Double, output: Array[Array[Double]]): Unit = {
    val filterSizeY = filter.size
    val filterSizeX = filter.head.size
    assert(filter.forall(_.sizeIs == filterSizeX))
    for  {row <- input.indices; col <- input(row).indices} {
      output(row)(col) = (for { i <- -filterSizeY / 2 to filterSizeY / 2; j <- -filterSizeX / 2 to filterSizeX / 2 } yield {
        if (row + i >= 0 && row + i < input.length && col + j >= 0 && col + j < input(row).length)
          input(row + i)(col + j) * filter(i + filterSizeY / 2)(j + filterSizeX / 2)
        else 0
      }).sum * multiplier
    }
  }
}
