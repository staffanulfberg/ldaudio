// Copyright 2021 Staffan Ulfberg staffan@ulfberg.se

// Code to brute force search parameters for the MMCM block in the Xilinx Artix 7 FPGA
object MMCM {
  def main(args: Array[String]): Unit = {
    val targetFrequency: Double = 48000 * 60 * 16

    var bestFrequency = -1e100
    val bestSettings = collection.mutable.ArrayBuffer[String]()

    val eps1 = 2.0 // how much better to clear all results
    val eps2 = 5.0 // how much worse to include in results

    val inputFrequency = 1e8
    for (d <- 1 to 10) {
      for (m <- BigDecimal(2) to BigDecimal(64) by BigDecimal(0.25)) {
        val oscillatorFrequency = inputFrequency / d * m.toDouble
        if (oscillatorFrequency >= 600e6 && oscillatorFrequency <= 1200e6) {
          for (f0 <- BigDecimal(2) to BigDecimal(128) by BigDecimal(0.25)) {
            val outputFrequency = oscillatorFrequency / f0.toDouble
            for (extraDiv <- 1 +: (2 to 256 by 2)) {
              val finalFrequency = outputFrequency / extraDiv
              val error = math.abs(targetFrequency - finalFrequency)
              val bestError = math.abs(targetFrequency - bestFrequency)
              if (error < bestError - eps1) {
                bestFrequency = finalFrequency
                bestSettings.clear()
              }
              if (error < bestError + eps2) {
                bestSettings += f"d=$d., m=$m, f0=$f0, extraDiv=$extraDiv, error=$error"
              }
            }
          }
        }
      }
    }
    println(bestSettings.mkString("\n"))
  }
}
