// Copyright 2022-2023 Staffan Ulfberg staffan@ulfberg.se
import java.io.*
import scala.io.Source
import scala.util.{Failure, Using}

/**
 * Reads two possible input files:
 * - a csv file exported from Picoscope containing 10 bit samples at 16.2 MHz, including the sampling
 * clock.  The sample rate of the input file can be anything faster than 32.4 MHz, so that the clock can be detected.
 *
 * - a binary file created using Picoscope streaming mode (big endian), using one 16 bit short per sample.
 * bits 0-9 contain the adc data and bit 10 the clock.  The sample rate should be 41.666666 MHz (this is the
 * lowest possible on the Picoscope that is higher than 32.4 MHz).
 *
 * The output is a binary file with one 16 bit big endian (compatible with DataInputStream) short per clock in the
 * input file.
 */
object MuseSampleConverter {
  val linesForInitialStatistics = 5000

  def main(args: Array[String]): Unit = {
    var inputFilename: Option[String] = None
    var inputIsBinary = false
    var outputFilename: Option[String] = None
    var outputLimit: Option[Int] = None

    def processArgs: List[String] => Boolean = {
      case Nil => true
      case "-o" :: fn :: rest =>
        outputFilename = Some(fn)
        processArgs(rest)
      case "-l" :: l :: rest =>
        outputLimit = Some(l.toInt)
        processArgs(rest)
      case "-b" :: rest =>
        inputIsBinary = true
        processArgs(rest)
      case fn :: rest if inputFilename.isEmpty =>
        inputFilename = Some(fn)
        processArgs(rest)
      case _ => false
    }

    assert(processArgs(args.toList), "Bad command line arguments.")
    if (inputFilename.isEmpty) {
      System.err.println("No input filename")
      System.exit(1)
    }

    val output = DataOutputStream(BufferedOutputStream(FileOutputStream(outputFilename.get)))

    if (inputIsBinary)
      convertBinary(inputFilename.get, output)
    else
      convertCsv(inputFilename.get, output, outputLimit)

    output.close()
  }

  def convertCsv(inputFilename: String, output: DataOutputStream, outputLimit: Option[Int]): Unit = {
    def parseLine(s: String): IndexedSeq[Double] = {
      s.split(',').toIndexedSeq.map(_.toDouble)
    }

    Using(Source.fromFile(inputFilename)) { source =>
      val lines = source.getLines()
      val channelNames = {
        val line = lines.next()
        val parts = line.split(',').toSeq
        assert(parts.sizeIs >= 12, "Too header few columns")
        if (parts.head != "Time") throw Exception("First line does not have Time as first column")
        parts.tail.map(_.stripPrefix("Channel "))
      }
      // Assume channels are D0-D9. D10 is used for the clock.
      assert(channelNames.zipWithIndex.take(11).forall { case (name, i) => name == s"D$i" })

      val timeMultiplier = {
        val line = lines.next()
        val s = Seq.fill(channelNames.size)(""",\(\w+\)""").mkString("")
        if (line.matches("""\(s\)""" + s)) 1.0
        else if (line.matches("""\(ms\)""" + s)) 1e-3
        else throw Exception("Units (second) line incorrect")
      }
      assert(lines.next() == "", "Third line incorrect")

      val firstValues = lines.take(linesForInitialStatistics).map(parseLine).toIndexedSeq

      val inputSampleRate = {
        val times = firstValues.map(_(0) * timeMultiplier)
        val tDelta = (times.last - times.head) / (linesForInitialStatistics - 1)
        1 / tDelta
      }
      System.err.println(f"Input sample rate: $inputSampleRate")

      var lineNumber = 4
      var timeStep = 0
      var outputValueCounter = 0
      val inputStartTime = firstValues.head(0) * timeMultiplier
      var prevT = inputStartTime - 1 / inputSampleRate
      var tInput = 0.0
      var prevClk = 0
      (firstValues.iterator ++ lines.map(parseLine)).foreach { seq =>
        val t = seq(0) * timeMultiplier
        if (t - prevT > 1 / inputSampleRate * 1.5) {
          val nMissing = ((t - prevT) / (1 / inputSampleRate) - 0.5).toInt
          System.err.println(s"Missing $nMissing samples")
        }
        prevT = t

        val clk = seq(11).toInt
        val v = (0 to 9).map(i => seq(i + 1).toInt * math.pow(2, i)).sum
        if (clk == 0 && prevClk == 1) {
          println(v)
          output.writeShort(v.toShort)
          outputValueCounter += 1
        }

        prevClk = clk
        lineNumber += 1
        timeStep += 1
        tInput += 1 / inputSampleRate

        if (outputLimit.exists(_ < timeStep))
          System.exit(0)
      }
    } match {
      case Failure(exception) => System.err.println(exception.toString)
      case _ =>
    }
  }

  def convertBinary(inputFilename: String, output: DataOutputStream): Unit = {
    Using(DataInputStream(BufferedInputStream(FileInputStream(inputFilename)))) { stream =>
      val inputSampleRate = 1 / 24e-9
      System.err.println(f"Input sample rate: $inputSampleRate")

      var prevClk = 0
      Iterator.continually(stream.readShort()).foreach { input =>
        val clk = input & 0x400
        if (clk == 0 && prevClk != 0) {
          output.writeShort(input & 0x3ff)
        }
        prevClk = clk
      }
    } match {
      case Failure(exception) =>
        exception match {
          case _: IOException =>
            System.err.println(s"IOException: ${exception.getMessage}")
          case _ =>
            System.err.println(exception.toString)
        }
      case _ =>
        System.err.println("Should not get here!")
    }
  }
}
