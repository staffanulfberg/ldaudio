// Copyright 2021 Staffan Ulfberg staffan@ulfberg.se
package ac3

import rs.{ByteWithValue, GF256WithStatus, ReedSolomon}

import scala.collection.mutable.ArrayBuffer

// Decodes the symbols from the QPSK demodulator into AC3 blocks
object SymbolDecoder {
  import gfnumeric.GF256_187.given
  import gfnumeric.GFNumeric
  val rsC1 = ReedSolomon(37, 33, 120, true)
  val rsC2 = ReedSolomon(36, 32, 120, true)

  private def runReedSolomonC1(byteData: IndexedSeq[Byte]): IndexedSeq[GF256WithStatus] = {
    require(byteData.sizeIs == 37)
    val data = byteData.map(ByteWithValue(_).toGF256)
    val result = rsC1.decode(data)
    assert(result.sizeIs == 33)
    result
  }

  private def runReedSolomonC2(data: IndexedSeq[GF256WithStatus]): IndexedSeq[Byte] = {
    require(data.sizeIs == 36)
    val result = rsC2.decode(data)
    assert(result.sizeIs == 32)
    result.map(_.toByte.v)
  }

  // The symbols input are characters '0' to '3'!
  def decode(symbols: Seq[Byte]): Seq[Seq[Byte]] = {
    val binSymbols = symbols.map(_.toChar).map {
      case '0' => 0.toByte
      case '1' => 1.toByte
      case '2' => 2.toByte
      case '3' => 3.toByte
    }
    // Symbol error simulation
//      .map { b =>
//        if (scala.util.Random.nextDouble() < 0.002)
//          ((b + scala.util.Random.nextInt(4)) % 4).toByte
//        else b
//      }

    val frames = arrangeInFrames(binSymbols)

    val blocksBuffer = ArrayBuffer[IndexedSeq[Byte]]()
    val currentBlock = ArrayBuffer[Byte]()
    var expectedSeq = 0
    var consecutiveInSequence = 0
    frames.dropWhile(_._1 != 0).foreach { frame => // find first frame with sequence no 0, and process everything from there
      val usedFrameNo: Int = if (frame._1 != expectedSeq) {
        if (consecutiveInSequence < 3) {
          currentBlock.clear()
          consecutiveInSequence = 0
          -1
        } else {
          consecutiveInSequence = 0
          expectedSeq
        }
      } else {
        consecutiveInSequence += 1
        frame._1
      }

      currentBlock.appendAll(frame._2)

      if (usedFrameNo == 71 && currentBlock.sizeIs == 72 * 37) { // block completed
        blocksBuffer += currentBlock.toIndexedSeq
        currentBlock.clear()
        expectedSeq = 0
      } else
        expectedSeq = usedFrameNo + 1
    }
    val blocks = blocksBuffer.toSeq

    val correctedDataBuffer = ArrayBuffer[Byte]()
    blocks.foreach { block =>
      // `block` is a matrix of size 36 (rows) x 74 (columns) in row major order
      // `c1CorrectedBlock` is a matrix of size 36 x 66
      val c1CorrectedBlock = ArrayBuffer[GF256WithStatus]()
      def readC1Codeword(k: Int): IndexedSeq[Byte] = (0 until 37).map(i => block((k / 2) * 74 + i * 2 + k % 2))
      (0 until 36).foreach { k =>
        val c1CorrectedRow = runReedSolomonC1(readC1Codeword(2 * k)).lazyZip(runReedSolomonC1(readC1Codeword(2 * k + 1))).flatMap(_.toList)
        c1CorrectedBlock.appendAll(c1CorrectedRow)
      }

      def readC2Codeword(k: Int): IndexedSeq[GF256WithStatus] = (0 until 36).map { i => c1CorrectedBlock(k + i * 66) }
      (0 until 66).foreach { k =>
        val correctedCodeword = runReedSolomonC2(readC2Codeword(k))
        val bytes = {
          if (k == 0) {
            if (correctedCodeword(0) != 0x10 || correctedCodeword(1) != 0x00) {
              println(s"Block does not start with 0x10")
              IndexedSeq.empty // no bytes output
            } else
              correctedCodeword.drop(2)
          } else
            correctedCodeword
        }
        correctedDataBuffer.appendAll(bytes)
      }
    }
    val correctedData = correctedDataBuffer.toIndexedSeq
    println("C1 statistics")
    rsC1.printStatistics()
    println("C2 statistics")
    rsC2.printStatistics()

    val ac3FrameBuffer = ArrayBuffer[Seq[Byte]]()
    val currentAc3FrameBuffer = ArrayBuffer[Byte]()
    var currentAc3FrameSize = 0

    // returns the number of bytes in the new block
    def checkStartOfAc3Block(offset: Int): Boolean =
      offset < correctedData.size - 5 && correctedData(offset) == 0x0B && correctedData(offset + 1) == 0x77 && correctedData(offset + 4) == 0x1c

    var i = 0
    var inSync = false
    while (i < correctedData.size) {
      if (currentAc3FrameSize == 0) {
        while (i < correctedData.size && correctedData(i) == 0x00)
          i += 1
        if (checkStartOfAc3Block(i)) { // what we expect
          currentAc3FrameSize = 1536
          inSync = true
        } else {
          if (inSync)
            println(s"Non-zero byte that does not seem to be the start of AC3 block -- searching for next block of zeroes")
          inSync = false
          while (i < correctedData.size && correctedData(i) != 0x00)
            i += 1
        }
      }
      if (currentAc3FrameSize != 0) {
        currentAc3FrameBuffer += correctedData(i)
        if (currentAc3FrameBuffer.size == currentAc3FrameSize) {
          ac3FrameBuffer += currentAc3FrameBuffer.toSeq
          currentAc3FrameBuffer.clear()
          currentAc3FrameSize = 0
        }
      }
      i += 1
    }
    ac3FrameBuffer.toSeq
  }

  // Finds the sync pattens and breaks the sequence up into frames (the
  // output tuples are (sync number 0-71, sequence of 37 bytes)
  def arrangeInFrames(symbols: Seq[Byte]): Seq[(Byte, IndexedSeq[Byte])] = {
    val output = ArrayBuffer[(Byte, IndexedSeq[Byte])]()

    var syncFrameSymbolsSeen = 0
    val syncFrameNo = new Array[Byte](4)
    var symbolInFrameCounter = 0
    val symbolsInFrame = new Array[Byte](37 * 4)
    var consecutiveSynched = 0
    var autoSyncAt = -1
    symbols.zipWithIndex.foreach { (s, index) =>
      if (syncFrameSymbolsSeen < 12 && autoSyncAt < index) {
        val isNextSyncSymbol = syncFrameSymbolsSeen match {
          case 0 => s == 0
          case 1 => s == 1
          case 2 => s == 1
          case 3 => s == 3
          case 4 | 5 | 6 | 7 => syncFrameNo(syncFrameSymbolsSeen - 4) = s; true
          case 8 | 9 | 10 | 11 => s == 0
          case _ => throw Exception("cannot happen")
        }
        if (isNextSyncSymbol)
          syncFrameSymbolsSeen += 1
        else {
          if (consecutiveSynched > 0) {
            println(s"Missing sync index $index (consecutiveSynched=$consecutiveSynched)!")
            consecutiveSynched -= 1
            for (j <- 0 until 4)
              syncFrameNo(j) = (((((output.last._1.toInt + 1) % 72) & 0xff) >> (6 - 2 * j)) & 3).toByte
            autoSyncAt = index + 12 - syncFrameSymbolsSeen
          } else {
            syncFrameSymbolsSeen = 0
          }
        }
      } else if (index >= autoSyncAt) {
        if (syncFrameSymbolsSeen == 12 && symbolInFrameCounter == 0)
          consecutiveSynched = math.min(consecutiveSynched + 1, 7)
        else if (index == autoSyncAt)
          syncFrameSymbolsSeen = 12

        symbolsInFrame(symbolInFrameCounter) = s
        symbolInFrameCounter += 1
        if (symbolInFrameCounter == 37 * 4) {
          val bytes = symbolsInFrame.toSeq.grouped(4).map { case Seq(a, b, c, d) => (a << 6 | b << 4 | c << 2 | d).toByte }.toIndexedSeq
          output += ((
            syncFrameNo match { case Array(a, b, c, d) => (a << 6 | b << 4 | c << 2 | d).toByte },
            bytes
          ))
          symbolInFrameCounter = 0
          syncFrameSymbolsSeen = 0
        }
      }
    }

    output.toSeq
  }
}