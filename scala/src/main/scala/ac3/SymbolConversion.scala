// Copyright 2021 Staffan Ulfberg staffan@ulfberg.se
package ac3

object SymbolConversion {
  // Utility when doing exhaustive search to try different symbol interpretations
  def symbolsToBytes(symbols: String, offset: Int, swap12: Boolean, littleEndian: Boolean): IndexedSeq[Byte] = {
    val symbolValues = symbols map {
      case '0' => 0
      case '1' => if (swap12) 2 else 1
      case '2' => if (swap12) 1 else 2
      case '3' => 3
    }

    symbolValues.drop(offset).grouped(4).filter(_.sizeIs == 4).map { case Seq(a, b, c, d) =>
      if (littleEndian) (a | b << 2 | c << 4 | d << 6).toByte else (a << 6 | b << 4 | c << 2 | d).toByte
    }.toIndexedSeq
  }

  def symbolsToBits(symbols: String, bitOffset: Int, swap12: Boolean): IndexedSeq[Boolean] = {
    symbols.flatMap { (s: Char) => s match
      case '0' => Seq(false, false)
      case '1' => if (swap12) Seq(true, false) else Set(false, true)
      case '2' => if (swap12) Seq(false, true) else Set(true, false)
      case '3' => Seq(true, true)
    }.drop(bitOffset)
  }
}
