// Copyright 2021-2022 Staffan Ulfberg staffan@ulfberg.se
package ac3

import scala.collection.mutable.ArrayBuffer

// the toggleSequence indicates when the sequence toggles state,
// so different from the more commonly seen hi/low sequences
enum AC3Preamble(val toggleSequence: IndexedSeq[Boolean]) {
  case B extends AC3Preamble("10011100".map(_ == '1'))
  case M extends AC3Preamble("10010011".map(_ == '1'))
  case W extends AC3Preamble("10010110".map(_ == '1'))
}

// One AC3 subframe contains 4 bytes
case class AC3Subframe(preamble: AC3Preamble, bytes: IndexedSeq[Byte]) {
  override def toString: String = f"${preamble.toString} ${bytes.map(b => f"$b%02x").mkString("")}"
}

/*
 * Decodes the AC3 bitstream from a sampled SPDIF signal.
 */
object SpdifDecoder {
  // reads binary samples and outputs raw spdif frames
  def readSubframes(sampleFrequency: Double, bits: IndexedSeq[Boolean]): IndexedSeq[AC3Subframe] = {
    val samplesPerBit = sampleFrequency / (48000 * 64)

    // Since the clock is present all the time we do not need to have a phase locked loop -- just count the number of samples between bit transitions
    var index = 0 // how many bits we read
    var lastEdgeIndex = 0
    var lastBit = false

    // start by extracting all the biphase "bits", encoded by when the signal is toggled in a hlf bit interval
    val toggleSequence = bits.iterator.flatMap { sampledBit =>
      index += 1
      val toggles = if (sampledBit != lastBit) {
        val timeSinceLastTransition = index - lastEdgeIndex
        val toggles =
          if (timeSinceLastTransition >= samplesPerBit * 0.35 && timeSinceLastTransition <= samplesPerBit * 0.65)
            Seq(true)
          else if (timeSinceLastTransition >= samplesPerBit * 0.85 && timeSinceLastTransition <= samplesPerBit * 1.15)
            Seq(false, true)
          else if (timeSinceLastTransition >= samplesPerBit * 1.35 && timeSinceLastTransition <= samplesPerBit * 1.75)
            Seq(false, false, true)
          else {
            if (index > samplesPerBit) {
              // maybe mark somehow?
              println(s"Error at index $index, non-toggling samples=$timeSinceLastTransition")
            }
            Seq()
          }
        lastEdgeIndex = index
        toggles
      } else
        Seq()
      lastBit = sampledBit
      toggles.map(t => (t, index))
    }

    val output = ArrayBuffer[AC3Subframe]()

    // We put the biphase samples in a small circular buffer and start building a subframe when we see a synchronization preamble
    val synchBuffer = new Array[Boolean](8)
    var synchBufferIx = 0
    var lastPreamble: Option[AC3Preamble] = None

    val subframeSize = 32 * 2 - 8 // in "half bits"
    val currentSubframe = new Array[Boolean](subframeSize / 2) // starts after the preamble
    var subframeIndex = 0
    var isSynched = false
    toggleSequence.foreach { (b, sampleIndex) =>
      //println(s"$sampleIndex $b")
      synchBuffer(synchBufferIx) = b
      synchBufferIx = (synchBufferIx + 1) % 8

      val synchBufferSequence = (0 until 8).map(i => synchBuffer((synchBufferIx + i) % 8))
      val preamble = AC3Preamble.values.find(_.toggleSequence == synchBufferSequence)

      preamble.fold {
        if (isSynched) {
          if (subframeIndex < subframeSize) {
            if (subframeIndex % 2 == 0) {
              if (!b) {
                println(s"unexpected non-toggle at bit boundary (at subframeIndex $subframeIndex), sample index $sampleIndex")
                isSynched = false
              }
            } else {
              currentSubframe(subframeIndex / 2) = b
            }
          } else if (subframeIndex < subframeSize + 7) { // expecting next preamble
            val preambleHalfBits = subframeIndex - subframeSize + 1
            val synchBufferPartialSequence = (0 until preambleHalfBits).map(i => synchBuffer((synchBufferIx + 8 - preambleHalfBits + i) % 8))
            val preamble = AC3Preamble.values.find(_.toggleSequence.take(preambleHalfBits) == synchBufferPartialSequence)
            if (preamble.isEmpty)
              println(s"End of subframe but it seems no new preamble is present, sample index $sampleIndex")
          } else {
            println(s"unexpected non-preamble sample, index $sampleIndex")
            isSynched = false
          }
          subframeIndex += 1
        }
      } { p =>
        //println(s"Preamble $p at index $sampleIndex")
        if (subframeIndex == subframeSize + 7) {
          val subframeBytes = (Seq(false, false, false, false) ++ currentSubframe).grouped(8).map(_.zipWithIndex.map((b, i) => (if (b) 1 else 0) << i).sum).toSeq.reverse.map(b => (b & 0xff).toByte)
          val subframe = AC3Subframe(lastPreamble.get, subframeBytes.toIndexedSeq)
          output += subframe
        } else if (isSynched) {
          println(s"unexpected synch preamble before end of subframe (subframeIndex=$subframeIndex), sample index $sampleIndex")
        }
        subframeIndex = 0
        isSynched = true
        lastPreamble = Some(p)
      }
    }

    output.toIndexedSeq
  }
}
