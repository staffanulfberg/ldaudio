// Copyright 2021 Staffan Ulfberg staffan@ulfberg.se
package ac3

import java.io.{FileOutputStream, PrintWriter}
import scala.collection.mutable.ArrayBuffer

object QpskDecoder {
  def decode(inputSampleFrequency: Double, values: Seq[Short]): IndexedSeq[Byte] = {
    val k = 16 // samples per carrier cycle
    // resample at 2.88 * k MHz
    val buffer = ArrayBuffer[Byte]()
    var t = 0.0
    var inputTime = 0.0
    val avg = values.take(10000).map(_.toInt).sum / 10000
    values.foreach { v =>
      while (t < inputTime) {
        buffer += (if (v > avg) 1.toByte else -1.toByte)
        t += 1 / (2.88e6 * k)
      }
      inputTime += 1 / inputSampleFrequency
    }
    val samples = buffer.toArray.toIndexedSeq

    val cyclesPerSymbol = 10
    val samplesBetweenSymbols = k * cyclesPerSymbol // symbols at 288 kHz for a total bit rate of 576 kbps if samplesPerSymbol == 10
    val start = samplesBetweenSymbols * 2
    val end = samples.size

    val majorities = decodeSymbolsByBestPhaseDifference(samples, k, samplesBetweenSymbols, start, end)

    val reclocked = reclock(majorities.toIndexedSeq, None /* Some(new PrintWriter(new FileOutputStream("reclock.txt"))) */)
//    val bytes = reclocked.grouped(4).filter(_.sizeIs == 4).map { case IndexedSeq(a, b, c, d) =>
//      (a << 6 | b << 4 | c << 2 | d).toByte
//    }

    val symbols = reclocked.map {
            case 0 => '0'.toByte
            case 1 => '1'.toByte
            case 2 => '2'.toByte
            case 3 => '3'.toByte
          }
    symbols
  }

  private def decodeSymbolsByBestPhaseDifference(samples: IndexedSeq[Byte], k: Int, samplesBetweenSymbols: Int, start: Int, end: Int) = {
    val compareIntervalSize = 32
    val majorities = ArrayBuffer[Byte]()
    var i = start
    while (i < end) {
      val s = new Array[Int](4)
      for (ph <- 0 until 4) {
        val phase = ph * k / 4
        var sum = 0
        for (j <- 0 until compareIntervalSize) {
          sum += samples(i - j) * samples(i - j - samplesBetweenSymbols - phase)
        }
        s(ph) = sum
      }
      val a = s(0) - s(2)
      val b = s(1) - s(3)
      val winner = if (math.abs(a) > math.abs(b)) (if (a > 0) 0 else 3) else (if (b > 0) 1 else 2)
      majorities += winner.toByte
      i += 1
    }
    majorities
  }

  def reclock(majorities: IndexedSeq[Byte], p: Option[PrintWriter]): IndexedSeq[Int] = {
    val output = collection.mutable.ArrayBuffer[Int]()

//    val omega = 2 * math.Pi * 1800 // undamped frequency
//    val zeta = 1.0 // damping factor
//    val g1 = 1 - math.exp(-2 * zeta * omega / 288000)
//    val g2 = 1 + math.exp(-2 * omega * zeta / 288000) - 2 * math.exp(-omega * zeta / 288000) * math.cos(omega / 288000 * math.sqrt(1 - zeta * zeta));
    val g1 = 1 / 16.0
    val g2 = 1 / 512.0 // these constants yield approximately a natural frequency of 2060 Hz and damping factor 0.72

    var totalBitsIn = 0
    val counterBits: Int = 16
    var clkCounter: Int = 0

    // we get a new sample with frequency 16 * 2.88 MHz = 46.08 MHz
    val nominalFrequency = 576000 / 2
    val nominalAdd: Int = ((1 << counterBits) * nominalFrequency.toLong / 46080000).toInt

    var lastIn = 0.toByte
    var errorSum: Int = 0
    val errorSumBits = 21
    var filterOut: Int = 0 // need to keep this under nominalAdd to ensure counter is strictly increasing

    var togglePositions: List[Int] = Nil
    val toggleCountHistogram = collection.mutable.HashMap[Int, Int]().withDefaultValue(0)

    majorities.foreach { dataIn =>
      totalBitsIn += 1

      if (dataIn != lastIn) {
        togglePositions = clkCounter :: togglePositions
        lastIn = dataIn
      }

      val filterNow = if (filterOut < -nominalAdd) {
        // println(s"filterOut=$filterOut at bit $totalBitsIn")
        filterOut += nominalAdd
        -nominalAdd
      } else {
        val tmp = filterOut
        filterOut = 0
        tmp
      }

      val newCounter = (clkCounter + nominalAdd + filterNow) & ((1 << counterBits) - 1)
      var o = -1
      if (newCounter < clkCounter) {
        output += lastIn
        o = lastIn

        toggleCountHistogram(togglePositions.size) += 1
        val error = if (togglePositions.nonEmpty) {
          val togglePos = (togglePositions.last + togglePositions.head) / 2
          -(togglePos - (1 << (counterBits - 1)))
        } else 0
        errorSum = (errorSum + error) << (32 - errorSumBits) >> (32 - errorSumBits) // truncate and sign extend
        //filterOut = error / 64 + errorSum / (1 << 11)
        filterOut = (error * g1 + errorSum * g2).toInt
        togglePositions = Nil
      }
      clkCounter = newCounter

      p.foreach(_.println(s"$dataIn $o"))
    }

    toggleCountHistogram.keySet.toSeq.sorted.foreach { k => println(s"$k toggles, ${toggleCountHistogram(k)} times}")}

    output.toIndexedSeq
  }
}
