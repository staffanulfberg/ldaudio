lazy val root = (project in file("."))
  .settings(
    name := "ScalaFpgaSupport",
    version := "0.1.0",
    scalaVersion := "3.3.0",
    scalacOptions ++= Seq(
      "-deprecation",
      "-feature",
    ),
    libraryDependencies ++= Seq(
      "org.scalacheck" %% "scalacheck" % "1.17.0" % "test",
      "org.scala-lang.modules" %% "scala-parallel-collections" % "1.0.4",
      "org.scalanlp" %% "breeze" % "2.1.0",
    )
  )
