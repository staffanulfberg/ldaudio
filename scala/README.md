### Scala command line tools ###

All tools can be run from sbt, but I tend to run them using the debugger in IntelliJ mostly.

The main tools are called PicoscopeConverter and PicoscopeAnalyzer. The names are, of course, since I
used a Picoscope unit to capture data used for the experiments.

### PicoscopeConverter ###

Unfortunately, the Picoscope software doesn't offer a documented compact way to export data.  I sampled at 62.5 MHz using one or
two channels, and then exported the sampled data (1 second worth) as csv.  The output file is very large (several gigabytes) and
takes too long to process, so to make it easier to handle the PicoscopeConverter tool converts the csv file to
a binary file using one unsigned byte per sample, at a given resample rate.  The files themselves are just raw byte files and have no
headers or other information in them.  Using only one byte doesn't lose any precision since

the Picoscope unit only has an 8 bit A/D converter anyway. The tool tries to find the mapping between the 8 bit data
and the voltage values in the csv file by looking at the set of actual decimal values used in the file.
The input sample rate is determined from the input file (the first column in the csv file is time).

Command line options:

    * -or [sample rate]: Output sample rate.  Can be lower or higher than the input sample rate, but lower is probably the most useful.
    * -of [format]: Output format.  Consists of two parts. The first part is "txt" or "bin" to produce a text or binary file (txt is good for debugging),
    followed by a colon and then any combination of "t", "a", and "b" for text output, or just "a" or "b" for binary output.
    * -o [filename]: Output file name.
    * -l [count]: Limit the output to some given number of samples.
    * [filename]: input filename

Example:

      PicoscopeConverter -o mars-62.5MHz.b.bin -of bin:b -or 62.5e6 mars-62.5MHz.csv
This reads mars-62.5MHz.csv and outputs in binary channel B at 62.5MHz in the file mars-62.5MHz.b.bin.

Of course, this is a bit of a simplification but shows argument usage. If you really want a command line tool called "PicoscopeConverter",
it has to be wrapped in a shell script since the code is compiled into java bytecode.  Using sbt this is the same command as above:

      sbt 'runMain PicoscopeCoverter -o mars-62.5MHz.b.bin -of bin:b -or 62.5e6 mars-62.5MHz.csv'

### PicoscopeAnalyzer ###

PicoscopeAnalyzer reads binary files from PicoscopeConverter (or any other tool -- it is a very simple format with one byte per sample),
and performs different kinds of analysis, including demodulating and decoding the input file.

Command line options:

    * -a [action]: action is one of Passthrough, Bias, Eye, EyeEma, SampleEfm, DecodeEfm, DecodeCirc, FFT, FilterAC3, DemodulateAC3Qpsk, DecodeAC3, DecodeSpdif
        * Passthrough: just write the input to the output file; useful for debugging, especially lds input.
        * Bias: checks if there is a bias of high and low (useful for EFM signals to check that the crossover point is correct)
        * Eye: makes a histogram of the number of samples between succesive high/low transitions in an EFM signal
        * EyeEma: Same as Eye, but uses exponential moving average to set the threshold between high/low.
        * SampleEfm: Takes an analog EFM signal and produces a file with only two levels.
        * DecodeEfm: not implemented
        * DecodeCirc: Decodes the EFM signal into actual sound data that can be played back, with for example sox.
        * FFT: Produces a text file suitable for plotting a diagram showing frequency content in the input.
        * FilterAC3: Filters the input signal with a bandpass filter at 2.88 MHz.
        * DemodulateAC3Qpsk: Demodulates AC3RF samples into symbols. The output file consists of the characters "0", "1", "2", and "3" for easy inspection. (requires the input file to be pre-filtered using FilterAC3)
        * DecodeAC3: Decodes symbols (from DemodulateAC3Qpsk) to an AC3 file.
        * DecodeSpdif: Decodes a file containing SPDIF samples into SPDIF subframs in a text file.
    * -l [count]: Read only the given number of bytes from the input.
    * -lds: Instead of using the unsigned byte file format, read an lds file (from DomesdayDuplicator). The sample rate still has to be given.
    * -o [filename]: output filename
    * -sr [sample rate]: specifies the input sample rate
    * [filename]: input filename

Examples:

      PicoscopyAnalyzer -a FilterAC3 -sr 62.5e6 -o mars-62.5MHz-filtered.bin mars-62.5MHz.a.bin
This reads mars-62.5MHz.a.bin sampled at 62.5 MHz and bandpass fileters the signal (2.88 MHz) to produce the file mars-62.5MHz-filtered.bin
in the same format.

      PicoscopyAnalyzer -a DemodulateAC3Qpsk -sr 62.5e6 -o mars-symbols.txt mars-62.5MHz-filtered.bin
This reads mars-62.5MHz-filtered.bin sampled at 62.5 MHz (the output from the previous example), demodulates the QPSK data and writes mars-symbols.txt.

      PicoscopyAnalyzer -a DecodeAC3 -o mars.ac3 mars-symbols.txt
Reads symbols from the previous command, deinterleaves the data, and runs it through Reed-Solomon error correction before
finally assembling AC3 blocks writing them to mars.ac3.  The AC3 data can be played back with, e.g., with ffmpeg:

       ffplay -codec:a:0 ac3 mars.ac3

