# README #

### Decoding of EFM, AC3RF, and MUSE ###

This repository started out when I was learning about and reverse engineering the digital audio signals
present on laserdiscs.  The main motivation was initially to build FPGA implementations of an EFM decoder,
and of an AC3RF demodulator.  This work is more or less completed, but there is some additional work needed
to make the decoders work 100%: expect a glitch or two per side of a disc being played.

I've also investigated how MUSE (https://en.wikipedia.org/wiki/Multiple_sub-Nyquist_sampling_encoding)
works in detail.  I realized that an FPGA implementation would be too much work so the current version
uses a combination of CPU/GPU on a regular computer.  I put that code in the same repo, so the name
ldaudio doesn't accurately describe everything here.

If you want to decode laserdisc data in general, the ld-decode project (https://github.com/happycube/ld-decode)
is probably a better place to start.

The top level directories contain the following:

> scala 

Code that I used to investigate how the formats work, and to test algorithms before implementing them
in VHDL.  Is also generates some lookup tables for the EFM decoding. and for GF[256] arithmetics,
used in VHDL.

There are a few command line tools do decode EFM and demodulate AC3RF, and a separate tool to decode MUSE.  
Notice that the code was optimized for being quick to write and to work; optimizations have been done only when
slowness was interfering with development productivity.

The MUSE code is not entirely up to date, since
I switched to C++ in order to make the decoding in real-time.  Most of it is back-ported, however,
since for me, it is a bit faster to do initial quick programming in Scala than in C++.

> src / wcfg / project / project.tcp / makeproj.bat

VHDL code that implements the EFM decoding and the AC3RF demodulation.  I used the Digilent Arty A7-35T
development board and made a small PMOD to filter and sample the input signal.  The project directory contains
the Vivado project, which can be rebuilt using the tcl file.  It happens that the tcl file is a bit out of
date if I haven't rebuilt the project recently from scratch, but if so it should be easy to update.

> ac3ice

Since the Xilinx XC7A35 has a lot more gates and power than is actually needed, I also ported the AC3RF
demodulator to the Lattice iCE40HX8K, using the Olimex iCE40HX8K-EVB development board.  This is very cheap
at only around 30 EUR.  I got the demodulator to work after quite a bit of optimization that wasn't necessary
for the Xilinx.  Most of the code is shared, and this directory only contains the Lattice specific
files, and the iCE Cube project links to sources in /src as required.  This hasn't been tested in a while
so I cannot guarantee that it is currently working.

I have not tried to make the EFM decoder work on the Lattice FPGA.  In any case, both decoders won't
fit simultaneously.

> ac3-efm-pmod

This is a KiCad project that contains the schematic and a PCB design for the PMOD used to capture EFM
and AC3RF.  It also has analog and SPDIF output audio interfaces.

> musecpp

This is a software MUSE decoder written in C++ and GLSL for the GPU specific parts.  The decoder is more or
less complete, but is still evolving.

> octave / python

Random utilities to analyze log files and to create filters.

### How demodulation of the AC3RF signal works ###

To store AC3 on laserdiscs, the data is first encoded using a Reed-Solomon error correcting code, and then
modulated using QPSK with a 2.88 MHz carrier.  The modulation is documented in a patent
(https://patents.google.com/patent/US5748834A/en), but I know of no prior source of information about how the
modulated data is framed, interleaved, and Reed-Solomon encoded.

Here is a description of how the decoder implemented in this project works; see the code for any details left out or mail me questions if you want
me to elaborate on something specific.

First, the signal has to be bandpass filtered. I created a simple FIR filter with a passband at 2.88 +/- 0.15 MHz.
In the hardware implementation, the PMOD contains an analog filter, so the FPGA only handles the signal
after this step.

2.88 MHz is exactly 10 times the symbol rate in the QPSK modulation (so we have 288 k symbols per second,
and every symbol represents two bits).  The decoder samples the signal (a binary sample) at 16x2.88 MHz, which means that comparing
samples 160 samples apart in the middle of symbols are the same if there is no phase shift.  If there is a 180 degree phase shift,
samples 168 samples apart are the same.  Comparing samples 164 and 172 samples apart show if the signal is phase shifted 90 or 270 degress,
respectively.  Representing the binary sample as -1 or +1 means that multiplying the time shifted samples yields 1 if the signals are in phase,
and -1 if they are in opposite phase.  So, we add the signals 160 and 168 samples apart together, and do the same for the 164/172 time delays, 
and we do this for a few cycles of the signal.  Out of the two added values, we assume we see a 0 or 180 degree phase shift if the first
has the largest absolute value; otherwise it is a 90 or 270 degree phase shift.  The sign of the value with the highest absolute value
tells which of the two values we see.  The output from this part of the decoder is 0, 1, 2, or 3, which is treated as
a two bit number.

Notice that the absolute phase in QPSK is 45, 135, 225, or 315 degrees from the carrier wave. However, the encoder adds the the next symbol to the previous one
mod 4, which means that the relative phase shift between two consecutive symbols tells us which symbol was transmitted.  That shift is 
always 0, 90, 180, or 270 degrees, and the original carrier does not need to be recovered.

This signal switches between the different output symbols over time, and needs to be read when it is stable, in the middle of each symbol.
When the signal switches between values, there are frequently glitches where the signal switches between values a few times before it stays
in a  new stable state.  The demodulator implements a DPLL that is locked to the symbol rate, to reliably find the center of each symbol in time.
The phase detector looks at when the signal switches state; the current implementation ignores any time it switches more than once, so
only glitch-free symbol changes are used. It seems there are enough of them to make the DPLL stable.

The symbols are then combined four by four into bytes, most significant symbol first. In order to synchronize the byte boundaries, however,
the symbols first needs to be examined to find the synchronization pattern:

        0 1 1 3 <n n n n> 0 0 0 0
The nnnn part is a number between 0 and 71 (represented by symbols 0000 to 1013) that increases every time it is seen.

The synchronization pattern appears every 160 symbols, or 40 bytes, which means there are 37 bytes of data between synchronization
patterns, that is grouped into 72 consecutive frames.

There are two levels of Reed Solomon coding used for error correction.  The inner code (which is called C1 in the scala code)
works on pairs of frames.  Taking the frames two by two results in double frames of 74 bytes.  Taking every other byte in this double frame
yields two 37 byte Reed-Solomon codewords.  They are corrected separately but the interleaved byte order remains.
See below for the Reed-Solomon parameters.

For the outer code, the data has to be de-interleaved.  This is done by writing the data into a 36x74 size matrix (or, discarding the parity
information from C1, a 36x66 matrix).  Two consecutive frames are put
in the same row, so the sequence of 72 frames fills the 36 rows of the matrix.  The columns of this matrix are Reed-Solomon codewords, 
with the 32 first rows containing data, and the last four rows containing parity symbols.  So, there are 66 columns that contain data,
and the data is read column by column from left to right.

I resolved to exhaustive search to find the Reed-Solomon parameters.  It turns out that C1 and C2 are, respectively, (37,33) and (36,32)
codes over GF[2^8]. The irreducible polynomial is x^8 + x^7 + x^2 + x + 1 (0x187), with primitive element 2.  The first consecutive root of the generator polynomial
is 2^120.

So, each block of data consist of 66*32=2112 bytes.  The two first bytes of each block is always 1000 (hex) and is
not part of the AC3 data.  There are 25 blocks of data per second (288000 symbols / 160 / 72), that contain 31.25 syncframes of
AC3 data, 1536 bytes each.  So, four input blocks makes five output syncframes, and every fourth input block does start with an AC3
syncframe (I'm not sure that is guaranteed, however).  Since 4x2110 is more than 5x1536 there is also some padding between blocks; this padding
consist of zeroes only.

The first two bytes of an AC3 syncframe is always 0B77 (hex), followed by 2 bytes CRC and then another byte that is always 1C (hex) for 
laserdiscs (it indicates that the samplerate is 48 kHz and that syncframes are 1536 bytes in size -- there is a table in the AC3 specification 
for the encoding of this byte). So, once we found the start of an AC3 block, we can just read 1536 bytes, and then skip zeroes until we find 
the next block, which should start with 0B77.

Thanks to Ian and Leighton Smallshire for convincing me to look for the inner Reed-Solomon code!  When I first figured out how this all
works I didn't realize that a second layer of encoding was being used, and assumed that the parity bytes represented some auxiliary data.
