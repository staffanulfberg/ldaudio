library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.reedSolomon_types.all;

entity top is
  port ( 
    CLK100MHZ: in std_logic;
    LED1: out std_logic;
    LED2: out std_logic;
    BUT1: in std_logic;
    digitizedIn: in std_logic;
    tx_spdif: out std_logic;
    debug: out std_logic_vector(3 downto 0)
  );
end top;

architecture rtl of top is 
  signal rsEnabled: std_logic;
  signal ac3C1Done: std_logic;
  signal ac3C1Status: CompletionStatus;
  signal ac3C2Done: std_logic;
  signal ac3C2Status: CompletionStatus;
  signal ac3DebugSignal: std_logic_vector(3 downto 0);
  signal qpskClk: std_logic;  -- 2.88 * 16 MHz
begin
  rsEnabled <= BUT1;
  debug <= ac3debugSignal;

  updateAc3LEDs: process (qpskClk)
    variable c1timer: natural range 0 to 4095 := 0;
    variable c2timer: natural range 0 to 4095 := 0;
  begin
    if rising_edge(qpskClk) then
      if c1timer /= 0 then
        c1timer := c1timer - 1;
      else
        LED1 <= '0';
      end if;
      if ac3C1Done = '1' then
        case ac3C1Status is
          when NoErrors =>
          when CorrectedOneError =>
          when CorrectedTwoErrors =>
            LED1 <= '1';
          when CorrectedErasures | Running => -- won't happen
          when Failed =>
            LED1 <= '1';
        end case;
        c1timer := 4095;
      end if;
      
      if c2timer /= 0 then
        c2timer := c2timer - 1;
      else
        LED2 <= '0';
      end if;
      if ac3C2Done = '1' then
        case ac3C2Status is
          when NoErrors =>
          when CorrectedOneError =>
            LED2 <= '1';           
          when CorrectedTwoErrors =>
            LED2 <= '1';
          when CorrectedErasures =>
            LED2 <= '1';
          when Failed =>
            LED2 <= '1';
          when Running => -- won't happen
        end case;
        c2timer := 4095; 
      end if;
    end if;
  end process;

  -- qpskClk generated at 2.88 * 16 MHz (not exact -- 46.09 vs wanted 46.08 MHz)
  ac3ice_pll_inst: entity ac3ice_pll port map(
    REFERENCECLK => CLK100MHZ,
    PLLOUTCORE => open,
    PLLOUTGLOBAL => qpskClk,
    RESET => '1'
  );

  ac3main: entity work.ac3main port map (
    qpskClk => qpskClk,
    ac3In => digitizedIn,
    tx_spdif => tx_spdif,
    frameNoError => open,
    c1Enable => '1',
    c1Done => ac3C1Done,
    c1Status => ac3C1Status,
    c2Enable => '1',
    c2Done => ac3C2Done,
    c2Status => ac3C2Status,
    debugPins => ac3DebugSignal,
    dpllErrorSumDebug => open
  );  
end rtl;
